package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class WSNursingBloodTransfusionConfiguration extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public String wardId;
    
    public Integer timeToStart=0;
    
    public String controlStartTime;
    
    public Integer timeToEnd=0;
    
    public String controlEndTime;
    
    public String preparationDoctorOrRn;
    
    public String promptNotFullMatched;

    public WSNursingBloodTransfusionConfiguration ()
    {
    }

    public WSNursingBloodTransfusionConfiguration (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("wardId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("wardId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.wardId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.wardId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("timeToStart"))
        {	
	        java.lang.Object obj = soapObject.getProperty("timeToStart");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.timeToStart = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.timeToStart = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("controlStartTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("controlStartTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.controlStartTime = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.controlStartTime = (String)obj;
            }    
        }
        if (soapObject.hasProperty("timeToEnd"))
        {	
	        java.lang.Object obj = soapObject.getProperty("timeToEnd");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.timeToEnd = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.timeToEnd = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("controlEndTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("controlEndTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.controlEndTime = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.controlEndTime = (String)obj;
            }    
        }
        if (soapObject.hasProperty("preparationDoctorOrRn"))
        {	
	        java.lang.Object obj = soapObject.getProperty("preparationDoctorOrRn");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.preparationDoctorOrRn = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.preparationDoctorOrRn = (String)obj;
            }    
        }
        if (soapObject.hasProperty("promptNotFullMatched"))
        {	
	        java.lang.Object obj = soapObject.getProperty("promptNotFullMatched");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.promptNotFullMatched = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.promptNotFullMatched = (String)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return this.wardId!=null?this.wardId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==1)
        {
            return timeToStart;
        }
        if(propertyIndex==2)
        {
            return this.controlStartTime!=null?this.controlStartTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return timeToEnd;
        }
        if(propertyIndex==4)
        {
            return this.controlEndTime!=null?this.controlEndTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return this.preparationDoctorOrRn!=null?this.preparationDoctorOrRn:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==6)
        {
            return this.promptNotFullMatched!=null?this.promptNotFullMatched:SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 7;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "wardId";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "timeToStart";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "controlStartTime";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "timeToEnd";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "controlEndTime";
            info.namespace= "";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "preparationDoctorOrRn";
            info.namespace= "";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "promptNotFullMatched";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
