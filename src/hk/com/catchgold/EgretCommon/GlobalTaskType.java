package hk.com.catchgold.EgretCommon;

public enum GlobalTaskType
{
    ALL(-1), MEDICATION(0), OBS(1), APPOINTMENT(3), LAB(4), BEDSIDECARE(5), OPERATION(6), FEED(7);
    int type;
    GlobalTaskType(int p) {
       type = p;
    }
    public int showType() {
       return type;
    } 
	
    
	public String showName() { 
		switch (type)
		{
			case -1: 
				return "ALL"; 
			case 0: 
				return "MEDICATION"; 
			case 1: 
				return "OBS"; 
			case 3: 
				return "APPOINTMENT"; 
			case 4: 
				return "LAB"; 
			case 5: 
				return "BEDSIDECARE"; 
			case 6: 
				return "OPERATION"; 
			case 7: 
				return "FEED"; 
				
		}
		return null; 
	}
	
}