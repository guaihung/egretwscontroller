package hk.com.catchgold.EgretCommon.Enums;

public enum DFT
{
    /// <summary>
    /// dd/MM/yyyy HH:mm:ss
    /// </summary>
    Full,
    /// <summary>
    /// dd/MM/yyyy HH:mm
    /// </summary>
    NonSec,
    /// <summary>
    /// dd/MM/yyyy HH:mm:00
    /// </summary>
    NonSec2,
    /// <summary>
    /// dd/MM HH:mm
    /// </summary>
    NonYearSec,
    /// <summary>
    /// dd/MM HH:mm:ss
    /// </summary>
    NonYear,
    /// <summary>
    /// dd/MM/yyyy
    /// </summary>
    LongDate,
    /// <summary>
    /// dd/MM/yy
    /// </summary>
    ShortDate,
    /// <summary>
    /// dd/MM/yy
    /// </summary>
    ShortDate2,
    /// <summary>
    /// HH:mm dd/MM
    /// </summary>
    NonYearSecLabel,
    /// <summary>
    /// dd/MM/yy HH:mm
    /// </summary>
    NonSecShortYear,
    /// <summary>
    /// dd/MM
    /// </summary>
    NonYearDate
}
