package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class WSNursingOutstandingTask extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public Long id=0L;
    
    public String wardId;
    
    public String teamId;
    
    public String userGroup;
    
    public String patientId;
    
    public Integer patientSeries=0;
    
    public String patientAdmissionId;
    
    public Integer taskType=0;
    
    public Integer taskSubType=0;
    
    public org.joda.time.DateTime planExeTime;
    
    public org.joda.time.DateTime planEarlyExeTime;
    
    public org.joda.time.DateTime planLateExeTime;
    
    public Boolean canBeExe=false;
    
    public Enums.outstandingTaskStatus exeStatus;
    
    public String taskDetail;
    
    public String sourceDetail;
    
    public String exeDetail;
    
    public String executorId;
    
    public String executorName;
    
    public org.joda.time.DateTime exeTime;
    
    public String checkerId;
    
    public String checkerName;
    
    public org.joda.time.DateTime checkTime;
    
    public String patientName;
    
    public String bedNo;
    
    public WSNursingTask task;

    public WSNursingOutstandingTask ()
    {
    }

    public WSNursingOutstandingTask (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("id"))
        {	
	        java.lang.Object obj = soapObject.getProperty("id");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.id = new Long(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Long){
                this.id = (Long)obj;
            }    
        }
        if (soapObject.hasProperty("wardId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("wardId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.wardId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.wardId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("teamId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("teamId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.teamId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.teamId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("userGroup"))
        {	
	        java.lang.Object obj = soapObject.getProperty("userGroup");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.userGroup = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.userGroup = (String)obj;
            }    
        }
        if (soapObject.hasProperty("patientId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("patientId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.patientId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.patientId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("patientSeries"))
        {	
	        java.lang.Object obj = soapObject.getProperty("patientSeries");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.patientSeries = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.patientSeries = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("patientAdmissionId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("patientAdmissionId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.patientAdmissionId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.patientAdmissionId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("taskType"))
        {	
	        java.lang.Object obj = soapObject.getProperty("taskType");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.taskType = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.taskType = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("taskSubType"))
        {	
	        java.lang.Object obj = soapObject.getProperty("taskSubType");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.taskSubType = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.taskSubType = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("planExeTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("planExeTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.planExeTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.planExeTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("planEarlyExeTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("planEarlyExeTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.planEarlyExeTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.planEarlyExeTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("planLateExeTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("planLateExeTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.planLateExeTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.planLateExeTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("canBeExe"))
        {	
	        java.lang.Object obj = soapObject.getProperty("canBeExe");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.canBeExe = new Boolean(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Boolean){
                this.canBeExe = (Boolean)obj;
            }    
        }
        if (soapObject.hasProperty("exeStatus"))
        {	
	        java.lang.Object obj = soapObject.getProperty("exeStatus");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.exeStatus = Enums.outstandingTaskStatus.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.outstandingTaskStatus){
                this.exeStatus = (Enums.outstandingTaskStatus)obj;
            }    
        }
        if (soapObject.hasProperty("taskDetail"))
        {	
	        java.lang.Object obj = soapObject.getProperty("taskDetail");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.taskDetail = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.taskDetail = (String)obj;
            }    
        }
        if (soapObject.hasProperty("sourceDetail"))
        {	
	        java.lang.Object obj = soapObject.getProperty("sourceDetail");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.sourceDetail = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.sourceDetail = (String)obj;
            }    
        }
        if (soapObject.hasProperty("exeDetail"))
        {	
	        java.lang.Object obj = soapObject.getProperty("exeDetail");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.exeDetail = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.exeDetail = (String)obj;
            }    
        }
        if (soapObject.hasProperty("executorId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("executorId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.executorId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.executorId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("executorName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("executorName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.executorName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.executorName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("exeTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("exeTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.exeTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.exeTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("checkerId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("checkerId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.checkerId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.checkerId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("checkerName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("checkerName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.checkerName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.checkerName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("checkTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("checkTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.checkTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.checkTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("patientName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("patientName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.patientName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.patientName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("bedNo"))
        {	
	        java.lang.Object obj = soapObject.getProperty("bedNo");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.bedNo = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.bedNo = (String)obj;
            }    
        }
        if (soapObject.hasProperty("task"))
        {	
	        java.lang.Object j = soapObject.getProperty("task");
	        this.task = (WSNursingTask)__envelope.get(j,WSNursingTask.class);
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return id;
        }
        if(propertyIndex==1)
        {
            return this.wardId!=null?this.wardId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.teamId!=null?this.teamId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return this.userGroup!=null?this.userGroup:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==4)
        {
            return this.patientId!=null?this.patientId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return patientSeries;
        }
        if(propertyIndex==6)
        {
            return this.patientAdmissionId!=null?this.patientAdmissionId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==7)
        {
            return taskType;
        }
        if(propertyIndex==8)
        {
            return taskSubType;
        }
        if(propertyIndex==9)
        {
            return this.planExeTime!=null?this.planExeTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==10)
        {
            return this.planEarlyExeTime!=null?this.planEarlyExeTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==11)
        {
            return this.planLateExeTime!=null?this.planLateExeTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==12)
        {
            return canBeExe;
        }
        if(propertyIndex==13)
        {
            return this.exeStatus!=null?this.exeStatus.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==14)
        {
            return this.taskDetail!=null?this.taskDetail:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==15)
        {
            return this.sourceDetail!=null?this.sourceDetail:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==16)
        {
            return this.exeDetail!=null?this.exeDetail:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==17)
        {
            return this.executorId!=null?this.executorId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==18)
        {
            return this.executorName!=null?this.executorName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==19)
        {
            return this.exeTime!=null?this.exeTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==20)
        {
            return this.checkerId!=null?this.checkerId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==21)
        {
            return this.checkerName!=null?this.checkerName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==22)
        {
            return this.checkTime!=null?this.checkTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==23)
        {
            return this.patientName!=null?this.patientName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==24)
        {
            return this.bedNo!=null?this.bedNo:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==25)
        {
            return this.task!=null?this.task:SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 26;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.LONG_CLASS;
            info.name = "id";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "wardId";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "teamId";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "userGroup";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "patientId";
            info.namespace= "";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "patientSeries";
            info.namespace= "";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "patientAdmissionId";
            info.namespace= "";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "taskType";
            info.namespace= "";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "taskSubType";
            info.namespace= "";
        }
        if(propertyIndex==9)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "planExeTime";
            info.namespace= "";
        }
        if(propertyIndex==10)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "planEarlyExeTime";
            info.namespace= "";
        }
        if(propertyIndex==11)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "planLateExeTime";
            info.namespace= "";
        }
        if(propertyIndex==12)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "canBeExe";
            info.namespace= "";
        }
        if(propertyIndex==13)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "exeStatus";
            info.namespace= "";
        }
        if(propertyIndex==14)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "taskDetail";
            info.namespace= "";
        }
        if(propertyIndex==15)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "sourceDetail";
            info.namespace= "";
        }
        if(propertyIndex==16)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "exeDetail";
            info.namespace= "";
        }
        if(propertyIndex==17)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "executorId";
            info.namespace= "";
        }
        if(propertyIndex==18)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "executorName";
            info.namespace= "";
        }
        if(propertyIndex==19)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "exeTime";
            info.namespace= "";
        }
        if(propertyIndex==20)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "checkerId";
            info.namespace= "";
        }
        if(propertyIndex==21)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "checkerName";
            info.namespace= "";
        }
        if(propertyIndex==22)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "checkTime";
            info.namespace= "";
        }
        if(propertyIndex==23)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "patientName";
            info.namespace= "";
        }
        if(propertyIndex==24)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "bedNo";
            info.namespace= "";
        }
        if(propertyIndex==25)
        {
            info.type = WSNursingTask.class;
            info.name = "task";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
