package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;
import java.math.BigDecimal;

public class prepareAndCheckBloodProduct extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public String patientId;
    
    public Integer series=0;
    
    public String admissionId;
    
    public String wbn;
    
    public String productCode;
    
    public String tds;
    
    public WSUser preparer;
    
    public String wardId;
    
    public org.joda.time.DateTime scheduledStartTime;
    
    public Integer duration;
    
    public String durationDesc;
    
    public BigDecimal rate;
    
    public BigDecimal amount;
    
    public WSUser checker;
    
    public String cliCode;
    
    public Integer updateNo=0;

    public prepareAndCheckBloodProduct ()
    {
    }

    public prepareAndCheckBloodProduct (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("patientId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("patientId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.patientId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.patientId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("series"))
        {	
	        java.lang.Object obj = soapObject.getProperty("series");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.series = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.series = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("admissionId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("admissionId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.admissionId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.admissionId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("wbn"))
        {	
	        java.lang.Object obj = soapObject.getProperty("wbn");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.wbn = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.wbn = (String)obj;
            }    
        }
        if (soapObject.hasProperty("productCode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("productCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.productCode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.productCode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("tds"))
        {	
	        java.lang.Object obj = soapObject.getProperty("tds");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.tds = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.tds = (String)obj;
            }    
        }
        if (soapObject.hasProperty("preparer"))
        {	
	        java.lang.Object j = soapObject.getProperty("preparer");
	        this.preparer = (WSUser)__envelope.get(j,WSUser.class);
        }
        if (soapObject.hasProperty("wardId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("wardId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.wardId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.wardId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("scheduledStartTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("scheduledStartTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.scheduledStartTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.scheduledStartTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("duration"))
        {	
	        java.lang.Object obj = soapObject.getProperty("duration");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.duration = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.duration = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("durationDesc"))
        {	
	        java.lang.Object obj = soapObject.getProperty("durationDesc");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.durationDesc = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.durationDesc = (String)obj;
            }    
        }
        if (soapObject.hasProperty("rate"))
        {	
	        java.lang.Object obj = soapObject.getProperty("rate");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.rate = new BigDecimal(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof BigDecimal){
                this.rate = (BigDecimal)obj;
            }    
        }
        if (soapObject.hasProperty("amount"))
        {	
	        java.lang.Object obj = soapObject.getProperty("amount");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.amount = new BigDecimal(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof BigDecimal){
                this.amount = (BigDecimal)obj;
            }    
        }
        if (soapObject.hasProperty("checker"))
        {	
	        java.lang.Object j = soapObject.getProperty("checker");
	        this.checker = (WSUser)__envelope.get(j,WSUser.class);
        }
        if (soapObject.hasProperty("cliCode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("cliCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.cliCode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.cliCode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("updateNo"))
        {	
	        java.lang.Object obj = soapObject.getProperty("updateNo");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.updateNo = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.updateNo = (Integer)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return this.patientId!=null?this.patientId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==1)
        {
            return series;
        }
        if(propertyIndex==2)
        {
            return this.admissionId!=null?this.admissionId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return this.wbn!=null?this.wbn:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==4)
        {
            return this.productCode!=null?this.productCode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return this.tds!=null?this.tds:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==6)
        {
            return this.preparer!=null?this.preparer:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==7)
        {
            return this.wardId!=null?this.wardId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==8)
        {
            return this.scheduledStartTime!=null?this.scheduledStartTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==9)
        {
            return this.duration!=null?this.duration:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==10)
        {
            return this.durationDesc!=null?this.durationDesc:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==11)
        {
            return this.rate!=null?this.rate.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==12)
        {
            return this.amount!=null?this.amount.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==13)
        {
            return this.checker!=null?this.checker:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==14)
        {
            return this.cliCode!=null?this.cliCode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==15)
        {
            return updateNo;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 16;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "patientId";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "series";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "admissionId";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "wbn";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "productCode";
            info.namespace= "";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "tds";
            info.namespace= "";
        }
        if(propertyIndex==6)
        {
            info.type = WSUser.class;
            info.name = "preparer";
            info.namespace= "";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "wardId";
            info.namespace= "";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "scheduledStartTime";
            info.namespace= "";
        }
        if(propertyIndex==9)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "duration";
            info.namespace= "";
        }
        if(propertyIndex==10)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "durationDesc";
            info.namespace= "";
        }
        if(propertyIndex==11)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "rate";
            info.namespace= "";
        }
        if(propertyIndex==12)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "amount";
            info.namespace= "";
        }
        if(propertyIndex==13)
        {
            info.type = WSUser.class;
            info.name = "checker";
            info.namespace= "";
        }
        if(propertyIndex==14)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "cliCode";
            info.namespace= "";
        }
        if(propertyIndex==15)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "updateNo";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
