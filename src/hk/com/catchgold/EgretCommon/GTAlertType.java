package hk.com.catchgold.EgretCommon;

public class GTAlertType {
    private int _code;
    private String _name;

    public GTAlertType(int code, String name)
    {
        _code = code;
        _name = name;
    }
    
    public int getCode()
    {
    	return _code; 
    }
    
    public void setCode(int value)
    {
    	_code = value; 
    }
    
    public String getName()
    {
    	return _name; 
    }
    
    public void setName(String value)
    {
    	_name = value; 
    }
}
