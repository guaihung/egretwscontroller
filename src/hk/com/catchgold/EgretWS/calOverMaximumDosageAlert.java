package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import org.ksoap2.serialization.PropertyInfo;

public class calOverMaximumDosageAlert extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public BigDecimal age;
    
    public BigDecimal bodyWeight;
    
    public ArrayList< WSAdminDosage>curDetailList =new ArrayList<WSAdminDosage>();
    
    public ArrayList< WSAdminDosage>historyDetailList =new ArrayList<WSAdminDosage>();
    
    public String cliCode;
    
    public Integer updateNo=0;

    public calOverMaximumDosageAlert ()
    {
    }

    public calOverMaximumDosageAlert (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("age"))
        {	
	        java.lang.Object obj = soapObject.getProperty("age");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.age = new BigDecimal(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof BigDecimal){
                this.age = (BigDecimal)obj;
            }    
        }
        if (soapObject.hasProperty("bodyWeight"))
        {	
	        java.lang.Object obj = soapObject.getProperty("bodyWeight");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.bodyWeight = new BigDecimal(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof BigDecimal){
                this.bodyWeight = (BigDecimal)obj;
            }    
        }
        if (soapObject.hasProperty("curDetailList"))
        {	
	        int size = soapObject.getPropertyCount();
	        this.curDetailList = new ArrayList<WSAdminDosage>();
	        for (int i0=0;i0< size;i0++)
	        {
	            PropertyInfo info=new PropertyInfo();
	            soapObject.getPropertyInfo(i0, info);
                java.lang.Object obj = info.getValue();
	            if (obj!=null && info.name.equals("curDetailList"))
	            {
                    java.lang.Object j =info.getValue();
	                WSAdminDosage j1= (WSAdminDosage)__envelope.get(j,WSAdminDosage.class);
	                this.curDetailList.add(j1);
	            }
	        }
        }
        if (soapObject.hasProperty("historyDetailList"))
        {	
	        int size = soapObject.getPropertyCount();
	        this.historyDetailList = new ArrayList<WSAdminDosage>();
	        for (int i0=0;i0< size;i0++)
	        {
	            PropertyInfo info=new PropertyInfo();
	            soapObject.getPropertyInfo(i0, info);
                java.lang.Object obj = info.getValue();
	            if (obj!=null && info.name.equals("historyDetailList"))
	            {
                    java.lang.Object j =info.getValue();
	                WSAdminDosage j1= (WSAdminDosage)__envelope.get(j,WSAdminDosage.class);
	                this.historyDetailList.add(j1);
	            }
	        }
        }
        if (soapObject.hasProperty("cliCode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("cliCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.cliCode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.cliCode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("updateNo"))
        {	
	        java.lang.Object obj = soapObject.getProperty("updateNo");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.updateNo = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.updateNo = (Integer)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return this.age!=null?this.age.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==1)
        {
            return this.bodyWeight!=null?this.bodyWeight.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.cliCode!=null?this.cliCode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return updateNo;
        }
        if(propertyIndex>=+4 && propertyIndex< + 4+this.curDetailList.size())
        {
            return this.curDetailList.get(propertyIndex-(+4));
        }
        if(propertyIndex>=+4+this.curDetailList.size() && propertyIndex< + 4+this.curDetailList.size()+this.historyDetailList.size())
        {
            return this.historyDetailList.get(propertyIndex-(+4+this.curDetailList.size()));
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 4+curDetailList.size()+historyDetailList.size();
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "age";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "bodyWeight";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "cliCode";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "updateNo";
            info.namespace= "";
        }
        if(propertyIndex>=+4 && propertyIndex <= +4+this.curDetailList.size())
        {
            info.type = WSAdminDosage.class;
            info.name = "curDetailList";
            info.namespace= "";
        }
        if(propertyIndex>=+4+this.curDetailList.size() && propertyIndex <= +4+this.curDetailList.size()+this.historyDetailList.size())
        {
            info.type = WSAdminDosage.class;
            info.name = "historyDetailList";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
