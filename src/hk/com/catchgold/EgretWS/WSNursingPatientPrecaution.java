package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class WSNursingPatientPrecaution extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public String patientId;
    
    public Integer series=0;
    
    public String admissionId;
    
    public Boolean precaution=false;
    
    public Enums.isolationLevel isolation;
    
    public String diagnosis;
    
    public String wardRemarks;
    
    public String delMode;
    
    public org.joda.time.DateTime dcDate;
    
    public org.joda.time.DateTime wakeUpTime;
    
    public Boolean precautionNeedUpdate=false;
    
    public Boolean isolationNeedUpdate=false;
    
    public Boolean diagnosisNeedUpdate=false;
    
    public Boolean remarkNeedUpdate=false;
    
    public Boolean delModeNeedUpdate=false;
    
    public Boolean dcDateNeedUpdate=false;
    
    public Boolean wakeUpTimeNeedUpdate=false;

    public WSNursingPatientPrecaution ()
    {
    }

    public WSNursingPatientPrecaution (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("patientId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("patientId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.patientId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.patientId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("series"))
        {	
	        java.lang.Object obj = soapObject.getProperty("series");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.series = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.series = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("admissionId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("admissionId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.admissionId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.admissionId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("precaution"))
        {	
	        java.lang.Object obj = soapObject.getProperty("precaution");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.precaution = new Boolean(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Boolean){
                this.precaution = (Boolean)obj;
            }    
        }
        if (soapObject.hasProperty("isolation"))
        {	
	        java.lang.Object obj = soapObject.getProperty("isolation");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.isolation = Enums.isolationLevel.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.isolationLevel){
                this.isolation = (Enums.isolationLevel)obj;
            }    
        }
        if (soapObject.hasProperty("diagnosis"))
        {	
	        java.lang.Object obj = soapObject.getProperty("diagnosis");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.diagnosis = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.diagnosis = (String)obj;
            }    
        }
        if (soapObject.hasProperty("wardRemarks"))
        {	
	        java.lang.Object obj = soapObject.getProperty("wardRemarks");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.wardRemarks = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.wardRemarks = (String)obj;
            }    
        }
        if (soapObject.hasProperty("delMode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("delMode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.delMode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.delMode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("dcDate"))
        {	
	        java.lang.Object obj = soapObject.getProperty("dcDate");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.dcDate = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.dcDate = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("wakeUpTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("wakeUpTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.wakeUpTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.wakeUpTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("precautionNeedUpdate"))
        {	
	        java.lang.Object obj = soapObject.getProperty("precautionNeedUpdate");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.precautionNeedUpdate = new Boolean(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Boolean){
                this.precautionNeedUpdate = (Boolean)obj;
            }    
        }
        if (soapObject.hasProperty("isolationNeedUpdate"))
        {	
	        java.lang.Object obj = soapObject.getProperty("isolationNeedUpdate");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.isolationNeedUpdate = new Boolean(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Boolean){
                this.isolationNeedUpdate = (Boolean)obj;
            }    
        }
        if (soapObject.hasProperty("diagnosisNeedUpdate"))
        {	
	        java.lang.Object obj = soapObject.getProperty("diagnosisNeedUpdate");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.diagnosisNeedUpdate = new Boolean(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Boolean){
                this.diagnosisNeedUpdate = (Boolean)obj;
            }    
        }
        if (soapObject.hasProperty("remarkNeedUpdate"))
        {	
	        java.lang.Object obj = soapObject.getProperty("remarkNeedUpdate");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.remarkNeedUpdate = new Boolean(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Boolean){
                this.remarkNeedUpdate = (Boolean)obj;
            }    
        }
        if (soapObject.hasProperty("delModeNeedUpdate"))
        {	
	        java.lang.Object obj = soapObject.getProperty("delModeNeedUpdate");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.delModeNeedUpdate = new Boolean(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Boolean){
                this.delModeNeedUpdate = (Boolean)obj;
            }    
        }
        if (soapObject.hasProperty("dcDateNeedUpdate"))
        {	
	        java.lang.Object obj = soapObject.getProperty("dcDateNeedUpdate");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.dcDateNeedUpdate = new Boolean(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Boolean){
                this.dcDateNeedUpdate = (Boolean)obj;
            }    
        }
        if (soapObject.hasProperty("wakeUpTimeNeedUpdate"))
        {	
	        java.lang.Object obj = soapObject.getProperty("wakeUpTimeNeedUpdate");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.wakeUpTimeNeedUpdate = new Boolean(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Boolean){
                this.wakeUpTimeNeedUpdate = (Boolean)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return this.patientId!=null?this.patientId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==1)
        {
            return series;
        }
        if(propertyIndex==2)
        {
            return this.admissionId!=null?this.admissionId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return precaution;
        }
        if(propertyIndex==4)
        {
            return this.isolation!=null?this.isolation.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return this.diagnosis!=null?this.diagnosis:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==6)
        {
            return this.wardRemarks!=null?this.wardRemarks:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==7)
        {
            return this.delMode!=null?this.delMode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==8)
        {
            return this.dcDate!=null?this.dcDate:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==9)
        {
            return this.wakeUpTime!=null?this.wakeUpTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==10)
        {
            return precautionNeedUpdate;
        }
        if(propertyIndex==11)
        {
            return isolationNeedUpdate;
        }
        if(propertyIndex==12)
        {
            return diagnosisNeedUpdate;
        }
        if(propertyIndex==13)
        {
            return remarkNeedUpdate;
        }
        if(propertyIndex==14)
        {
            return delModeNeedUpdate;
        }
        if(propertyIndex==15)
        {
            return dcDateNeedUpdate;
        }
        if(propertyIndex==16)
        {
            return wakeUpTimeNeedUpdate;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 17;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "patientId";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "series";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "admissionId";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "precaution";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "isolation";
            info.namespace= "";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "diagnosis";
            info.namespace= "";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "wardRemarks";
            info.namespace= "";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "delMode";
            info.namespace= "";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "dcDate";
            info.namespace= "";
        }
        if(propertyIndex==9)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "wakeUpTime";
            info.namespace= "";
        }
        if(propertyIndex==10)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "precautionNeedUpdate";
            info.namespace= "";
        }
        if(propertyIndex==11)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "isolationNeedUpdate";
            info.namespace= "";
        }
        if(propertyIndex==12)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "diagnosisNeedUpdate";
            info.namespace= "";
        }
        if(propertyIndex==13)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "remarkNeedUpdate";
            info.namespace= "";
        }
        if(propertyIndex==14)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "delModeNeedUpdate";
            info.namespace= "";
        }
        if(propertyIndex==15)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "dcDateNeedUpdate";
            info.namespace= "";
        }
        if(propertyIndex==16)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "wakeUpTimeNeedUpdate";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
