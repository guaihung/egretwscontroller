package hk.com.catchgold.EgretCommon.Enums;

public enum StationType
{ 
    NURSE,
    DOCTOR,
    PATHOLOGY,
    CA,
    MEI,
    OT
}
