package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class WSSpecialInstruction extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public Integer id=0;
    
    public String type;
    
    public String description;
    
    public WSUser recorder;
    
    public org.joda.time.DateTime recordTime;

    public WSSpecialInstruction ()
    {
    }

    public WSSpecialInstruction (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("id"))
        {	
	        java.lang.Object obj = soapObject.getProperty("id");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.id = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.id = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("type"))
        {	
	        java.lang.Object obj = soapObject.getProperty("type");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.type = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.type = (String)obj;
            }    
        }
        if (soapObject.hasProperty("description"))
        {	
	        java.lang.Object obj = soapObject.getProperty("description");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.description = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.description = (String)obj;
            }    
        }
        if (soapObject.hasProperty("recorder"))
        {	
	        java.lang.Object j = soapObject.getProperty("recorder");
	        this.recorder = (WSUser)__envelope.get(j,WSUser.class);
        }
        if (soapObject.hasProperty("recordTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("recordTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.recordTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.recordTime = (org.joda.time.DateTime)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return id;
        }
        if(propertyIndex==1)
        {
            return this.type!=null?this.type:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.description!=null?this.description:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return this.recorder!=null?this.recorder:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==4)
        {
            return this.recordTime!=null?this.recordTime:SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 5;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "id";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "type";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "description";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = WSUser.class;
            info.name = "recorder";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "recordTime";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
