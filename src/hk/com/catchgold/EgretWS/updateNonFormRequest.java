package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class updateNonFormRequest extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public Integer requestId=0;
    
    public String patientName;
    
    public org.joda.time.DateTime scheduledStartTime;
    
    public Enums.requestUrgentFlag urgentFlag;
    
    public WSUser updater;
    
    public String cliCode;
    
    public Integer updateNo=0;

    public updateNonFormRequest ()
    {
    }

    public updateNonFormRequest (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("requestId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("requestId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.requestId = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.requestId = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("patientName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("patientName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.patientName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.patientName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("scheduledStartTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("scheduledStartTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.scheduledStartTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.scheduledStartTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("urgentFlag"))
        {	
	        java.lang.Object obj = soapObject.getProperty("urgentFlag");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.urgentFlag = Enums.requestUrgentFlag.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.requestUrgentFlag){
                this.urgentFlag = (Enums.requestUrgentFlag)obj;
            }    
        }
        if (soapObject.hasProperty("updater"))
        {	
	        java.lang.Object j = soapObject.getProperty("updater");
	        this.updater = (WSUser)__envelope.get(j,WSUser.class);
        }
        if (soapObject.hasProperty("cliCode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("cliCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.cliCode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.cliCode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("updateNo"))
        {	
	        java.lang.Object obj = soapObject.getProperty("updateNo");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.updateNo = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.updateNo = (Integer)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return requestId;
        }
        if(propertyIndex==1)
        {
            return this.patientName!=null?this.patientName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.scheduledStartTime!=null?this.scheduledStartTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return this.urgentFlag!=null?this.urgentFlag.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==4)
        {
            return this.updater!=null?this.updater:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return this.cliCode!=null?this.cliCode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==6)
        {
            return updateNo;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 7;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "requestId";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "patientName";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "scheduledStartTime";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "urgentFlag";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = WSUser.class;
            info.name = "updater";
            info.namespace= "";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "cliCode";
            info.namespace= "";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "updateNo";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
