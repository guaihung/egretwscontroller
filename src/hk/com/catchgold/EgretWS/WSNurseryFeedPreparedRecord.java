package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class WSNurseryFeedPreparedRecord extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public Long id=0L;
    
    public String patientId;
    
    public Integer series=0;
    
    public String admissionId;
    
    public String barcode;
    
    public WSNurseryFeedOrder feedOrder;
    
    public Long outstandingTaskId=0L;
    
    public Enums.feedAdminStatus status;
    
    public org.joda.time.DateTime planTime;
    
    public Integer volume=0;
    
    public String unit;
    
    public org.joda.time.DateTime preparedTime;
    
    public String preparerCode;
    
    public String preparerName;
    
    public String remark;
    
    public Integer labelNumber=0;

    public WSNurseryFeedPreparedRecord ()
    {
    }

    public WSNurseryFeedPreparedRecord (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("id"))
        {	
	        java.lang.Object obj = soapObject.getProperty("id");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.id = new Long(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Long){
                this.id = (Long)obj;
            }    
        }
        if (soapObject.hasProperty("patientId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("patientId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.patientId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.patientId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("series"))
        {	
	        java.lang.Object obj = soapObject.getProperty("series");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.series = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.series = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("admissionId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("admissionId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.admissionId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.admissionId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("barcode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("barcode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.barcode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.barcode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("feedOrder"))
        {	
	        java.lang.Object j = soapObject.getProperty("feedOrder");
	        this.feedOrder = (WSNurseryFeedOrder)__envelope.get(j,WSNurseryFeedOrder.class);
        }
        if (soapObject.hasProperty("outstandingTaskId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("outstandingTaskId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.outstandingTaskId = new Long(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Long){
                this.outstandingTaskId = (Long)obj;
            }    
        }
        if (soapObject.hasProperty("status"))
        {	
	        java.lang.Object obj = soapObject.getProperty("status");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.status = Enums.feedAdminStatus.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.feedAdminStatus){
                this.status = (Enums.feedAdminStatus)obj;
            }    
        }
        if (soapObject.hasProperty("planTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("planTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.planTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.planTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("volume"))
        {	
	        java.lang.Object obj = soapObject.getProperty("volume");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.volume = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.volume = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("unit"))
        {	
	        java.lang.Object obj = soapObject.getProperty("unit");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.unit = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.unit = (String)obj;
            }    
        }
        if (soapObject.hasProperty("preparedTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("preparedTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.preparedTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.preparedTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("preparerCode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("preparerCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.preparerCode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.preparerCode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("preparerName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("preparerName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.preparerName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.preparerName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("remark"))
        {	
	        java.lang.Object obj = soapObject.getProperty("remark");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.remark = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.remark = (String)obj;
            }    
        }
        if (soapObject.hasProperty("labelNumber"))
        {	
	        java.lang.Object obj = soapObject.getProperty("labelNumber");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.labelNumber = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.labelNumber = (Integer)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return id;
        }
        if(propertyIndex==1)
        {
            return this.patientId!=null?this.patientId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return series;
        }
        if(propertyIndex==3)
        {
            return this.admissionId!=null?this.admissionId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==4)
        {
            return this.barcode!=null?this.barcode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return this.feedOrder!=null?this.feedOrder:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==6)
        {
            return outstandingTaskId;
        }
        if(propertyIndex==7)
        {
            return this.status!=null?this.status.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==8)
        {
            return this.planTime!=null?this.planTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==9)
        {
            return volume;
        }
        if(propertyIndex==10)
        {
            return this.unit!=null?this.unit:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==11)
        {
            return this.preparedTime!=null?this.preparedTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==12)
        {
            return this.preparerCode!=null?this.preparerCode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==13)
        {
            return this.preparerName!=null?this.preparerName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==14)
        {
            return this.remark!=null?this.remark:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==15)
        {
            return labelNumber;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 16;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.LONG_CLASS;
            info.name = "id";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "patientId";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "series";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "admissionId";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "barcode";
            info.namespace= "";
        }
        if(propertyIndex==5)
        {
            info.type = WSNurseryFeedOrder.class;
            info.name = "feedOrder";
            info.namespace= "";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.LONG_CLASS;
            info.name = "outstandingTaskId";
            info.namespace= "";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "status";
            info.namespace= "";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "planTime";
            info.namespace= "";
        }
        if(propertyIndex==9)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "volume";
            info.namespace= "";
        }
        if(propertyIndex==10)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "unit";
            info.namespace= "";
        }
        if(propertyIndex==11)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "preparedTime";
            info.namespace= "";
        }
        if(propertyIndex==12)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "preparerCode";
            info.namespace= "";
        }
        if(propertyIndex==13)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "preparerName";
            info.namespace= "";
        }
        if(propertyIndex==14)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "remark";
            info.namespace= "";
        }
        if(propertyIndex==15)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "labelNumber";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
