package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;
import java.math.BigDecimal;

public class WSIVFFlowInfo extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public Integer ivfDuration=0;
    
    public String ivfDurationDesc;
    
    public BigDecimal ivfRate;

    public WSIVFFlowInfo ()
    {
    }

    public WSIVFFlowInfo (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("ivfDuration"))
        {	
	        java.lang.Object obj = soapObject.getProperty("ivfDuration");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.ivfDuration = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.ivfDuration = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("ivfDurationDesc"))
        {	
	        java.lang.Object obj = soapObject.getProperty("ivfDurationDesc");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.ivfDurationDesc = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.ivfDurationDesc = (String)obj;
            }    
        }
        if (soapObject.hasProperty("ivfRate"))
        {	
	        java.lang.Object obj = soapObject.getProperty("ivfRate");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.ivfRate = new BigDecimal(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof BigDecimal){
                this.ivfRate = (BigDecimal)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return ivfDuration;
        }
        if(propertyIndex==1)
        {
            return this.ivfDurationDesc!=null?this.ivfDurationDesc:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.ivfRate!=null?this.ivfRate.toString():SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 3;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "ivfDuration";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "ivfDurationDesc";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "ivfRate";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
