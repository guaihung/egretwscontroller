package hk.com.catchgold.EgretCommon;

import hk.com.catchgold.EgretWS.*;
import java.lang.Exception;
import hk.com.catchgold.EgretWS.Enums.*;
import hk.com.catchgold.EgretWSController.WSController;

import java.util.Hashtable;

import org.joda.time.DateTime;

import android.util.Log;

public class GlobalCache {
    private static Hashtable NursingRecordTemplates = new Hashtable();
    private static Hashtable NursingRecordTemplateDetails = new Hashtable();
    private static Hashtable NursingRecordDictionary = new Hashtable();
    private static Hashtable PatientPictures = new Hashtable();
    private static Hashtable UserPermissions = new Hashtable();
    private static Hashtable WardTeams = new Hashtable();
    private static DateTime _lastRefreshTime = null;
    private static char de = GlobalVars.de;

    static WSNursingRecordForm[] forms;
    public static WSNursingRecordForm[] GetNursingRecordTemplates(String deptno, formType type, subFormType subType) throws Exception {
        	try {
        		forms = WSController.GetFormTemplates(deptno, type, subType);
        	}  catch (java.lang.Exception ex){
        		ex.printStackTrace();
        		throw ex;
        	}

        	if (forms == null){
        		return new WSNursingRecordForm[0];
        	}
        	
        return forms;
    }
    
    static WSNursingRecordForm form;
    public static WSNursingRecordForm GetNursingRecordTemplateDetail(String code) throws Exception{
        	try {
	            form = WSController.GetFormTemplateByCode(null, 1, null, code);
	    	} 
        	catch (java.lang.Exception ex){
	    		ex.printStackTrace();
	    		throw ex;
	    	}
        	
        	if (form == null){
        		return new WSNursingRecordForm();
        	}
        return form;
    }

    static WSIODictionaryItem[] newItems;
    public static WSIODictionaryItem[] GetNursingRecordDictionary(ioType type, String code, String wardId) throws Exception{
        	try {
	            newItems = WSController.GetIODictionary(type, code, wardId);
	            Log.e("Mcs", " newItems  " + newItems.length);
	    	}  catch (java.lang.Exception ex){
	    		ex.printStackTrace();
	    		throw ex;
	    	}
        	
        	if (newItems == null){
        		return newItems = new WSIODictionaryItem[0];
        	}
        return newItems;
    }

//    public static WSIODictionaryItem[] GetNursingRecordDictionary(ioType type, String code, String wardId) {
//        String key = Integer.toString(type.getCode()) + de + code;
//        WSIODictionaryItem[] items = null;
//        	try {
//	            WSIODictionaryItem[] newItems = WSController.GetIODictionary(type, code, wardId);
//	            Log.e("Mcs", " newItems  " + newItems.length);
//	    	} 
//        	catch (java.lang.Exception ex){
//	    		ex.printStackTrace();
//	    	}
//
//        WSIODictionaryItem[] tempitems = (WSIODictionaryItem[])NursingRecordDictionary.get(key);
//        if (tempitems != null) {
//            items = new WSIODictionaryItem[tempitems.length];
//            if (items.length > 0){
//            	 Log.e("Mcs", " items  " + items.length);
//            	 for (int i=0; i < tempitems.length ;i++){
//            		 items [i] = tempitems[i];
//            	 }
////            	items = tempitems.clone(); 
//                //tempitems.CopyTo(items, 0);
//            }
//        }
//
//        return items;
//    }
    
    /// <summary>
    /// Wade 2009-12-18
    /// </summary>
    /// <param name="patientId"></param>
    /// <param name="admissionId"></param>
    public static byte[] GetPatientPicture(String patientId, String admissionId)
    {
        //每隔30分钟刷新一次缓存
    	DateTime now = new DateTime();
    	
    	// if (_lastRefreshTime == null || DateTime.Now.Subtract(_lastRefreshTime.Value).TotalMinutes > 30)
    	if (_lastRefreshTime == null || (long)(now.toDate().getTime() - _lastRefreshTime.toDate().getTime()) > 30 * 60 * 1000)
        {
            // _lastRefreshTime = DateTime.Now;
    		// by default, it is set to "NOW"
    		_lastRefreshTime = new DateTime(); 
            PatientPictures.clear();
        }

        String key = patientId + de + admissionId;
        byte[] picture = null;
        if (!PatientPictures.containsKey(key))
        {
        	try {
        		byte[] newpic = WSController.GetAdmissionPictureById(patientId, admissionId);
        		PatientPictures.put(key, newpic);
	    	} 
        	catch (java.lang.Exception ex)
	    	{
	    		ex.printStackTrace();
	    	}
        }

        byte[] tempic = (byte[])PatientPictures.get(key);
        if (tempic != null)
        {
            picture = new byte[tempic.length];
            if (picture.length > 0)
            {
                // tempic.CopyTo(picture, 0);
            	picture = tempic.clone(); 
            	// tempic.CopyTo(picture, 0);
            }
        }

        return picture;
    }

    /// <summary>
    /// Wade 2009-12-18
    /// </summary>
    /// <param name="patientId"></param>
    /// <param name="admissionId"></param>
    public static void RemovePatientPicture(String patientId, String admissionId)
    {
        PatientPictures.remove(patientId + de + admissionId);
    }

    public static WSUserPermission GetUserSubSystemPermission(String userCode, int functionId) throws Exception {
    	 WSUserPermission userPermission;
    	 try {
    		 userPermission = WSController.GetUserSubSystemPermission(userCode, functionId);
    	 } catch (java.lang.Exception ex){
    		 ex.printStackTrace();
    		 throw ex;
    	 }
    	 
    	 if (userPermission == null){
    		 return new WSUserPermission();
    	 }
//        String key = userCode + de + Integer.toString(functionId);
//        WSUserPermission userPermission = null;
//        if (!UserPermissions.containsKey(key))
//        {
//        	try {
//        		WSUserPermission newUserPermission = WSController.GetUserSubSystemPermission(userCode, functionId);
//        		UserPermissions.put(key, newUserPermission);
//	    	} 
//        	catch (java.lang.Exception ex)
//	    	{
//	    		ex.printStackTrace();
//	    	}
//        }
//
//        WSUserPermission tempUserPermission = (WSUserPermission)UserPermissions.get(key);
//        if (tempUserPermission != null)
//        {
//            userPermission = tempUserPermission;
//        }

        return userPermission;
    }

    public static WSWardTeam[] GetTeamList(String wardId, boolean hasAll) throws Exception {
    	 WSWardTeam[] teams;
    	  try {
    		  teams = WSController.GetTeamList(wardId, hasAll);
		} catch (java.lang.Exception e) {
			e.printStackTrace();
			throw e;
		}
    	  
    	  if (teams == null){
    		  return new WSWardTeam[0];
    	  }
//        String key = wardId + de + Boolean.toString(hasAll);
//        WSWardTeam[] teams = null;
//        if (!WardTeams.containsKey(key))
//        {
//        	try {
//	            WSWardTeam[] newTeams = WSController.GetTeamList(wardId, hasAll);
//	            WardTeams.put(key, newTeams);
//	    	} 
//        	catch (java.lang.Exception ex)
//	    	{
//	    		// ex.printStackTrace();
//        		Log.e("Egret", "GetTeamList Exception", ex); 
//	    	}
//        }
//
//        WSWardTeam[] tempTeams = (WSWardTeam[])WardTeams.get(key);
//        if (tempTeams != null)
//        {
//            teams = tempTeams;
//        }

        return teams;
    }


    public static void Clear()
    {
        NursingRecordTemplates.clear();
        NursingRecordTemplateDetails.clear();
        NursingRecordDictionary.clear();
        PatientPictures.clear();
        UserPermissions.clear();
        WardTeams.clear();
        GlobalVars.LabelPrinterAddress = null;
    }

}
