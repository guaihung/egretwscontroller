package hk.com.catchgold.EgretCommon;

import java.net.InetAddress;
import java.net.UnknownHostException;

import android.util.Log;

public class Dns {
	public static String GetHostName()
	{
		String hostName = ""; 
		try {
			hostName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("Egret", "Unknown host error"); 
			hostName = "Unknown host";
		} 
		
		return hostName; 
	}
}
