package hk.com.catchgold.egretwstest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpsServiceConnectionSE;
import org.ksoap2.transport.HttpsTransportSE;
import org.ksoap2.transport.ServiceConnection;
import org.kxml2.io.KXmlSerializer;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.util.Log;

public class CustomKeepAliveHttpsTransportSE extends HttpsTransportSE {

	private HttpsServiceConnectionSE conn = null;
	private final String host;
	private final int port;
	private final String file;
	private final int timeout;
	private HashMap prefixes = new HashMap();
	private String xmlVersionTag = "";
	private int bufferLength = ServiceConnection.DEFAULT_BUFFER_SIZE;
	
	@Override
	public List call(String arg0, SoapEnvelope arg1, List arg2, File arg3) throws HttpResponseException, IOException, XmlPullParserException {
		return super.call(arg0, arg1, arg2, arg3);
	}

	@Override
	public List call(String soapAction, SoapEnvelope envelope, List headers) throws HttpResponseException, IOException, XmlPullParserException {
		return super.call(soapAction, envelope, headers);
	}

	@Override
	public void call(String soapAction, SoapEnvelope envelope) throws HttpResponseException, IOException, XmlPullParserException {
		super.call(soapAction, envelope);
	}

	@Override
	protected void parseResponse(SoapEnvelope envelope, InputStream is,
			List returnedHeaders) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		super.parseResponse(envelope, is, returnedHeaders);
	}

	@Override
	protected void sendData(byte[] requestData, ServiceConnection connection, SoapEnvelope envelope) throws IOException {
		// TODO Auto-generated method stub
		super.sendData(requestData, connection, envelope);
	}

	@Override
	protected byte[] createRequestData(SoapEnvelope envelope, String encoding)
			throws IOException {
		Log.e("Mcs", " createRequestData ");
		   ByteArrayOutputStream bos = new ByteArrayOutputStream(bufferLength);
	        byte result[] = null;
	        bos.write(xmlVersionTag.getBytes());
	        XmlSerializer xw = new KXmlSerializer();

	        final Iterator keysIter = prefixes.keySet().iterator();

	        xw.setOutput(bos, encoding);
	        while (keysIter.hasNext()) {
	            String key = (String) keysIter.next();
	            xw.setPrefix(key, (String) prefixes.get(key));
	        }
	        envelope.write(xw);
	        xw.flush();
	        bos.write('\r');
	        bos.write('\n');
	        bos.flush();
	        result = bos.toByteArray();
	        xw = null;
	        bos = null;
	        return result;
	}

	@Override
	protected byte[] createRequestData(SoapEnvelope envelope)
			throws IOException {
		// TODO Auto-generated method stub
		return super.createRequestData(envelope);
	}

	@Override
	public String getHost() throws MalformedURLException {
		// TODO Auto-generated method stub
		return super.getHost();
	}

	@Override
	public String getPath() throws MalformedURLException {
		// TODO Auto-generated method stub
		return super.getPath();
	}

	@Override
	public int getPort() throws MalformedURLException {
		// TODO Auto-generated method stub
		return super.getPort();
	}

	@Override
	public HashMap getPrefixes() {
		// TODO Auto-generated method stub
		return super.getPrefixes();
	}

	@Override
	protected void parseResponse(SoapEnvelope envelope, InputStream is)
			throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		super.parseResponse(envelope, is);
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		super.reset();
	}

	@Override
	public void setUrl(String url) {
		// TODO Auto-generated method stub
		super.setUrl(url);
	}

	@Override
	public void setXmlVersionTag(String tag) {
		 xmlVersionTag = tag;
	}

	public CustomKeepAliveHttpsTransportSE(String host, int port, String file, int timeout) {
		super(host, port, file, timeout);
		this.host = host;
		this.port = port;
		this.file = file;
		this.timeout = timeout;
	}

}
