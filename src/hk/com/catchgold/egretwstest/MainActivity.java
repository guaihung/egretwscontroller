package hk.com.catchgold.egretwstest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.joda.time.DateTime;
















import hk.com.catchgold.EgretCommon.GlobalCache;
import hk.com.catchgold.EgretCommon.GlobalMethods;
import hk.com.catchgold.EgretCommon.GlobalTaskType;
import hk.com.catchgold.EgretCommon.GlobalVars;
import hk.com.catchgold.EgretGlobal.Global;
// import hk.com.catchgold.EgretWS.EgretWSBinding;
// import hk.com.catchgold.EgretWS.WSUser;
import hk.com.catchgold.EgretWS.*;
import hk.com.catchgold.EgretWS.Enums.bloodTransfusionStatus;
import hk.com.catchgold.EgretWS.Enums.formType;
import hk.com.catchgold.EgretWS.Enums.subFormType;
import hk.com.catchgold.EgretWS.Enums.taskStatus1;
import hk.com.catchgold.EgretWS.Enums.taskSubType;
import hk.com.catchgold.EgretWS.Enums.taskType;
import hk.com.catchgold.EgretWSController.WSController;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	String userName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button button = (Button)findViewById(R.id.button_test);
		button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        		new Thread(){
            		public void run(){
                    	// EgretWSBinding service = new EgretWSBinding(); 
                    	try {

                    		WSNursingOrder[] nursingOrders = WSController.GetDispensedDrugByBatch("3933346", 0, "141040909", "9");
                    		
                    		for (WSNursingOrder o : nursingOrders)
                    		{
                    			Log.e("Egret", "Order name: " + o.itemName); 
                    			
                    			WSNursingOrder[] ors = new WSNursingOrder[1]; 
                    			ors[0] = o; 
                    			try {
                    				WSController.ReceiveDrugs(ors, "9", "0880");
                    			}
                    			catch (java.lang.Exception ex)
                    			{
                    				Log.e("Egret", "Drug " + o.itemName + " recv failed", ex);
                    			}
                    		}
                    		
                    		
/*                    		
        	                bloodTransfusionStatus[] status = new bloodTransfusionStatus[2];
        	                status[0] = bloodTransfusionStatus.PREPARE_CHECKED;
        	                status[1] = bloodTransfusionStatus.PREPARED;
        	                
                   			WSNursingBloodTransfusionRecord[] bldRecords = WSController.GetPatientBloodTransfusionRecords("TMP012", 1, "12988945",
           		                    null, null, null, null, null, null, null, null, status);
                    		
                    		Log.e("Egret", "Number of returned records: " + bldRecords.length); 
                    		
                    		for (WSNursingBloodTransfusionRecord r : bldRecords)
                    		{
                    			Log.e("Egret", "Record Info " + r.barcodeId);
                    		}
*/                    		
                    		
/*                    		
                    		
                    		String t = "TMP016";
                    		WSPatientVisit pv = WSController.GetAdmissionById(t, 1, null);

                    		Log.e("EgretWS", "PV Info: " + pv.patient.id + " " + pv.patient.enFirstName + " " + pv.patient.enLastName); 
                    		
//                    		WSDictionaryItem taskType = new WSDictionaryItem();
//                            taskType.code = "1";
//                            taskType.name = "Observation";
                            
                    		taskType taskType_t = taskType.OBS;  
                    		
//                            WSDictionaryItem taskSubType = new WSDictionaryItem();
//                            taskType.code = Integer.toString(GlobalTaskType.ALL.showType());
//                            taskType.name = "ALL";
                            
                            
                            int viewType = 0; 
                            taskStatus1 status = taskStatus1.ALL_GENERATED;   
                            
                            DateTime startTime = new DateTime(0); 
                            DateTime endTime = new DateTime();
                            
                            
                            WSNursingTask[] m_task = WSController.GetPatientTaskList(pv, taskType_t, null, taskStatus1.ON, 5002, null, null);
                            
                            Log.e("EgretWS", "List Count: " + m_task.length); 
                            
                            

                            // WnsMethods.ShowTaskView(taskType, null, _patientVisit);

                    		
                    		
//                            WSNursingOutstandingTaskListPage page = WSController.GetHistoryOutstandingTasks(null, null, "6", "000009",
//                                    "IC", taskType.OBS, null, null, 0, 65536);
//  
//                            Log.e("EgretWS", "Task count: " + page.nursingOutstandingTaskList.size());
 */
                    		
/*
                    		ArrayList<WSDictionaryItem> subTypes = WSController.GetOutstandingTaskSubType(1);
                    		
                    		if (subTypes == null)
                    			Log.e("Egret(GUAI)", "Number of tasks: 0(NULL)");
                    		else
                    		{
                    			Log.e("Egret(GUAI)", "Number of tasks: " + subTypes.size());
                    			for (int i=0; i<subTypes.size(); i++)
                    				Log.e("Egret(GUAI)", i + " " + subTypes.get(i).name + " " + subTypes.get(i).code);
                    		
                    		}
*/                    		
/*                    		
                    		ArrayList<WSDictionaryItem> tasks = WSController.GetOutstandingTaskType();
                    		
                    		if (tasks == null)
                    			Log.e("Egret(GUAI)", "Number of tasks: 0(NULL)");
                    		else
                    		{
                    			Log.e("Egret(GUAI)", "Number of tasks: " + tasks.size());
                    			for (int i=0; i<tasks.size(); i++)
                    				Log.e("Egret(GUAI)", i + " " + tasks.get(i).name + " " + tasks.get(i).code);
                    		
                    		}
*/                    		
/*                    		
                    		DateTime dbtime = new DateTime();
                    		DateTime _medStart = new DateTime();
                    		DateTime _medEnd = dbtime.minusDays(7);
                    		WSDrugRepeatInfo[] nursingOrder = WSController.GetDrugRepeatHistory(614680, _medStart, _medEnd);
                    		
                    		Log.e("EgretWSTest", "Drug count: " + nursingOrder.length);
                    		for (int i=0; i<nursingOrder.length; i++)
                    		{
                    			
                    			Log.e("EgretWSTest", nursingOrder[i].bedNo + " Hihihi ");
                    		}
*/                    		
/*                    		
        					// WSUser user = service.login("0966", "966966", "EgretPPC", -1);
                    		String barcode = "<BT>WP</BT>5DEF99DC4A7CAA4F"; 
                    		
                    		// String t = GlobalMethods.GetBarcodeValue(barcode, "URN"); 
                    		// String t = "#DGZpf";
                    		String t = "TMP005";
                    		Log.e("Egret", "Decrypted Patient URN: " + t);
                    		
                    		
                    		String ttt = GlobalMethods.GetBarcodeValue(barcode, "URN");
                    		Log.e("Egret", "Decrypted Patient URN2 : " + ttt);
                    		
                    		WSPatientVisit pv = WSController.GetAdmissionById(t, 1, null);
                    		Log.e("Egret", "PV " + pv.patient.enFirstName + " " + pv.patient.enLastName);
*/                    		
/*                    		
                    		
                    		WSNursingRecordForm[] forms =  GlobalCache.GetNursingRecordTemplates("6", formType.OBSERVATION, subFormType.ALL);
                    		
                    		if (forms == null)
                    		{
                    			Log.e("Egret", "Forms is null");
                    		}
                    		else 
                    		{
	                    		for (WSNursingRecordForm form : forms)
	                    		{
	                    			Log.e("Egret", form.templateName + " " + form.templateCode + " " + form.clientDefinedTemplate + " " + form.subFormType); 
	                    		}
                    		}
                    		
                    		
                    		WSNursingRecordForm form_t =  GlobalCache.GetNursingRecordTemplateDetail("OBS_INPUT_001");
                    		for (WSNursingRecordFormItem item : form_t.sections.get(0).items)
                    		{
                    			Log.e("Egret", "Item " + item.name + " " + item.specialFlag + " " + item.inputMethod + " " + item.nullFlag + " " + item.dataType);
                    		}
                    		
                    		
                    		
                    		WSUser user = WSController.Login("0966", "966966"); 
        					Log.e("egret", user.enName);
        					userName = user.enName; 
        					
                    		
                    		WSSysFunction func = WSController.GetUserSubSystem(user.code);
                    		
                    		if (func == null)
                    			Log.e("egret", "Func NULL"); 
                    		else 
                    			Log.e("egret", "Func length: " +  func.childFunctions.size());
                    		
                    		handler.sendEmptyMessage(1);
*/                    		
        				} catch (java.lang.Exception e) {
        					// TODO Auto-generated catch block
        					Log.e("EgretWSTest", "Error", e);
        					// editText.setText("Hello World");
        				} 
            		}
            	}.start();		

            }
         });
		
		Button button2 = (Button)findViewById(R.id.button1);
		button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        		new Thread(){
            		public void run(){
                    	// EgretWSBinding service = new EgretWSBinding();
            			// WSController controller = new WSController(); 
                    	try {
                    		WSWardTeam[] teamList_t = GlobalCache.GetTeamList("11", true);
				    		// teamList = new ArrayList<String>(); 
				    		String [] teamList = new String[teamList_t.length];
				    		
				    		for (int i=0; i<teamList_t.length; i++ )
				    			teamList[i] = teamList_t[i].fullName; 
				    			
				    		
				    		Log.e("MCS(Guai", "Team List triggered, length: " + teamList_t.length); 

/*                    		
                    		DateTime dateTime = WSController.GetTime();
                    		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
                    		userName = formatter.format(dateTime.toDate());
                    		handler.sendEmptyMessage(1);
//        					WSUser user = service.login("0966", "966966", "EgretPPC", -1);
//        					Log.e("egret", user.enName);
//        					userName = user.enName; 
//        					handler.sendEmptyMessage(1);
*/
        				} catch (java.lang.Exception e) {
        					// TODO Auto-generated catch block
        					e.printStackTrace();
        					// editText.setText("Hello World");
        				} 
            		}
            	}.start();		

            }
         });

		
		Button button3 = (Button)findViewById(R.id.button2);
		button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        		new Thread(){
            		public void run(){
                    	// EgretWSBinding service = new EgretWSBinding();
            			// WSController controller = new WSController();
                    	try {
                    		
                    		WSDictionaryItem[] items = WSController.GetDrugAdminGivenRemarks(); 
                    		
                    		userName = String.valueOf(items.length) + " "; 
                    		for (int i=0; i<items.length; i++)
                    		{
                    			userName += items[i].name + "|"; 
                    		}
                    		handler.sendEmptyMessage(1);
                    		
                    		
//        					WSUser user = service.login("0966", "966966", "EgretPPC", -1);
//        					Log.e("egret", user.enName);
//        					userName = user.enName; 
//        					handler.sendEmptyMessage(1);

        				} catch (java.lang.Exception e) {
        					// TODO Auto-generated catch block
        					e.printStackTrace();
        					// editText.setText("Hello World");
        				} 
            		}
            	}.start();		

            }
         });

	}


	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what){
			case 1:
				EditText editText = (EditText)findViewById(R.id.editText_test);
				editText.setText(userName);
				break;
			}
		}
	};
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
