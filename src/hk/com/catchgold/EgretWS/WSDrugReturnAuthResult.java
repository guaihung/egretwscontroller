package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class WSDrugReturnAuthResult extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public String staffName;
    
    public String staffCode;
    
    public String jobRole;
    
    public String authResult;

    public WSDrugReturnAuthResult ()
    {
    }

    public WSDrugReturnAuthResult (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("staffName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("staffName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.staffName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.staffName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("staffCode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("staffCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.staffCode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.staffCode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("jobRole"))
        {	
	        java.lang.Object obj = soapObject.getProperty("jobRole");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.jobRole = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.jobRole = (String)obj;
            }    
        }
        if (soapObject.hasProperty("authResult"))
        {	
	        java.lang.Object obj = soapObject.getProperty("authResult");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.authResult = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.authResult = (String)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return this.staffName!=null?this.staffName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==1)
        {
            return this.staffCode!=null?this.staffCode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.jobRole!=null?this.jobRole:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return this.authResult!=null?this.authResult:SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 4;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "staffName";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "staffCode";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "jobRole";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "authResult";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
