package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class WSMotherBabyMatcher extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public String patientId;
    
    public String match1Id;
    
    public String match1EnName;
    
    public String match1CnName;
    
    public String match1BedNo;
    
    public Enums.motherBabyMatcherClass match1Class;
    
    public String match2Id;
    
    public String match2EnName;
    
    public String match2CnName;
    
    public String match2BedNo;
    
    public Enums.motherBabyMatcherClass match2Class;
    
    public String wardId;
    
    public Integer scanNum=0;
    
    public Enums.motherBabyMatcherStatus status;

    public WSMotherBabyMatcher ()
    {
    }

    public WSMotherBabyMatcher (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("patientId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("patientId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.patientId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.patientId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("match1Id"))
        {	
	        java.lang.Object obj = soapObject.getProperty("match1Id");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.match1Id = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.match1Id = (String)obj;
            }    
        }
        if (soapObject.hasProperty("match1EnName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("match1EnName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.match1EnName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.match1EnName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("match1CnName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("match1CnName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.match1CnName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.match1CnName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("match1BedNo"))
        {	
	        java.lang.Object obj = soapObject.getProperty("match1BedNo");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.match1BedNo = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.match1BedNo = (String)obj;
            }    
        }
        if (soapObject.hasProperty("match1Class"))
        {	
	        java.lang.Object obj = soapObject.getProperty("match1Class");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.match1Class = Enums.motherBabyMatcherClass.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.motherBabyMatcherClass){
                this.match1Class = (Enums.motherBabyMatcherClass)obj;
            }    
        }
        if (soapObject.hasProperty("match2Id"))
        {	
	        java.lang.Object obj = soapObject.getProperty("match2Id");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.match2Id = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.match2Id = (String)obj;
            }    
        }
        if (soapObject.hasProperty("match2EnName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("match2EnName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.match2EnName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.match2EnName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("match2CnName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("match2CnName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.match2CnName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.match2CnName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("match2BedNo"))
        {	
	        java.lang.Object obj = soapObject.getProperty("match2BedNo");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.match2BedNo = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.match2BedNo = (String)obj;
            }    
        }
        if (soapObject.hasProperty("match2Class"))
        {	
	        java.lang.Object obj = soapObject.getProperty("match2Class");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.match2Class = Enums.motherBabyMatcherClass.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.motherBabyMatcherClass){
                this.match2Class = (Enums.motherBabyMatcherClass)obj;
            }    
        }
        if (soapObject.hasProperty("wardId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("wardId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.wardId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.wardId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("scanNum"))
        {	
	        java.lang.Object obj = soapObject.getProperty("scanNum");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.scanNum = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.scanNum = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("status"))
        {	
	        java.lang.Object obj = soapObject.getProperty("status");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.status = Enums.motherBabyMatcherStatus.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.motherBabyMatcherStatus){
                this.status = (Enums.motherBabyMatcherStatus)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return this.patientId!=null?this.patientId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==1)
        {
            return this.match1Id!=null?this.match1Id:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.match1EnName!=null?this.match1EnName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return this.match1CnName!=null?this.match1CnName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==4)
        {
            return this.match1BedNo!=null?this.match1BedNo:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return this.match1Class!=null?this.match1Class.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==6)
        {
            return this.match2Id!=null?this.match2Id:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==7)
        {
            return this.match2EnName!=null?this.match2EnName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==8)
        {
            return this.match2CnName!=null?this.match2CnName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==9)
        {
            return this.match2BedNo!=null?this.match2BedNo:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==10)
        {
            return this.match2Class!=null?this.match2Class.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==11)
        {
            return this.wardId!=null?this.wardId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==12)
        {
            return scanNum;
        }
        if(propertyIndex==13)
        {
            return this.status!=null?this.status.toString():SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 14;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "patientId";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "match1Id";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "match1EnName";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "match1CnName";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "match1BedNo";
            info.namespace= "";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "match1Class";
            info.namespace= "";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "match2Id";
            info.namespace= "";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "match2EnName";
            info.namespace= "";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "match2CnName";
            info.namespace= "";
        }
        if(propertyIndex==9)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "match2BedNo";
            info.namespace= "";
        }
        if(propertyIndex==10)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "match2Class";
            info.namespace= "";
        }
        if(propertyIndex==11)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "wardId";
            info.namespace= "";
        }
        if(propertyIndex==12)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "scanNum";
            info.namespace= "";
        }
        if(propertyIndex==13)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "status";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
