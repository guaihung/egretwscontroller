package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;
import java.util.ArrayList;
import org.ksoap2.serialization.PropertyInfo;

public class uploadPlanTimeLog extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public String freqCode;
    
    public String freqName;
    
    public String wardId;
    
    public ArrayList< org.joda.time.DateTime>orderRegularCycleTimes =new ArrayList<org.joda.time.DateTime>();
    
    public Boolean isManualSetPlanTime=false;
    
    public ArrayList< org.joda.time.DateTime>configRegularCycleTimes =new ArrayList<org.joda.time.DateTime>();
    
    public String timeExpression;
    
    public ArrayList< org.joda.time.DateTime>defaultTimePoints =new ArrayList<org.joda.time.DateTime>();

    public uploadPlanTimeLog ()
    {
    }

    public uploadPlanTimeLog (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("freqCode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("freqCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.freqCode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.freqCode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("freqName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("freqName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.freqName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.freqName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("wardId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("wardId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.wardId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.wardId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("orderRegularCycleTimes"))
        {	
	        int size = soapObject.getPropertyCount();
	        this.orderRegularCycleTimes = new ArrayList<org.joda.time.DateTime>();
	        for (int i0=0;i0< size;i0++)
	        {
	            PropertyInfo info=new PropertyInfo();
	            soapObject.getPropertyInfo(i0, info);
                java.lang.Object obj = info.getValue();
	            if (obj!=null && info.name.equals("orderRegularCycleTimes"))
	            {
                    java.lang.Object j =info.getValue();
	                org.joda.time.DateTime j1= Helper.ConvertFromWebService(j.toString());
	                this.orderRegularCycleTimes.add(j1);
	            }
	        }
        }
        if (soapObject.hasProperty("isManualSetPlanTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("isManualSetPlanTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.isManualSetPlanTime = new Boolean(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Boolean){
                this.isManualSetPlanTime = (Boolean)obj;
            }    
        }
        if (soapObject.hasProperty("configRegularCycleTimes"))
        {	
	        int size = soapObject.getPropertyCount();
	        this.configRegularCycleTimes = new ArrayList<org.joda.time.DateTime>();
	        for (int i0=0;i0< size;i0++)
	        {
	            PropertyInfo info=new PropertyInfo();
	            soapObject.getPropertyInfo(i0, info);
                java.lang.Object obj = info.getValue();
	            if (obj!=null && info.name.equals("configRegularCycleTimes"))
	            {
                    java.lang.Object j =info.getValue();
	                org.joda.time.DateTime j1= Helper.ConvertFromWebService(j.toString());
	                this.configRegularCycleTimes.add(j1);
	            }
	        }
        }
        if (soapObject.hasProperty("timeExpression"))
        {	
	        java.lang.Object obj = soapObject.getProperty("timeExpression");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.timeExpression = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.timeExpression = (String)obj;
            }    
        }
        if (soapObject.hasProperty("defaultTimePoints"))
        {	
	        int size = soapObject.getPropertyCount();
	        this.defaultTimePoints = new ArrayList<org.joda.time.DateTime>();
	        for (int i0=0;i0< size;i0++)
	        {
	            PropertyInfo info=new PropertyInfo();
	            soapObject.getPropertyInfo(i0, info);
                java.lang.Object obj = info.getValue();
	            if (obj!=null && info.name.equals("defaultTimePoints"))
	            {
                    java.lang.Object j =info.getValue();
	                org.joda.time.DateTime j1= Helper.ConvertFromWebService(j.toString());
	                this.defaultTimePoints.add(j1);
	            }
	        }
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return this.freqCode!=null?this.freqCode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==1)
        {
            return this.freqName!=null?this.freqName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.wardId!=null?this.wardId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return isManualSetPlanTime;
        }
        if(propertyIndex==4)
        {
            return this.timeExpression!=null?this.timeExpression:SoapPrimitive.NullSkip;
        }
        if(propertyIndex>=+5 && propertyIndex< + 5+this.orderRegularCycleTimes.size())
        {
            return this.orderRegularCycleTimes.get(propertyIndex-(+5));
        }
        if(propertyIndex>=+5+this.orderRegularCycleTimes.size() && propertyIndex< + 5+this.orderRegularCycleTimes.size()+this.configRegularCycleTimes.size())
        {
            return this.configRegularCycleTimes.get(propertyIndex-(+5+this.orderRegularCycleTimes.size()));
        }
        if(propertyIndex>=+5+this.orderRegularCycleTimes.size()+this.configRegularCycleTimes.size() && propertyIndex< + 5+this.orderRegularCycleTimes.size()+this.configRegularCycleTimes.size()+this.defaultTimePoints.size())
        {
            return this.defaultTimePoints.get(propertyIndex-(+5+this.orderRegularCycleTimes.size()+this.configRegularCycleTimes.size()));
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 5+orderRegularCycleTimes.size()+configRegularCycleTimes.size()+defaultTimePoints.size();
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "freqCode";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "freqName";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "wardId";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "isManualSetPlanTime";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "timeExpression";
            info.namespace= "";
        }
        if(propertyIndex>=+5 && propertyIndex <= +5+this.orderRegularCycleTimes.size())
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "orderRegularCycleTimes";
            info.namespace= "";
        }
        if(propertyIndex>=+5+this.orderRegularCycleTimes.size() && propertyIndex <= +5+this.orderRegularCycleTimes.size()+this.configRegularCycleTimes.size())
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "configRegularCycleTimes";
            info.namespace= "";
        }
        if(propertyIndex>=+5+this.orderRegularCycleTimes.size()+this.configRegularCycleTimes.size() && propertyIndex <= +5+this.orderRegularCycleTimes.size()+this.configRegularCycleTimes.size()+this.defaultTimePoints.size())
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "defaultTimePoints";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
