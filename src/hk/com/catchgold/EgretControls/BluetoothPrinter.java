package hk.com.catchgold.EgretControls;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.SystemClock;
import android.util.Log;

public class BluetoothPrinter {

	private String _printerName;
	private String _printerAddress;
	private String _printerPassword;
	private BluetoothDevice _device;
	private boolean _isFirstSearch = true;
	private boolean _isConnected = false;
	BluetoothSocket socket;

	// Steve comment: need update later
	// private RemoteDevice _printer;
	// private RemoteDevices _queryResult;
	// Bluetooth _bluetooth;

	// / <summary>
	// / 打印机是否连上
	// / </summary>
	public boolean getIsConnected() {
		return _isConnected;
	}

	private StringBuilder _commands = new StringBuilder();

	// / <summary>
	// /
	// / </summary>
	// / <param name="printerName">蓝牙打印机名称</param>
	// / <param name="printerAddress">蓝牙打印机地址</param>
	public BluetoothPrinter(String printerName, String printerAddress, String printerPassword, BluetoothDevice device) {
		_printerName = printerName;
		_printerAddress = printerAddress;
		_printerPassword = printerPassword;
		_device = device;
	}

	// / <summary>
	// / 链接打印机
	// / </summary>
	public void Connect() {
		Search();

		if (!_isConnected) {
			try {
				// BluetoothSocket socket =
				// device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
				// socket.connect();
				// socket.
				Method m = _device.getClass().getMethod("createRfcommSocket", new Class[] { int.class });
				socket = (BluetoothSocket) m.invoke(_device, Integer.valueOf(1));
				if (!socket.isConnected()){
					socket.connect();
				}

				// OutputStream os = socket.getOutputStream();
				// String BILL = "";
				//
				// BILL = "\nInvoice No: ABCDEF28060000005" + "    " +
				// "04-08-2011\n";
				// BILL = BILL + "-----------------------------------------";
				// BILL = BILL + "\n\n";
				// BILL = BILL + "Total Qty:" + "      " + "2.0\n";
				// BILL = BILL + "Total Value:" + "     " + "17625.0\n";
				// BILL = BILL + "-----------------------------------------\n";
				// os.write(BILL.getBytes());

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.d("EgretWS", "Steve comment: need implement BlueToothPrinter.Connect");

			// // System.Diagnostics.Debug.WriteLine("Pair Starting : " +
			// System.DateTime.Now.ToString() + "\r\n");
			// Log.d("EgretWS", "Pair Starting : " + DateTime.now().toString() +
			// "\r\n");
			// String deviceName = _printerAddress;
			// for (int i = 0; i < _queryResult.Length; i++)
			// {
			// if (_queryResult[i].Address == deviceName)
			// _printer = _queryResult[i];
			// }
			// String password = "123";
			// password = _printerPassword;
			// if (!_printer.IsPaired)
			// {
			// System.Threading.Thread.Sleep(500);
			// if (password.Equals(""))
			// {
			// _printer.Pair();
			// }
			// else
			// {
			// _printer.Pair(password);
			// }
			// }
			// _isConnected = true;
			// System.Diagnostics.Debug.WriteLine("Pair Ending : " +
			// System.DateTime.Now.ToString() + "\r\n");
		}

//		_commands = new StringBuilder();
	}

	// / <summary>
	// / 断开打印机
	// / </summary>
	public void Disconnect() {
		try {
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// ~BluetoothPrinter()
	// {
	// try
	// {
	// _bluetooth.Dispose();
	// }
	// catch
	// {
	// }
	// }

	// / <summary>
	// / 添加一条打印指令
	// / </summary>
	// / <param name="command"></param>
	public void AddCommand(String command) {
		if (_commands == null)
			_commands = new StringBuilder();
		Log.e("Alvin", "AddCommand: " + command); 
		_commands.append(command).append("\r\n");
	}

	// / <summary>
	// / 清除打印指令
	// / </summary>
	public void ClearCommand() {
		_commands = new StringBuilder();
	}

	// / <summary>
	// / 打印标签
	// / </summary>
	
	int retry_times = 0;
	int retry_max = 5;
	public void Print() throws Exception {
		Log.e("Mcs", "Print ");
		try {
			SystemClock.sleep(750);
			OutputStream os = socket.getOutputStream();
			os.write(_commands.toString().getBytes("ASCII"));
			Log.e("Alvin", "Print: " + _commands);
			// Log.d("EgretWS",
			// "Steve comment: need implement BlueToothPrinter.Print");
			// if (!_isConnected)
			// {
			// throw new Exception("Please establish a connection first!");
			// }
			// System.Diagnostics.Debug.WriteLine("Printer Starting : " +
			// System.DateTime.Now.ToString() + "\r\n");
			//
			// if (!_printer.IsComPortOpened)
			// {
			// System.Threading.Thread.Sleep(500);
			// _printer.OpenPort();
			// }
			// _printer.Write(Encoding.ASCII.GetBytes(_commands.ToString()));
			// _printer.ClosePort();
			// System.Diagnostics.Debug.WriteLine("Printer Ending : " +
			// System.DateTime.Now.ToString() + "\r\n");
		} catch (Exception e) {
			e.printStackTrace();
			if (retry_times < retry_max) {
				retry_times++;
				Log.e("Mcs", "BT Print retry = " + retry_times);
				Connect();
				SystemClock.sleep(1000);
				Print();
			} else {
				throw new Exception("Failed to connect the printer, please check!", e);
			}
		} finally {
			Log.e("Mcs", " BT Print finally");
			this.Disconnect();
			retry_times = 0;
		}
	}

	public void Print(boolean isReadyToPrint) throws Exception {
		Log.e("Mcs", "Print ");
		try {
			SystemClock.sleep(750);
			OutputStream os = socket.getOutputStream();
			os.write(_commands.toString().getBytes("ASCII"));
			Log.e("Alvin", "Print: " + _commands);
			isReadyToPrint = true;
			// Log.d("EgretWS",
			// "Steve comment: need implement BlueToothPrinter.Print");
			// if (!_isConnected)
			// {
			// throw new Exception("Please establish a connection first!");
			// }
			// System.Diagnostics.Debug.WriteLine("Printer Starting : " +
			// System.DateTime.Now.ToString() + "\r\n");
			//
			// if (!_printer.IsComPortOpened)
			// {
			// System.Threading.Thread.Sleep(500);
			// _printer.OpenPort();
			// }
			// _printer.Write(Encoding.ASCII.GetBytes(_commands.ToString()));
			// _printer.ClosePort();
			// System.Diagnostics.Debug.WriteLine("Printer Ending : " +
			// System.DateTime.Now.ToString() + "\r\n");
		} catch (Exception e) {
			e.printStackTrace();
			if (retry_times < retry_max) {
				retry_times++;
				Log.e("Mcs", "BT Print retry = " + retry_times);
				Connect();
				SystemClock.sleep(1000);
				Print();
			} else {
				isReadyToPrint = true;
				throw new Exception("Failed to connect the printer, please check!", e);
			}
		} finally {
			Log.e("Mcs", " BT Print finally");
			this.Disconnect();
			retry_times = 0;
		}
	}
	private void Search() {
		Log.d("EgretWS", "Steve comment: need implement BlueToothPrinter.Search");
		if (_isFirstSearch) {
			// Log.d("EgretWS",
			// "Steve comment: need implement BlueToothPrinter.Search");
			// System.Diagnostics.Debug.WriteLine("Search Starting : " +
			// System.DateTime.Now.ToString() + "\r\n");
			// //开始搜索蓝牙设备，具体表现为RemoteDevices属性
			// _bluetooth = new Bluetooth();
			// _bluetooth.RadioMode =
			// BTH_RADIO_MODE.BTH_DISCOVERABLE_AND_CONNECTABLE;
			// _queryResult = _bluetooth.RemoteDevices;
			// boolean finded = false;
			// for (int i = 0; i < _queryResult.Length; i++)
			// {
			// if (_queryResult[i].Address == _printerAddress)
			// {
			// finded = true;
			// break;
			// }
			// }
			// //如果蓝牙打印机没有开启或者其他原因导致没有搜索到，则把本手持机对应的打印机信息添加到远程设备中
			// if (!finded)
			// {
			// RemoteDevice remoteDevice = new RemoteDevice(_printerName,
			// _printerAddress, "");
			// _queryResult.Add(remoteDevice);
			// }
			// _isFirstSearch = false;
			// System.Diagnostics.Debug.WriteLine("Search Ending : " +
			// System.DateTime.Now.ToString() + "\r\n");
		}
	}
}
