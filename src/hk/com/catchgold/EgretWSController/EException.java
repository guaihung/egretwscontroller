package hk.com.catchgold.EgretWSController;

import java.io.Serializable;

public class EException extends Exception implements Serializable {
		/// <summary>
	    /// Steve Kuo @CatchGold 21-01-2015
	    /// </summary>
	    
	    private String _caption;
	    // private MessageBoxIcon _icon;
	    private Boolean _log;

	    //region Constructor

	    /// <summary>
	    /// Gary 2007-03-29
	    /// </summary>
	    // public EException() : this("�ݱ`", MessageBoxIcon.Hand, "")
	    public EException() 
	    { }

	    /// <summary>
	    /// Gary 2007-03-29
	    /// </summary>
	    /// <param name="message"></param>
	    // public EException(String message) : this("�ݱ`", MessageBoxIcon.Hand, message)
	    public EException(String message) 
	    {  
	    }
	    
	    public EException(String caption, String message) 
	    {  
	    }

	    /// <summary>
	    /// Gary 2007-03-29
	    /// </summary>
	    /// <param name="message"></param>
	    /// <param name="inner"></param>
	    // public EException(string message, Exception inner) : this("�ݱ`", MessageBoxIcon.Hand, message, false, inner)
	    public EException(String message, Exception inner) 
	    { }

	    /// <summary>
	    /// Gary 2007-03-29
	    /// </summary>
	    /// <param name="caption"></param>
	    /// <param name="icon"></param>
	    /// <param name="message"></param>
	    // public EException(string caption, MessageBoxIcon icon, string message) : base(message)
//	    {
//	        _caption = caption;
//	        _icon = icon;
//	    }

	    /// <summary>
	    /// Gary 2007-03-29
	    /// </summary>
	    /// <param name="caption"></param>
	    /// <param name="icon"></param>
	    /// <param name="message"></param>
//	    public EException(string caption, MessageBoxIcon icon, string message, bool log)
//	        : base(message)
//	    {
//	        _caption = caption;
//	        _icon = icon;
//	        _log = log;
//	    }

	    /// <summary>
	    /// Gary 2007-03-29
	    /// </summary>
	    /// <param name="caption"></param>
	    /// <param name="icon"></param>
	    /// <param name="message"></param>
	    /// <param name="inner"></param>
//	    public EException(string caption, MessageBoxIcon icon, string message, bool log, Exception inner)
//	        : base(message, inner)
//	    {
//	        _caption = caption;
//	        _icon = icon;
//	        _log = log;
//	    }

	    //endregion

	    /// <summary>
	    /// Gary 2007-03-29
	    /// </summary>
	    public String getCaption()
	    {
	         return _caption;
	     }

	    /// <summary>
	    /// Gary 2007-03-29
	    /// </summary>
//	    public MessageBoxIcon Icon
//	    {
//	        get
//	        {
//	            return _icon;
//	        }
//	    }

	    /// <summary>
	    /// Gary 2008-10-23
	    /// </summary>
	    public Boolean getLog()
	    {
	            return _log;
	    }
}
