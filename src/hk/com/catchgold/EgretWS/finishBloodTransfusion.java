package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;
import java.math.BigDecimal;

public class finishBloodTransfusion extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public String barcodeId;
    
    public WSUser finisher;
    
    public BigDecimal amountGiven;
    
    public String reason;
    
    public String cliCode;
    
    public Integer updateNo=0;

    public finishBloodTransfusion ()
    {
    }

    public finishBloodTransfusion (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("barcodeId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("barcodeId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.barcodeId = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.barcodeId = (String)obj;
            }    
        }
        if (soapObject.hasProperty("finisher"))
        {	
	        java.lang.Object j = soapObject.getProperty("finisher");
	        this.finisher = (WSUser)__envelope.get(j,WSUser.class);
        }
        if (soapObject.hasProperty("amountGiven"))
        {	
	        java.lang.Object obj = soapObject.getProperty("amountGiven");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.amountGiven = new BigDecimal(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof BigDecimal){
                this.amountGiven = (BigDecimal)obj;
            }    
        }
        if (soapObject.hasProperty("reason"))
        {	
	        java.lang.Object obj = soapObject.getProperty("reason");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.reason = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.reason = (String)obj;
            }    
        }
        if (soapObject.hasProperty("cliCode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("cliCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.cliCode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.cliCode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("updateNo"))
        {	
	        java.lang.Object obj = soapObject.getProperty("updateNo");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.updateNo = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.updateNo = (Integer)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return this.barcodeId!=null?this.barcodeId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==1)
        {
            return this.finisher!=null?this.finisher:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.amountGiven!=null?this.amountGiven.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return this.reason!=null?this.reason:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==4)
        {
            return this.cliCode!=null?this.cliCode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return updateNo;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 6;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "barcodeId";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = WSUser.class;
            info.name = "finisher";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "amountGiven";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "reason";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "cliCode";
            info.namespace= "";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "updateNo";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
