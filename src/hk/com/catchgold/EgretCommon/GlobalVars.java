package hk.com.catchgold.EgretCommon;

import android.bluetooth.BluetoothDevice;
import hk.com.catchgold.EgretCommon.GlobalType.GlobalSystemType;
import hk.com.catchgold.EgretCommon.Enums.StationType;
import hk.com.catchgold.EgretControls.BluetoothPrinter;
import hk.com.catchgold.EgretWS.WSSysRole;
import hk.com.catchgold.EgretWS.WSWardTeam;

public class GlobalVars {

	// Global Variables

	public static boolean isNoLabelToPrint = false;
	public static String RmkContentType = "";// 1.VacOT,2.VacSpecialMark
	public static boolean isFeedToInputIO = false;
	public static boolean isFeedToOutput = false;
	public static boolean isFeedToInputIOAndOutput = false;
	public static BluetoothPrinter _printer;
	public static boolean isFirstSearch = true;
	public static boolean isConnected = false;
	public static String BlueToothPrintName = "";
	public static String BlueToothMacAddress = "";
	public static String BlueToothPassword = "";

	// Edit By Zyq 2012-12-07

	// / <summary>
	// / 打印机类型
	// / </summary>
	public enum PrinterType {
		Zebra, Sewoo
	}

	// / <summary>
	// / 当前全局选定的打印机类型(默认为:Zebra,非蓝牙)
	// / </summary>
	public static PrinterType Printer = PrinterType.Zebra;
	// / <summary>
	// / 标志护士站的OfflinePrint（OfflinePrint有两种：化验室的与护士站NurseStation的）
	// / </summary>
	public static boolean NSOfflinePrintFlag = false;
	// / <summary>
	// / 系统中常用的分隔符
	// / </summary>
	public static char de = '§';
	// / <summary>
	// / 系统中常用的双分隔符
	// / </summary>
	public String sde = "§§";
	// / <summary>
	// / IVF单位
	// / </summary>
	public static String IvUnit = "ml";
	// / <summary>
	// / IVF临界值，大于50是输液，否则是打针
	// / </summary>
	public static int IvfLevel = 50;
	// / <summary>
	// / 移动打印机地址，每次登陆第一次打印操作时加载
	// / </summary>
	// public static IPEndPoint LabelPrinterAddress = null;
	public static String LabelPrinterAddress = null;
	public static int LabelPrinterPort = 0;

	// / <summary>
	// / 代表母液的处方代码
	// / </summary>
	public static String IvCode = "system";
	// / <summary>
	// / Hospira Pump上的Line标签
	// / </summary>
	public static String PumpLineHeader = "SP";
	// / <summary>
	// / EWELL标签上条码的条码类型代码，和HIS的标签区别开来，AD=Administration
	// / </summary>
	public static String EwellLabelHeader = "AD";
	// / <summary>
	// / EWELL标签上条码中，执行记录的ID所在节点的名称
	// / </summary>
	public static String EwellLabelIDKEY = "ID";
	// / <summary>
	// / 病人腕带前缀
	// / </summary>
	public static String WirstBandHeader = "<BT>WP</BT>";
	// / <summary>
	// / 血袋标签前缀
	// / </summary>
	public static String BloodPackLabelHeader = "<BT>TL</BT>";
	// / <summary>
	// / 血袋执行标签前缀
	// / </summary>
	public static String BloodExecLabelHeader = "<BT>BL</BT>";
	// / <summary>
	// / MEI标签前缀
	// / </summary>
	public static String XrayLabelHeader = "<BT>MEI</BT>";
	// / <summary>
	// / 登录LAB系统时，保存当前所选的病区
	// / </summary>
	public static String LabCurrentWardId = null;
	// / <summary>
	// / 打印机超时时间
	// / </summary>
	public static int PrinterTimeout = 10000;
	// / <summary>
	// / 表示查询所有分类Alert对应的代码
	// / </summary>
	public static GTAlertType AlertType_ALL = new GTAlertType(-1, "ALL");
	// / <summary>
	// / IO默认单位
	// / </summary>
	public static String IODefaultUnit = "ml";
	// / <summary>
	// / 当前组
	// / </summary>
	public static WSWardTeam CurrentTeam = new WSWardTeam();
	// / <summary>
	// / 当前角色
	// / </summary>
	public static WSSysRole CurrentRole = new WSSysRole();
	// / <summary>
	// / 当前工作站Id
	// / </summary>
	public static int StationId = 0;
	// / <summary>
	// / 当前工作站Code
	// / </summary>
	public static String StationCode = "";
	// / <summary>
	// / 当前工作站类型
	// / </summary>
	public static StationType StationType;
	// / <summary>
	// / 当前是否可以退出
	// / </summary>
	public static boolean IsEixt = false;
	// / <summary>
	// / 当前系统版本
	// / </summary>
	public static GlobalSystemType SystemType;
	// / <summary>
	// / Alert刷新间隔
	// / </summary>
	public static int AlertInterval = 120000;
	// / <summary>
	// / SpecialFlag = PLAN_TIME
	// / </summary>
	public static String S_PLANTIME = "PLAN_TIME";
	// / <summary>
	// / 是出院带药
	// / </summary>
	public String DISCHARGE_YES = "YES";
	// / <summary>
	// / 非出院带药
	// / </summary>
	public String DISCHARGE_NO = "NO";
	// / <summary>
	// / 客户端模板代码
	// / </summary>
	public static String TemplateCode_Intake = "OBS_INTAKE_001";
	public static String TemplateCode_Output = "OBS_OUTPUT_001";
	public static String TemplateCode_Event = "OBS_EVENT_001";

	public static String dosageV = "";
	public static boolean overMin = false;

	// / <summary>
	// / 当前病区的所有Team
	// / </summary>
	public static WSWardTeam[] teams;

	// / <summary>
	// / yunfei 获取dll的版本号
	// / </summary>
	public static String getSoftWareVersion() {
		return "Steve comment: need update code for function getSoftWareVersion()";
		//
		// XmlNode node = App.Config.GetNode("/egretPPC/app/localFiles");
		// List<String> versions = new List<String>();
		// foreach (XmlNode item in node.ChildNodes)
		// {
		// versions.Add(item.SelectSingleNode("version").InnerText);
		// }
		// versions.Sort(SortByVersionNumber);
		// return versions[0];
	}

	// / <summary>
	// / yunfei version从大到小排序
	// / </summary>
	// / <param name="lhs"></param>
	// / <param name="rhs"></param>
	// / <returns></returns>
	private static int SortByVersionNumber(String lhs, String rhs) {
		String[] strl = lhs.split(".");
		String[] strr = rhs.split(".");

		// String[] strl = lhs.Split('.');
		// String[] strr = rhs.Split('.');

		long length = Math.max(strl.length, strr.length);

		for (int i = 0; i < length; i++) {
			long l = 0;
			long r = 0;
			try {
				l = Long.parseLong(strl[i]);
				r = Long.parseLong(strr[i]);
			} catch (java.lang.Exception e) {
			}
			if (l < r) {
				return 1;
			} else if (l > r) {
				return -1;
			} else {
				continue;
			}
		}
		return 0;
	}

	// / <summary>
	// / 是否锁屏标志位
	// / </summary>
	public static boolean IsScreenLocked;

}
