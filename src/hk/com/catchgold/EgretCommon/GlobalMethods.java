package hk.com.catchgold.EgretCommon;

import hk.com.catchgold.EgretCommon.Enums.DFT;
import hk.com.catchgold.EgretGlobal.Global;
import hk.com.catchgold.EgretWS.Enums.orderRepeatStatus;
import hk.com.catchgold.EgretWS.Enums.printerType;
import hk.com.catchgold.EgretWS.Enums.requestDetailDescription;
import hk.com.catchgold.EgretWS.WSPrinterMapping;
import hk.com.catchgold.EgretWS.WSRequest;
import hk.com.catchgold.EgretWS.WSRequestDetail;
import hk.com.catchgold.EgretWS.WSSysRole;
import hk.com.catchgold.EgretWS.WSUserPermission;
import hk.com.catchgold.EgretWSController.EException;
import hk.com.catchgold.EgretWSController.WSController;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Pattern;

import javax.xml.parsers.*;

import org.joda.time.DateTime;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.graphics.Point;
import android.util.Base64;
import android.util.Log;

























import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;






public class GlobalMethods {
    // #region 类型判断
    public static boolean IsNurWard(String curWard)
    {
        //判断当前病房是否是Nursery病房
        boolean isNurWard = false;
        String[] nurWards = { "B", "D", "N", "S", "N3", "N5", "N9" };
        for (int i = 0; i < nurWards.length; i++)
        {
            if (curWard == nurWards[i])
                isNurWard = true;
        }
        return isNurWard;
    }

    /// <summary>
    /// Wade 2010-5-21 检查是否为整数
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static boolean IsInt(String str)
    {
    	return str.matches("-?\\d+"); 
        // return System.Text.RegularExpressions.Regex.IsMatch(str, "^-?[1-9]\\d*$");
    }

    public static boolean IsDecimal(String str)
    { 
        // return System.Text.RegularExpressions.Regex.IsMatch(str, @"^\d+?\d+(\.\d+)?$ ");
    	
    	  try  
    	  {  
    	    double d = Double.parseDouble(str);  
    	  }  
    	  catch(NumberFormatException nfe)  
    	  {  
    	    return false;  
    	  }  
    	  return true;  
    	  
    }

//    #endregion
//
//    #region 打印相关

    /// <summary>
    /// Wade 2010-1-9
    /// </summary>
    /// <returns></returns>
    public static String GetPrinterAddress(String device_name)  {
        //boolean setFlag = true;
        //try
        //{
        //    //读取打印机地址
        //    if (GlobalVars.LabelPrinterAddress == null)
        //    {
        //        WSPrinterMapping address = WSController.GetPrinterMapping(printerType.LABELPRINTER, Dns.GetHostName());
        //        if (address != null && !String.IsNullOrEmpty(address.printerId) && address.printerId.IndexOf(':') > -1)
        //        {
        //            String[] printInfo = address.printerId.Split(':');
        //            String ip = printInfo[0];
        //            int port = int.Parse(printInfo[1]);
        //            GlobalVars.LabelPrinterAddress = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(ip), port);
        //        }
        //        else
        //            setFlag = false;
        //    }
        //}
        //catch
        //{
        //    setFlag = false;
        //}

        boolean setFlag;
		try {
			setFlag = IsPrinterAddressAvailable(device_name);
			if (!setFlag){
				Log.e("Egret", "Printer address setting is wrong!Please check!"); 
				
				// throw new EException("Error", MessageBoxIcon.Hand, "Printer address setting is wrong!Please check!", false);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}

        return GlobalVars.LabelPrinterAddress;
    }

    public static boolean IsPrinterAddressAvailable(String device_name) throws Exception{
        boolean setFlag = true;
        try {
            //读取打印机地址
            if (GlobalVars.LabelPrinterAddress == null) {
                WSPrinterMapping address = WSController.GetPrinterMapping(printerType.LABELPRINTER, device_name);
                if (address != null && address.printerId != null && !address.printerId.trim().isEmpty() && address.printerId.indexOf(':') > -1) {
                    String[] printInfo = address.printerId.split(":");
                    String ip = printInfo[0];
                    int port = Integer.parseInt(printInfo[1]);
                    // GlobalVars.LabelPrinterAddress = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(ip), port);
                    GlobalVars.LabelPrinterAddress = ip;
                    GlobalVars.LabelPrinterPort = port; 
                } else {
                	setFlag = false;
                }
            }
        } catch (Exception ex) {
        	ex.printStackTrace();
            setFlag = false;
            throw ex;
        }

        return setFlag;
    }

    
//     public static void PrintOneCommand(TcpClient zebraClient, NetworkStream mynetworkstream, StreamWriter mystreamwriter, String command)
    public static void PrintOneCommand()
    {
    	Log.e("Egret", "GlobalMethods PrintOneCommand is still not implemented"); 
//    	
//    	if (zebraClient.Client.Connected == true)
//        {
//            mynetworkstream = zebraClient.GetStream();
//            mystreamwriter = new StreamWriter(mynetworkstream);
//            mystreamwriter.WriteLine(command.ToString());
//            mystreamwriter.Flush();
//        }
    }

//     public static void PrintBlankLabel(TcpClient zebraClient, NetworkStream mynetworkstream, StreamWriter mystreamwriter)
    public static void PrintBlankLabel()
    {
    	Log.e("Egret", "GlobalMethods PrintBlankLabel is still not implemented"); 
    	
    	
/*    	
        StringBuilder command = new StringBuilder();
        String end = "\r\n";
        command.Append("! 0 200 200 296 1" + end);
        command.Append("LABEL" + end);
        command.Append("CONTRAST 0" + end);
        command.Append("TONE 0" + end);
        command.Append("SPEED 3" + end);
        command.Append("PAGE-WIDTH 480" + end);
        command.Append("GAP-SENSE 25" + end);//25
        command.Append("SET-TOF 0" + end);
        command.Append(";// PAGE 0000000004800296" + end);
        command.Append("LINE 10 60 470 60 2");
        command.Append("LINE 10 120 470 120 2");
        command.Append("LINE 10 180 470 180 2");
        command.Append("LINE 10 240 470 240 2");
        command.Append("FORM" + end);
        command.Append("PRINT");

        PrintOneCommand(zebraClient, mynetworkstream, mystreamwriter, command.ToString());
*/        
    }

//    #endregion
//    #region 消息显示

    public static void ShowInfomation(String message)
    {
    	Log.e("Egret", "GlobalMethods ShowInfomation is still not implemented");
        // return MessageBox.Show(message, "INFO", 0, MessageBoxIcon.Asterisk, 0);
    }

    // public static DialogResult ShowWarning(String message)
    public static void ShowWarning(String message)
    {
    	Log.e("Egret", "GlobalMethods ShowWarning is still not implemented");
        // return MessageBox.Show(message, "INFO", 0, MessageBoxIcon.Exclamation, 0);
    }

//    public static DialogResult ShowQuestion(String message, MessageBoxButtons buttons)
//    {
//        return MessageBox.Show(message, "Warning", buttons, MessageBoxIcon.Question, 0);
//    }

    // public static DialogResult ShowQuestion(String message)
    public static void ShowQuestion(String message)
    {
    	Log.e("Egret", "GlobalMethods ShowQuestion is still not implemented");
        // return MessageBox.Show(message, "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question, 0);
    }

//    #endregion
//
//    #region 解析条码

    /// <summary>
    /// Wade 2009-12-21 根据条码节点，得到条码值
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    public static String GetBarcodeValue(String barcode, String node)
    {
        if (barcode.startsWith("<BT>WP</BT>"))
        {
            if (node.equals("BT"))
                return "WP";
            else
            {
                barcode = "<URN>" + Decrypt(barcode.substring(11), "testword") + "</URN>";
            }

        }
        else if (barcode.startsWith("<BT>" + GlobalVars.EwellLabelHeader + "</BT>"))
        {
            if (node.equals("BT"))
                return GlobalVars.EwellLabelHeader;
            else
            {
                String header = "<BT>" + GlobalVars.EwellLabelHeader + "</BT>";
                if (barcode.indexOf("<" + GlobalVars.EwellLabelIDKEY + ">") < 0)
                    barcode = "<" + GlobalVars.EwellLabelIDKEY + ">" + barcode.substring(header.length()) 
                        + "</" + GlobalVars.EwellLabelIDKEY + ">";
            }
        }
        else if (barcode.startsWith("<BT>VX</BT>"))
        {
            if (node.equals("BT"))
                return "VX";
            else
            {
                String header = "<BT>VX</BT>";
                return barcode.substring(header.length());
            }
        }
        else if (barcode.startsWith("<BT>MX</BT>"))
        {
            if (node.equals("BT"))
                return "MX";
            else
            {
                String header = "<BT>MX</BT>";
                return barcode.substring(header.length());
            }
        }

        String nodeText = "";
        try {
	        DocumentBuilderFactory documentBuildFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder doccumentBuilder = documentBuildFactory.newDocumentBuilder();
	        String xmlContent = "<root>" + barcode + "</root>"; 
	        Document document = doccumentBuilder.parse(new ByteArrayInputStream(xmlContent.getBytes()));

	        
	        Node xmlNode; 
	        if (node == null || node.isEmpty())
	        	xmlNode = document.getElementsByTagName("root").item(0);
	        else 
	        	xmlNode = document.getElementsByTagName(node).item(0);
	        
	        if (xmlNode != null && xmlNode.hasChildNodes())
	        	nodeText = xmlNode.getFirstChild().getNodeValue(); 
	        else 
	        	nodeText = xmlNode.getNodeValue();
        	
        	
        } catch (Exception ex)
        {
        	Log.e("Egret", "GetBarcodeValue: XML Node Parsing failed: " + ex.getMessage()); 
        	ex.printStackTrace();
        }
        
        
        // System.out.println(nodeText);
        
        
//        XmlDocument xmlDoc = new XmlDocument();
//        try
//        {
//            xmlDoc.LoadXml("<root>" + barcode + "</root>");
//        }
//        catch (Exception ex)
//        {
//            throw new Exception("Can't convert to xml file!");
//        }
//
//        XmlNode xmlNode;
//        if (!String.IsNullOrEmpty(node))
//            xmlNode = xmlDoc.SelectSingleNode("/root/" + node);
//        else
//            xmlNode = xmlDoc.SelectSingleNode("/root");
//
//        if (xmlNode == null)
//            throw new EException("Error", MessageBoxIcon.Hand, "Unkown bar-code, please check!", false);
//         return xmlNode.InnerText;
        
        return nodeText; 
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    
    /// <summary>
    /// Wade 2009-12-21 解密
    /// </summary>
    /// <param name="pToDecrypt"></param>
    /// <param name="sKey"></param>
    /// <returns></returns>
    public static String Decrypt(String pToDecrypt, String sKey)
    {
    	try 
    	{
/*    		
    		Log.e("Egret(GUAI)", pToDecrypt + " " + sKey); 
    		
            KeySpec myKey = new DESKeySpec(sKey.getBytes());
            SecretKey key = SecretKeyFactory.getInstance("DES").generateSecret(myKey);
            Cipher ecipher = Cipher.getInstance("DESEDE/ECB/PKCS5Padding");
            ecipher.init(Cipher.DECRYPT_MODE, key);

            byte[] data = pToDecrypt.getBytes("UTF8");

            byte[] crypt = ecipher.doFinal(data);

            String s = new String(Base64.encode(crypt, Base64.DEFAULT), "UTF-8");

            Log.e("Egret(GUAI)", "String " + s); 
            
            return s; 
*/    		
/*    		
    		
            String encryptionKey = sKey;
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(encryptionKey.getBytes("UTF-8"), 0, encryptionKey.length());
            byte[] encryptionKeyBytes = messageDigest.digest();

            SecretKeySpec Key = new SecretKeySpec(encryptionKeyBytes,"DESede");
            Cipher cipher = Cipher.getInstance("DESEDE/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, Key);
            // byte[] encryptedBytes = cipher.doFinal(Password.getBytes("UTF-8"));
            byte[] encryptedBytes = cipher.doFinal(pToDecrypt.getBytes("UTF-8"));

            // String PasswordCripto = new String(Base64.encode(encryptedBytes, Base64.DEFAULT), "UTF-8");
            String PasswordCripto = new String(encryptedBytes);
            
            Log.e("Egret(GUAI)", "YYYYYYZZZZZZ " + PasswordCripto);
            
            
            return PasswordCripto;
*/
    		
    		
            String myEncryptionKey = sKey;
            String myEncryptionScheme_2 = "DES";
            String myEncryptionScheme = "DES/CBC/PKCS5Padding";
            
            byte [] keyAsBytes = myEncryptionKey.getBytes("ASCII");
            KeySpec myKeySpec = new DESKeySpec(keyAsBytes);
            SecretKeyFactory mySecretKeyFactory = SecretKeyFactory.getInstance(myEncryptionScheme_2);
            Cipher cipher = Cipher.getInstance(myEncryptionScheme);
            SecretKey key = mySecretKeyFactory.generateSecret(myKeySpec);
            
            
            String decryptedText = null;
            
            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(keyAsBytes));
            //BASE64Decoder base64decoder = new BASE64Decoder();
            // byte[] encryptedText = pToDecrypt.getBytes();
            byte[] encryptedText = hexStringToByteArray(pToDecrypt);
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText = new String(plainText);

            return decryptedText;
            
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	return null; 
    	
    	
    	
//        DESCryptoServiceProvider des = new DESCryptoServiceProvider();
//
//        //Put the input String into the byte array 
//        byte[] inputByteArray = new byte[pToDecrypt.length / 2];
//        for (int x = 0; x < pToDecrypt.length / 2; x++)
//        {
//            int i = (Convert.ToInt32(pToDecrypt.substring(x * 2, 2), 16));
//            inputByteArray[x] = (byte)i;
//        }
//
//        //建立加密对象的密钥和偏移量，此值重要，不能修改 
//        des.Key = ASCIIEncoding.ASCII.GetBytes(sKey);
//        des.IV = ASCIIEncoding.ASCII.GetBytes(sKey);
//        MemoryStream ms = new MemoryStream();
//        CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
//        //Flush the data through the crypto stream into the memory stream 
//        cs.Write(inputByteArray, 0, inputByteArray.length);
//        cs.FlushFinalBlock();
//
//        //Get the decrypted data back from the memory stream 
//        //建立StringBuild对象，CreateDecrypt使用的是流对象，必须把解密后的文本变成流对象 
//        StringBuilder ret = new StringBuilder();
//
//        byte[] tmp = ms.ToArray();
//        return System.Text.Encoding.Default.GetString(tmp, 0, tmp.length);
    }
    
    /// <summary>
    /// Arthur 加密
    /// </summary>
    /// <param name="pToEncrypt"></param>
    /// <param name="sKey"></param>
    /// <returns></returns>
    public static String Encrypt(String pToEncrypt, String sKey)
    {
    	try 
    	{
            String myEncryptionKey = sKey;
            String myEncryptionScheme = "DES";
            byte [] keyAsBytes = myEncryptionKey.getBytes("UTF-8");
            KeySpec myKeySpec = new DESKeySpec(keyAsBytes);
            SecretKeyFactory mySecretKeyFactory = SecretKeyFactory.getInstance(myEncryptionScheme);
            Cipher cipher = Cipher.getInstance(myEncryptionScheme);
            SecretKey key = mySecretKeyFactory.generateSecret(myKeySpec);
    		
            cipher.init(Cipher.ENCRYPT_MODE, key);
            //BASE64Decoder base64decoder = new BASE64Decoder();
            byte[] eText = pToEncrypt.getBytes();
            byte[] encryptedText = cipher.doFinal(eText);
            String encryptedString = toHexString(encryptedText).toUpperCase();  
            
            return encryptedString;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	return null; 
    	
//        DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
//        byte[] bytes = Encoding.Default.GetBytes(pToEncrypt);
//        provider.Key = Encoding.ASCII.GetBytes(sKey);
//        provider.IV = Encoding.ASCII.GetBytes(sKey);
//        MemoryStream stream2 = new MemoryStream();
//        CryptoStream stream = new CryptoStream(stream2, provider.CreateEncryptor(), CryptoStreamMode.Write);
//        stream.Write(bytes, 0, bytes.length);
//        stream.FlushFinalBlock();
//        StringBuilder builder = new StringBuilder();
//        foreach (byte num in stream2.ToArray())
//        {
//            builder.AppendFormat("{0:X2}", num);
//        }
//        return builder.ToString();
    }
    
    public static String toHexString(byte b[]) {  
        StringBuffer hexString = new StringBuffer();  
        for (int i = 0; i < b.length; i++) {  
            String plainText = Integer.toHexString(0xff & b[i]);  
            if (plainText.length() < 2)  
                plainText = "0" + plainText;  
            hexString.append(plainText);  
        }  

        return hexString.toString();  
    } 
    
    public static boolean IsWristBand(String barcode)
    {
        return CheckLabelBand(barcode, GlobalVars.WirstBandHeader);
    }

    public static String GetWristBandId(String barcode)
    {
        return Decrypt(GetLabelBandId(barcode, GlobalVars.WirstBandHeader), "testword");
    }

    public static boolean IsBloodExecLabel(String barcode)
    {
        return CheckLabelBand(barcode, GlobalVars.BloodExecLabelHeader);
    }

    public static boolean IsSpecimenBarcode(String barcode)
    {
        return barcode.startsWith("<BT>SI</BT>");
    }

    public static String GetBloodExecLabelId(String barcode)
    {
        return GetLabelBandId(barcode, GlobalVars.BloodExecLabelHeader);
    }

    public static boolean IsBloodPackLabel(String barcode)
    {
        return CheckLabelBand(barcode, GlobalVars.BloodPackLabelHeader);
    }

    public static String GetBloodPackLabelWBN(String barcode)
    {
        return GetBarcodeValue(barcode, "WBN");
    }

    public static String GetBloodPackLabelPC(String barcode)
    {
        return GetBarcodeValue(barcode, "PC");
    }

    public static String GetBloodPackLabelURN(String barcode)
    {
        return GetBarcodeValue(barcode, "PT");
    }

    public static String GetBloodPackLabelTDS(String barcode)
    {
        if (barcode.contains("<TDS>"))
        {
            return GetBarcodeValue(barcode, "TDS");
        }
        else
        {
            return null;
        }
    }

    private static boolean CheckLabelBand(String barcode, String header)
    {
        if (barcode != null && header != null && barcode.startsWith(header))
            return true;
        else
            return false;
    }

    private static String GetLabelBandId(String barcode, String header)
    {
        if (barcode != null && header != null)
            return barcode.substring(header.length());
        else
            return "";
    }

    public static boolean IsXrayLabel(String barcode)
    {
        return CheckLabelBand(barcode, GlobalVars.XrayLabelHeader);
    }

    public static String GetXrayLabelURN(String barcode)
    {
        return GetBarcodeValue(barcode, "URN");
    }

    public static String GetXrayLabelXN(String barcode)
    {
        return GetBarcodeValue(barcode, "XN");
    }

    public static boolean IsCotLabel(String barcode)
    {
        return !CheckLabelBand(barcode, "<");
    }
//    #endregion
//
//    #region 日期时间

    /// <summary>
    /// Wade 2009-11-24 返回日期时间格式 0:年月日时分 1:月日时分   2:年月日时分秒 3:月日时分秒 
    ///                                  4:长日期格式 5:短日期格式 6:标签日期格式
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static String GetDateFmt(DFT type)
    {
        String format = "yyyy-MM-dd HH:mm";
        switch (type)
        {
            case NonSec:
                format = "dd/MM/yyyy HH:mm";
                break;
            case NonSec2:
                format = "dd/MM/yyyy HH:mm:00";
                break;
            case NonYearSec:
                format = "dd/MM HH:mm";
                break;
            case Full:
                format = "dd/MM/yyyy HH:mm:ss";
                break;
            case NonYear:
                format = "dd/MM HH:mm:ss";
                break;
            case LongDate:
                format = "dd/MM/yyyy";
                break;
            case ShortDate:
                format = "dd/MM/yy";
                break;
            case ShortDate2:
                format = "dd/MM/yy";
                break;
            case NonYearSecLabel:
                format = "HH:mm dd/MM";
                break;
            case NonSecShortYear:
                format = "dd/MM/yy HH:mm";
                break;
            case NonYearDate:
                format = "dd/MM";
                break;
        }
        return format;
    }

    // private static DateTimeFormatInfo _dtf = null;
    private static SimpleDateFormat _dtf = null;

    /// <summary>
    /// Wade 2009-12-12 获取日期格式
    /// </summary>
    /// <returns></returns>
//  public static DateTimeFormatInfo GetDateFormatInfo()
//  {
//      if (_dtf == null)
//      {
//          _dtf = new DateTimeFormatInfo();
//          _dtf.ShortDatePattern = "dd-MM-yy";
//      }
//      return _dtf;
//  }
    public static SimpleDateFormat GetDateFormatInfo()
    {
        if (_dtf == null)
        {
            _dtf = new SimpleDateFormat();
            _dtf.applyPattern("dd-MM-yy");
        }
        return _dtf;
    }

    /// <summary>
    /// Wade 2009-12-12 获取日期格式
    /// </summary>
    /// <returns></returns>
//    public static DateTimeFormatInfo GetDateFormatInfo(String dateFormat)
//    {
//        if (_dtf == null)
//        {
//            _dtf = new DateTimeFormatInfo();
//            _dtf.ShortDatePattern = dateFormat;
//        }
//        return _dtf;
//    }
    public static SimpleDateFormat GetDateFormatInfo(String dateFormat)
    {
        if (_dtf == null)
        {
            _dtf = new SimpleDateFormat();
            _dtf.applyPattern(dateFormat);
        }
        return _dtf;
    }    
    
    /// <summary>
    /// Wade 2010-3-11 将时间以特定格式显示 0:年月日时分 1:月日时分   2:年月日时分秒 3:月日时分秒 
    ///                                  4:长日期格式 5:短日期格式 
    /// </summary>
    /// <param name="dateTime"></param>
    /// <param name="specFlag"></param>
    /// <param name="formatType"></param>
    /// <returns></returns>
//    public static String GetDateStr(DateTime dateTime, boolean specFlag, DFT type)
//    {
//        if (specFlag)
//            return dateTime.ToString(GetDateFmt(type));
//        else
//            return "";
//    }
  public static String GetDateStr(DateTime dateTime, boolean specFlag, DFT type)
  {
      if (specFlag)
      {
    	  String dateFormatStr = GetDateFmt(type);
    	  SimpleDateFormat dateFormat = new SimpleDateFormat(); 
          // return dateTime.ToString(GetDateFmt(type));
    	  
    	  return dateFormat.format(dateTime.toDate()); 
      }
      else
          return "";
  }    
    

    /// <summary>
    /// GT 2011-3-31 传入两个日期时间beginTime和endTime，返回一个包含两个时间的数组，其中第一个时间为beginTime+00:00:00，第二个时间为endTime+23:59:59
    /// </summary>
    /// <param name="startTime">开始时间</param>
    /// <param name="endTime">结束时间</param>
    /// <returns>返回一个包含两个处理后的时间的数组</returns>
    public static DateTime[] GetDateAddTime(DateTime startTime, DateTime endTime)
    {
        DateTime[] dateTime = new DateTime[2];
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        dateTime[0] = new DateTime(startTime.getYear(), startTime.getMonthOfYear(), startTime.getDayOfMonth(), 0, 0, 0); 
        dateTime[1] = new DateTime(endTime.getYear(), endTime.getMonthOfYear(), endTime.getDayOfMonth(), 23, 59, 59);
        
//        dateTime[0] = Convert.ToDateTime(startTime.ToShortDateString() + " 00:00:00");
//        dateTime[1] = Convert.ToDateTime(endTime.ToShortDateString() + " 23:59:59");
        return dateTime;
    }
//    #endregion
//
//    #region 用户管理
    public static String GetWardId()
    {
        String wardId = "";
        switch (GlobalVars.StationType)
        {
            case DOCTOR:
                wardId = "DOCTOR";
                break;
            case PATHOLOGY:
                wardId = "LAB";
                break;
            default:
                break;
        }
        return wardId;
    }

    public static boolean GetAdminFlag() throws Exception{
        WSUserPermission permission = GlobalCache.GetUserSubSystemPermission(Global.User.code, GlobalVars.StationId);
        if (permission != null && permission.roles != null) {
        	for (int i=0; i<permission.roles.size(); i++){
        		WSSysRole role = permission.roles.get(i); 
	              if (role.departmet.code.equals(GlobalMethods.GetWardId())) {
	                  return role.adminFlag;
	              }
        	}
        	
//            foreach (WSSysRole role in permission.roles)
//            {
//                if (role.departmet != null)
//                {
//                    if (role.departmet.code == GlobalMethods.GetWardId())
//                    {
//                        return role.adminFlag;
//                    }
//                }
//            }
        }

        return false;
    }

//    #endregion
//
//    #region MEI工作站

    public static String GetRequestDetails(WSRequest request)
    {
        String content = "";
        if (request.details != null)
        {
        	for (int i=0; i<request.details.size(); i++)
        	{
        		WSRequestDetail detail = request.details.get(i); 
        		content += "-" + GlobalMethods.GetDateStr(detail.operationTime, false, DFT.NonSec) + "\r\n";
        		content += " " + GetRequestDetailDesc(detail.description, false) + " " + detail.operatorName + "\r\n\r\n";
        		
        	}
//            foreach (WSRequestDetail detail in request.details)
//            {
//                content += "-" + GlobalMethods.GetDateStr(detail.operationTime, detail.operationTimeSpecified, DFT.NonSec) + "\r\n";
//                content += " " + GetRequestDetailDesc(detail.description, detail.descriptionSpecified) + " " + detail.operatorName + "\r\n\r\n";
//            }
        }
        return content;
    }

    public static String GetRequestDetailDesc(requestDetailDescription desc, boolean descFlag)
    {
        if (descFlag)
        {
            switch (desc)
            {
                case CANCEL_REQUEST:
                    return "Cancelled by";
                case COUNTER_SIGN_REQUEST:
                    return "Checked by";
                case CREATE_REQUEST:
                    return "Created by";
                case MODIFY_REQEUST:
                    return "Modified by";
                case SET_REMINDER:
                    return "Set reminder by";
                default:
                    return "";
            }
        }
        else
            return "";
    }

//    #endregion
//
//    #region Time Expression解析
    /// <summary>
    /// 解析Time Expression转换成字符串
    /// </summary>
    /// <param name="expression"></param>
    /// <returns></returns>
    public static String ParseTimeExpression(String expression)
    {
        try
        {
            StringBuilder retStr = new StringBuilder();
            if (expression.startsWith("R"))
            {
                // String[] split1 = expression.TrimStart('R').Split('/');
            	String[] split1 = expression.substring(1).split("/");
                String[] split2 = split1[0].split(",");
                // foreach (var item in split2)
                for (int i=0; i<split2.length; i++)
                {
                	String item = split2[i]; 
                    if (item.contains("-"))
                    {
                        String[] split3 = item.split("-");
                        TimeStruct timePoint1 = ParseTimePoint(Integer.parseInt(split3[0]));
                        TimeStruct timePoint2 = ParseTimePoint(Integer.parseInt(split3[1]));
                        
                        String tp1Hour = "00" + Integer.toString(timePoint1.Hour); 
                        String tp1Min = "00" + Integer.toString(timePoint1.Min);
                        String tp2Hour = "00" + Integer.toString(timePoint2.Hour);
                        String tp2Min = "00" + Integer.toString(timePoint2.Min);
                        
                        tp1Hour = tp1Hour.substring(tp1Hour.length() - 2); 
                        tp1Min = tp1Min.substring(tp1Min.length() - 2);
                        tp2Hour = tp2Hour.substring(tp2Hour.length() - 2);
                        tp2Min = tp2Min.substring(tp2Min.length() - 2);

//                        TimeStruct timePoint1 = ParseTimePoint(int.Parse(split3[0]));
//                        TimeStruct timePoint2 = ParseTimePoint(int.Parse(split3[1]));
                        retStr.append(tp1Hour + tp1Min + " to " + tp2Hour + tp2Min + ", ");
                    }
                    else
                    {
                    	TimeStruct timePoint = ParseTimePoint(Integer.parseInt(item));
                    	
                        String tpHour = "00" + Integer.toString(timePoint.Hour); 
                        String tpMin = "00" + Integer.toString(timePoint.Min);
                        
                        tpHour = tpHour.substring(tpHour.length() - 2); 
                        tpMin = tpMin.substring(tpMin.length() - 2);

                        retStr.append(tpHour + tpMin + ", ");

                    	
//                        TimeStruct timePoint = ParseTimePoint(int.Parse(item));
//                        retStr.Append(timePoint.Hour.ToString("00") + timePoint.Min.ToString("00") + ", ");
                    }
                }
                
                // retStr.Remove(retStr.length() - 2, 2);
                return retStr.substring(0, retStr.length() - 2); 
            }
            else
            {
                return "";
            }
        }
        catch (Exception ex)
        {
            // App.ExceptionHandler(ex);
        	ex.printStackTrace();
        	Log.e("Egret", ex.getMessage()); 
            return "";
        }
    }

    /// <summary>
    /// 把分钟数表示成日、时、分格式
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    public static TimeStruct ParseTimePoint(int time)
    {
        TimeStruct ret = new TimeStruct();
        ret.Day = time / 1440;
        ret.Hour = (time - (time / 1440) * 1440) / 60;
        ret.Min = time - (time / 1440) * 1440 - (time - (time / 1440) * 1440) / 60 * 60;
        return ret;
    }
//    #endregion
//
//    #region 系统库扩展方法

    /// <summary>
    /// yunfei 2010-8-25 获取一个panel中获得焦点的Control对象
    /// </summary>
    /// <param name="pnl"></param>
    /// <returns></returns>
    
    public static void GetFocused()
    {
    	Log.e("Egret", "GetFocused: This function is not implemented, see Android how to get the focussed control");     	
    }
    
    
//    public static Control GetFocused(this Panel pnl)
//    {
//        foreach (Control obj in pnl.Controls)
//        {
//            if (obj.Focused == true)
//            {
//                return obj;
//            }
//            if (obj is Panel)
//            {
//                Control ret = GetFocused(obj as Panel);
//                if (ret != null)
//                {
//                    return ret;
//                }
//            }
//        }
//        return null;
//    }

    /// <summary>
    /// yunfei 2010-8-26计算Panel内容的总长度与宽度
    /// </summary>
    /// <param name="pnl"></param>
    /// <returns></returns>
    
    public static Point CalcWidthAndHeight()
    {
    	Log.e("Egret", "CalcWidthAndHeight: This function is not implemented, have to check what is this function for");     	
    	return null; 
    }
    
//    public static Point CalcWidthAndHeight(this Panel pnl)
//    {
//        Point bound = new Point(pnl.Bounds.Width, pnl.Bounds.Height);
//        foreach (Control obj in pnl.Controls)
//        {
//            if (obj.Location.X < 0)//在边界左边
//            {
//                if (System.Math.Abs(obj.Location.X) + pnl.Bounds.Width > bound.X)
//                {
//                    bound.X = System.Math.Abs(obj.Location.X) + pnl.Bounds.Width;
//                }
//            }
//            if (obj.Location.X + obj.Bounds.Width - pnl.Width + pnl.Bounds.Width > bound.X)//在边界右边
//            {
//                bound.X = obj.Location.X + obj.Bounds.Width - pnl.Width + pnl.Bounds.Width;
//            }
//            if (obj.Location.Y < 0)//在边界上面
//            {
//                if (System.Math.Abs(obj.Location.Y) + pnl.Bounds.Height > bound.Y)
//                {
//                    bound.Y = System.Math.Abs(obj.Location.Y) + pnl.Bounds.Height;
//                }
//            }
//            if (obj.Location.Y + obj.Bounds.Height - pnl.Height + pnl.Bounds.Height > bound.Y)//在边界下面
//            {
//                bound.Y = obj.Location.Y + obj.Bounds.Height - pnl.Height + pnl.Bounds.Height;
//            }
//        }
//        return bound;
//    }

    /// <summary>
    /// yunfei 2010-8-25 移动滚动条使得获得焦点的项显示在可视位置
    /// </summary>
    /// <param name="pnl"></param>
    public static void FocusedItemInScreen()
    {
    	Log.e("Egret", "FocusedItemInScreen: This function is not implemented, have to check what is this function for");
    }
    
//    public static void FocusedItemInScreen(this Panel pnl)
//    {
//        Control ctrl = pnl.GetFocused();
//        if (ctrl == null)
//        {
//            return;
//        }
//        int x = ctrl.Location.X;
//        int y = ctrl.Location.Y;
//        Control par = ctrl.Parent;
//
//        while (par != pnl)
//        {
//            x += par.Location.X;
//            y += par.Location.Y;
//            par = par.Parent;
//        }
//
//        Point moveDir = new Point();
//
//        Point maxWidthAndHeight = pnl.CalcWidthAndHeight();
//
//        //计算X轴方向应该移动的距离
//        if (x < 0)//X轴方向需要向右移动
//        {
//            moveDir.X = -(x * (pnl.Bounds.Width + 80) / maxWidthAndHeight.X + 1);//根据几何关系得出计算公式
//        }
//        else if ((x + ctrl.Bounds.Width) > pnl.Bounds.Width)//X轴方向需要向左移动
//        {
//            moveDir.X = (x - pnl.Bounds.Width + ctrl.Bounds.Width + 80) * (pnl.Bounds.Width) / maxWidthAndHeight.X + 1;
//        }
//
//        //计算Y轴方向应该移动的距离
//        if ((y < 0))//Y轴方向需要向下移动
//        {
//            moveDir.Y = -(y * (pnl.Bounds.Height + 80) / maxWidthAndHeight.Y + 1);
//        }
//        else if ((y + ctrl.Bounds.Height) > pnl.Bounds.Height)//Y轴方向需要向上移动
//        {
//            moveDir.Y = (y - pnl.Bounds.Height + ctrl.Bounds.Height) * (pnl.Bounds.Height + 80) / maxWidthAndHeight.Y + 1;
//        }
//
//        moveDir.X += pnl.AutoScrollPosition.X;
//        moveDir.Y += pnl.AutoScrollPosition.Y;
//
//        //移动滚动条
//        pnl.AutoScrollPosition = moveDir;
//    }

    /// <summary>
    /// 如果是-1返回/
    /// </summary>
    /// <param name="num"></param>
    /// <returns></returns>
    
    
    public static String GetValidString(int num)
    {
        return (num == -1 ? "/" : Integer.toString(num));
    }

    /// <summary>
    /// 通过枚举值获取特定格式的datetime字符串
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="dft"></param>
    /// <returns></returns>
    public static String ToFormattedString(DateTime dt, DFT dft)
    {
        // return dt.ToString(GetDateFmt(dft));
    	
    	SimpleDateFormat format = new SimpleDateFormat(GetDateFmt(dft)); 
    	String dtStr = format.format(dt.toDate());
    	
    	return dtStr; 
    	// return dt.toString(GetDateFmt(dft));
    }

    /// <summary>
    /// 一个字符串如果过长，则自动截去
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static String GetDisplayString(String source)
    {
        if (source.length() > 150)
        {
            source = source.substring(0, 150) + ".....";
            Log.d("Egret", "Hey, the string is trimmed to 150 characters"); 
        }

        return source;
    }

    /// <summary>
    /// 判断一个整数能否被另一个整数整除
    /// </summary>
    /// <param name="lhs"></param>
    /// <param name="rhs"></param>
    /// <returns></returns>
    public static boolean IsMultipleOf(int lhs, int rhs)
    {
        if (rhs == 0)
        {
            return false;
        }
        if (lhs - rhs * (lhs / rhs) == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
//    #endregion
//
//    #region 其他
    /// <summary>
    /// 根据start index，page size与total length算出页数
    /// </summary>
    /// <param name="startIndex"></param>
    /// <param name="pageSize"></param>
    /// <param name="totalLength"></param>
    /// <returns></returns>
    public static String GetPage(int startIndex, int pageSize, int totalLength)
    {
        return Integer.toString((int)(Math.ceil((((double)(startIndex + 1)) / ((double)pageSize))))) + "/" +
                        ((totalLength == 0) ? "1" : (Integer.toString((int)Math.ceil(((double)totalLength) / ((double)pageSize))))) + "(" + Integer.toString(totalLength) + ")";
    }

    /// <summary>
    /// 将orderRepeatStatus转换成字符串
    /// </summary>
    /// <param name="status"></param>
    /// <returns></returns>
    public static String GetRepeatStatusString(orderRepeatStatus status)
    {
        switch (status)
        {
            case NOTINREPEATING:
                return "";
                // break;
            case ROUTINEREPEAT:
                return "R";
                // break;
            case URGENTREPEAT:
                return "U";
                // break;
            case NEEDNOTREPEAT:
                return "X";
                // break;
            default:
                return "";
                // break;
        }
    }
    // #endregion

    public static double CalAge(DateTime birthday)
    {
        DateTime calToday = DateTime.now();
        DateTime calDob = birthday;
        if (calToday.compareTo(calDob) < 0)
            return 0;

        int year = calToday.getYear() - calDob.getYear();
        int month = year * 12 + calToday.getMonthOfYear() - calDob.getMonthOfYear();
        double days = calToday.getDayOfMonth() - calDob.getDayOfMonth();
        if (days < 0)
            month--;

        if (month >= 1)
        {
            return month;
        }
        else
        {
        	days = (long)(calToday.getMillis() - calDob.getMillis()) /  86400000; 
        			
            // days = calToday.Subtract(calDob).TotalMilliseconds / 86400000;
            if (days == 0)
                days++;
            
            
            
            Calendar monthCal = Calendar.getInstance();
            monthCal.set(Calendar.YEAR, calToday.getYear());
            monthCal.set(Calendar.MONTH, calToday.getMonthOfYear());
            monthCal.set(Calendar.DAY_OF_MONTH, 1);
            
 
            return days / monthCal.getActualMaximum(Calendar.DAY_OF_MONTH);
            
            // return days / DateTime.getDaysInMonth(calToday.getYear(), calToday.getMonthOfYear());
        }

        //if (birthday >= DateTime.Now)
        //{
        //    return 0;
        //}

        //TimeSpan age = DateTime.Now - birthday;
        //return age.TotalDays / 30;
    }

    public static String CalAgeString(DateTime birthday)
    {
        if (birthday == null)
            return "";

        DateTime calToday = DateTime.now();
        DateTime calDob = birthday;
        if(calToday.compareTo(calDob) < 0)
            return "";

        int year = calToday.getYear() - calDob.getYear();
        int month = year * 12 + calToday.getMonthOfYear() - calDob.getMonthOfYear();
        long days = calToday.getDayOfMonth() - calDob.getDayOfMonth();

        if (month > 12 || (month == 12 && days >= 0))
            return month / 12 + "y";
        if (days < 0)
            month--;
        //大于等于1个月并小于12个月的，按月计算，向下取整
        if (month >= 1)
            return month + "m";
        else
        {
            //不足1个月，按天计算，当天出生的，算1天
            // days = (long)calToday.Subtract(calDob).TotalMilliseconds / 86400000;
        	days = (long)(calToday.getMillis() - calDob.getMillis()) / 86400000;
            if (days == 0)
                days++;
            return days + "d";
        }
    }

    
    /// <summary>
    /// ztm 2011-2-28 根据设定长度返回包含长字符串行数的StringBuilder
    /// </summary>
    /// <param name="control"></param>
    /// <param name="longString"></param>
    /// <param name="builder"></param>
    /// <param name="font"></param>
    /// <param name="stringWidth"></param>
    /// <returns></returns>
    
//    public static StringBuilder GetLongStringAutoChangeLine(Control control, String longString, StringBuilder builder, Font font, int stringWidth)
//    {
//        using (Graphics g = control.CreateGraphics())
//        {
//            for (int i = 0, j = 0; i < longString.length; i++)
//            {
//                builder.Append(longString[i]);
//                if (g.MeasureString(longString.Substring(j, i - j + 1), font).Width >= stringWidth)
//                {
//                    builder.AppendLine();//超出，换行
//                    j = i + 1;
//                }
//            }
//        }
//        return builder;
//    }
    
    
    public static StringBuilder GetLongStringAutoChangeLine()
    {
    	Log.d("Egret", "Function GetLongStringAutoChangeLine in GlobalMethods is not implemented. This is a function to get a string with newline and calculated string, should be no use");
    	return null; 
//        using (Graphics g = control.CreateGraphics())
//        {
//            for (int i = 0, j = 0; i < longString.length; i++)
//            {
//                builder.Append(longString[i]);
//                if (g.MeasureString(longString.Substring(j, i - j + 1), font).Width >= stringWidth)
//                {
//                    builder.AppendLine();//超出，换行
//                    j = i + 1;
//                }
//            }
//        }
//        return builder;
    }
    /// <summary>
    /// Zyq 2012-12-20 蓝牙打印出现waiting界面
    /// </summary>
    /// <param name="control"></param>
    /// <param name="longString"></param>
    /// <param name="builder"></param>
    /// <param name="font"></param>
    /// <param name="stringWidth"></param>
    /// <returns></returns>
    
    // public static Panel _lblStatus = null;
//    public static void ShowPrintStautsBlueTooth(boolean isStart,Form _form)
//    {
//        if (_form == null)
//            return;
//
//        if (_lblStatus == null)
//        {
//            if (GlobalVars.SystemType == GlobalSystemType.WM61)
//            {
//                Label lblMsg = new Label();
//                lblMsg.ForeColor = System.Drawing.Color.Blue;
//                lblMsg.Location = new System.Drawing.Point(3, 19);
//                lblMsg.Name = "lblMsg";
//                lblMsg.Size = new System.Drawing.Size(166, 45);
//                lblMsg.Text = "Printing Label,                   please wait...";
//                lblMsg.TextAlign = System.Drawing.ContentAlignment.TopCenter;
//
//                _lblStatus = new Panel();
//                _lblStatus.BackColor = System.Drawing.SystemColors.Info;
//                _lblStatus.Controls.Add(lblMsg);
//                _lblStatus.Name = "pnlPrinterStatus";
//                _lblStatus.Size = new System.Drawing.Size(172, 100);
//                _lblStatus.Visible = false;
//                //_lblStatus.Paint += new PaintEventHandler(lblStatus_Paint);
//
//                int top = (_form.Height - _lblStatus.Height) / 2;
//                int left = (_form.Width - _lblStatus.Width) / 2;
//                _lblStatus.Location = new System.Drawing.Point(left, top);
//                _form.Controls.Add(_lblStatus);
//
//                _lblStatus.BringToFront();
//            }
//            else if (GlobalVars.SystemType == GlobalSystemType.WM65)
//            {
//                Label lblMsg = new Label();
//                lblMsg.ForeColor = System.Drawing.Color.Blue;
//                lblMsg.Location = new System.Drawing.Point(6, 38);
//                lblMsg.Name = "lblMsg";
//                lblMsg.Size = new System.Drawing.Size(332, 90);
//                lblMsg.Text = "Printing Label,                   please wait...";
//                lblMsg.TextAlign = System.Drawing.ContentAlignment.TopCenter;
//
//                _lblStatus = new Panel();
//                _lblStatus.BackColor = System.Drawing.SystemColors.Info;
//                _lblStatus.Controls.Add(lblMsg);
//                _lblStatus.Name = "pnlPrinterStatus";
//                _lblStatus.Size = new System.Drawing.Size(344, 200);
//                _lblStatus.Visible = false;
//                //_lblStatus.Paint += new PaintEventHandler(lblStatus_Paint);
//
//                int top = (_form.Height - _lblStatus.Height) / 2;
//                int left = (_form.Width - _lblStatus.Width) / 2;
//                _lblStatus.Location = new System.Drawing.Point(left, top);
//                _form.Controls.Add(_lblStatus);
//
//                _lblStatus.BringToFront();
//            }
//        }
//
//        if (isStart)
//            _lblStatus.Visible = true;
//        else
//        {
//            _lblStatus.Visible = false;
//            _lblStatus.Dispose();
//            _lblStatus = null;
//        }
//
//        _form.Update();
//    }
    public static void ShowPrintStautsBlueTooth()
    {
    	Log.d("Egret", "ShowPrintStautsBlueTooth is not implemented. See how to integrate ");
    }

    
    
    /// <summary>
    /// Zyq 2012-12-20 蓝牙打印机开启
    /// </summary>
    /// <param name="control"></param>
    /// <param name="longString"></param>
    /// <param name="builder"></param>
    /// <param name="font"></param>
    /// <param name="stringWidth"></param>
    /// <returns></returns>


}
