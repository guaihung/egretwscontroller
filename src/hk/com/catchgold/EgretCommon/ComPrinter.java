package hk.com.catchgold.EgretCommon;

import org.joda.time.DateTime;

import hk.com.catchgold.EgretWS.*;
import android.util.Log;

public class ComPrinter {

    private static ComPrinter cp = null;
    public ComPrinter() 
    {
        //if (!OpenPort())
        //{
        //    cp = null;
        //}
    }

    //private bool OpenPort()
    //{
    //    long lResult = LKPrint.OpenPort("COM1:", 38400);
    //    if (lResult != 0)
    //    {
    //        MessageBox.Show("Open Port Failed");
    //        return false;
    //    }
    //    return true;
    //}
    public static ComPrinter getInstance()
    {
        if (cp == null)
        {
            cp = new ComPrinter();
            return cp;
        }
        else
        {

            return cp;
        }
    }
    public void OnPrintBlankLabel()
    {
    	Log.e("Egret", "Steve comment: ComPrinter OnPrintBlankLabel is not implemented"); 
/*    	
        if (OnPrintCheck())
        {
            LKPrint.SetForm("0", 200, 200, "406", 1);
            LKPrint.SetFormMode();

            LKPrint.PrintLine(10, 60, 470, 60, 2);
            LKPrint.PrintLine(10, 120, 470, 120, 2);
            LKPrint.PrintLine(10, 180, 470, 180, 2);
            LKPrint.PrintLine(10, 240, 470, 240, 2);

            LKPrint.PrintForm();
        }
        
*/        
    }
    
    
    // public void OnPrintSampleLabel(WSNursingOrderAdminRecord record, WSPatientVisit pv, DateTime? endTime, bool isNew, Form parentForm)
    // public void OnPrintSampleLabel(WSNursingOrderAdminRecord record, WSPatientVisit pv, DateTime endTime, boolean isNew, Form parentForm)
    public void OnPrintSampleLabel(WSNursingOrderAdminRecord record, WSPatientVisit pv, DateTime endTime, boolean isNew)
    {
    	Log.e("Egret", "Steve comment: ComPrinter OnPrintSampleLabel is not implemented");
    	
//        if (OnPrintCheck())
//        {
//            LKPrint.SetForm("0", 200, 200, "406", 1);
//            LKPrint.SetFormMode();
//            LKPrint.CPCLPrintText(0, 7, 0, "0", "0", "QRCODE TEST", 0);
//            LKPrint.CPCL2DPrintBarCode(0, 1, "0", "36", 4, 0, 1, 0, "SEWOO TECH\r\nLK-P30");
//            LKPrint.PrintForm();
//        }
    }
    
    
    public void OnPrintOnLine(WSLabSpecimenCollectionDetail detail, WSPatientVisit pv)
    {
    	Log.e("Egret", "Steve comment: ComPrinter OnPrintOnLine is not implemented");
    	
//    	if (OnPrintCheck())
//        {
//            string strEncryptPatientId = GlobalMethods.Encrypt(pv.patient.id, "testword");
//            LKPrint.SetForm("0", 200, 200, "296", detail.labelPrintedCountThisTime);
//            LKPrint.SetTone(0);
//            LKPrint.SetSpeed(3);
//            LKPrint.SetPageWidth(400);
//            LKPrint.SetFormMode();
//
//            string patientName = pv.patient.enFirstName + " " + pv.patient.enLastName;
//            if (patientName.Length > 30)
//                patientName = patientName.Substring(0, 30);
//
//            LKPrint.CPCLPrintText(0, 7, 0, "11", "0", patientName, 0);
//
//            string patientInfo = pv.patient.anumber + " " + pv.age + " " + pv.patient.sex + " " + detail.instrumentCodes;
//
//
//            LKPrint.CPCLPrintText(0, 7, 0, "11", "26", patientInfo, 0);
//
//            LKPrint.CPCLPrintText(0, 7, 0, "11", "51", detail.info1, 0);
//            LKPrint.CPCLPrintText(0, 7, 0, "11", "76", detail.info2, 0);
//
//
//            LKPrint.CPCLPrintBarCode(0, 5, 1, 1, 56, "50", "101", detail.barcodeId, 0);
//            LKPrint.CPCLPrintText(0, 7, 0, "11", "161", detail.barcodeId + "  " + DateTime.Now.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSec))
//                + "  " + Global.User.code, 0);
//
//            LKPrint.PrintForm();
//            OnPrintBlankLabel();
//
//        }
    }
    public void OnPrintOffLine(WSPatientVisit pv, int copies, String staffId)
    {
    	Log.e("Egret", "Steve comment: ComPrinter OnPrintOffLine is not implemented");
    
//        if (OnPrintCheck())
//        {
//            string strEncryptPatientId = GlobalMethods.Encrypt(pv.patient.id, "testword");
//            LKPrint.SetForm("0", 200, 200, "296", copies);
//            LKPrint.SetTone(0);
//            LKPrint.SetSpeed(3);
//            LKPrint.SetPageWidth(400);
//            LKPrint.SetFormMode();
//
//            string patientName = pv.patient.enFirstName + " " + pv.patient.enLastName;
//            if (patientName.Length > 30)
//                patientName = patientName.Substring(0, 30);
//
//            string patientInfo = pv.patient.anumber + " " + pv.age + " " + pv.patient.sex + " " + pv.inpatientVisit.bedName;
//            if (GlobalVars.NSOfflinePrintFlag)
//            {
//                LKPrint.CPCLPrintText(0, 7, 0, "11", "0", "", 0);
//                LKPrint.CPCLPrintText(0, 7, 0, "11", "40", patientName, 0);
//                LKPrint.CPCLPrintText(0, 7, 0, "11", "65", patientInfo, 0);
//                LKPrint.CPCLPrintText(0, 7, 0, "11", "90", DateTime.Now.ToString(GlobalMethods.GetDateFmt(DFT.Full)) + " " + Global.User.code, 0);
//
//
//                string barcode = "<BT>OF</BT><U>" + strEncryptPatientId + "</U><O>" + staffId + "</O><T>" + DateTime.Now.ToString(GlobalMethods.GetDateFmt(DFT.NonSec)) + "</T>";
//                LKPrint.CPCL2DPrintBarCode(0, 1, "160", "140", 2, 0, 1, 3, barcode);
//
//            }
//            else
//            {
//                LKPrint.CPCLPrintText(0, 7, 0, "11", "0", patientName, 0);
//                LKPrint.CPCLPrintText(0, 7, 0, "11", "26", patientInfo, 0);
//                LKPrint.CPCLPrintText(0, 7, 0, "11", "51", DateTime.Now.ToString(GlobalMethods.GetDateFmt(DFT.Full)) + " " + Global.User.code, 0);
//
//                string barcode = "<BT>OF</BT><U>" + strEncryptPatientId + "</U><O>" + staffId + "</O><T>" + DateTime.Now.ToString(GlobalMethods.GetDateFmt(DFT.NonSec)) + "</T>";
//                LKPrint.CPCL2DPrintBarCode(0, 1, "140", "100", 2, 0, 1, 3, barcode);
//            }
//
//            LKPrint.PrintForm();
//            OnPrintBlankLabel();
//
//        }

    }

    public void OnPrintBloodTransfusion(WSNursingBloodTransfusionRecord record, WSPatientVisit pv)
    {
    	Log.e("Egret", "Steve comment: ComPrinter OnPrintBloodTransfusion is not implemented");
    	
//        if (OnPrintCheck())
//        {
//            LKPrint.SetForm("0", 200, 200, "296", 1);
//            LKPrint.SetTone(0);
//            LKPrint.SetSpeed(3);
//            LKPrint.SetPageWidth(480);
//            LKPrint.SetFormMode();
//
//            LKPrint.PrintLine(10, 60, 476, 60, 2);
//
//            LKPrint.CPCLPrintText(0, 4, 0, "21", "0", pv.inpatientVisit.bedName, 0);
//            LKPrint.CPCLPrintText(0, 5, 0, "149", "1", "Start at:", 0);
//            LKPrint.CPCLPrintText(0, 5, 0, "158", "28", "End at:", 0);
//            LKPrint.CPCLPrintText(0, 5, 0, "378", "3", record.frequency + " " + (record.status == bloodTransfusionStatus.PREPARE_CHECKED ? "*" : ""), 0);
//
//            if (record.rateSpecified && record.rate > 0)
//            {
//                string rate = record.rate.ToString("0") + "ml/hr";
//                if (rate.Length > 8)
//                    rate = rate.Substring(0, 8);
//                LKPrint.CPCLPrintText(0, 5, 0, "378", "32", rate, 0);
//            }
//            if (record.scheduledStartTimeSpecified)
//            {
//                LKPrint.CPCLPrintText(0, 5, 0, "238", "2", record.scheduledStartTime.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel)), 0);
//                if (record.duration != 0)
//                {
//                    LKPrint.CPCLPrintText(0, 5, 0, "238", "29", record.scheduledStartTime.AddMinutes(record.duration).ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel)), 0);
//                }
//            }
//
//            LKPrint.CPCLPrintText(0, 5, 0, "21", "69", record.bloodNature.ToString(), 0);
//            LKPrint.CPCLPrintText(0, 5, 0, "21", "94", record.wbn + " " + record.tds, 0);
//
//            string groupFactor = record.bloodGroup + " " + record.rhFactor;
//            if (groupFactor.Length > 20)
//            {
//                LKPrint.CPCLPrintText(0, 5, 0, "21", "119", record.bloodGroup, 0);
//                LKPrint.CPCLPrintText(0, 5, 0, "21", "144", record.rhFactor, 0);
//            }
//            else
//                LKPrint.CPCLPrintText(0, 5, 0, "21", "119", record.bloodGroup + " " + record.rhFactor, 0);
//
//
//
//            string barcode = "<BT>BL</BT>" + record.barcodeId;
//            LKPrint.CPCL2DPrintBarCode(0, 1, "360", "96", 3, 0, 1, 3, barcode);
//
//            LKPrint.PrintLine(10, 209, 476, 209, 2);
//            string prepareInfo = "Prepare by " + record.preparerCode + " @ ";
//            if (record.preparedTimeSpecified)
//                prepareInfo += record.preparedTime.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel));
//
//            LKPrint.CPCLPrintText(0, 5, 0, "13", "218", prepareInfo, 0);
//            LKPrint.PrintForm();
//
//            OnPrintBlankLabel();
//        }

    }

    // public void OnPrintWard(WSNursingOrderAdminRecord record, WSPatientVisit pv, List<WardPrintData> dataList, int fromIndex, int endIndex, DateTime? endTime, int labelCount, int labelIndex)
    public void OnPrintWard(WSNursingOrderAdminRecord record, WSPatientVisit pv, int fromIndex, int endIndex, DateTime endTime, int labelCount, int labelIndex)
    {
    	Log.e("Egret", "Steve comment: ComPrinter OnPrintWard is not implemented");
//        if (OnPrintCheck())
//        {
//            LKPrint.SetForm("0", 200, 200, "406", 1);
//            LKPrint.SetTone(0);
//            LKPrint.SetSpeed(3);
//            LKPrint.SetPageWidth(480);
//            LKPrint.SetFormMode();
//
//            LKPrint.PrintLine(10, 38, 480, 38, 2);
//            LKPrint.PrintLine(10, 163, 480, 163, 2);
//            LKPrint.CPCLPrintText(0, 4, 0, "16", "2", pv.inpatientVisit.bedName, 0);
//
//            bool isIV = false;
//
//            if (record.subRoute == "IV Infusion" || (record.subRoute == "Injection" && GlobalVars.overMin))
//            {
//                isIV = true;
//            }
//            if (isIV)
//            {
//                LKPrint.CPCLPrintText(0, 5, 0, "12", "168", (string.IsNullOrEmpty(record.subRoute) ? "" : record.subRoute.ToString()) + " " + GlobalVars.dosageV + "ml", 0);
//                string freqRate = record.flowInfo.ivfDurationDesc;//include overMin
//                if (record.flowInfo.ivfRateSpecified && record.flowInfo.ivfRate > 0)
//                {
//                    string rate = record.flowInfo.ivfRate.ToString("0") + "ml/hr";
//                    if (rate.Length > 8)
//                        rate = rate.Substring(0, 8);
//                    freqRate += " " + rate;
//                }
//                LKPrint.CPCLPrintText(0, 5, 0, "12", "198", freqRate, 0);
//                LKPrint.CPCLPrintText(0, 5, 2, "310", "170", (record.status == orderAdministrationStatus.PREPAREDANDCHECKED ? "*" : ""), 0);
//                LKPrint.CPCLPrintText(0, 5, 0, "340", "178", labelIndex.ToString() + "/" + labelCount.ToString(), 0);
//                LKPrint.CPCLPrintText(0, 5, 0, "12", "218", "Schedule at:", 0);
//                string startTime = (record.executePlanTimeSpecified ? record.executePlanTime.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel)) : "");
//                LKPrint.CPCLPrintText(0, 5, 0, "150", "220", startTime, 0);// (record.executePlanTimeSpecified ? record.executePlanTime.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel)))
//                //--------------------------------------
//                LKPrint.CPCLPrintText(0, 5, 0, "63", "238", "End at:", 0);
//                LKPrint.CPCLPrintText(0, 5, 0, "150", "239", (endTime != null ? endTime.Value.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel)) : ""), 0);
//            }
//            else
//            {
//                LKPrint.CPCLPrintText(0, 5, 0, "12", "178", (string.IsNullOrEmpty(record.subRoute) ? "" : record.subRoute.ToString()), 0);
//                string freqRate = record.flowInfo.ivfDurationDesc;
//                if (record.flowInfo.ivfRateSpecified && record.flowInfo.ivfRate > 0)
//                {
//                    string rate = record.flowInfo.ivfRate.ToString("0") + "ml/hr";
//                    if (rate.Length > 8)
//                        rate = rate.Substring(0, 8);
//                    freqRate += " " + rate;
//                }
//                LKPrint.CPCLPrintText(0, 5, 0, "150", "178", freqRate, 0);
//                LKPrint.CPCLPrintText(0, 5, 0, "310", "170", (record.status == orderAdministrationStatus.PREPAREDANDCHECKED ? "*" : ""), 0);
//                LKPrint.CPCLPrintText(0, 5, 0, "330", "178", labelIndex.ToString() + "/" + labelCount.ToString(), 0);
//                LKPrint.CPCLPrintText(0, 5, 0, "12", "201", "Schedule at:", 0);
//                LKPrint.CPCLPrintText(0, 5, 0, "150", "203", (record.executePlanTimeSpecified ? record.executePlanTime.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel)) : ""), 0);
//                //--------------------------------------
//                string patientName = pv.patient.enFirstName + " " + pv.patient.enLastName;
//                if (patientName.Length > 20)
//                    patientName = patientName.Substring(0, 20);
//                LKPrint.CPCLPrintText(0, 5, 0, "12", "229", patientName, 0);
//            }
//
//            string mixInfo = "Prepare by ";
//            if (record.mixer != null)
//                mixInfo += record.mixer.code + " ";
//            if (record.mixTimeSpecified)
//                mixInfo += "@ " + record.mixTime.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel));
//            LKPrint.CPCLPrintText(0, 5, 0, "141", "18", mixInfo, 0);
//
//
//            if (GlobalVars.Printer == GlobalVars.PrinterType.Sewoo)
//            {
//                string barcode = "<BT>AD</BT>" + record.barcodeId;
//                LKPrint.CPCL2DPrintBarCode(0, 1, "380", "165", 3, 0, 1, 3, barcode);
//            }
//
//
//            //添加药物信息
//            //行高
//            int h = 20;
//            int y = 50;
//            for (int i = fromIndex; i <= endIndex; i++)
//            {
//                WardPrintData printData = dataList[i];
//                LKPrint.CPCLPrintText(0, 5, 0, "12", y.ToString(), printData.nameArray[0].ToString(), 0);
//                LKPrint.CPCLPrintText(0, 5, 0, "358", y.ToString(), printData.dosage, 0);
//                y += h;
//
//                for (int nameIndex = 1; nameIndex < printData.nameArray.Count; nameIndex++)
//                {
//                    LKPrint.CPCLPrintText(0, 5, 0, "12", y.ToString(), printData.nameArray[nameIndex].ToString(), 0);
//                    y += h;
//                }
//
//                LKPrint.CPCLPrintText(0, 5, 0, "12", y.ToString(), printData.route, 0);
//                y += h;
//            }
//            LKPrint.PrintForm();
//        }
  
    }

    // public void OnPrintVac(String curCategory, WSNursingOrderAdminRecord record, WSPatientVisit pv, List<EgretPPC.Common.WardPrintData> dataList, int fromIndex, int endIndex, DateTime? endTime, int labelCount, int labelIndex)
    public void OnPrintVac(String curCategory, WSNursingOrderAdminRecord record, WSPatientVisit pv, int fromIndex, int endIndex, DateTime endTime, int labelCount, int labelIndex)
    {
     
    	Log.e("Egret", "Steve comment: ComPrinter OnPrintVac is not implemented");
    	
//    	
//        if (OnPrintCheck())
//        {
//            LKPrint.SetForm("0", 200, 200, "296", 1);
//            LKPrint.SetTone(0);
//            LKPrint.SetSpeed(3);
//            LKPrint.SetPageWidth(480);
//            LKPrint.SetFormMode();
//
//
//            LKPrint.PrintLine(10, 39, 478, 39, 2);
//            LKPrint.PrintLine(10, 163, 480, 163, 2);
//            LKPrint.CPCLPrintText(0, 4, 0, "16", "0", pv.inpatientVisit.bedName, 0);
//
//            #region 标签底部内容
//            LKPrint.CPCLPrintText(0, 5, 0, "12", "168", "IMI  " + curCategory, 0);
//            LKPrint.CPCLPrintText(0, 5, 2, "310", "160", (record.status == orderAdministrationStatus.PREPAREDANDCHECKED ? "*" : ""), 0);
//            LKPrint.CPCLPrintText(0, 5, 0, "340", "168", labelIndex.ToString() + "/" + labelCount.ToString(), 0);
//            LKPrint.CPCLPrintText(0, 5, 0, "12", "208", "Schedule at:", 0);
//            LKPrint.CPCLPrintText(0, 5, 0, "150", "210", (record.executePlanTimeSpecified ? record.executePlanTime.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel)) : ""), 0);
//
//            //--------------------------------------
//            string patientName = pv.patient.enFirstName + " " + pv.patient.enLastName;
//            if (patientName.Length > 20)
//                patientName = patientName.Substring(0, 20);
//            LKPrint.CPCLPrintText(0, 5, 0, "12", "232", patientName, 0);
//
//            #endregion
//
//            string mixInfo = "Prepare by ";
//            if (record.mixer != null)
//                mixInfo += record.mixer.code + " ";
//            if (record.mixTimeSpecified)
//                mixInfo += "@ " + record.mixTime.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel));
//            LKPrint.CPCLPrintText(0, 5, 0, "141", "8", mixInfo, 0);
//
//            string barcode = "<BT>VX</BT>" + record.barcodeId;
//            LKPrint.CPCL2DPrintBarCode(0, 1, "380", "165", 3, 0, 1, 3, barcode);
//
//            //添加药物信息
//            //行高
//            int h = 20;
//            int y = 50;
//            for (int i = fromIndex; i <= endIndex; i++)
//            {
//                WardPrintData printData = dataList[i];
//                LKPrint.CPCLPrintText(0, 5, 0, "12", y.ToString(), printData.nameArray[0].ToString(), 0);
//                LKPrint.CPCLPrintText(0, 5, 0, "358", y.ToString(), printData.dosage, 0);
//                y += h;
//
//                for (int nameIndex = 1; nameIndex < printData.nameArray.Count; nameIndex++)
//                {
//                    LKPrint.CPCLPrintText(0, 5, 0, "12", y.ToString(), printData.nameArray[nameIndex].ToString(), 0);
//                    y += h;
//                }
//
//                LKPrint.CPCLPrintText(0, 5, 0, "12", y.ToString(), printData.route, 0);
//                y += h;
//            }
//            LKPrint.PrintForm();
//        }
//      
    }
    public void OnPrintMilk(WSNurseryFeedPreparedRecord record, WSPatientVisit pv)
    {
    	
    	Log.e("Egret", "Steve comment: ComPrinter OnPrintMilk is not implemented");
    	
//    	
//        
//        if (OnPrintCheck())
//        {
//            LKPrint.SetForm("0", 200, 200, "406", 1);
//            LKPrint.SetTone(0);
//            LKPrint.SetSpeed(3);
//            LKPrint.SetPageWidth(480);
//            LKPrint.SetFormMode();
//
//            LKPrint.PrintLine(10, 53, 476, 53, 2);
//
//            #region 顶部
//            LKPrint.CPCLPrintText(0, 4, 0, "10", "4", pv.inpatientVisit.bedName, 0);
//
//            string patientName = pv.patient.enFirstName + " " + pv.patient.enLastName;
//            if (patientName.Length > 20)
//                patientName = patientName.Substring(0, 20);
//            LKPrint.CPCLPrintText(0, 5, 0, "139", "16", patientName, 0);
//            #endregion
//            #region 中间
//            LKPrint.CPCLPrintText(0, 5, 0, "21", "63", record.feedOrder.brandName, 0);
//            LKPrint.CPCLPrintText(0, 5, 0, "21", "94", record.volume + record.unit, 0);
//            LKPrint.CPCLPrintText(0, 4, 0, "135", "115", record.remark, 0);
//            string planTime = record.planTimeSpecified ? record.planTime.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel)) : "";
//            LKPrint.CPCLPrintText(0, 5, 1, "21", "170", "Schedule at:" + planTime, 0);
//            //二维码
//            string barcode = "<BT>MX</BT>" + record.barcode;
//            LKPrint.CPCL2DPrintBarCode(0, 1, "360", "115", 3, 0, 1, 3, barcode);
//
//            #endregion
//            #region 底部
//            LKPrint.PrintLine(10, 210, 476, 210, 2);
//
//            string prepareInfo = "Prepare by " + record.preparerCode + " @ ";
//            if (record.preparedTimeSpecified)
//                prepareInfo += record.preparedTime.ToString(GlobalMethods.GetDateFmt(DFT.NonYearSecLabel));
//            LKPrint.CPCLPrintText(0, 5, 0, "15", "220", prepareInfo, 0);
//            #endregion
//            LKPrint.PrintForm();
//        }
//   
    }
    public boolean OnOpenPort()
    {
//        long lResult = LKPrint.OpenPort("COM1:", 38400);
//        if (lResult != 0)
//        {
//            MessageBox.Show("Open Port Failed");
//            return false;
//        }
        return true;
    }
    public void OnClosePort()
    {
//        long lResult = LKPrint.ClosePort();
//        if (lResult != 0)
//        {
//            MessageBox.Show("Close Port Failed!!!");
//            return;
//        }
    }
    private boolean OnPrintCheck()
    {
//        long lResult;
//        int lState;
//
//        lResult = LKPrint.PrinterCheck();
//        if (lResult.Equals(-1) || lResult.Equals(-2))
//        {
//            switch (lResult)
//            {
//                case -1: // Bluetooth Write Fail. Please Check Connection.
//                   // MessageBox.Show("Write Error");
//                    break;
//                case -2:
//                    MessageBox.Show("Fail to Read Printer Status");
//                    break;
//            }
//        }
//        lResult = LKPrint.status();
//
//        if ((lResult & LKPrint.LK_STS_BUSY) == LKPrint.LK_STS_BUSY)
//        {
//            //MessageBox.Show("Printer is busy");
//            return true;
//        }
//
//        if ((lResult & LKPrint.LK_STS_PAPER_EMPTY) == LKPrint.LK_STS_PAPER_EMPTY)
//        {
//            MessageBox.Show("Paper is empty");
//            return false;
//        }
//
//        if ((lResult & LKPrint.LK_STS_COVER_OPEN) == LKPrint.LK_STS_COVER_OPEN)
//        {
//            MessageBox.Show("Cover is opened");
//            return false;
//        }
//
//        if ((lResult & LKPrint.LK_STS_BATTERY_LOW) == LKPrint.LK_STS_BATTERY_LOW)
//        {
//            MessageBox.Show("Printer's battery is low");
//            return false;
//        }
//
//        if ((lResult & 0x0f) != 0x00)
//        {
//            return false;
//        }

        return true;
    
    }


}
