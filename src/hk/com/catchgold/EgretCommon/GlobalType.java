package hk.com.catchgold.EgretCommon;





public class GlobalType {

//    Steve comment: this structure is removed
//    public enum GlobalSystemType
//    {
//        WM61,
//        WM65
//    }
	
	
	public enum GlobalSystemType
	{
		HERO2_Version1; 
	}
	
	public enum ExceptionType
	{
	    //用药异常
	    DRUGGIVEN,
	    DRUGNOTGIVEN,
	    //DRUGNOTGIVEN_KEEP,
	    //DRUGNOTGIVEN_DISCARD,
	
	    //OBS异常
	    OBSSUCCESS,
	    OBSEXCEPTIONUNFINISH,
	    OBSEXCEPTIONFINISH,
	    //FEED异常
	    FEEDSUCCESS,
	    FEEDEXCEPTIONUNFINISH,
	    FEEDEXCEPTIONFINISH,
	
	    //LAB异常
	    LABSUCCESS,
	    LABEXCEPTIONUNFINISH,
	    LABEXCEPTIONFINISH,
	
	    //Bedside异常
	    BEDSUCCESS,
	    BEDEXCEPTIONUNFINISH,
	    BEDEXCEPTIONFINISH,
	    BEDPRNFINISH,
	    BEDPRNEXCEPTIONUNFINISH,
	    BEDPRNEXCEPTIONFINISH
	}
	
	
	
	
	public enum DoctorType
	{ 
	    Attn,
	    Cons,
	    Unknown
	}
	
	public enum MeiReqStatus
	{ 
	    New,
	    Sent,
	    Confirmed,
	    Cancelled,
	    Completed
	}
	
	public enum EpxDrugStatus
	{ 
	    New(10), Dispensed(15), Stop(20), Sent(30), Repeat(40); 
	    int type;
	    EpxDrugStatus(int p) {
	       type = p;
	    }
	    int showStatus() {
	       return type;
	    }
	}
	
	public enum HANDLER_MESSAGE
	{
		ALERT_ALARMMESSAGE_CLICK(101), OFFDRUG_ALARMMESSAGE_Click(102);
		
	    int msg;
	    HANDLER_MESSAGE(int p) {
	    	msg = p;
	    }
	    int showMessage() {
	       return msg;
	    }
	}


	
}
