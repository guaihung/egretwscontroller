package hk.com.catchgold.EgretWSController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.joda.time.DateTime;
import org.ksoap2.serialization.KvmSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.provider.MediaStore.Files;
import android.util.Log;
import hk.com.catchgold.EgretWS.*;
import hk.com.catchgold.EgretWS.Enums.*;

import java.lang.Exception;
import java.lang.reflect.Array;

public class WSController {

	private static EgretWSBinding _egretService;
	public static int _updateNo = -1;
	public static String _cliCode = "EgretPPC";
	private static int _retryTimes = 1;
	// private static int m_timeout = 18000;

	// Wade 2010-1-14 测试AP切换时 需要添加WEB方法访问情况的LOG
	public static String _cliInfo = "";
	public static String _userCode = "";
	private static String _directory = "";
	private static Date _lastUploadTime = new Date();
	// private static DateTime _lastUploadTime ;

	// private static DateTime _lastUploadTime = DateTime.;
	// private static int _uploadInterval = 30;
	private static String _logFileName = "WebRequest";
	// private static boolean _logIsEmpty = true;
	private static Boolean m_log = null;

	static {
		try {
			if (_egretService == null)
				_egretService = new EgretWSBinding();
		} catch (java.lang.Exception ex) {
			ex.printStackTrace();
		}

	}

	public static void Init(String url) throws Exception {
		if (_egretService == null)
			_egretService = new EgretWSBinding();
		_egretService.SetURLIP(url);
		InitSystemInfo();
		_updateNo = GetUpdateNo();
		// try {
		// if (_egretService == null)
		// _egretService = new EgretWSBinding();
		// _egretService.SetURLIP(url);
		// } catch (java.lang.Exception ex) {
		// ex.printStackTrace();
		// }
		//
		// InitSystemInfo();
		//
		// try {
		// // 获取应用程序基本信息
		// _updateNo = GetUpdateNo();
		// } catch (java.lang.Exception ex) {
		// ex.printStackTrace();
		// }
	}

	private static void InitSystemInfo() {
		try {

			// 获取客户端本地信息，方便日志的跟踪定位
			String strHostName = InetAddress.getLocalHost().getHostName(); // 得到本机的主机名
			String strAddr = InetAddress.getLocalHost().getHostAddress(); // 取得本机IP
			// String strAddr = ipEntry.AddressList[0].ToString();
			_cliInfo = strAddr + ": " + strHostName;
		} catch (java.lang.Exception ex) {
			ex.printStackTrace();
		}
		// 获取应用程序的名称及当前路径
		// Android :
		// context.getApplicationInfo().loadLabel(context.getPackageManager());
		// AssemblyName asmName = Assembly.GetExecutingAssembly().GetName();
		// String appName = asmName.Name;
		// _directory = asmName.CodeBase.Substring(0, asmName.CodeBase.Length -
		// appName.Length - 5);

		// Android need get running directory
		_directory = System.getProperty("user.dir");

	}

	private static int GetUpdateNo() throws java.lang.Exception {
		int ret = 0;
		try {
			getUpdateNo request = new getUpdateNo();
			request.cliCode = _cliCode;

			for (int i = 0; i < _retryTimes && ret == 0; i++) {
				try {
					ret = _egretService.getUpdateNo(_cliCode);
				} catch (java.lang.Exception ex) {
						throw ex;
				}
			}
		} catch (java.lang.Exception ex) {
			throw ex;
		}

		return ret;
	}

	public static int getUpdateNo() {
		return _updateNo;
	}

	public static void setUpdateNo(int UpdateNo) {
		_updateNo = UpdateNo;
	}

	// region Toolkit
	// / <summary>
	// / Steve 2015-01-21
	// / </summary>
	private static Exception MakeWSException(java.lang.Exception ex) {
		return ex;
		/*
		 * // if (ex is System.Net.WebException) if (ex instanceof
		 * java.net.SocketException) { // return new
		 * egret.Exception("network exception", MessageBoxIcon.Exclamation,
		 * "Fail to connect the server, please check the network ！", true, ex);
		 * return new Exception("Network Exception",
		 * "Fail to connect the server, please check the network ！"); } // else
		 * if (ex is SoapException) else if (ex instanceof org.ksoap2.SoapFault)
		 * { // SoapException sex = (SoapException)ex; // if (sex.Detail != null
		 * && sex.Detail.FirstChild != null // &&
		 * sex.Detail.FirstChild.LocalName != null &&
		 * sex.Detail.FirstChild.LocalName.Equals("WSMessage"))
		 * 
		 * // javax.xml.soap.SOAPException sex =
		 * (javax.xml.soap.SOAPException)ex; // if (sex.getMessage() != null) //
		 * { // //// int level = int.Parse(sex.Message.Substring(0, 1)); ////
		 * String errmsg = sex.Message.Substring(1); //// switch (level) //// {
		 * //// case 0: //// return new Exception("INFO",
		 * MessageBoxIcon.Asterisk, errmsg); //// case 1: //// return new
		 * Exception("Warnning", MessageBoxIcon.Exclamation, errmsg); ////
		 * default: //// return new Exception("Error", MessageBoxIcon.Hand,
		 * errmsg); //// } // String errmsg = sex.getMessage(); // return new
		 * Exception("Error", errmsg); // } // else { // return new
		 * Exception("Server error", MessageBoxIcon.Hand,
		 * "Server error, please retry later or contact the system admin！");
		 * return new Exception("Server error",
		 * "Server error, please retry later or contact the system admin！"); } }
		 * else { // return ex; return new Exception(); }
		 */
	}

	// / <summary>
	// / Gary 2010-10-30
	// / </summary>
	// / <param name="level"></param>
	// / <param name="message"></param>
	public static void LogToFile(WSLogLevel level, String message) {
		// 获取应用程序的名称及当前路径
		// AssemblyName asmName = Assembly.GetExecutingAssembly().GetName();
		// String appName = asmName.Name;
		// String directory = asmName.CodeBase.Substring(0,
		// asmName.CodeBase.Length - appName.Length - 5);

		// Android need get running directory
		_directory = System.getProperty("user.dir");

		// 添加错误日志
		// StreamWriter sw = null;
		String fileName = "";
		FileOutputStream sw = null;
		File file = null;
		try {
			// sw = File.AppendText(directory + "\\" + _cliCode + ".log");
			fileName = _directory + "\\" + _cliCode + ".log";
			file = new File(fileName);
			sw = new FileOutputStream(file, true);

			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (java.lang.Exception ex) {
			// Log.e("Failed to open log file " + fileName + " for writing");
			System.out.println("Failed to open log file " + fileName + " for writing");
		}
		/*
		 * catch (UnauthorizedAccessException) { MessageBox.Show(
		 * "Authorization denied, unable to create the local log file!\r\n Please contact the system admin."
		 * , "System Error", 0, MessageBoxIcon.Hand, 0); } catch
		 * (PathTooLongException) { MessageBox.Show(
		 * "Log dir or file name too long, unable to create! \r\n Please change the dir to install the system or contact the system admin."
		 * , "System Error", 0, MessageBoxIcon.Hand, 0); } catch (Exception e) {
		 * MessageBox.Show("Unable to create the log file! \r\n" + e.Message,
		 * "System Error", 0, MessageBoxIcon.Hand, 0); }
		 */
		if (sw != null) {
			try {

				// sw.Write((int)level);
				// sw.Write(DateTime.Now.ToString(" yyyy-MM-dd HH:mm:ss"));
				// sw.Write("           ");
				//
				// int length = message.Length;
				// if (length > 2000)
				// {
				// length = 2000;
				// message = message.Substring(0, 2000);
				// }
				// sw.Write(length.ToString().PadLeft(4));
				// sw.WriteLine(" " + message);
				//

				String content = level.toString() + " ";
				content += new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "           ";
				content += message + "\n";

				byte[] contentInBytes = content.getBytes();

				sw.write(contentInBytes);
				sw.flush();
				sw.close();
			} catch (java.lang.Exception e) {
				// MessageBox.Show("Unable to write the log file!\r\n" +
				// e.Message, "System Error", 0, MessageBoxIcon.Hand, 0);
				// Log.e("Unable to write the log file!\r\n" + e.getMessage());
				System.out.println("Unable to write the log file!\r\n" + e.getMessage());

			} finally {
				// sw.close();
			}
		}
	}

	// / <summary>
	// / Gary 2010-10-30
	// / </summary>
	// / <param name="level"></param>
	// / <param name="message"></param>

	public static void LogToFileWebRequest(String flag, String requestName, int ticks) {
		// Android need get running directory
		_directory = System.getProperty("user.dir");

		String fileName = "";
		FileOutputStream sw = null;
		File file = null;
		try {
			// sw = File.AppendText(directory + "\\" + _cliCode + ".log");
			fileName = _directory + "\\" + _cliCode + "Web.log";
			file = new File(fileName);
			sw = new FileOutputStream(file, true);

			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (java.lang.Exception ex) {
			// Log.e("Failed to open log file " + fileName + " for writing");
			System.out.println("Failed to open log file " + fileName + " for writing");
		}

		if (sw != null) {
			try {
				String content = flag.toString() + "|";
				content += new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "|";
				content += requestName + "|";
				content += ticks + "\n";

				byte[] contentInBytes = content.getBytes();

				sw.write(contentInBytes);
				sw.flush();
				sw.close();
			} catch (java.lang.Exception e) {
				// MessageBox.Show("Unable to write the log file!\r\n" +
				// e.Message, "System Error", 0, MessageBoxIcon.Hand, 0);
				// Log.e("Unable to write the log file!\r\n" + e.getMessage());
				System.out.println("Unable to write the log file!\r\n" + e.getMessage());

			} finally {
				// sw.close();
			}
		}
	}

	// endregion

	// region Reset Network

	// 空闲2小时就重置网络,尝试解决连接数过多的问题
	// private static int alertTicks = System.Environment.TickCount;
	// private static int resetTicks = 120 * 60 * 1000; //超过2小时重置网络
	// private static int resetTicks = 5000; //超过2小时重置网络
	private static void CheckWirelessAdapter(String functionName) {
		if (functionName != "hasUnreadAlerts") {
			// alertTicks = System.Environment.TickCount;
		} else {
			// Steve comment: need check why reset network every 2 hours
			// Steve comment: need check how to reset the network
			/*
			 * int nowTicks = System.Environment.TickCount; if (nowTicks -
			 * alertTicks > resetTicks) { try {
			 * AutoLocalLog("--Wireless Reset--"); Cursor.Current =
			 * Cursors.WaitCursor;
			 * SetWirelessAdapterStatus(Symbol.Fusion.WLAN.Adapter
			 * .PowerStates.OFF); System.Threading.Thread.Sleep(2000); } finally
			 * {
			 * SetWirelessAdapterStatus(Symbol.Fusion.WLAN.Adapter.PowerStates.
			 * ON); alertTicks = System.Environment.TickCount; Cursor.Current =
			 * Cursors.Default; } }
			 */
		}
	}

	/*
	 * // Steve comment: private static void
	 * SetWirelessAdapterStatus(Symbol.Fusion.WLAN.Adapter.PowerStates status) {
	 * Symbol.Fusion.WLAN.WLAN wlan = new
	 * Symbol.Fusion.WLAN.WLAN(Symbol.Fusion.FusionAccessType.COMMAND_MODE);
	 * Symbol.Fusion.WLAN.Adapter adapter = wlan.Adapters[0]; adapter.PowerState
	 * = status; }
	 */
	// #endregion
	// / <summary>
	// / Wade 2009-10-7 如果发生网络异常需要重试
	// / </summary>
	// / <param name="retryTimes"></param>
	// / <param name="interval"></param>
	// / <param name="throwIfFail"></param>
	// / <param name="function"></param>
	/*
	 * private static void Retry(int retryTimes, Object request,
	 * NoArgumentHandler function) throws Exception { try { if (m_log == null) {
	 * // AssemblyName asmName = Assembly.GetExecutingAssembly().GetName(); //
	 * String appName = asmName.Name; // String directory =
	 * asmName.CodeBase.Substring(0, asmName.CodeBase.Length - appName.Length -
	 * 5); // String configFileName = directory + "\\" + _cliCode + ".xml"; //
	 * XmlDocument xml = new XmlDocument();
	 * 
	 * // Steve comment: Android need get running directory _directory =
	 * System.getProperty("user.dir"); String configFileName = _directory + "\\"
	 * + _cliCode + ".xml"; // XmlDocument xml = new XmlDocument();
	 * 
	 * 
	 * DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	 * DocumentBuilder docBuilder = docFactory.newDocumentBuilder(); Document
	 * doc = docBuilder.parse (new File(configFileName));
	 * 
	 * // xml.Load(configFileName);
	 * 
	 * m_log = false; try { // if
	 * (xml.SelectSingleNode("/egretPPC/app/logToFile").InnerText == "1") //
	 * m_log = true; NodeList listOfNodes =
	 * doc.getElementsByTagName("/egretPPC/app/logToFile");
	 * 
	 * if (listOfNodes != null && listOfNodes.getLength() > 0) {
	 * org.w3c.dom.Element firstElement =
	 * (org.w3c.dom.Element)listOfNodes.item(0); if
	 * (firstElement.getNodeValue().toString() == "1") m_log = true; } } catch
	 * (java.lang.Exception ex) { Element rootElement =
	 * doc.createElement("egretPPC"); doc.appendChild(rootElement); Element
	 * appElement = doc.createElement("app");
	 * rootElement.appendChild(appElement);
	 * rootElement.setAttribute("logToFile", "0");
	 * 
	 * 
	 * // write the content into xml file TransformerFactory transformerFactory
	 * = TransformerFactory.newInstance(); Transformer transformer =
	 * transformerFactory.newTransformer(); DOMSource source = new
	 * DOMSource(doc); StreamResult result = new StreamResult(new
	 * File(configFileName)); } // catch (Exception ex) // { // } } } catch
	 * (java.lang.Exception ex) { m_log = false; }
	 * 
	 * if (m_log == false)//找不到这个节点或者为0表示不打log { if (function != null) { for
	 * (int i = 0; i < retryTimes; ++i) { try { if (i == 0)
	 * _egretService.SetTimeout(18000); else _egretService.SetTimeout(20000);
	 * 
	 * // Steve comment: retry by logging and call the function with wireless
	 * connectivity function.TheFunction(); //
	 * AutoLocalLog(request.GetType().Name);//Record Log // function(); //
	 * CheckWirelessAdapter(request.GetType().Name); break; } catch (Exception
	 * wex) { if (i == retryTimes - 1) { wex.printStackTrace(); throw wex; }
	 * else { } } } } } else//不为0表示打log { int ticks = 0;
	 * 
	 * for (int i = 1; i <= retryTimes; i++) { try { if (i == 0)
	 * _egretService.SetTimeout(18000); else _egretService.SetTimeout(20000);
	 * 
	 * // Steve comment: retry by logging and call the function with wireless
	 * connectivity function.TheFunction(); //
	 * AutoLocalLog(request.GetType().Name);//Record Log // ticks =
	 * Environment.TickCount; // function(); // ticks = Environment.TickCount -
	 * ticks; // LogToFileWebRequest("-", request.GetType().Name, ticks); //
	 * CheckWirelessAdapter(request.GetType().Name); break; } catch (Exception
	 * wex) { if (wex instanceof java.net.SocketTimeoutException) { // if (i ==
	 * 3) // { // ticks = Environment.TickCount - ticks; //
	 * LogToFileWebRequest("*", request.GetType().Name, ticks); // throw new
	 * Exception("Warnning", MessageBoxIcon.Exclamation,
	 * "Connection timeout! Please try again."); // } // else // { // ticks =
	 * Environment.TickCount - ticks; // LogToFileWebRequest("+",
	 * request.GetType().Name, ticks); // } if (i == 3) throw new
	 * Exception("Warnning", "Connection timeout! Please try again."); } else
	 * throw wex; } } } }
	 */
	// / <summary>
	// / Steve 2015-01-23
	// / </summary>
	// / <param name="filepath"></param>
	// / <returns></returns>
	public static boolean CheckIfEndWithLinebreak() {
		try {
			String fileName = _directory + "\\" + _logFileName + ".log";

			File f = new File(fileName);
			String message = "";
			if (f.exists() && !f.isDirectory()) {
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((message = br.readLine()) != null) {
					// process the line.
				}
				br.close();

				if (message != "" && message.contains("\r\n"))
					return true;
				else
					return false;
			} else
				return true;
		} catch (Exception ex) {
			// MessageBox.Show("Unable to read the log file!" + ex.Message,
			// "Error", 0, MessageBoxIcon.Hand, 0);
			ex.printStackTrace();
		}

		return false;
	}

	public static void LogToServer() {
		try {
			// 上传日志
			String logFileName = _directory + "\\" + _logFileName + ".log";
			// const int Max_Length = 4000;
			int Max_Length = 4000;

			File f = new File(logFileName);
			String message = "";
			ArrayList recordList = new ArrayList();
			if (f.exists() && !f.isDirectory()) {
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((message = br.readLine()) != null) {
					WSLogRecord record = new WSLogRecord();
					record.cliCode = _cliCode;
					record.cliInfo = _cliInfo;

					if (message == null || message.trim() == "")
						continue;

					record.logLevel = WSLogLevel.INFO;

					if (message.length() < 19) {
						record.logTime = DateTime.now();
						record.message = message;
					} else {
						try {
							record.logTime = DateTime.parse(message.substring(0, 19));
						} catch (Exception ex) {
							record.logTime = DateTime.now();
						}
						record.message = message.substring(19);

						// ztm 2011.10.21 添加长度限制为4000
						if (record.message.length() > Max_Length)
							record.message = record.message.substring(0, Max_Length);
					}

					record.userCode = _userCode;
					recordList.add(record);
				}
				br.close();
				// WSLogRecord[] logRecords =
				// (WSLogRecord[])recordList.toArray();

				try {
					WSController.UploadLogRecords(recordList);
				} catch (Exception ex) {
					// 下一次上传
					ex.printStackTrace();
				}

				f.delete();
			}
		} catch (Exception ex) {
			// MessageBox.Show(ex.Message);
			ex.printStackTrace();
		}
	}

	public static void LogToFile(String message, boolean isEnd) {
		// 添加错误日志
		// StreamWriter sw = null;
		String fileName = "";
		FileOutputStream sw = null;
		File file = null;
		try {
			// sw = File.AppendText(directory + "\\" + _cliCode + ".log");
			fileName = _directory + "\\" + _logFileName + ".log";
			file = new File(fileName);
			sw = new FileOutputStream(file, true);

			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (java.lang.Exception ex) {
			// Log.e("Failed to open log file " + fileName + " for writing");
			System.out.println("Failed to open log file " + fileName + " for writing");
		}

		boolean isEndWithBreak = CheckIfEndWithLinebreak();

		if (sw != null) {

			try {
				String content = "";
				if (isEnd)
					content = message;
				else {
					if (!isEndWithBreak)
						content = "\r\n" + message;
					else
						content = message;
				}

				byte[] contentInBytes = content.getBytes();

				sw.write(contentInBytes);
				sw.flush();
				sw.close();
			} catch (java.lang.Exception e) {
				// MessageBox.Show("Unable to write the log file!\r\n" +
				// e.Message, "System Error", 0, MessageBoxIcon.Hand, 0);
				// Log.e("Unable to write the log file!\r\n" + e.getMessage());
				System.out.println("Unable to write the log file!\r\n" + e.getMessage());

			} finally {
				// sw.close();
			}
		}
	}

	// / <summary>
	// / 根据WardId获取该病区的Login和Logout的设置时间
	// / </summary>
	// / <param name="wardId"></param>
	// / <returns></returns>
	public static WSLogTime GetLogTimeByWard(String wardId) throws Exception {
		try {
			getLogTimeByWard request = new getLogTimeByWard();
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLogTime logTime = null;

			for (int i = 0; i < _retryTimes && logTime == null; i++) {
				try {
					logTime = _egretService.getLogTimeByWard(request.wardId, request.cliCode, request.updateNo);
				} catch (Exception ex) {
						throw MakeWSException(ex);
				}
			}

			return logTime;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// endregion

	// region BaseInfo
	// / <summary>
	// / Gary 2009-07-20 上传日志的不RETRY，减少断网后的等待时间
	// / </summary>
	public static void UploadLogRecord(WSLogRecord logRecord) throws Exception {
		try {
			uploadLogRecord request = new uploadLogRecord();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.items = logRecord;
			// Retry(_retryTimes, request, delegate
			// {
			_egretService.SetTimeout(5000);
			// _egretService.uploadLogRecordsAsync(request);
			_egretService.uploadLogRecord(logRecord, _cliCode, _updateNo);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// public static void UploadLogRecords(WSLogRecord[] logRecords)
	public static void UploadLogRecords(ArrayList<WSLogRecord> logRecords) throws Exception {
		try {
			uploadLogRecords request = new uploadLogRecords();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.items = logRecords;
			// Retry(_retryTimes, request, delegate
			// {
			_egretService.SetTimeout(5000);
			_egretService.uploadLogRecords(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Gary 2009-08-09
	// / </summary>
	// / <param name="userCode"></param>
	// / <param name="password"></param>
	// / <returns></returns>
	public static WSUser Login(String userCode, String password) throws Exception {
		// try {
		// _egretService = new EgretWSBinding();

		login request = new login();
		WSUser user = null;
		for (int i = 0; i < _retryTimes && user == null; i++) {
			try {
				user = _egretService.login(userCode, password, _cliCode, _updateNo);
			} catch (Exception ex) {
					throw MakeWSException(ex);
			}
		}

		// request.userCode = userCode;
		// request.password = password;
		// request.cliCode = _cliCode;
		// request.updateNo = _updateNo;
		// WSUser user = null;
		// Retry(_retryTimes, request, delegate
		// {
		// user = _egretService.login(request).@return;
		// });

		if (user == null)// || user.id == null)
			return null;
		else
			return user;
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// throw MakeWSException(ex);
		// }
	}

	// / <summary>
	// / Wade 2009-8-22 获取指定病区和小组床位列表
	// / </summary>
	// / <param name="wardid"></param>
	// / <returns></returns>
	public static WSPatientVisit[] GetBedList(String wardid, String teamid) throws Exception {
		try {
			getBedList request = new getBedList();
			request.wardid = wardid;
			request.teamid = teamid;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSPatientVisit[] pv = null;
			getBedListResponse response = null;

			for (int i = 0; i < _retryTimes && pv == null; i++) {
				try {
					response = _egretService.getBedList(wardid, teamid, _cliCode, _updateNo);
					pv = GetObjectArrayFromResponse(response, WSPatientVisit.class);
				} catch (Exception ex) {
						throw MakeWSException(ex);
				}

				// if (response != null)
				// pv = (WSPatientVisit[]) response.toArray();
			}
			// Retry(_retryTimes, request, delegate
			// {
			// pv = _egretService.getBedList(request);
			// });
			return pv;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-8-24 获取没有分组的床位列表
	// / </summary>
	// / <param name="wardid"></param>
	// / <returns></returns>
	public static WSPatientVisit[] GetNoTeamBedList(String wardid) throws Exception {
		try {
			getNoTeamBedList request = new getNoTeamBedList();
			request.wardid = wardid;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSPatientVisit[] pv = null;
			getNoTeamBedListResponse response = null;

			for (int i = 0; i < _retryTimes && pv == null; i++) {
				try {
					response = _egretService.getNoTeamBedList(wardid, _cliCode, _updateNo);
					pv = GetObjectArrayFromResponse(response, WSPatientVisit.class);
				} catch (Exception ex) {
						throw MakeWSException(ex);
				}

				// if (response != null)
				// pv = (WSPatientVisit[]) response.toArray();
			}
			// Retry(_retryTimes, request, delegate
			// {
			// pv = _egretService.getNoTeamBedList(request);
			// });
			return pv;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-8-22 获取指定病区全部床位列表
	// / </summary>
	// / <param name="wardid"></param>
	// / <returns></returns>
	public static WSPatientVisit[] GetAllBedList(String wardid) throws Exception {
		try {
			getAllBedList request = new getAllBedList();
			request.wardid = wardid;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSPatientVisit[] pv = null;

			getAllBedListResponse response = null;
			for (int i = 0; i < _retryTimes && pv == null; i++) {
				try {
					response = _egretService.getAllBedList(wardid, _cliCode, _updateNo);
					pv = GetObjectArrayFromResponse(response, WSPatientVisit.class);
				} catch (Exception ex) {
						throw MakeWSException(ex);
				}
				// if (response != null)
				// pv = (WSPatientVisit[]) response.toArray();
			}

			// Retry(_retryTimes, request, delegate
			// {
			// pv = _egretService.getAllBedList(request);
			// });
			return pv;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-27
	// / </summary>
	// / <param name="code"></param>
	// / <returns></returns>
	public static WSPatientVisit[] GetBedListByDoctor(String code) throws Exception {
		try {
			getBedListByDoctor request = new getBedListByDoctor();
			request.doctorCode = code;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSPatientVisit[] pv = null;

			getBedListByDoctorResponse response = null;
			for (int i = 0; i < _retryTimes && pv == null; i++) {
				try {
					response = _egretService.getBedListByDoctor(code, _cliCode, _updateNo);
					pv = GetObjectArrayFromResponse(response, WSPatientVisit.class);
				} catch (Exception ex) {
						throw MakeWSException(ex);
				}
			}
			return pv;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-8-22 通过病人ID获取住院病人信息
	// / </summary>
	// / <param name="patientId"></param>
	// / <returns></returns>
	public static WSPatientVisit GetAdmissionById(String patientId, int series, String admissionId) throws Exception {
		try {
			getAdmissionById request = new getAdmissionById();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSPatientVisit pv = null;

			getAdmissionByIdResponse response = null;

			for (int i = 0; i < _retryTimes && pv == null; i++) {
				try {
					pv = _egretService.getAdmissionById(patientId, admissionId, series, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return pv;
		} catch (Exception ex) {
			// throw MakeWSException(ex);
			Log.e("Egret(GUAI)", "GetAdmissionById Error!", ex);
			throw ex;
		}
	}

	// / <summary>
	// / Wade 2010-5-31 通过病人床号获取住院病人信息
	// / </summary>
	// / <param name="patientId"></param>
	// / <returns></returns>
	public static WSPatientVisit GetInPatientByBed(String bedNo) throws Exception {
		try {
			getInPatientByBed request = new getInPatientByBed();
			request.bedNo = bedNo;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSPatientVisit pv = null;

			// getAdmissionByIdResponse response = null;
			for (int i = 0; i < _retryTimes && pv == null; i++) {
				try {
					pv = _egretService.getInPatientByBed(bedNo, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			return pv;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-8-22 获取服务器时间
	// / </summary>
	// / <returns></returns>
	public static DateTime GetTime() throws Exception {
		try {
			// _egretService = new EgretWSBinding();

			getTime request = new getTime();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			DateTime dt = DateTime.now();

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					DateTime dtN = _egretService.getTime(_cliCode, _updateNo);
					if (dtN != null) {
						dt = dtN;
						success = true;
					}
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// dt = _egretService.getTime(request).@return;
			// });
			return dt;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-8-22 获取检查报表列表
	// / </summary>
	// / <param name="patientid"></param>
	// / <param name="series"></param>
	// / <returns></returns>
	public static WSClinicalReport[] GetClinicalReportList(String admissionid, int series, String patientid, DateTime starttime, DateTime endtime) throws Exception {
		try {
			getClinicalReportList request = new getClinicalReportList();
			request.admissionid = admissionid;
			request.series = series;
			request.patientid = patientid;
			request.starttime = starttime;
			// request.starttimeSpecified = true;
			request.endtime = endtime;
			// request.endtimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSClinicalReport[] crs = null;

			getClinicalReportListResponse response = null;
			for (int i = 0; i < _retryTimes && crs == null; i++) {
				try {
					response = _egretService.getClinicalReportList(admissionid, patientid, series, starttime, endtime, _cliCode, _updateNo);
					crs = GetObjectArrayFromResponse(response, WSClinicalReport.class);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return crs;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-8-22 获取检查报表内容
	// / </summary>
	// / <param name="report"></param>
	// / <returns></returns>
	public static WSClinicalReport GetClinicalReport(WSClinicalReport report) throws Exception {
		try {
			getClinicalReport request = new getClinicalReport();
			request.report = report;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSClinicalReport cr = null;

			// getClinicalReportResponse response = null;
			for (int i = 0; i < _retryTimes && cr == null; i++) {
				try {
					cr = _egretService.getClinicalReport(report, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return cr;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-8-23 获取当前病区责任组列表 hasAll = true 则显示ALL选项
	// / </summary>
	// / <param name="wardid"></param>
	// / <returns></returns>
	public static WSWardTeam[] GetTeamList(String wardid, boolean hasAll) throws Exception {
		try {
			getWardInfo request = new getWardInfo();
			request.arg0 = wardid;
			WSWardInfo info = null;

			for (int i = 0; i < _retryTimes && info == null; i++) {
				try {
					info = _egretService.getWardInfo(wardid);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}

			}

			WSWardTeam[] teams = null;
			if (info != null) {
				ArrayList<WSWardTeam> teamlist = null;
				if (info.teams != null) {
					// teamlist = (ArrayList<WSWardTeam>) info.teams.clone();


					teamlist = new ArrayList<WSWardTeam>(info.teams);

					// foreach (WSWardTeam team in info.teams)
					// {
					// teamlist.Add(team);
					// }
				}

				if (hasAll) {
					WSWardTeam allTeam = new WSWardTeam();
					allTeam.id = 0;
					allTeam.code = "all";
					allTeam.fullName = "ALL";
					allTeam.abbName = "ALL";
					if (teamlist == null)
						teamlist = new ArrayList<WSWardTeam>();
					teamlist.add(allTeam);
				}

				// for (WSWardTeam team_t : teamlist)
				// Log.e("MCS(Guai)", "Teams Item Type 3: " +
				// team_t.getClass().getCanonicalName());

				// WSWardTeam[] teams = new WSWardTeam[teamlist.Count];
				// teamlist.CopyTo(teams);

				// teamlist.getClass().getCanonicalName());

				teams = new WSWardTeam[teamlist.size()];
				int i = 0;
				for (WSWardTeam team_t : teamlist) {
					// Log.e("MCS(Guai)", "Teams Item Type 3: " +
					// team_t.getClass().getCanonicalName());
					teams[i++] = team_t;
				}

				// teams = (WSWardTeam[]) teamlist.toArray();
			}
			return teams;
		} catch (Exception ex) {
			Log.e("Egret", "WSController GetTeamList Exception !!", ex);
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-8-23 获取当前病区角色列表
	// / </summary>
	// / <param name="wardid"></param>
	// / <returns></returns>
	public static WSDictionaryItem[] GetRoleList(String wardid) throws Exception {
		try {
			WSDictionaryItem[] dis = new WSDictionaryItem[3];
			dis[0] = new WSDictionaryItem();
			dis[0].code = "IC";
			dis[0].name = "IC";
			dis[1] = new WSDictionaryItem();
			dis[1].code = "Reporter";
			dis[1].name = "Reporter";
			dis[2] = new WSDictionaryItem();
			dis[2].code = "Runner";
			dis[2].name = "Runner";
			return dis;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-8-24 获取住院病人照片
	// / </summary>
	// / <param name="patid"></param>
	// / <param name="admid"></param>
	// / <returns></returns>
	public static byte[] GetAdmissionPictureById(String patid, String admid) throws Exception {
		getAdmissionPictureById request = new getAdmissionPictureById();
		request.patientId = patid;
		request.admissionId = admid;
		request.cliCode = _cliCode;
		request.updateNo = _updateNo;
		byte[] temp = null;

		for (int i = 0; i < _retryTimes && temp == null; i++) {
			try {
				temp = _egretService.getAdmissionPictureById(patid, admid, _cliCode, _updateNo);
			} catch (Exception ex) {
				
					throw MakeWSException(ex);
			}
		}

		return temp;
	}

	// / <summary>
	// / Wade 2009-8-24 设置住院病人图片
	// / </summary>
	// / <param name="admissionDt"></param>
	// / <param name="admissionId"></param>
	// / <param name="ifnew"></param>
	// / <param name="patientId"></param>
	// / <param name="picture"></param>
	public static boolean SetAdmissionPicture(DateTime admissionDt, String admissionId, boolean ifnew, String patientId, byte[] picture, String reason) throws Exception {
		try {
			setAdmissionPicture request = new setAdmissionPicture();
			request.admissionDt = admissionDt;
			// request.admissionDtSpecified = true;
			request.admissionId = admissionId;
			request.ifnew = ifnew;
			// request.ifnewSpecified = true;
			request.patientId = patientId;
			request.picture = picture;
			request.refusereason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			boolean ret = false;

			for (int i = 0; i < _retryTimes && !ret; i++) {
				try {
					ret = _egretService.setAdmissionPicture(patientId, admissionId, admissionDt, picture, ifnew, reason, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-8-27 从一组删除指定的病人
	// / </summary>
	// / <param name="wardid"></param>
	// / <param name="teamid"></param>
	// / <param name="accountid"></param>
	// / <returns></returns>
	public static boolean RemovefromOneTeam(String wardid, String teamid, String accountid) throws Exception {
		try {
			removefromOneTeam request = new removefromOneTeam();
			request.wardid = wardid;
			request.teamid = teamid;
			request.accountid = accountid;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			boolean ret = false;

			for (int i = 0; i < _retryTimes && !ret; i++) {
				try {
					ret = _egretService.removefromOneTeam(wardid, teamid, accountid, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-8-27 添加一个病人到指定组
	// / </summary>
	// / <param name="admissionid"></param>
	// / <param name="allocater"></param>
	// / <param name="attd"></param>
	// / <param name="bedname"></param>
	// / <param name="patientid"></param>
	// / <param name="patientname"></param>
	// / <param name="teamid"></param>
	// / <param name="wardid"></param>
	// / <returns></returns>
	public static boolean SeparateToOneTeam(String admissionid, String allocater, String attd, String bedname, String patientid, String patientname, String teamid, String wardid) throws Exception {
		try {
			separateToOneTeam request = new separateToOneTeam();
			request.admissionid = admissionid;
			request.allocater = allocater;
			request.attd = attd;
			request.bedname = bedname;
			request.patientid = patientid;
			request.patientname = patientname;
			request.teamid = teamid;
			request.wardid = wardid;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			boolean ret = false;

			for (int i = 0; i < _retryTimes && !ret; i++) {
				try {
					ret = _egretService.separateToOneTeam(wardid, teamid, patientid, admissionid, patientname, attd, allocater, bedname, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-3-1 添加一组病人到指定组
	// / </summary>
	// / <param name="patientAllocations"></param>
	// / <param name="allocaterCode"></param>
	public static void AllocateToOneTeam(WSNursingPatientAllocation[] patientAllocations, String allocaterCode) throws Exception {
		try {
			allocateToOneTeam request = new allocateToOneTeam();
			// request.patientAllocations = patientAllocations;
			request.patientAllocations = new ArrayList<WSNursingPatientAllocation>(Arrays.asList(patientAllocations));
			request.allocaterCode = allocaterCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.allocateToOneTeam(request);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-3-1 获取用户的功能权限
	// / </summary>
	// / <param name="userId"></param>
	// / <returns></returns>
	public static WSSysFunction GetUserSysFunction(String userCode, int functionId) throws Exception {
		try {
			// #if DEBUG
			// WSSysFunction func1 = new WSSysFunction();
			// func1.funCode = "001";
			// func1.funName = "Nurse";
			// func1.funObject = "NURSE";

			// WSSysFunction func2 = new WSSysFunction();
			// func2.funCode = "002";
			// func2.funName = "Pathology";
			// func2.funObject = "PATHOLOGY";

			// WSSysFunction func = new WSSysFunction();
			// func.childFunctions = new WSSysFunction[2];
			// func.childFunctions[0] = func1;
			// func.childFunctions[1] = func2;
			// return func;
			// #else
			getUserSysFunction request = new getUserSysFunction();
			request.userCode = userCode;
			request.functionId = functionId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSSysFunction ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getUserSysFunction(userCode, functionId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-3-7 根据用户名，取得该用户能访问的子系统
	// / </summary>
	// / <param name="userCode"></param>
	// / <returns></returns>
	public static WSSysFunction GetUserSubSystem(String userCode) throws Exception {
		try {
			getUserSubSystem request = new getUserSubSystem();
			request.userCode = userCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSSysFunction ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getUserSubSystem(userCode, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-3-7 取得用户某子系统的功能权限
	// / </summary>
	// / <param name="userCode"></param>
	// / <param name="functionId"></param>
	// / <returns></returns>
	public static WSUserPermission GetUserSubSystemPermission(String userCode, int functionId) throws Exception {
		try {
			getUserSubSystemPermission request = new getUserSubSystemPermission();
			request.userCode = userCode;
			request.functionId = functionId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSUserPermission ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getUserSubSystemPermission(userCode, functionId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSMasterView GetMasterView(String wardId, String teamId, String userGroup, taskType alertType, taskSubType alertSubType, String clientId) throws Exception {
		try {
			getMasterView request = new getMasterView();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.clientId = clientId;
			request.teamId = teamId;
			request.wardId = wardId;
			request.userGroup = userGroup;
			// if (alertType != null)
			// {
			// request.typeSpecified = true;
			// request.type = alertType.Value;
			// }
			//
			// if (alertSubType != null)
			// {
			// request.subTypeSpecified = true;
			// request.subType = alertSubType.Value;
			// }

			WSMasterView rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getMasterView(wardId, teamId, clientId, userGroup, alertType, alertSubType, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			if (rets.patientVisit == null) {
				// rets.patientVisit = new WSPatientVisit[0];
				rets.patientVisit = new ArrayList<WSPatientVisit>(0);
			}
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// endregion

	// region FP&Iso

	public static void ModifyPatientPrecautionInfo(WSNursingPatientPrecaution precaution, WSUser executer) throws Exception {
		try {
			modifyPatientPrecautionInfo request = new modifyPatientPrecautionInfo();
			request.wsPrecaution = precaution;
			request.executer = executer;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			// _egretService.modifyPatientPrecautionInfo(request);
			_egretService.modifyPatientPrecautionInfo(precaution, executer, _cliCode, _updateNo);
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// region Medication
	// / <summary>
	// / 将Hospira pump信息传给MIS
	// / </summary>
	// / <param name="barcodeId"></param>
	// / <param name="pumpId"></param>
	// / <param name="lineId"></param>
	// / <param name="mode"></param>
	// / <returns></returns>

	// / <summary>
	// / 获取某病房是否是Hospira Ward
	// / </summary>
	// / <param name="wardId"></param>
	// / <returns></returns>
	public static boolean IsHospiraWard(String wardId) throws Exception {
		try {
			isHospiraWard request = new isHospiraWard();
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			boolean ret = false;

			for (int i = 0; i < _retryTimes && !ret; i++) {
				try {
					ret = _egretService.isHospiraWard(wardId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-9-7 根据医嘱代码从医院现有系统取得医嘱信息，并校验该医嘱是否属于该病区
	// / Wade 2009-12-4
	// / </summary>
	// / <param name="orderCode">医嘱代码</param>
	// / <param name="wardId">病区Id</param>
	// / <returns></returns>
	public static WSNursingOrder[] GetOrderByCode(String orderCode, String wardId) throws Exception {
		// try
		// {
		// getOrderByCode request = new getOrderByCode();
		// request.orderCode = orderCode;
		// request.wardId = wardId;
		// request.cliCode = _cliCode;
		// request.updateNo = _updateNo;
		// WSNursingOrder[] orders = null;
		// Retry(_retryTimes, _interval, delegate
		// {
		// orders = _egretService.getOrderByCode(request);
		// });
		// return orders;
		// }
		// catch (Exception ex)
		// {
		// throw MakeWSException(ex);
		// }
		return null;
	}

	// / <summary>
	// / Wade 2009-9-7 病区收药，系统会判断该药是否属于该病区，是否属于同一个病人，如果某药已经收过，则系统自动剔除
	// / Wade 2009-12-4
	// / </summary>
	// / <param name="drugs">已收的药</param>
	// / <param name="wardId">病区Id</param>
	// / <param name="receiver">收药人</param>
	public static void ReceiveDrugs(WSNursingOrder[] drugs, String wardId, String receiverCode) throws Exception {
		try {
			receiveDrugs request = new receiveDrugs();
			// request.drugs = new
			// ArrayList<WSNursingOrder>(Arrays.asList(drugs));
			request.drugs = new ArrayList<WSNursingOrder>();
			for (WSNursingOrder order : drugs)
				request.drugs.add(order);

			request.wardId = wardId;
			request.receiverCode = receiverCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.receiveDrugs(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-9-8 得到楼层Team列表
	// / </summary>
	// / <param name="wardcode"></param>
	public static WSWardInfo GetWardInfo(String wardcode) throws Exception {
		try {
			getWardInfo request = new getWardInfo();
			request.arg0 = wardcode;
			WSWardInfo info = null;

			for (int i = 0; i < _retryTimes && info == null; i++) {
				try {
					info = _egretService.getWardInfo(wardcode);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return info;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-4 取得某药物的信息，包括药品的基本信息(药品名称、剂量、规格等等)和状态等
	// / </summary>
	// / <param name="codes"></param>
	// / <returns></returns>
	public static WSNursingOrder[] GetOrderByUniqueCode(String uniqueCode, String wardId) throws Exception {
		try {
			getOrderByUniqueCode request = new getOrderByUniqueCode();
			request.uniqueCode = uniqueCode;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrder[] ret = null;

			getOrderByUniqueCodeResponse response = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getOrderByUniqueCode(uniqueCode, wardId, _cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(response, WSNursingOrder.class);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingOrder[] GetSameGroupOrderInHIS(String uniqueCode, String wardId) throws Exception {
		try {
			getSameGroupOrderInHIS request = new getSameGroupOrderInHIS();
			request.uniqueCode = uniqueCode;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrder[] ret = null;

			getSameGroupOrderInHISResponse response = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getSameGroupOrderInHIS(uniqueCode, wardId, _cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(response, WSNursingOrder.class);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / GT 2011-11-7 获取与病房的药关联的医嘱列表
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="itemcode"></param>
	// / <returns></returns>
	public static WSNursingOrder[] GetWardStockRelatedOrder(String patientId, int series, String admissionId, String itemcode) throws Exception {
		try {
			getWardStockRelatedOrder request = new getWardStockRelatedOrder();
			request.patientId = patientId;
			request.admissionId = admissionId;
			request.series = series;
			request.itemCode = itemcode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrder[] ret = null;

			getWardStockRelatedOrderResponse response = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getWardStockRelatedOrder(patientId, series, admissionId, itemcode, _cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(response, WSNursingOrder.class);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 取plantime与remarks
	// / </summary>
	public static WSNursingOrder GetOrderByUniqueCodeInMCS(String uniqueCode, String wardId) throws Exception {
		try {
			getOrderByUniqueCodeInMCS request = new getOrderByUniqueCodeInMCS();
			request.uniqueCode = uniqueCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrder ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getOrderByUniqueCodeInMCS(uniqueCode, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-8 取得母液的信息，包括药品的基本信息(药品名称、剂量、规格等等)和状态等
	// / </summary>
	// / <param name="codes"></param>
	// / <returns></returns>
	public static WSNursingOrder GetItemByCode(String itemCode) throws Exception {
		try {
			getItemByCode request = new getItemByCode();
			request.itemCode = itemCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrder ret = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getItemByCode(itemCode, _cliCode, _updateNo);

				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-4 配药，未双签，系统会校验药物是否属于同一个人，药物是否有效，药物的用法是否一致，是否能配药
	// / </summary>
	// / <param name="drugs">待完成配药的药物，只需要填写医嘱代码和药物代码即可</param>
	// / <param name="mixer">配药人</param>
	// / <param name="wardId">病区Id</param>
	// / <returns></returns>
	public static WSNursingOrderAdminRecord PrepareDrugWithoutCounterSign(WSNursingOrder[] drugs, WSUser preparer, String wardId, int ivfDuration, String ivfDurationDesc, DateTime plantime, String patientId, int series, String admissionId, BigDecimal rate) throws Exception // ,
																																																																					// String
																																																																					// overMins
	{
		try {
			prepareDrugWithoutCounterSign request = new prepareDrugWithoutCounterSign();

			request.drugs = new ArrayList<WSNursingOrder>(Arrays.asList(drugs));
			request.preparer = preparer;
			request.wardId = wardId;
			request.plantime = plantime;
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.flowInfo = new WSIVFFlowInfo();// request.flowInfo.ivfDuration
													// = overMins;//overMins
			request.flowInfo.ivfDuration = ivfDuration;
			request.flowInfo.ivfDurationDesc = ivfDurationDesc;
			request.flowInfo.ivfRate = rate;
			// if (rate != 0)
			// request.flowInfo.ivfRateSpecified = true;
			// else
			// request.flowInfo.ivfRateSpecified = false;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrderAdminRecord ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.prepareDrugWithoutCounterSign(request);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-4 配药检查，仁安存在配药后，不当场检查的情况
	// / </summary>
	// / <param name="adminId">标签上的条码Id</param>
	// / <param name="checker">检查人</param>
	public static void CheckDrugPreparation(String barcodeId, WSUser checker, String wardId) throws Exception {
		try {
			checkDrugPreparation request = new checkDrugPreparation();
			request.barcodeId = barcodeId;
			request.checker = checker;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.checkDrugPreparation(barcodeId, checker, wardId, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / ztm 2011-03-30 检查该药物是否已被Received
	// / </summary>
	// / <param name="autoNo">药物的唯一AutoNo</param>
	// / <param name="orderUniqueCode">Order唯一编码</param>
	// / <param name="wardId">病区号</param>
	// / <returns>是否Received的标志位</returns>
	public static boolean CheckIfDrugHasBeenReceived(String autoNo, String orderUniqueCode, String wardId) throws Exception {
		try {
			checkIfDrugHasBeenReceived request = new checkIfDrugHasBeenReceived();
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.autoNo = autoNo;
			request.orderUniqueCode = orderUniqueCode;
			boolean ret = false;

			for (int i = 0; i < _retryTimes && !ret; i++) {
				try {
					ret = _egretService.checkIfDrugHasBeenReceived(autoNo, orderUniqueCode, wardId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-4 配药同时进行双签，需要检查药品是否在收药列表中(仁安医院的情况是母液不会出现在收药列表)
	// / </summary>
	// / <param name="drugs">药品列表</param>
	// / <param name="mixer">配药人</param>
	// / <param name="checker">配药检查人</param>
	// / <param name="wardId">病区Id</param>
	// / <returns>执行纪录的唯一Id,用于条码打印</returns>
	public static WSNursingOrderAdminRecord PrepareDrugWithCounterSign(WSNursingOrder[] drugs, WSUser preparer, WSUser checker, String wardId, int ivfDuration, String ivfDurationDesc, DateTime plantime, String patientId, int series, String admissionId, BigDecimal rate) throws Exception // ,
																																																																								// String
																																																																								// overMins
	{
		try {
			prepareDrugWithCounterSign request = new prepareDrugWithCounterSign();
			request.drugs = new ArrayList<WSNursingOrder>(Arrays.asList(drugs));
			;
			request.preparer = preparer;
			request.checker = checker;
			request.wardId = wardId;
			request.planTime = plantime;
			// request.planTimeSpecified = true;
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.flowInfo = new WSIVFFlowInfo();// request.flowInfo.ivfDuration
													// = overMins;//overMins
			request.flowInfo.ivfDuration = ivfDuration;
			request.flowInfo.ivfDurationDesc = ivfDurationDesc;
			request.flowInfo.ivfRate = rate;
			// if (rate != 0)
			// request.flowInfo.ivfRateSpecified = true;
			// else
			// request.flowInfo.ivfRateSpecified = false;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrderAdminRecord ret = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.prepareDrugWithCounterSign(request);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-4 取得病人某段时间内的所有医嘱内容，包括有效和已经停止的 已废止
	// / </summary>
	// / <param name="patientId">病人Id，不能为空</param>
	// / <param name="series">流水号</param>
	// / <param name="admissionId">入院Id，可以为空</param>
	// / <param name="beginTime">开始时间，不能为空</param>
	// / <param name="endTime">结束时间，不能为空</param>
	// / <returns></returns>
	public static WSNursingOrder[] GetPatientOrder(String patientId, int series, String admissionId, DateTime beginTime, DateTime endTime) throws Exception {
		// try
		// {
		// getPatientOrder request = new getPatientOrder();
		// request.patientId = patientId;
		// request.series = series;
		// request.admissionId = admissionId;
		// request.beginTime = beginTime;
		// request.beginTimeSpecified = true;
		// request.endTime = endTime;
		// request.endTimeSpecified = true;
		// request.cliCode = _cliCode;
		// request.updateNo = _updateNo;
		// WSNursingOrder[] rets = null;
		// Retry(_retryTimes, _interval, delegate
		// {
		// rets = _egretService.getPatientOrder(request);
		// });
		// return rets;
		// }
		// catch (Exception ex)
		// {
		// throw MakeWSException(ex);
		// }
		return null;
	}

	// / <summary>
	// / Wade 2009-12-4 取得病人当前正在生效的医嘱
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <returns></returns>
	public static WSNursingOrder[] GetPatientOnOrders(String patientId, int series, String admissionId, DateTime beginTime, DateTime endtime) throws Exception {
		try {
			getPatientOnOrders request = new getPatientOnOrders();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endtime;
			// request.endTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrder[] rets = null;

			getPatientOnOrdersResponse ret = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getPatientOnOrders(patientId, series, admissionId, beginTime, endtime, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSNursingOrder.class);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-4 取得病人某段时间内的所有已经停止的医嘱
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="beginTime"></param>
	// / <param name="endTime"></param>
	// / <returns></returns>
	public static WSNursingOrder[] GetPatientOffOrders(String patientId, int series, String admissionId, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			getPatientOffOrders request = new getPatientOffOrders();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrder[] rets = null;

			getPatientOffOrdersResponse ret = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getPatientOffOrders(patientId, series, admissionId, beginTime, endTime, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSNursingOrder.class);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-6 根据条码Id取得相应的执行纪录
	// / </summary>
	// / <param name="barcodeId"></param>
	// / <returns></returns>
	public static WSNursingOrderAdminRecord GetAdminRecordByBarcodeId(String barcodeId, String wardId) throws Exception {
		try {
			getAdminRecordByBarcodeId request = new getAdminRecordByBarcodeId();
			request.barcodeId = barcodeId;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrderAdminRecord ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getAdminRecordByBarcodeId(barcodeId, wardId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-6 取病人未执行的用药纪录
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <returns></returns>
	public static WSNursingOrderAdminRecord[] GetPatientPreparedAdminRecords(String patientId, int series, String admissionId) throws Exception {
		try {
			getPatientPreparedAdminRecords request = new getPatientPreparedAdminRecords();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrderAdminRecord[] rets = null;

			getPatientPreparedAdminRecordsResponse ret = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getPatientPreparedAdminRecords(patientId, series, admissionId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSNursingOrderAdminRecord.class);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void DoAdministrationFromAdminRecordWithHospiraInfo(WSUser executer, String wardId, WSOverMaxDosageAlert[] omdList, WSDrugHospiraInfo[] hospirainfos) throws Exception {
		try {
			doAdministrationFromAdminRecordWithHospiraInfo request = new doAdministrationFromAdminRecordWithHospiraInfo();
			if (omdList != null){
				request.omdList = new ArrayList<WSOverMaxDosageAlert>(Arrays.asList(omdList));
			} else {
				request.omdList = new ArrayList<WSOverMaxDosageAlert>();
			}
			request._operator = executer;
			request.wardId = wardId;
			request.wsDrugHospiraInfos = new ArrayList<WSDrugHospiraInfo>(Arrays.asList(hospirainfos));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.doAdministrationFromAdminRecordWithHospiraInfo(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-6 执行某条用药执行纪录
	// / </summary>
	// / <param name="adminRecord"></param>
	// / <param name="wardId"></param>
	public static void DoAdministrationFromAdminRecord(String barcodeId, WSUser executer, String wardId, WSOverMaxDosageAlert[] omdList) throws Exception {
		try {
			doAdministrationFromAdminRecord request = new doAdministrationFromAdminRecord();
			request._operator = executer;
			request.barcodeId = barcodeId;
			if (omdList != null){
				request.omdList = new ArrayList<WSOverMaxDosageAlert>(Arrays.asList(omdList));
			} else {
				request.omdList = new ArrayList<WSOverMaxDosageAlert>();
			}
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.doAdministrationFromAdminRecord(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-6 完成某些药物的执行
	// / </summary>
	// / <param name="orders"></param>
	// / <param name="executer"></param>
	// / <param name="checker"></param>
	// / <param name="wardId"></param>
	public static void DoAdministrationFromDrugs(WSNursingOrder[] orders, WSUser executer, WSUser checker, String wardId, WSPatientVisit pv) throws Exception {
		try {
			doAdministrationFromDrugs request = new doAdministrationFromDrugs();
			if (orders != null){
				request.orders = new ArrayList<WSNursingOrder>(Arrays.asList(orders));
			} else {
				request.orders = new ArrayList<WSNursingOrder>();
			}
			request._operator = executer;
			request.checker = checker;
			request.wardId = wardId;
			request.admissionId = pv.admissionId;
			request.omdList = null;
			request.patientId = pv.patient.id;
			request.series = pv.series;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.doAdministrationFromDrugs(request);
					success = true;
				} catch (Exception ex) {
//					throw new SocketTimeoutException();
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-27 药已给 从Record异常执行
	// / </summary>
	// / <param name="adminRecord"></param>
	// / <param name="excuter"></param>
	// / <param name="remark"></param>
	public static void DoGivenAbnormalAdminFromAdminRecord(WSNursingOrderAdminRecord adminRecord, WSUser excuter, String wardId, String remark) throws Exception {
		try {
			adminRecord.executer = excuter;
			adminRecord.remark = remark;

			doGivenAbnormalAdminFromAdminRecord request = new doGivenAbnormalAdminFromAdminRecord();
			request.adminRecord = adminRecord;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.wardId = wardId;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.doGivenAbnormalAdminFromAdminRecord(adminRecord, wardId, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-27 药已给 从Orders异常执行
	// / </summary>
	// / <param name="adminRecord"></param>
	// / <param name="excuter"></param>
	// / <param name="remark"></param>
	public static void DoGivenAbnormalAdminFromDrugs(WSNursingOrder[] orders, WSUser executer, WSUser checker, DateTime givenTime, String reason, String wardId) throws Exception {
		try {
			doGivenAbnormalAdminFromDrugs request = new doGivenAbnormalAdminFromDrugs();
			request.orders = new ArrayList<WSNursingOrder>(Arrays.asList(orders));
			request._operator = executer;
			request.checker = checker;
			request.givenTime = givenTime;
			// request.givenTimeSpecified = true;
			request.wardId = wardId;
			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.doGivenAbnormalAdminFromDrugs(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-27 药未给
	// / </summary>
	// / <param name="barcodeId"></param>
	// / <param name="canceler"></param>
	// / <param name="reason"></param>
	public static void DoNotGivenKeepAbnormalAdminFromAdminRecord(WSNursingOrderAdminRecord adminRecord, WSUser excuter, String wardId, String remark) throws Exception {
		try {
			adminRecord.executer = excuter;
			adminRecord.remark = remark;

			doNotGivenKeepAbnormalAdminFromAdminRecord request = new doNotGivenKeepAbnormalAdminFromAdminRecord();
			request.adminRecord = adminRecord;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.wardId = wardId;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.doNotGivenKeepAbnormalAdminFromAdminRecord(adminRecord, wardId, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// /
	// / </summary>
	// / <param name="adminRecord"></param>
	// / <param name="excuter"></param>
	// / <param name="wardId"></param>
	// / <param name="remark"></param>
	public static void DoNotGivenKeepAbnormalAndDiscardLabelAdminFromAdminRecord(WSNursingOrderAdminRecord adminRecord, WSUser excuter, String wardId, String remark) throws Exception {
		try {
			adminRecord.executer = excuter;
			adminRecord.remark = remark;

			doNotGivenKeepAbnormalAndDiscardLabelAdminFromAdminRecord request = new doNotGivenKeepAbnormalAndDiscardLabelAdminFromAdminRecord();
			request.adminRecord = adminRecord;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.wardId = wardId;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.doNotGivenKeepAbnormalAndDiscardLabelAdminFromAdminRecord(adminRecord, wardId, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// /
	// / </summary>
	// / <param name="adminRecord"></param>
	// / <param name="excuter"></param>
	// / <param name="wardId"></param>
	// / <param name="remark"></param>
	public static void DoNotGivenDiscardAbnormalAdminFromAdminRecord(WSNursingOrderAdminRecord adminRecord, WSUser excuter, String wardId, String remark) throws Exception {
		try {
			adminRecord.executer = excuter;
			adminRecord.remark = remark;

			doNotGivenDiscardAbnormalAdminFromAdminRecord request = new doNotGivenDiscardAbnormalAdminFromAdminRecord();
			request.adminRecord = adminRecord;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.wardId = wardId;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.doNotGivenDiscardAbnormalAdminFromAdminRecord(adminRecord, wardId, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-27 药未给
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="adminId"></param>
	// / <param name="canceler"></param>
	// / <param name="reason"></param>
	public static void DoNotGivenKeepAbnormalAdminFromDrugs(WSNursingOrder[] orders, WSUser executer, WSUser checker, String reason, String wardId) throws Exception {
		try {
			doNotGivenKeepAbnormalAdminFromDrugs request = new doNotGivenKeepAbnormalAdminFromDrugs();
			request.orders = new ArrayList<WSNursingOrder>(Arrays.asList(orders));
			request._operator = executer;
			request.checker = checker;
			request.reason = reason;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.doNotGivenKeepAbnormalAdminFromDrugs(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void DoNotGivenDiscardAbnormalAdminFromDrugs(WSNursingOrder[] orders, WSUser executer, WSUser checker, String reason, String wardId) throws Exception {
		try {
			doNotGivenDiscardAbnormalAdminFromDrugs request = new doNotGivenDiscardAbnormalAdminFromDrugs();
			request.orders = new ArrayList<WSNursingOrder>(Arrays.asList(orders));
			request._operator = executer;
			request.checker = checker;
			request.reason = reason;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.doNotGivenDiscardAbnormalAdminFromDrugs(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-11 已执行记录列表
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="beginTime"></param>
	// / <param name="endTime"></param>
	// / <returns></returns>
	public static WSNursingOrderAdminRecord[] GetPatientAdministrationRecords(String patientId, int series, String admissionId, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			getPatientAdministrationRecords request = new getPatientAdministrationRecords();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrderAdminRecord[] rets = null;

			getPatientAdministrationRecordsResponse ret = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getPatientAdministrationRecords(patientId, series, admissionId, beginTime, endTime, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSNursingOrderAdminRecord.class);
					// if (ret != null)
					// rets = (WSNursingOrderAdminRecord[]) ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getPatientAdministrationRecords(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-11 根据医嘱得到执行记录的列表
	// / </summary>
	// / <param name="orderCode"></param>
	// / <param name="itemCode"></param>
	// / <param name="beginTime"></param>
	// / <param name="endTime"></param>
	// / <returns></returns>
	public static WSNursingOrderAdminRecord[] GetPatientAdministrationRecordsByOrderUQCode(String patientId, String orderUQCode, DateTime beginTime, DateTime endTime, int series, String admissionId) throws Exception {
		try {
			getPatientAdministrationRecordsByOrderUQCode request = new getPatientAdministrationRecordsByOrderUQCode();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.orderUQCode = orderUQCode;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrderAdminRecord[] rets = null;

			getPatientAdministrationRecordsByOrderUQCodeResponse ret = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getPatientAdministrationRecordsByOrderUQCode(patientId, series, admissionId, orderUQCode, beginTime, endTime, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSNursingOrderAdminRecord.class);
					// if (ret != null)
					// rets = (WSNursingOrderAdminRecord[]) ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getPatientAdministrationRecordsByOrderUQCode(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSAdminOperationRecord[] GetAdminOperationRecordsByRecordId(int recordId) throws Exception {
		try {
			getAdminOperationRecordsByRecordId request = new getAdminOperationRecordsByRecordId();
			request.adminId = recordId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSAdminOperationRecord[] rets = null;

			getAdminOperationRecordsByRecordIdResponse ret = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getAdminOperationRecordsByRecordId(recordId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSAdminOperationRecord.class);
					// if (ret != null)
					// rets = (WSAdminOperationRecord[]) ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getAdminOperationRecordsByRecordId(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-11 修改时间点
	// / </summary>
	// / <param name="orders"></param>
	// / <param name="modifier"></param>
	public static void UpdateOrderNextPlanTime(WSNursingOrder[] orders, String modifierCode, String wardId) throws Exception {
		try {
			updateOrderNextPlanTime request = new updateOrderNextPlanTime();
			request.modifierCode = modifierCode;
			request.wardId = wardId;
			request.orders = new ArrayList<WSNursingOrder>(Arrays.asList(orders));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			Log.e("Mcs", " modifierCode " + modifierCode);
			Log.e("Mcs", " wardId " + wardId);
			Log.e("Mcs", " request.orders " + request.orders);
			Log.e("Mcs", " cliCode " + _cliCode);
			Log.e("Mcs", " updateNo " + _updateNo);

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.updateOrderNextPlanTime(request);
					success = true;
				} catch (Exception ex) {
					Log.e("Mcs", " UpdateOrderNextPlanTime " + ex);
					ex.printStackTrace();
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.updateOrderNextPlanTime(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static boolean IsWardStockItem(String itemCode, String wardId) throws Exception {
		try {
			isWardStockItem request = new isWardStockItem();
			request.itemCode = itemCode;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			boolean isWardStockItem = false;

			for (int i = 0; i < _retryTimes && !isWardStockItem; i++) {
				try {
					isWardStockItem = _egretService.isWardStockItem(itemCode, wardId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			// Retry(_retryTimes, request, delegate
			// {
			// isWardStockItem = _egretService.isWardStockItem(request).@return;
			// });
			return isWardStockItem;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-28 暂废止
	// / </summary>
	// / <param name="freqCode"></param>
	// / <param name="route"></param>
	// / <returns></returns>
	public static int[] GetFreqPresettedTimePoints(String freqCode, String route) throws Exception {
		// try
		// {
		// getFreqPresettedTimePoints request = new
		// getFreqPresettedTimePoints();
		// request.freqCode = freqCode;
		// request.route = route;
		// request.routeSpecified = true;
		// request.cliCode = _cliCode;
		// request.updateNo = _updateNo;
		// int[] rets = null;
		// Retry(_retryTimes, _interval, delegate
		// {
		// rets = _egretService.getFreqPresettedTimePoints(request);
		// });
		// return rets;
		// }
		// catch (Exception ex)
		// {
		// throw MakeWSException(ex);
		// }
		return null;
	}

	// / <summary>
	// / Wade 2009-12-11
	// / </summary>
	// / <param name="orders"></param>
	// / <param name="stopper"></param>
	public static void OffOrder(WSNursingOrder[] orders, String stopperCode, String wardId) throws Exception {
		try {
			offOrder request = new offOrder();
			request.orders = new ArrayList<WSNursingOrder>(Arrays.asList(orders));
			request.operatorCode = stopperCode;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.offOrder(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.offOrder(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-13
	// / </summary>
	// / <param name="orders"></param>
	// / <param name="stopper"></param>
	// public static WSDictionaryItem[] GetAbnormalAdminReasons()
	// {
	// try
	// {
	// getAbnormalAdminReasons request = new getAbnormalAdminReasons();
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// WSDictionaryItem[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.getAbnormalAdminReasons(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	public static WSDictionaryItem[] GetDrugAdminGivenRemarks() throws Exception {
		try {
			// _egretService = new EgretWSBinding();

			getDrugAdminGivenRemarks request = new getDrugAdminGivenRemarks();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getDrugAdminGivenRemarksResponse ret = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getDrugAdminGivenRemarks(_cliCode, _updateNo);
					// rets = GetObjectArrayFromResponse(ret,
					// WSDictionaryItem.class);
					rets = GetObjectArrayFromResponse(ret, WSDictionaryItem.class);

					// ArrayList<WSDictionaryItem> ret2 =
					// GetObjectArrayFromResponse(ret, WSDictionaryItem.class);
					// rets = new WSDictionaryItem[ret2.size()];
					// rets = (WSDictionaryItem[]) ret2.toArray(rets);

					// ArrayList<WSDictionaryItem> rets2 = new
					// ArrayList<WSDictionaryItem>();
					//
					// for (int j=0; j<ret.getPropertyCount(); j++)
					// {
					// rets2.add((WSDictionaryItem)ret.getProperty(j));
					// }
					//
					// rets = new WSDictionaryItem[rets2.size()];
					// rets = (WSDictionaryItem[])rets2.toArray(rets);

					// if (ret != null)
					// {
					// Log.e("EgretPPC", ret.getPropertyCount() + " ");
					// rets = (WSDictionaryItem[]) ret.getProperty(0);
					//
					// }

					// rets = (WSDictionaryItem[]) ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getDrugAdminGivenRemarks(request);
			// });
			return rets;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw MakeWSException(ex);
		}
	}

	public static WSDictionaryItem[] GetDrugAdminNotGivenRemarks() throws Exception {
		try {
			// _egretService = new EgretWSBinding();

			getDrugAdminNotGivenRemarks request = new getDrugAdminNotGivenRemarks();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getDrugAdminNotGivenRemarksResponse ret = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getDrugAdminNotGivenRemarks(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSDictionaryItem.class);
					// if (ret != null)
					// rets = (WSDictionaryItem[]) ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getDrugAdminNotGivenRemarks(request);
			// });
			return rets;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw MakeWSException(ex);
		}
	}

	public static WSDictionaryItem[] GetVaccinationNotGivenReason() throws Exception {
		try {
			getVaccinationNotGivenReason request = new getVaccinationNotGivenReason();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getVaccinationNotGivenReasonResponse ret = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getVaccinationNotGivenReason(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSDictionaryItem.class);
					// if (ret != null)
					// rets = (WSDictionaryItem[]) ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getVaccinationNotGivenReason(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-4-28
	// / </summary>
	// / <param name="orders"></param>
	// / <param name="stopper"></param>
	public static WSDictionaryItem[] GetAbnormalAdminReasons2() throws Exception {
		try {
			getAbnormalAdminReasons2 request = new getAbnormalAdminReasons2();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getAbnormalAdminReasons2Response ret = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getAbnormalAdminReasons2(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSDictionaryItem.class);
					// if (ret != null)
					// rets = (WSDictionaryItem[]) ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getAbnormalAdminReasons2(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-13
	// / </summary>
	// / <param name="orders"></param>
	// / <param name="stopper"></param>
	public static WSDictionaryItem[] GetNurseRemarks() throws Exception {
		try {
			getNurseRemarks request = new getNurseRemarks();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getNurseRemarksResponse ret = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getNurseRemarks(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSDictionaryItem.class);
					// if (ret != null)
					// rets = (WSDictionaryItem[]) ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getNurseRemarks(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-14
	// / </summary>
	// / <param name="orders"></param>
	// / <param name="modifier"></param>
	public static void UpdateNurseRemarks(WSNursingOrder[] orders, String modifierCode) throws Exception {
		try {
			updateNurseRemarks request = new updateNurseRemarks();
			request.orderId = orders[0].id;
			request.modifierCode = modifierCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.remark = orders[0].nurseRemark;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.updateNurseRemarks(orders[0].id, orders[0].nurseRemark, modifierCode, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.updateNurseRemarks(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 查询用户信息
	// / </summary>
	// / <param name="usercode"></param>
	// / <returns></returns>
	public static WSUser GetUserByCode(String usercode) throws Exception {
		try {
			getUserByCode request = new getUserByCode();
			request.userCode = usercode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSUser user = null;

			getNurseRemarksResponse ret = null;
			for (int i = 0; i < _retryTimes && user == null; i++) {
				try {
					user = _egretService.getUserByCode(usercode, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// user = _egretService.getUserByCode(request).@return;
			// });
			return user;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}

	}

	// / <summary>
	// / Wade 2009-12-17 获取打印机地址
	// / </summary>
	// / <param name="type"></param>
	// / <param name="clientId"></param>
	// / <returns></returns>
	public static WSPrinterMapping GetPrinterMapping(printerType type, String clientId) throws Exception {
		try {
			getPrinterMapping request = new getPrinterMapping();

			// if (type == null)
			// request.printerTypeSpecified = false;
			// else
			// {
			// request.printerType = type.Value;
			// request.printerTypeSpecified = true;
			// }

			request.printerType = type;
			request.clientId = clientId;
			WSPrinterMapping ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getPrinterMapping(clientId, type, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getPrinterMapping(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-21 病区收药 已废止
	// / </summary>
	// / <param name="orderCode"></param>
	// / <param name="wardId"></param>
	// / <returns></returns>
	public static WSNursingOrder[] GetDispensedDrugByBatch(String batchNo, int lot, String autoNo, String wardId) throws Exception {
		try {
			getDispensedDrugByBatch request = new getDispensedDrugByBatch();
			request.batchNo = batchNo;
			request.lot = lot;
			request.autoNo = autoNo;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrder[] rets = null;

			getDispensedDrugByBatchResponse ret = new getDispensedDrugByBatchResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getDispensedDrugByBatch(batchNo, lot, autoNo, wardId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSNursingOrder.class);
					// if (ret != null)
					// rets = (WSNursingOrder[]) ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getDispensedDrugByBatch(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-27 根据条码Id查询该条码是否有对应的Intake纪录
	// / </summary>
	// / <param name="barcodeId"></param>
	// / <returns></returns>
	public static WSIntakeRecord GetIntakeByBarcode(String barcodeId) throws Exception {
		try {
			getIntakeByBarcode request = new getIntakeByBarcode();
			request.barcodeId = barcodeId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSIntakeRecord ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getIntakeByBarcode(barcodeId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getIntakeByBarcode(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-29 取消某处于暂停状态的医嘱
	// / </summary>
	// / <param name="orderId"></param>
	// / <param name="canceler"></param>
	public static void CancelWithhold(int orderId, WSUser canceler) throws Exception {
		try {
			cancelWithhold request = new cancelWithhold();
			request.orderId = orderId;
			request.canceler = canceler;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.cancelWithhold(orderId, canceler, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.cancelWithhold(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-29 暂停某药的使用，药物暂停后，还算有效医嘱，只不过该药不能被执行。
	// / 暂停分两种 1、暂停若干个dosage，到时间点自动激活
	// / 2、一直处于暂停状态，除非有人为干预
	// / </summary>
	// / <param name="orderId"></param>
	// / <param name="times">暂停次数，如果为-1，表示无限期停止，直到用户取消</param>
	// / <param name="oper"></param>
	public static void WithholdOrder(int orderId, int withholdDose, WSUser oper) throws Exception {
		try {
			withholdOrder request = new withholdOrder();
			request.orderId = orderId;
			request.withholdDose = withholdDose;
			request._operator = oper;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.withholdOrder(orderId, withholdDose, oper, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.withholdOrder(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-29 把药交由病人保管，该药不会在outstanding出现
	// / </summary>
	// / <param name="orders"></param>
	// / <param name="oper"></param>
	public static void GiveDrugToPatient(int orderId, String reason, WSUser oper) throws Exception {
		try {
			giveDrugToPatient request = new giveDrugToPatient();
			request.orderId = orderId;
			request.reason = reason;
			request._operator = oper;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.giveDrugToPatient(orderId, reason, oper, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.giveDrugToPatient(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / yunfei 2010-11-25 将给病人的药拿
	// / </summary>
	// / <param name="orderId"></param>
	// / <param name="reason"></param>
	// / <param name="oper"></param>
	public static void TakeBackDrug(int orderId, WSUser oper) throws Exception {
		try {
			takeBackDrug request = new takeBackDrug();
			request.orderId = orderId;
			request._operator = oper;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.takeBackDrug(orderId, oper, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.takeBackDrug(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-29
	// / </summary>
	// / <returns></returns>
	public static WSDictionaryItem[] GetToPatientRemarks() throws Exception {
		try {
			getToPatientRemarks request = new getToPatientRemarks();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getToPatientRemarksResponse ret = new getToPatientRemarksResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getToPatientRemarks(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSDictionaryItem.class);
					// if (ret != null)
					// rets = (WSDictionaryItem [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getToPatientRemarks(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-29
	// / </summary>
	// / <param name="orders"></param>
	// / <param name="stopper"></param>
	public static WSDictionaryItem[] GetMedicationUnits() throws Exception {
		try {
			getMedicationUnits request = new getMedicationUnits();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getMedicationUnitsResponse ret = new getMedicationUnitsResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getMedicationUnits(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSDictionaryItem.class);
					// if (ret != null)
					// rets = (WSDictionaryItem [])ret.toArray();
				} catch (Exception ex) {
					Log.e("EgretWS", "Error", ex);

					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getMedicationUnits(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-01-05 打印日志
	// / </summary>
	// / <param name="barcodeId"></param>
	// / <param name="recordId"></param>
	// / <param name="printerCode"></param>
	// / <param name="labelContent"></param>
	public static void AddAdminLabelPrintLog(String barcodeId, int recordId, String printerCode, String labelContent) throws Exception {
		try {
			addAdminLabelPrintLog request = new addAdminLabelPrintLog();
			request.barcodeId = barcodeId;
			request.recordId = recordId;
			request.printerCode = printerCode;
			request.labelContent = labelContent;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.addAdminLabelPrintLog(recordId, barcodeId, labelContent, printerCode, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.addAdminLabelPrintLog(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// /// <summary>
	// /// Wade 2009-12-4 取得某药物的信息，包括药品的基本信息(药品名称、剂量、规格等等)和状态等，增加了计划时间点的信息
	// /// </summary>
	// /// <param name="codes"></param>
	// /// <returns></returns>
	// public static WSNursingOrder GetOrderItemDetail(String[] codes)
	// {
	// try
	// {
	// getOrderItemDetail request = new getOrderItemDetail();
	// request.codes = codes;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// WSNursingOrder ret = null;
	// Retry(_retryTimes, request, delegate
	// {
	// ret = _egretService.getOrderItemDetail(request).@return;
	// });
	// return ret;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	// / <summary>
	// / 2010-10-25 yunfei 获取当前需要Repeat的药物列表
	// / </summary>
	// / <param name="wardId"></param>
	// / <param name="teamId"></param>
	// / <returns></returns>
	public static WSDrugRepeatInfo[] GetDrugsToRepeat(String wardId, String teamId) throws Exception {
		try {
			getDrugsToRepeat request = new getDrugsToRepeat();
			request.wardId = wardId;
			request.teamId = teamId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDrugRepeatInfo[] rets = null;

			getDrugsToRepeatResponse ret = new getDrugsToRepeatResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getDrugsToRepeat(wardId, teamId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSDrugRepeatInfo.class);
					// if (ret != null)
					// rets = (WSDrugRepeatInfo [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getDrugsToRepeat(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-10-25 yunfei 查询某条医嘱的Repeat记录
	// / </summary>
	// / <param name="wardId"></param>
	// / <param name="teamId"></param>
	// / <returns></returns>
	public static WSDrugRepeatInfo[] GetDrugRepeatHistory(int orderId, DateTime begin, DateTime end) throws Exception {
		try {
			getDrugRepeatHistory request = new getDrugRepeatHistory();
			request.beginTime = begin;
			// request.beginTimeSpecified = true;
			request.endTime = end;
			// request.endTimeSpecified = true;
			request.orderId = orderId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDrugRepeatInfo[] ret = null;

			getDrugRepeatHistoryResponse rets = new getDrugRepeatHistoryResponse();
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					rets = _egretService.getDrugRepeatHistory(orderId, begin, end, _cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(rets, WSDrugRepeatInfo.class);
					// if (rets != null)
					// ret = (WSDrugRepeatInfo [])rets.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getDrugRepeatHistory(request);
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-10-25 yunfei 完成对药物的Repeat操作
	// / </summary>
	// / <param name="wardId"></param>
	// / <param name="teamId"></param>
	// / <returns></returns>
	public static void RepeatDrug(WSUser opeNurse, String wardId, WSDrugRepeatInfo[] drugs) throws Exception {
		try {
			repeatDrug request = new repeatDrug();
			request._operator = opeNurse;
			request.wardId = wardId;
			request.drugsToRepeat = new ArrayList<WSDrugRepeatInfo>();
			for (WSDrugRepeatInfo drug : drugs) {
				request.drugsToRepeat.add(drug);
			}
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.repeatDrug(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.repeatDrug(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 根据orderId获取Plan time的设置
	// / </summary>
	// / <param name="wardId"></param>
	// / <param name="orderUniqCode"></param>
	// / <param name="freqCode"></param>
	// / <returns></returns>
	public static WSOrderPlanTimeConfig GetOrderPlanTimeConfig(String wardId, String orderUniqCode, String freqCode) throws Exception {
		try {
			getOrderPlanTimeConfig request = new getOrderPlanTimeConfig();
			request.orderUqCode = orderUniqCode;
			request.freqCode = freqCode;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSOrderPlanTimeConfig rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getOrderPlanTimeConfig(orderUniqCode, freqCode, wardId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getOrderPlanTimeConfig(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void RepeatOneDrug(WSUser opeNurse, String wardId, WSDrugRepeatInfo drug) throws Exception {
		try {
			repeatOneDrug request = new repeatOneDrug();
			request._operator = opeNurse;
			request.drugToRepeat = drug;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.repeatOneDrug(drug, opeNurse, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.repeatOneDrug(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-10-29 yunfei 获取一种药物可选的所有Route
	// / </summary>
	// / <param name="itemCode"></param>
	// / <returns></returns>
	public static WSOrderRoute[] GetItemRelatedRoute(String itemCode) throws Exception {
		try {
			getItemRelatedRoute request = new getItemRelatedRoute();
			request.itemCode = itemCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSOrderRoute[] rets = null;

			getItemRelatedRouteResponse ret = new getItemRelatedRouteResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getItemRelatedRoute(itemCode, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSOrderRoute.class);

					// if (ret!= null)
					// rets = (WSOrderRoute [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getItemRelatedRoute(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static int GetDefaultRepeatDose(String wardId, String freqCode) throws Exception {
		try {
			getDefaultRepeatDose request = new getDefaultRepeatDose();
			request.wardId = wardId;
			request.freqCode = freqCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			int rets = 0;

			for (int i = 0; i < _retryTimes && rets == 0; i++) {
				try {
					rets = _egretService.getDefaultRepeatDose(freqCode, wardId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getDefaultRepeatDose(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSOrderAdminIntervalInfo GetAdminIntervalInfoByOrder(String orderUqCode, DateTime planTime) throws Exception {
		try {
			getAdminIntervalInfoByOrder request = new getAdminIntervalInfoByOrder();
			request.orderUqCode = orderUqCode;
			// request.planTimeSpecified = true;
			request.planTime = planTime;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSOrderAdminIntervalInfo rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getAdminIntervalInfoByOrder(orderUqCode, planTime, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getAdminIntervalInfoByOrder(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSOrderAdminIntervalInfo[] GetAdminIntervalInfoByAdminRecord(String barcodeId) throws Exception {
		try {
			getAdminIntervalInfoByAdminRecord request = new getAdminIntervalInfoByAdminRecord();
			request.barcodeId = barcodeId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSOrderAdminIntervalInfo[] rets = null;

			getAdminIntervalInfoByAdminRecordResponse ret = new getAdminIntervalInfoByAdminRecordResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getAdminIntervalInfoByAdminRecord(barcodeId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSOrderAdminIntervalInfo.class);
					// if (ret!= null)
					// rets = (WSOrderAdminIntervalInfo [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getAdminIntervalInfoByAdminRecord(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static BigDecimal GetPatientLatestBW(String patientId, String admissionId, int series) throws Exception {
		try {
			getPatientLatestBW request = new getPatientLatestBW();
			request.admissionId = admissionId;
			request.patientId = patientId;
			request.series = series;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			BigDecimal rets = BigDecimal.ZERO;
			for (int i = 0; i < _retryTimes && rets == BigDecimal.ZERO; i++) {
				try {
					rets = _egretService.getPatientLatestBW(patientId, series, admissionId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getPatientLatestBW(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSAdminDosage[] GetAdminDosageForMDACalculation(String patientId, String admissionId, int series) throws Exception {
		try {
			getAdminDosageForMDACalculation request = new getAdminDosageForMDACalculation();
			request.patientId = patientId;
			request.admissionId = admissionId;
			request.series = series;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSAdminDosage[] rets = null;

			getAdminDosageForMDACalculationResponse ret = new getAdminDosageForMDACalculationResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getAdminDosageForMDACalculation(patientId, series, admissionId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSAdminDosage.class);
					// if (ret!= null)
					// rets = (WSAdminDosage [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getAdminDosageForMDACalculation(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSOverMaxDosageAlert[] CalOverMaximumDosageAlert(WSAdminDosage[] cur, WSAdminDosage[] history, BigDecimal weight, BigDecimal age) throws Exception {
		try {
			calOverMaximumDosageAlert request = new calOverMaximumDosageAlert();
			request.curDetailList = new ArrayList<WSAdminDosage>(Arrays.asList(cur));
			request.historyDetailList = new ArrayList<WSAdminDosage>(Arrays.asList(history));
			// request.bodyWeightSpecified = true;
			request.bodyWeight = weight;
			// request.ageSpecified = true;
			request.age = age;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSOverMaxDosageAlert[] rets = null;

			calOverMaximumDosageAlertResponse ret = new calOverMaximumDosageAlertResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.calOverMaximumDosageAlert(request);
					rets = GetObjectArrayFromResponse(ret, WSOverMaxDosageAlert.class);
					// if (ret!= null)
					// rets = (WSOverMaxDosageAlert [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.calOverMaximumDosageAlert(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingOrder[] GetPatientUngivenDischargeOrders(String patientId, String admissionId, int series) throws Exception {
		try {
			getPatientUngivenDischargeOrders request = new getPatientUngivenDischargeOrders();
			request.patientId = patientId;
			request.admissionId = admissionId;
			request.series = series;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrder[] rets = null;

			getPatientUngivenDischargeOrdersResponse ret = new getPatientUngivenDischargeOrdersResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getPatientUngivenDischargeOrders(patientId, series, admissionId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSNursingOrder.class);
					// if (ret!= null)
					// rets = (WSNursingOrder [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getPatientUngivenDischargeOrders(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void GiveDisDrugToPatient(String patientId, String admissionId, int series, String[] orderUqCodes, String operatorCode) throws Exception {
		try {
			giveDisDrugToPatient request = new giveDisDrugToPatient();
			request.patientId = patientId;
			request.admissionId = admissionId;
			request.series = series;
			request.orderUqCodes = new ArrayList<String>(Arrays.asList(orderUqCodes));
			request.operatorCode = operatorCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && !success; i++) {
				try {
					_egretService.giveDisDrugToPatient(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.giveDisDrugToPatient(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSDischargeDrugGivenRecord[] GetPatientDisDrugGivenRecords(String patientId, String admissionId, int series) throws Exception {
		try {
			getPatientDisDrugGivenRecords request = new getPatientDisDrugGivenRecords();
			request.patientId = patientId;
			request.admissionId = admissionId;
			request.series = series;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDischargeDrugGivenRecord[] rets = null;

			getPatientDisDrugGivenRecordsResponse ret = new getPatientDisDrugGivenRecordsResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getPatientDisDrugGivenRecords(patientId, series, admissionId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSDischargeDrugGivenRecord.class);
					// if (ret!= null)
					// rets = (WSDischargeDrugGivenRecord [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getPatientDisDrugGivenRecords(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// region Observation,Assement
	public static WSNursingRecordFormClientDef[] GetClientDefFormRecords(String patientId, int series, String admissionId, DateTime beginTime, DateTime endTime, formType type) throws Exception {
		try {
			getClientDefFormRecords request = new getClientDefFormRecords();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.formType = type;
			// request.formTypeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingRecordFormClientDef[] items = null;

			getClientDefFormRecordsResponse ret = new getClientDefFormRecordsResponse();
			// Steve comment: "templateCode" do not find in argument

			for (int i = 0; i < _retryTimes && items == null; i++) {
				try {
					ret = _egretService.getClientDefFormRecords(patientId, series, admissionId, "", type, beginTime, endTime, _cliCode, _updateNo);
					items = GetObjectArrayFromResponse(ret, WSNursingRecordFormClientDef.class);

					// if (ret!= null)
					// items = (WSNursingRecordFormClientDef [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			// Retry(_retryTimes, request, delegate
			// {
			// items = _egretService.getClientDefFormRecords(request);
			// });
			return items;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-11 按明细获得评估记录列表
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="templateCodes"></param>
	// / <param name="formType"></param>
	// / <param name="beginTime"></param>
	// / <param name="endTime"></param>
	// / <param name="cliCode"></param>
	// / <param name="updateNo"></param>
	// / <returns></returns>
	public static WSNursingRecordFormItem[] GetNursingRecords(String patientId, int series, String admissionId, String templateCodes, formType formType, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			getNursingRecords request = new getNursingRecords();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.templateCodes = templateCodes;
			request.formType = formType;
			// request.formTypeSpecified = true;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingRecordFormItem[] items = null;

			getNursingRecordsResponse ret = new getNursingRecordsResponse();
			// Steve comment: "templateCode" do not find in argument

			for (int i = 0; i < _retryTimes && items == null; i++) {
				try {
					ret = _egretService.getNursingRecords(patientId, series, admissionId, "", formType, beginTime, endTime, _cliCode, _updateNo);
					items = GetObjectArrayFromResponse(ret, WSNursingRecordFormItem.class);
					// if (ret!= null)
					// items = (WSNursingRecordFormItem [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// items = _egretService.getNursingRecords(request);
			// });
			return items;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static boolean IsClientDefTemplateInWard(String templateCode, String wardId) throws Exception {
		isClientDefTemplateInWard request = new isClientDefTemplateInWard();
		request.templateCode = templateCode;
		request.ward = wardId;
		request.cliCode = _cliCode;
		request.updateNo = _updateNo;
		boolean result = false;

		for (int i = 0; i < _retryTimes && result == false; i++) {
			try {
				result = _egretService.isClientDefTemplateInWard(templateCode, wardId, _cliCode, _updateNo);
			} catch (Exception ex) {
				
					throw MakeWSException(ex);
			}
		}

		// Retry(_retryTimes, request, delegate
		// {
		// result = _egretService.isClientDefTemplateInWard(request).@return;
		// });
		return result;
	}

	// / <summary>
	// / Wade 2009-10-11 获得当前病区模板列表
	// / </summary>
	// / <param name="wardId"></param>
	// / <param name="formType"></param>
	// / <param name="subFormType"></param>
	// / <returns></returns>
	public static WSNursingRecordForm[] GetFormTemplates(String wardId, formType formType, subFormType subFormType) throws Exception {
		try {
			getFormTemplates request = new getFormTemplates();
			request.wardId = wardId;
			request.formType = formType;
			// request.formTypeSpecified = true;
			request.subFormType = subFormType;
			// request.subFormTypeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingRecordForm[] items = null;

			getFormTemplatesResponse ret = new getFormTemplatesResponse();
			for (int i = 0; i < _retryTimes && items == null; i++) {
				try {
					ret = _egretService.getFormTemplates(wardId, formType, subFormType, _cliCode, _updateNo);
					items = GetObjectArrayFromResponse(ret, WSNursingRecordForm.class);
					// if (ret!= null)
					// items = (WSNursingRecordForm [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// items = _egretService.getFormTemplates(request);
			// });
			return items;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-11 获得具体一个模板的定义
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="templateCode"></param>
	// / <returns></returns>
	public static WSNursingRecordForm GetFormTemplateByCode(String patientId, int series, String admissionId, String templateCode) throws Exception {
		try {
			getFormTemplateByCode request = new getFormTemplateByCode();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.templateCode = templateCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingRecordForm item = null;

			for (int i = 0; i < _retryTimes && item == null; i++) {
				try {
					item = _egretService.getFormTemplateByCode(patientId, series, admissionId, templateCode, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// item = _egretService.getFormTemplateByCode(request).@return;
			// });
			return item;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-12 保存护理评估信息
	// / </summary>
	// / <param name="form"></param>
	public static WSNursingRecordFormItem[] SaveNursingRecordForm(WSNursingRecordForm form) throws Exception {
		try {
			saveNursingRecordForm request = new saveNursingRecordForm();
			request.wsForm = form;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingRecordFormItem[] items = null;

			saveNursingRecordFormResponse ret = new saveNursingRecordFormResponse();
			for (int i = 0; i < _retryTimes && items == null; i++) {
				try {
					ret = _egretService.saveNursingRecordForm(form, _cliCode, _updateNo);
					items = GetObjectArrayFromResponse(ret, WSNursingRecordFormItem.class);
					// if (ret!= null)
					// items = (WSNursingRecordFormItem [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// items = _egretService.saveNursingRecordForm(request);
			// });
			return items;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-12 根据计划时间得到护理评估历史记录
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="formTemplateCode"></param>
	// / <param name="planTime"></param>
	// / <returns></returns>
	public static WSNursingRecordForm[] GetRecordsByPlanTime(String patientId, int series, String admissionId, String formTemplateCode, DateTime planTime) throws Exception {
		try {
			getRecordsByPlanTime request = new getRecordsByPlanTime();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.formTemplateCode = formTemplateCode;
			request.planTime = planTime;
			// request.planTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingRecordForm[] retform = null;

			getRecordsByPlanTimeResponse ret = new getRecordsByPlanTimeResponse();
			for (int i = 0; i < _retryTimes && retform == null; i++) {
				try {
					ret = _egretService.getRecordsByPlanTime(patientId, series, admissionId, formTemplateCode, planTime, _cliCode, _updateNo);
					retform = GetObjectArrayFromResponse(ret, WSNursingRecordForm.class);
					// if (ret!= null)
					// retform = (WSNursingRecordForm [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// retform = _egretService.getRecordsByPlanTime(request);
			// });
			return retform;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-12 修改评估记录
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="records"></param>
	public static void UpdateNursingRecords(String patientId, int series, String admissionId, WSNursingRecordFormItem[] records) throws Exception {
		try {
			updateNursingRecords request = new updateNursingRecords();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.records = new ArrayList<WSNursingRecordFormItem>(Arrays.asList(records));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.updateNursingRecords(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.updateNursingRecords(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-17 得到一个ITEM的样式
	// / </summary>
	// / <param name="sectionId"></param>
	// / <param name="itemId"></param>
	// / <returns></returns>
	public static WSNursingRecordFormItem GetFormItem(int sectionId, int itemId) throws Exception {
		try {
			getFormItem request = new getFormItem();
			request.sectionId = sectionId;
			request.itemId = itemId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingRecordFormItem item = null;

			for (int i = 0; i < _retryTimes && item == null; i++) {
				try {
					item = _egretService.getFormItem(sectionId, itemId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			// Retry(_retryTimes, request, delegate
			// {
			// item = _egretService.getFormItem(request).@return;
			// });
			return item;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-24 增加病人的入量信息，需要校验该种入量是否已经录入
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="intakes"></param>
	public static void AddIntakes(String patientId, int series, String admissionId, WSIntakeRecord[] intakes) throws Exception {
		try {
			addIntakes request = new addIntakes();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.intakes = new ArrayList<WSIntakeRecord>(Arrays.asList(intakes));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.addIntakes(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.addIntakes(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-24 修改一次入量信息，需要校验录入人、入量操作明细信息
	// / </summary>
	// / <param name="wsIntake"></param>
	// / <param name="cliCode"></param>
	// / <param name="updateNo"></param>
	public static void UpdateIntake(WSIntakeRecord wsIntake) throws Exception {
		try {
			updateIntake request = new updateIntake();
			request.intake = wsIntake;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.updateIntake(wsIntake, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.updateIntake(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-24 取得某个病人某段时间的入量信息，包括总量
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="wardId"></param>
	// / <param name="beginTime"></param>
	// / <param name="endTime"></param>
	// / <returns></returns>
	public static WSIntakeInfo GetIntakeInfo(String patientId, int series, String admissionId, String wardId, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			getIntakeInfo request = new getIntakeInfo();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.wardId = wardId;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSIntakeInfo item = null;

			for (int i = 0; i < _retryTimes && item == null; i++) {
				try {
					item = _egretService.getIntakeInfo(patientId, series, admissionId, wardId, beginTime, endTime, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// item = _egretService.getIntakeInfo(request).@return;
			// });
			return item;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-24 保存出/入量的总量信息
	// / </summary>
	// / <param name="wsIOTotal"></param>
	public static void SaveIntakeOutTotal(WSIntakeOutTotal wsIOTotal) throws Exception {
		try {
			saveIntakeOutTotal request = new saveIntakeOutTotal();
			request.ioTotal = wsIOTotal;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.saveIntakeOutTotal(wsIOTotal, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.saveIntakeOutTotal(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-24 取得某个病人某段时间内的出量信息，包括出量总量
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="wardId"></param>
	// / <param name="beginTime"></param>
	// / <param name="endTime"></param>
	// / <returns></returns>
	public static WSOutInfo GetOutInfo(String patientId, int series, String admissionId, String wardId, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			getOutInfo request = new getOutInfo();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.wardId = wardId;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSOutInfo info = null;

			for (int i = 0; i < _retryTimes && info == null; i++) {
				try {
					info = _egretService.getOutInfo(patientId, series, admissionId, wardId, beginTime, endTime, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// info = _egretService.getOutInfo(request).@return;
			// });
			return info;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-24 增加病人的出量信息，对于同一时间点的同一种出量，不能重复录入
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="wsOuts"></param>
	public static void addOuts(String patientId, int series, String admissionId, WSOutRecord[] wsOuts) throws Exception {
		try {
			addOuts request = new addOuts();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.outs = new ArrayList<WSOutRecord>(Arrays.asList(wsOuts));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.addOuts(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.addOuts(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-24 更新一条出量纪录
	// / </summary>
	// / <param name="wsOut"></param>
	public static void UpdateOut(WSOutRecord wsOut) throws Exception {
		try {
			updateOut request = new updateOut();
			request.wsOut = wsOut;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.updateOut(wsOut, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.updateOut(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-24 查询某个时间点病人的出量信息
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="occurTime"></param>
	// / <returns></returns>
	public static WSOutRecord[] GetOutRecordByTime(String patientId, int series, String admissionId, DateTime occurTime) throws Exception {
		try {
			getOutRecordByTime request = new getOutRecordByTime();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.occurTime = occurTime;
			// request.occurTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSOutRecord[] records = null;

			getOutRecordByTimeResponse ret = new getOutRecordByTimeResponse();
			for (int i = 0; i < _retryTimes && records == null; i++) {
				try {
					ret = _egretService.getOutRecordByTime(patientId, series, admissionId, occurTime, _cliCode, _updateNo);
					records = GetObjectArrayFromResponse(ret, WSOutRecord.class);
					// if (ret!= null)
					// records = (WSOutRecord [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// records = _egretService.getOutRecordByTime(request);
			// });
			return records;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-26 取得出入量的字典信息
	// / </summary>
	// / <param name="type"></param>
	// / <param name="code"></param>
	// / <returns></returns>
	public static WSIODictionaryItem[] GetIODictionary(ioType type, String code, String wardId) throws Exception {
		try {
			getIODictionary request = new getIODictionary();
			request.ioType = type;
			// request.ioTypeSpecified = true;
			request.typeCode = code;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSIODictionaryItem[] items = null;

			getIODictionaryResponse ret = new getIODictionaryResponse();
			for (int i = 0; i < _retryTimes && items == null; i++) {
				try {
					ret = _egretService.getIODictionary(type, code, wardId, _cliCode, _updateNo);
					items = GetObjectArrayFromResponse(ret, WSIODictionaryItem.class);
					// if (ret!= null)
					// items = (WSIODictionaryItem [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// items = _egretService.getIODictionary(request);
			// });
			return items;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-10-30 得到Xray报告PDF文件
	// / </summary>
	// / <param name="xRayno"></param>
	// / <returns></returns>
	public static String GetXrayPdf(String xRayno) throws Exception {
		try {
			getXrayPdf request = new getXrayPdf();
			request.xRayno = xRayno;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			String ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getXrayPdf(xRayno, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getXrayPdf(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-1
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="type"></param>
	// / <param name="code"></param>
	// / <param name="beginTime"></param>
	// / <param name="endTime"></param>
	// / <returns></returns>
	public static WSNursingRecordForm[] GetNursingRecordForms(String patientId, int series, String admissionId, formType type, String code, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			getNursingRecordForms request = new getNursingRecordForms();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.formType = type;
			// request.formTypeSpecified = true;
			request.templateCode = code;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingRecordForm[] rets = null;

			getNursingRecordFormsResponse ret = new getNursingRecordFormsResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getNursingRecordForms(patientId, series, admissionId, code, type, beginTime, endTime, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSNursingRecordForm.class);
					// if (ret!= null)
					// rets = (WSNursingRecordForm [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getNursingRecordForms(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Arthur
	// / </summary>
	// / <param name="formId"></param>
	// / <returns></returns>
	public static boolean IsClientDefTemplate(int formId) throws Exception {
		isClientDefTemplate request = new isClientDefTemplate();
		request.formId = formId;
		request.cliCode = _cliCode;
		request.updateNo = _updateNo;
		boolean rest = false;

		for (int i = 0; i < _retryTimes && rest == false; i++) {
			try {
				rest = _egretService.isClientDefTemplate(formId, _cliCode, _updateNo);

			} catch (Exception ex) {
				
					throw MakeWSException(ex);
			}
		}

		// Retry(_retryTimes, request, delegate
		// {
		// rest = _egretService.isClientDefTemplate(request).@return;
		// });
		return rest;
	}

	public static WSNursingRecordFormClientDef GetClientDefFormRecordsById(int formId) throws Exception {
		getClientDefFormRecordsById request = new getClientDefFormRecordsById();
		request.formId = formId;
		request.cliCode = _cliCode;
		request.updateNo = _updateNo;
		WSNursingRecordFormClientDef rest = null;

		for (int i = 0; i < _retryTimes && rest == null; i++) {
			try {
				rest = _egretService.getClientDefFormRecordsById(formId, _cliCode, _updateNo);
			} catch (Exception ex) {
				
					throw MakeWSException(ex);
			}
		}

		// Retry(_retryTimes, request, delegate
		// {
		// rest = _egretService.getClientDefFormRecordsById(request).@return;
		// });
		return rest;
	}

	// / <summary>
	// / Wade 2009-11-1
	// / </summary>
	// / <param name="formId"></param>
	// / <param name="containInvalidRecords"></param>
	// / <returns></returns>
	public static WSNursingRecordForm GetNursingRecordForm(int formId, boolean containInvalidRecords) throws Exception {
		try {
			getNursingRecordForm request = new getNursingRecordForm();
			request.formId = formId;
			request.containInvalidRecords = containInvalidRecords;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingRecordForm ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getNursingRecordForm(formId, containInvalidRecords, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getNursingRecordForm(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-1
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="wsEvent"></param>
	public static void AddPatientEvent(String patientId, int series, String admissionId, WSPatientEvent wsEvent) throws Exception {
		try {
			addPatientEvent request = new addPatientEvent();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.wsEvent = wsEvent;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.addPatientEvent(patientId, series, admissionId, wsEvent, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.addPatientEvent(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-1
	// / </summary>
	// / <param name="wsEvent"></param>
	public static void DeletePatientEvent(WSPatientEvent wsEvent) throws Exception {
		try {
			deletePatientEvent request = new deletePatientEvent();
			request.wsEvent = wsEvent;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.deletePatientEvent(wsEvent, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.deletePatientEvent(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-1
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="beginTime"></param>
	// / <param name="endTime"></param>
	// / <param name="eventCode"></param>
	// / <returns></returns>
	public static WSPatientEvent[] GetPatientEvents(String patientId, int series, String admissionId, DateTime beginTime, DateTime endTime, String eventCode) throws Exception {
		try {
			getPatientEvents request = new getPatientEvents();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.eventCode = eventCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSPatientEvent[] rets = null;

			getPatientEventsResponse ret = new getPatientEventsResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getPatientEvents(patientId, series, admissionId, eventCode, beginTime, endTime, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSPatientEvent.class);
					// if (ret!= null)
					// rets = (WSPatientEvent [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getPatientEvents(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-1
	// / </summary>
	// / <returns></returns>
	public static WSDictionaryItem[] GetPatientEventDefs() throws Exception {
		try {
			Log.e("Alvin", "WSController GetPatientEventDefs");
			getPatientEventDefs request = new getPatientEventDefs();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;
			getPatientEventDefsResponse ret = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getPatientEventDefs(_cliCode, _updateNo);

					// if (ret!= null)
					// rets = (WSDictionaryItem [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			WSDictionaryItem ret_n = new WSDictionaryItem();
			ret_n.code = "";
			ret_n.name = "";

			if (ret == null) {
				rets = new WSDictionaryItem[1];
				rets[0] = ret_n;
			} else {
				ArrayList<WSDictionaryItem> array_t = new ArrayList<WSDictionaryItem>(ret);
				array_t.add(0, ret_n);
				rets = new WSDictionaryItem[array_t.size()];
				for (int i = 0; i < array_t.size(); i++) {
					rets[i] = array_t.get(i);
				}
				// rets = (WSDictionaryItem[]) array_t.toArray(rets);
			}

			return rets;

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getPatientEventDefs(request);
			// });
			//
			// ArrayList retlist = new ArrayList();
			// WSDictionaryItem ret = new WSDictionaryItem();
			// ret.code = "";
			// ret.name = "";
			// retlist.Add(ret);
			// if (rets != null)
			// {
			// foreach (WSDictionaryItem item in rets)
			// {
			// retlist.Add(item);
			// }
			// }
			//
			// WSDictionaryItem[] items = new WSDictionaryItem[retlist.Count];
			// retlist.CopyTo(items);
			//
			// return items;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-2
	// / </summary>
	// / <param name="wsEvent"></param>
	public static void UpdatePatientEvent(WSPatientEvent wsEvent) throws Exception {
		try {
			updatePatientEvent request = new updatePatientEvent();
			request.wsEvent = wsEvent;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.updatePatientEvent(wsEvent, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.updatePatientEvent(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-10
	// / </summary>
	// / <param name="groupId"></param>
	// / <returns></returns>
	public static WSIntakeRecord[] GetIntakeRecordsByGroupId(String groupId) throws Exception {
		try {
			getIntakeRecordsByGroupId request = new getIntakeRecordsByGroupId();
			request.groupId = groupId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSIntakeRecord[] rets = null;

			getIntakeRecordsByGroupIdResponse ret = new getIntakeRecordsByGroupIdResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getIntakeRecordsByGroupId(groupId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSIntakeRecord.class);
					// if (ret!= null)
					// rets = (WSIntakeRecord [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getIntakeRecordsByGroupId(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-19 删除护理评估项
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="recordId"></param>
	public static void DeleteNursingRecord(String patientId, int series, String admissionId, long recordId, String userCode) throws Exception {
		try {
			deleteNursingRecord request = new deleteNursingRecord();
			request.admissionId = admissionId;
			request.patientId = patientId;
			request.recordId = recordId;
			request.series = series;
			request.userCode = userCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.deleteNursingRecord(recordId, patientId, series, admissionId, userCode, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.deleteNursingRecord(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-19 删除入量记录
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="recordId"></param>
	// / <param name="userCode"></param>
	public static void DeleteIntakeRecord(String patientId, int series, String admissionId, int recordId, String userCode) throws Exception {
		try {
			deleteIntakeRecord request = new deleteIntakeRecord();
			request.admissionId = admissionId;
			request.patientId = patientId;
			request.recordId = recordId;
			request.series = series;
			request.userCode = userCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.deleteIntakeRecord(recordId, patientId, series, admissionId, userCode, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.deleteIntakeRecord(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-19 删除出量记录
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="recordId"></param>
	// / <param name="userCode"></param>
	public static void DeleteOutputRecord(String patientId, int series, String admissionId, int recordId, String userCode) throws Exception {
		try {
			deleteOutputRecord request = new deleteOutputRecord();
			request.admissionId = admissionId;
			request.patientId = patientId;
			request.recordId = recordId;
			request.series = series;
			request.userCode = userCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.deleteOutputRecord(recordId, patientId, series, admissionId, userCode, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.deleteOutputRecord(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-24 根据病人的Id取得某出院病人的信息
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <returns></returns>
	public static WSPatientVisit[] GetDischargedInPatientVisitByIdFromHIS(String patientId, int series, String admissionId) throws Exception {
		try {
			getDischargedInPatientVisitByIdFromHIS request = new getDischargedInPatientVisitByIdFromHIS();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSPatientVisit[] rets = null;

			getDischargedInPatientVisitByIdFromHISResponse ret = new getDischargedInPatientVisitByIdFromHISResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getDischargedInPatientVisitByIdFromHIS(patientId, series, admissionId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSPatientVisit.class);
					// if (ret!= null)
					// rets = (WSPatientVisit [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getDischargedInPatientVisitByIdFromHIS(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-11-24 取某段时间内出院的住院病人信息
	// / </summary>
	// / <param name="wardId"></param>
	// / <returns></returns>
	public static WSPatientVisit[] GetDischargedInPatientVisitFromHIS(String wardId, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			getDischargedInPatientVisitFromHIS request = new getDischargedInPatientVisitFromHIS();
			request.wardId = wardId;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSPatientVisit[] rets = null;

			getDischargedInPatientVisitFromHISResponse ret = new getDischargedInPatientVisitFromHISResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getDischargedInPatientVisitFromHIS(wardId, beginTime, endTime, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSPatientVisit.class);
					// if (ret!= null)
					// rets = (WSPatientVisit [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getDischargedInPatientVisitFromHIS(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-1-14 系统中报表的模板
	// / </summary>
	// / <param name="wardId"></param>
	// / <param name="type"></param>
	// / <param name="subType"></param>
	// / <returns></returns>
	public static WSReportTemplate[] GetReportTemplates(String wardId, reportType type, reportSubType subType) throws Exception {
		try {
			getReportTemplates request = new getReportTemplates();
			request.wardId = wardId;
			request.type = type;
			// request.typeSpecified = true;
			request.subType = subType;
			// request.subTypeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSReportTemplate[] rets = null;

			getReportTemplatesResponse ret = new getReportTemplatesResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getReportTemplates(wardId, type, subType, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSReportTemplate.class);

					// if (ret!= null)
					// rets = (WSReportTemplate [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getReportTemplates(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-1-14 生成PDF文件
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="wardId"></param>
	// / <param name="reportTemplateCode"></param>
	// / <param name="beginTime"></param>
	// / <param name="endTime"></param>
	// / <param name="printer"></param>
	// / <returns></returns>
	public static byte[] GenPDFVitalSignReport(String patientId, int series, String admissionId, String wardId, String reportTemplateCode, DateTime beginTime, DateTime endTime, String printer) throws Exception {
		try {
			genPDFVitalSignReport request = new genPDFVitalSignReport();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.wardId = wardId;
			request.reportTemplateCode = reportTemplateCode;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.printer = printer;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			byte[] rets = null;

			// genPDFVitalSignReportret = new genPDFVitalSignReportResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.genPDFVitalSignReport(patientId, series, admissionId, wardId, reportTemplateCode, beginTime, endTime, printer, _cliCode, _updateNo);
					// if (ret!= null)
					// rets = (bytes [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.genPDFVitalSignReport(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-2-4
	// / </summary>
	// / <param name="formId"></param>
	// / <param name="userCode"></param>
	public static void DeleteForm(int formId, String userCode) throws Exception {
		try {
			deleteForm request = new deleteForm();
			request.formId = formId;
			request.userCode = userCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.deleteForm(formId, userCode, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.deleteForm(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSDictionaryItem[] GetOutstandingTaskNoFollowUpRemarks() throws Exception {
		try {
			getOutstandingTaskNoFollowUpRemarks request = new getOutstandingTaskNoFollowUpRemarks();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getOutstandingTaskNoFollowUpRemarksResponse ret = new getOutstandingTaskNoFollowUpRemarksResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getOutstandingTaskNoFollowUpRemarks(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSDictionaryItem.class);

					// if (ret!= null)
					// rets = (WSDictionaryItem [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getOutstandingTaskNoFollowUpRemarks(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSDictionaryItem[] GetOutstandingTaskToBeDoneLaterRemarks() throws Exception {
		try {
			getOutstandingTaskToBeDoneLaterRemarks request = new getOutstandingTaskToBeDoneLaterRemarks();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getOutstandingTaskToBeDoneLaterRemarksResponse ret = new getOutstandingTaskToBeDoneLaterRemarksResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getOutstandingTaskToBeDoneLaterRemarks(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSDictionaryItem.class);
					// if (ret!= null)
					// rets = (WSDictionaryItem [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getOutstandingTaskToBeDoneLaterRemarks(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingRecordFormItem[] GetNursingRecordHistory(String admissionId, long RecordId, String patientId, int series) throws Exception {
		try {
			getNursingRecordHistory request = new getNursingRecordHistory();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			request.admissionId = admissionId;
			request.id = RecordId;
			// request.idSpecified=true;
			request.patientId = patientId;
			request.series = series;
			// request.seriesSpecified = true;

			WSNursingRecordFormItem[] rets = null;

			getNursingRecordHistoryResponse ret = new getNursingRecordHistoryResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getNursingRecordHistory(RecordId, patientId, series, admissionId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSNursingRecordFormItem.class);

					// if (ret!= null)
					// rets = (WSNursingRecordFormItem [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getNursingRecordHistory(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingRecordFormItem[] GetDeletedNursingRecords(String admissionId, String patientId, int series, DateTime startTime, DateTime endTime) throws Exception {
		try {
			getDeletedNursingRecords request = new getDeletedNursingRecords();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.admissionId = admissionId;
			request.patientId = patientId;
			request.series = series;
			// request.seriesSpecified = true;
			// request.startSpecified = true;
			request.start = startTime;
			// request.endSpecified = true;
			request.end = endTime;

			// WSNursingRecordFormItem[] rets = new WSNursingRecordFormItem[0];
			WSNursingRecordFormItem[] rets = null;

			getDeletedNursingRecordsResponse ret = new getDeletedNursingRecordsResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getDeletedNursingRecords(startTime, endTime, patientId, series, admissionId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(ret, WSNursingRecordFormItem.class);
					// if (ret!= null)
					// rets = (WSNursingRecordFormItem [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			if (rets == null)
				rets = new WSNursingRecordFormItem[0];

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getDeletedNursingRecords(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region Outstanding

	// / <summary>
	// / Wade 2009-12-3 获得任务分类的列表
	// / </summary>
	// / <returns>任务的枚举值（String型）列表</returns>
	// public static ArrayList<WSDictionaryItem[] GetOutstandingTaskType()
	// throws Exception
	public static ArrayList<WSDictionaryItem> GetOutstandingTaskType() throws Exception {
		try {
			getOutstandingTaskType request = new getOutstandingTaskType();
			ArrayList typelist = new ArrayList();
			String[] rets = null;

			// getOutstandingTaskTypeResponse ret = new
			// getOutstandingTaskTypeResponse();
			getOutstandingTaskTypeResponse ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getOutstandingTaskType();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			rets = GetObjectArrayFromResponse(ret, String.class);

			if (rets == null)
				rets = new String[0];

			// if (ret != null)
			// rets = (String[])ret.toArray();
			// else
			// rets = new String[0];

			for (int i = 0; i < rets.length; i++) {
				WSDictionaryItem item = new WSDictionaryItem();
				item.code = Integer.toString(i);
				item.name = rets[i];
				typelist.add(item);
			}

			// WSDictionaryItem[] items =
			// (WSDictionaryItem[])typelist.toArray();

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getOutstandingTaskType(request);
			// });
			// for (int i = 0; i < rets.Length; i++)
			// {
			// WSDictionaryItem item = new WSDictionaryItem();
			// item.code = i.ToString();
			// item.name = rets[i];
			// typelist.Add(item);
			// }
			//
			// WSDictionaryItem[] items = new WSDictionaryItem[typelist.Count];
			// typelist.CopyTo(items);

			// return items;
			return typelist;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-3 根据任务获得子任务的列表
	// / </summary>
	// / <param name="taskType">任务的枚举值（int值）</param>
	// / <returns>该任务下面包含的子任务（枚举的String值）</returns>
	public static ArrayList<WSDictionaryItem> GetOutstandingTaskSubType(int taskType) throws Exception {
		try {
			getOutstandingTaskSubType request = new getOutstandingTaskSubType();
			request.arg0 = taskType;
			ArrayList typelist = new ArrayList();
			String[] rets = null;

			// getOutstandingTaskSubTypeResponse ret = new
			// getOutstandingTaskSubTypeResponse();
			getOutstandingTaskSubTypeResponse ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getOutstandingTaskSubType(taskType);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			if (ret != null)
				rets = GetObjectArrayFromResponse(ret, String.class);
			else
				rets = new String[0];

			for (int i = 0; i < rets.length; i++) {
				if (i == 0)
					continue;

				WSDictionaryItem item = new WSDictionaryItem();
				item.code = Integer.toString(i + 10 * taskType);
				item.name = rets[i];
				typelist.add(item);
			}

			// WSDictionaryItem[] items =
			// (WSDictionaryItem[])typelist.toArray();

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getOutstandingTaskSubType(request);
			// });
			// for (int i = 0; i < rets.Length; i++)
			// {
			// if (i == 0)
			// continue;
			//
			// WSDictionaryItem item = new WSDictionaryItem();
			// item.code = (i + 10 * taskType).ToString();
			// item.name = rets[i];
			// typelist.Add(item);
			// }
			//
			// WSDictionaryItem[] items = new WSDictionaryItem[typelist.Count];
			// typelist.CopyTo(items);

			// return items;
			return typelist;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 获取一个病区的Outstanding task
	// / </summary>
	// / <param name="startTime"></param>
	// / <param name="endTime"></param>
	// / <param name="wardId"></param>
	// / <param name="teamId"></param>
	// / <param name="userGroup"></param>
	// / <param name="taskType"></param>
	// / <param name="taskSubType"></param>
	// / <param name="taskStatus"></param>
	// / <returns></returns>
	public static WSNursingOutstandingTaskListPage GetHistoryOutstandingTasks(DateTime startTime, DateTime endTime, String wardId, String teamId, String userGroup, taskType taskType, taskSubType taskSubType, outstandingTaskStatus taskStatus, int startIndex, int pageSize) throws Exception {
		try {
			getHistoryOutstandingTasks request = new getHistoryOutstandingTasks();
			request.startTime = startTime;
			// request.startTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.wardId = wardId;
			request.teamId = teamId;
			request.userGroup = userGroup;
			// request.pageRequest = new wsPageRequest();
			// request.pageRequest.startIndex = startIndex;
			// request.pageRequest.pageSize = pageSize;

			wsPageRequest pageRequest = new wsPageRequest();
			pageRequest.startIndex = startIndex;
			pageRequest.pageSize = pageSize;

			if (taskType != null) {
				request.taskType = taskType;
				// request.taskTypeSpecified = true;
			} else {
				// request.taskTypeSpecified = false;
				request.taskType = null;
			}
			if (taskSubType != null) {
				request.taskSubType = taskSubType;
				// request.taskSubTypeSpecified = true;
			} else {
				// request.taskSubTypeSpecified = false;
				request.taskSubType = null;
			}
			// if (taskStatus != null)
			// {
			// request.taskStatus = taskStatus.Value;
			// request.taskStatusSpecified = true;
			// }
			// else
			// {
			// request.taskStatusSpecified = false;
			// }
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOutstandingTaskListPage rets = null;

			for (int i = 0; i < 1 && rets == null; i++) {
				rets = _egretService.getHistoryOutstandingTasks(startTime, endTime, wardId, teamId, userGroup, taskType, taskSubType, pageRequest, _cliCode, _updateNo);
			}

			if (rets == null) {
				rets = new WSNursingOutstandingTaskListPage();
				rets.nursingOutstandingTaskList = new ArrayList<WSNursingOutstandingTask>();
			}

			// Retry(1, request, delegate
			// {
			// rets = _egretService.getHistoryOutstandingTasks(request).@return;
			// });
			// if (rets.nursingOutstandingTaskList == null)
			// {
			// rets. = new WSNursingOutstandingTask[0];
			// }
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingOutstandingTaskListPage GetCurrentOutstandingTasksByPatient(WSPatientVisit pv, taskType type, taskSubType subType, String group, int startIndex, int pageSize) throws Exception {
		try {
			getCurrentOutstandingTasksByPatient request = new getCurrentOutstandingTasksByPatient();
			request.ipVisit = pv;
			if (type != null) {
				request.taskType = type;
				// request.taskTypeSpecified = true;
			}
			if (subType != null) {
				request.taskSubType = subType;
				// request.taskSubTypeSpecified = true;
			}
			// request.pageRequest = new wsPageRequest();
			// request.pageRequest.startIndex = startIndex;
			// request.pageRequest.pageSize = pageSize;
			wsPageRequest pageRequest = new wsPageRequest();
			pageRequest.startIndex = startIndex;
			pageRequest.pageSize = pageSize;

			request.userGroup = group;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOutstandingTaskListPage rets = null;
			
			Log.e("Mcs", "1type : " + type);
			Log.e("Mcs", "1subType : " + subType);
			Log.e("Mcs", "1group : " + group);
			Log.e("Mcs", "1startIndex : " + startIndex);
			Log.e("Mcs", "1pageSize : " + pageSize);
			Log.e("Mcs", "1_cliCode : " + _cliCode);
			Log.e("Mcs", "1updateNo : " + _updateNo);
			
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getCurrentOutstandingTasksByPatient(pv, group, type, subType, pageRequest, _cliCode, _updateNo);
					Log.e("Mcs", "1rets : " + rets.nursingOutstandingTaskList.size());
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			if (rets == null) {
				rets = new WSNursingOutstandingTaskListPage();
				rets.nursingOutstandingTaskList = new ArrayList<WSNursingOutstandingTask>();
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getCurrentOutstandingTasksByPatient(request).@return;
			// });
			// if (rets.nursingOutstandingTaskList == null)
			// {
			// rets.nursingOutstandingTaskList = new
			// WSNursingOutstandingTask[0];
			// }
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingOutstandingTaskListPage GetHistoryOutstandingTasksByPatient(WSPatientVisit pv, taskType type, taskSubType subType, DateTime start, DateTime end, String group, int startIndex, int pageSize) throws Exception {
		try {
			getHistoryOutstandingTasksByPatient request = new getHistoryOutstandingTasksByPatient();
			request.ipVisit = pv;
			if (end != null) {
				request.endTime = end;
				// request.endTimeSpecified = true;
			}
			if (start != null) {
				request.startTime = start;
				// request.startTimeSpecified = true;
			}
			if (type != null) {
				request.taskType = type;
				// request.taskTypeSpecified = true;
			}
			if (subType != null) {
				request.taskSubType = subType;
				// request.taskSubTypeSpecified = true;
			}
			request.userGroup = group;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			// request.pageRequest = new wsPageRequest();
			// request.pageRequest.startIndex = startIndex;
			// request.pageRequest.pageSize = pageSize;
			wsPageRequest pageRequest = new wsPageRequest();
			pageRequest.startIndex = startIndex;
			pageRequest.pageSize = pageSize;

			WSNursingOutstandingTaskListPage rets = new WSNursingOutstandingTaskListPage();

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getHistoryOutstandingTasksByPatient(start, end, pv, group, type, subType, pageRequest, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			if (rets == null) {
				rets = new WSNursingOutstandingTaskListPage();
				rets.nursingOutstandingTaskList = new ArrayList<WSNursingOutstandingTask>();
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getHistoryOutstandingTasksByPatient(request).@return;
			// });
			// if (rets.nursingOutstandingTaskList == null)
			// {
			// rets.nursingOutstandingTaskList = new
			// WSNursingOutstandingTask[0];
			// }
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingOutstandingTaskListPage GetCurrentOutstandingTasksByWard(String wardId, String teamId, taskType type, taskSubType subType, String group, int startIndex, int pageSize) throws Exception {
		try {
			getCurrentOutstandingTasksByWard request = new getCurrentOutstandingTasksByWard();
			request.wardId = wardId;
			request.teamId = teamId;
			// request.pageRequest = new wsPageRequest();
			// request.pageRequest.startIndex = startIndex;
			// request.pageRequest.pageSize = pageSize;
			wsPageRequest pageRequest = new wsPageRequest();
			pageRequest.startIndex = startIndex;
			pageRequest.pageSize = pageSize;
			if (type != null) {
				request.taskType = type;
				// request.taskTypeSpecified = true;
			}
			if (subType != null) {
				request.taskSubType = subType;
				// request.taskSubTypeSpecified = true;
			}
			request.userGroup = group;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOutstandingTaskListPage rets = null;

			for (int i = 0; i < 1 && rets == null; i++) {
				rets = _egretService.getCurrentOutstandingTasksByWard(wardId, teamId, group, type, subType, pageRequest, _cliCode, _updateNo);
			}

			if (rets == null) {
				rets = new WSNursingOutstandingTaskListPage();
				rets.nursingOutstandingTaskList = new ArrayList<WSNursingOutstandingTask>();
			}

			// Retry(1, request, delegate
			// {
			// rets =
			// _egretService.getCurrentOutstandingTasksByWard(request).@return;
			// });
			// if (rets.nursingOutstandingTaskList == null)
			// {
			// rets.nursingOutstandingTaskList = new
			// WSNursingOutstandingTask[0];
			// }
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingOutstandingTaskTotal HasOutstandingTasks(String wardId, String teamId, String userGroup) throws Exception {
		try {
			hasOutstandingTasks request = new hasOutstandingTasks();
			request.wardId = wardId;
			request.teamId = teamId;
			request.userGroup = userGroup;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOutstandingTaskTotal rets = null;

			for (int i = 0; i < 1; i++) {
				rets = _egretService.hasOutstandingTasks(wardId, teamId, userGroup, _cliCode, _updateNo);
			}

			// Retry(1, request, delegate
			// {
			// rets = _egretService.hasOutstandingTasks(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / ztm 2011-4-20
	// / </summary>
	// / <param name="autoNos"></param>
	// / <param name="clicode"></param>
	// / <param name="updateNo"></param>
	public static void SkipNextOrderDose(String[] orderUqCodes) throws Exception {
		try {
			skipNextOrderDose request = new skipNextOrderDose();
			// request.orderUqCodes = orderUqCodes;
			request.orderUqCodes = new ArrayList<String>(Arrays.asList(orderUqCodes));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			for (int i = 0; i < 1; i++) {
				_egretService.skipNextOrderDose(request);
			}

			// Retry(1, request, delegate
			// {
			// _egretService.skipNextOrderDose(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region Alert

	// / <summary>
	// / Wade 2009-12-23 跟据区域及任务类别来获取当前未做的alertTasks
	// / </summary>
	// / <param name="wardId">病区ID</param>
	// / <param name="teamId">病区内病人的分组ID（为null则条件无约束）</param>
	// / <param name="userGroup">用户组别 （为null则条件无约束）</param>
	// / <param name="alertType">警告类型（如果任务类型赋值小于0，则显示所有类型的任务）</param>
	// / <param name="alertSubType">警告子类型（如果任务子类型赋值小于0，则显示所有子类型的任务）</param>
	// / <returns>当前条件下未执行的任务列表</returns>
	public static WSNursingAlertTask[] GetMedicationUnreadAlertTask(String wardId, String teamId, String userGroup, String clientId) throws Exception {
		try {
			getUnreadAlertTask request = new getUnreadAlertTask();
			request.wardId = wardId;
			request.teamId = teamId;
			request.userGroup = userGroup;
			request.alertType = 0;
			request.alertSubType = -1;
			request.clientId = clientId;
			WSNursingAlertTask[] rets = null;

			getUnreadAlertTaskResponse ret = new getUnreadAlertTaskResponse();
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					ret = _egretService.getUnreadAlertTask(wardId, teamId, clientId, userGroup, 0, -1);
					rets = GetObjectArrayFromResponse(ret, WSNursingAlertTask.class);
					// if (ret != null)
					// rets = (WSNursingAlertTask [])ret.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getUnreadAlertTask(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingAlertTaskListPage GetObsAlertTask(String wardId, String teamId, String userGroup, String clientId, int startIndex, int pageSize, taskType type) throws Exception {
		try {
			getOverdueAndOntimeAlertTasks request = new getOverdueAndOntimeAlertTasks();
			request.wardId = wardId;
			request.teamId = teamId;
			request.alertType = type;
			// request.alertTypeSpecified = true;
			// request.pageRequest = new wsPageRequest();
			// request.pageRequest.startIndex = startIndex;
			// request.pageRequest.pageSize = pageSize;
			wsPageRequest pageRequest = new wsPageRequest();
			pageRequest.startIndex = startIndex;
			pageRequest.pageSize = pageSize;

			request.userGroup = userGroup;
			request.clientId = clientId;
			WSNursingAlertTaskListPage rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getOverdueAndOntimeAlertTasks(wardId, teamId, clientId, userGroup, pageRequest, type, null);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			if (rets.nursingAlertTaskList == null) {
				rets.nursingAlertTaskList = new ArrayList<WSNursingAlertTask>(0);
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getOverdueAndOntimeAlertTasks(request).@return;
			// });
			// if (rets.nursingAlertTaskList == null)
			// {
			// rets.nursingAlertTaskList = new WSNursingAlertTask[0];
			// }
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingAlertTaskListPage GetLabNonBloodAlertTask(String wardId, String teamId, String userGroup, String clientId, int startIndex, int pageSize, taskType type) throws Exception {
		try {
			getUnreadSpecimenIntervalAlerts request = new getUnreadSpecimenIntervalAlerts();
			request.wardId = wardId;
			request.teamId = teamId;
			request.alertType = type;
			// request.alertTypeSpecified = true;
			// request.pageRequest = new wsPageRequest();
			// request.pageRequest.startIndex = startIndex;
			// request.pageRequest.pageSize = pageSize;
			wsPageRequest pageRequest = new wsPageRequest();
			pageRequest.startIndex = startIndex;
			pageRequest.pageSize = pageSize;

			request.userGroup = userGroup;
			request.clientId = clientId;
			WSNursingAlertTaskListPage rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getUnreadSpecimenIntervalAlerts(wardId, teamId, clientId, userGroup, type, null, pageRequest, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			if (rets.nursingAlertTaskList == null) {
				rets.nursingAlertTaskList = new ArrayList<WSNursingAlertTask>(0);
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getUnreadSpecimenIntervalAlerts(request).@return;
			// });
			// if (rets.nursingAlertTaskList == null)
			// {
			// rets.nursingAlertTaskList = new WSNursingAlertTask[0];
			// }
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-23 根据筛选条件（从病区角度）或者所有的（包括已执行的）alertTasks 已废除
	// / </summary>
	// / <param name="startTime">搜索区域的开始时间</param>
	// / <param name="endTime">搜索区域的截止时间</param>
	// / <param name="wardId">病区ID</param>
	// / <param name="teamId">病区内病人的分组ID（为null则条件无约束）</param>
	// / <param name="userGroup">用户组别 （为null则条件无约束）</param>
	// / <param name="alertType">警告类型（如果任务类型赋值小于0，则显示所有类型的任务）</param>
	// / <param name="alertSubType">警告子类型（如果任务子类型赋值小于0，则显示所有子类型的任务）</param>
	// / <param name="taskStatus">任务状态（如果任务状态赋值小于0，则显示所有状态的任务）</param>
	// / <returns>当前条件下所有的任务列表</returns>
	public static WSNursingAlertTask[] GetReadAlertTasks(DateTime startTime, DateTime endTime, String wardId, String teamId, String userGroup, int alertType, int alertSubType, int taskStatus) throws Exception {
		// try
		// {
		// getReadAlertTasks request = new getReadAlertTasks();
		// request.startTime = startTime;
		// request.startTimeSpecified = true;
		// request.endTime = endTime;
		// request.endTimeSpecified = true;
		// request.wardId = wardId;
		// request.teamId = teamId;
		// request.userGroup = userGroup;
		// request.alertType = alertType;
		// request.alertSubType = alertSubType;
		// request.taskStatus = taskStatus;
		// WSNursingAlertTask[] rets = null;
		// Retry(_retryTimes, request, delegate
		// {
		// rets = _egretService.getReadAlertTasks(request);
		// });
		// return rets;
		// }
		// catch (Exception ex)
		// {
		// throw MakeWSException(ex);
		// }
		return new WSNursingAlertTask[0];
	}

	// / <summary>
	// / Wade 2009-12-23 获得任务分类的列表
	// / </summary>
	// / <returns>任务的枚举值（String型）列表</returns>
	public static String[] GetAlertType() throws Exception {
		try {
			getAlertType request = new getAlertType();
			String[] rets = null;

			getAlertTypeResponse response = null;
			for (int i = 0; i < _retryTimes && response == null; i++) {
				try {
					response = _egretService.getAlertType();
					rets = GetObjectArrayFromResponse(response, String.class);

					// if (response != null)
					// rets = (String [])response.toArray();

				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getAlertType(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2009-12-23 根据任务获得子任务的列表
	// / </summary>
	// / <param name="alertType">alertType 任务的枚举值（int值）</param>
	// / <returns>该任务下面包含的子任务（枚举的String值）</returns>
	public static String[] GetAlertSubType(int alertType) throws Exception {
		try {
			getAlertSubType request = new getAlertSubType();
			request.arg0 = alertType;
			String[] rets = null;

			getAlertSubTypeResponse response = null;
			for (int i = 0; i < _retryTimes && response == null; i++) {
				try {
					response = _egretService.getAlertSubType(alertType);
					rets = GetObjectArrayFromResponse(response, String.class);
					// if (response != null)
					// rets = (String [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getAlertSubType(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void ReadAlerts(int[] alertIds, WSUser user) throws Exception {
		try {
			readAlerts request = new readAlerts();
			request._operator = user;
			request.alertIds = new ArrayList<Integer>();

			for (int i = 0; i < alertIds.length; i++)
				request.alertIds.add(Integer.valueOf(alertIds[i]));

			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.readAlerts(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.readAlerts(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void ReadAlert(int alertId, WSUser user) throws Exception {
		try {
			readAlerts request = new readAlerts();
			request._operator = user;
			// request.alertIds = new int[1];
			// request.alertIds[0] = alertId;

			request.alertIds = new ArrayList<Integer>();
			request.alertIds.add(Integer.valueOf(alertId));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.readAlerts(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.readAlerts(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void DismissSpecimenIntervalAlerts(int alertId, WSUser user) throws Exception {
		try {
			dismissSpecimenIntervalAlerts request = new dismissSpecimenIntervalAlerts();
			request._operator = user;
			// request.alertIds = new int[1];
			// request.alertIds[0] = alertId;
			request.alertIds = new ArrayList<Integer>();
			request.alertIds.add(Integer.valueOf(alertId));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.dismissSpecimenIntervalAlerts(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.dismissSpecimenIntervalAlerts(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingAlertTotal HasUnreadAlerts(String wardId, String teamId, String userGroup, taskType alertType, taskSubType alertSubType, String clientId) throws Exception {
		try {
			hasUnreadAlerts request = new hasUnreadAlerts();
			if (alertType != null) {
				request.alertType = alertType;
				// request.alertTypeSpecified = true;
			}
			if (alertSubType != null) {
				request.alertSubType = alertSubType;
				// request.alertSubTypeSpecified = true;
			}
			request.clientId = clientId;
			request.teamId = teamId;
			request.userGroup = userGroup;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingAlertTotal rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.hasUnreadAlerts(wardId, teamId, clientId, userGroup, alertType, alertSubType, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.hasUnreadAlerts(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingAlertTaskListPage GetTotalAbnormalAlerts(DateTime beginTime, DateTime endTime, taskType alertType, taskSubType alertSubType, int startIndex, int pageSize) throws Exception {
		try {
			getTotalAbnormalAlerts request = new getTotalAbnormalAlerts();
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			if (alertType != null) {
				request.alertType = alertType;
				// request.alertTypeSpecified = true;
			}
			if (alertSubType != null) {
				request.alertSubType = alertSubType;
				// request.alertSubTypeSpecified = true;
			}
			request.pageRequest = new wsPageRequest();
			request.pageRequest.startIndex = startIndex;
			request.pageRequest.pageSize = pageSize;
			request.userGroup = null;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingAlertTaskListPage rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getTotalAbnormalAlerts(beginTime, endTime, null, alertType, alertSubType, request.pageRequest, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getTotalAbnormalAlerts(request).@return;
			// });

			if (rets == null)
				rets = new WSNursingAlertTaskListPage();

			if (rets.nursingAlertTaskList == null) {
				rets.nursingAlertTaskList = new ArrayList<WSNursingAlertTask>();
			}
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingAlertTaskListPage GetUnreadAbnormalAlerts(String wardId, String teamId, String userGroup, taskType alertType, taskSubType alertSubType, String clientId, int startIndex, int pageSize) throws Exception {
		try {
			wsPageRequest pageRequest = new wsPageRequest();
			pageRequest.startIndex = startIndex;
			pageRequest.pageSize = pageSize;

			WSNursingAlertTaskListPage rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getUnreadAbnormalAlerts(wardId, teamId, clientId, userGroup, alertType, alertSubType, pageRequest, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			if (rets.nursingAlertTaskList == null) {
				rets.nursingAlertTaskList = new ArrayList<WSNursingAlertTask>(0);
			}

			// getUnreadAbnormalAlerts request = new getUnreadAbnormalAlerts();
			// request.wardId = wardId;
			// request.teamId = teamId;
			// request.userGroup = userGroup;
			// if (alertType != null)
			// {
			// request.alertType = alertType.Value;
			// request.alertTypeSpecified = true;
			// }
			// if (alertSubType != null)
			// {
			// request.alertSubType = alertSubType.Value;
			// request.alertSubTypeSpecified = true;
			// }
			// request.pageRequest = new wsPageRequest();
			// request.pageRequest.startIndex = startIndex;
			// request.pageRequest.pageSize = pageSize;
			// request.clientId = clientId;
			// request.cliCode = _cliCode;
			// request.updateNo = _updateNo;
			// WSNursingAlertTaskListPage rets = null;
			// Retry(1, request, delegate
			// {
			// rets = _egretService.getUnreadAbnormalAlerts(request).@return;
			// });
			// if (rets.nursingAlertTaskList == null)
			// {
			// rets.nursingAlertTaskList = new WSNursingAlertTask[0];
			// }
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingAlertTaskListPage GetReadAbnormalAlerts(DateTime startTime, DateTime endTime, String wardId, String teamId, String userGroup, taskType alertType, taskSubType alertSubType, String clientId, int startIndex, int pageSize) throws Exception {
		try {

			wsPageRequest pageRequest = new wsPageRequest();
			pageRequest.startIndex = startIndex;
			pageRequest.pageSize = pageSize;

			WSNursingAlertTaskListPage rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getReadAbnormalAlerts(startTime, endTime, wardId, teamId, clientId, userGroup, alertType, alertSubType, pageRequest, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			if (rets.nursingAlertTaskList == null) {
				rets.nursingAlertTaskList = new ArrayList<WSNursingAlertTask>(0);
			}

			// getReadAbnormalAlerts request = new getReadAbnormalAlerts();
			// request.startTime = startTime;
			// request.startTimeSpecified = true;
			// request.endTime = endTime;
			// request.endTimeSpecified = true;
			// request.wardId = wardId;
			// request.teamId = teamId;
			// request.userGroup = userGroup;
			// if (alertType != null)
			// {
			// request.alertType = alertType.Value;
			// request.alertTypeSpecified = true;
			// }
			// if (alertSubType != null)
			// {
			// request.alertSubType = alertSubType.Value;
			// request.alertSubTypeSpecified = true;
			// }
			// request.pageRequest = new wsPageRequest();
			// request.pageRequest.startIndex = startIndex;
			// request.pageRequest.pageSize = pageSize;
			// request.clientId = clientId;
			// request.cliCode = _cliCode;
			// request.updateNo = _updateNo;
			// WSNursingAlertTaskListPage rets = null;
			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getReadAbnormalAlerts(request).@return;
			// });
			// if (rets.nursingAlertTaskList == null)
			// {
			// rets.nursingAlertTaskList = new WSNursingAlertTask[0];
			// }
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// ztm 2011-2-24 关于Offdrug Request的相关接口
	public static WSNursingAlertTaskListPage GetUnreadOffDrugRequestAlerts(String wardId, String teamId, String userGroup, String clientId, int startIndex, int pageSize) throws Exception {
		try {
			wsPageRequest pageRequest = new wsPageRequest();
			pageRequest.startIndex = startIndex;
			pageRequest.pageSize = pageSize;

			WSNursingAlertTaskListPage rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getUnreadOffDrugRequestAlerts(wardId, teamId, clientId, userGroup, pageRequest, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			if (rets.nursingAlertTaskList == null) {
				rets.nursingAlertTaskList = new ArrayList<WSNursingAlertTask>(0);
			}

			// getUnreadOffDrugRequestAlerts request = new
			// getUnreadOffDrugRequestAlerts();
			//
			// WSNursingAlertTaskListPage rets = null;
			// request.wardId = wardId;
			// request.teamId = teamId;
			// request.userGroup = userGroup;
			// request.pageRequest = new wsPageRequest();
			// request.pageRequest.startIndex = startIndex;
			// request.pageRequest.pageSize = pageSize;
			// request.clientId = clientId;
			// request.cliCode = _cliCode;
			// request.updateNo = _updateNo;
			//
			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getUnreadOffDrugRequestAlerts(request).@return;
			// });
			// if (rets.nursingAlertTaskList == null)
			// {
			// rets.nursingAlertTaskList = new WSNursingAlertTask[0];
			// }
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingAlertTaskListPage GetReadOffDrugRequestAlerts(DateTime startTime, DateTime endTime, String wardId, String teamId, String userGroup, String clientId, int startIndex, int pageSize) throws Exception {
		try {
			wsPageRequest pageRequest = new wsPageRequest();
			pageRequest.startIndex = startIndex;
			pageRequest.pageSize = pageSize;

			WSNursingAlertTaskListPage rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getReadOffDrugRequestAlerts(wardId, teamId, clientId, userGroup, startTime, endTime, pageRequest, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			if (rets.nursingAlertTaskList == null) {
				rets.nursingAlertTaskList = new ArrayList<WSNursingAlertTask>(0);
			}

			// getReadOffDrugRequestAlerts request = new
			// getReadOffDrugRequestAlerts();
			// request.startTime = startTime;
			// request.startTimeSpecified = true;
			// request.endTime = endTime;
			// request.endTimeSpecified = true;
			// request.wardId = wardId;
			// request.teamId = teamId;
			// request.userGroup = userGroup;
			// request.pageRequest = new wsPageRequest();
			// request.pageRequest.startIndex = startIndex;
			// request.pageRequest.pageSize = pageSize;
			// request.clientId = clientId;
			// request.cliCode = _cliCode;
			// request.updateNo = _updateNo;
			//
			// WSNursingAlertTaskListPage rets = null;
			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getReadOffDrugRequestAlerts(request).@return;
			// });
			// if (rets.nursingAlertTaskList == null)
			// {
			// rets.nursingAlertTaskList = new WSNursingAlertTask[0];
			// }
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void ReadOffDrugRequest(String requestId, WSUser user) throws Exception {
		try {
			readOffDrugRequest request = new readOffDrugRequest();
			request._operator = user;
			request.requestId = requestId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.readOffDrugRequest(requestId, user, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.readOffDrugRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region Lab

	// / <summary>
	// / Wade 2010-1-6 取得病区化验请求的汇总信息(特指未完成的)
	// / </summary>
	// / <returns></returns>
	public static WSLabRequestWardSummary[] GetWardLabRequestSummary() throws Exception {
		try {
			getWardLabRequestSummary request = new getWardLabRequestSummary();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabRequestWardSummary[] rets = null;

			getWardLabRequestSummaryResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getWardLabRequestSummary(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSLabRequestWardSummary.class);

					// if (response != null)
					// rets = (WSLabRequestWardSummary[])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getWardLabRequestSummary(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-1-6 取得病区的化验请求的信息(特指未完成的)
	// / </summary>
	// / <param name="wardId"></param>
	// / <returns></returns>
	public static WSLabRequest[] GetWardLabRequests(String wardId) throws Exception {
		try {
			getWardLabRequests request = new getWardLabRequests();
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabRequest[] rets = null;

			getWardLabRequestsResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getWardLabRequests(wardId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSLabRequest.class);

					// if (response != null)
					// rets = (WSLabRequest[])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getWardLabRequests(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 根据WardId获取该Ward里可供选用的所有打印机 gt 2011-10-20
	// / </summary>
	// / <param name="wardId"></param>
	// / <returns></returns>
	public static WSPrinterMapping[] GetPrinterMappingByWard(String clientId, String wardId) throws Exception {
		try {
			getPrinterMappingByWard request = new getPrinterMappingByWard();
			request.clientId = clientId;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSPrinterMapping[] rets = null;

			getPrinterMappingByWardResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getPrinterMappingByWard(clientId, wardId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSPrinterMapping.class);

					// if (response != null)
					// rets = (WSPrinterMapping[])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getPrinterMappingByWard(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-1-10
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <returns></returns>
	public static WSLabRequest GetPatientLabRequest(String patientId, int series, String admissionId) throws Exception {
		try {
			getPatientLabRequest request = new getPatientLabRequest();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabRequest ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getPatientLabRequest(patientId, series, admissionId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getPatientLabRequest(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-1-6 对已完成的样本采集的化验请求进行确认
	// / </summary>
	// / <param name="specCollection"></param>
	public static void ConfirmSpecimenCollection(String patientId, int series, String admissionId, WSLabSpecimenCollectionDetail[] collectionDetail, String confirmStaff, String wardId) throws Exception {
		try {
			confirmSpecimenCollection request = new confirmSpecimenCollection();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.collectionDetails = new ArrayList<WSLabSpecimenCollectionDetail>(Arrays.asList(collectionDetail));
			request.confirmStaff = confirmStaff;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.confirmSpecimenCollection(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.confirmSpecimenCollection(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-1-7
	// / </summary>
	// / <param name="specDetails"></param>
	public static void DoSpecimenCollection(String patientId, int series, String admissionId, WSLabSpecimenCollectionDetail[] collectionDetail, String collectorCode, String wardId) throws Exception {
		try {
			doSpecimenCollection request = new doSpecimenCollection();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.collectionDetails = new ArrayList<WSLabSpecimenCollectionDetail>(Arrays.asList(collectionDetail));
			request.collectorCode = collectorCode;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.doSpecimenCollection(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.doSpecimenCollection(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-1-11 处理样本采集过程中出现的异常
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="collectionDetail"></param>
	// / <param name="collectorCode"></param>
	// / <param name="wardId"></param>
	public static void DoAbnormalSpecimenCollection(String patientId, int series, String admissionId, WSLabSpecimenCollectionDetail[] collectionDetail, String collectorCode, String reason, String wardId) throws Exception {
		try {
			doAbnormalSpecimenCollection request = new doAbnormalSpecimenCollection();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.collectionDetails = new ArrayList<WSLabSpecimenCollectionDetail>(Arrays.asList(collectionDetail));
			request.collectorCode = collectorCode;
			request.reason = reason;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.doAbnormalSpecimenCollection(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.doAbnormalSpecimenCollection(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-1-11 取得病区的样本采集纪录信息
	// / </summary>
	// / <param name="wardId"></param>
	// / <param name="beginTime"></param>
	// / <param name="endTime"></param>
	// / <returns></returns>
	public static WSLabSpecimenCollection[] GetWardSpecimenCollections(String wardId, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			getWardSpecimenCollections request = new getWardSpecimenCollections();
			request.wardId = wardId;
			request.beginTime = beginTime;
			request.endTime = endTime;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			WSLabSpecimenCollection[] rets = null;

			getWardSpecimenCollectionsResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getWardSpecimenCollections(wardId, beginTime, endTime, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSLabSpecimenCollection.class);
					// if (response != null)
					// rets = (WSLabSpecimenCollection[])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getWardSpecimenCollections(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-1-12 取得样本采集的历史明细信息
	// / </summary>
	// / <param name="collectionId"></param>
	// / <returns></returns>
	public static WSLabSpecimenCollectionDetail[] GetSpecimenCollectionDetails(int collectionId) throws Exception {
		try {
			getSpecimenCollectionDetails request = new getSpecimenCollectionDetails();
			request.collectionId = collectionId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabSpecimenCollectionDetail[] rets = null;

			getSpecimenCollectionDetailsResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getSpecimenCollectionDetails(collectionId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSLabSpecimenCollectionDetail.class);

					// if (response != null)
					// rets =
					// (WSLabSpecimenCollectionDetail[])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getSpecimenCollectionDetails(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-1-13
	// / </summary>
	// / <returns></returns>
	public static WSDictionaryItem[] GetAbnormalSpecimenCollectionRemarks() throws Exception {
		try {
			getAbnormalSpecimenCollectionRemarks request = new getAbnormalSpecimenCollectionRemarks();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getAbnormalSpecimenCollectionRemarksResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getAbnormalSpecimenCollectionRemarks(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSDictionaryItem.class);

					// if (response != null)
					// rets = (WSDictionaryItem [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getAbnormalSpecimenCollectionRemarks(request);
			// });

			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-1-12 取得样本采集的历史明细信息
	// / </summary>
	// / <param name="collectionId"></param>
	// / <returns></returns>
	public static WSLabSpecimenCollectionDetail[] GetPatientSpecimenCollections(String patientId, int series, String admissionId, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			getPatientSpecimenCollections request = new getPatientSpecimenCollections();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.endTime = endTime;
			// request.endTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabSpecimenCollectionDetail[] rets = null;

			getPatientSpecimenCollectionsResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getPatientSpecimenCollections(patientId, series, admissionId, beginTime, endTime, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSLabSpecimenCollectionDetail.class);

					// if (response != null)
					// rets = (WSLabSpecimenCollectionDetail
					// [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getPatientSpecimenCollections(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-17 取得从某个时间点开始最近一次抽血的信息
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="beginTime"></param>
	// / <returns></returns>
	public static WSLabSpecimenCollectionDetail GetLatestConfirmedSpecimenCollection(String patientId, int series, String admissionId, DateTime beginTime) throws Exception {
		try {
			getLatestConfirmedSpecimenCollection request = new getLatestConfirmedSpecimenCollection();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.beginTime = beginTime;
			// request.beginTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabSpecimenCollectionDetail ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getLatestConfirmedSpecimenCollection(patientId, series, admissionId, beginTime, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret =
			// _egretService.getLatestConfirmedSpecimenCollection(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei 获取一个病人的Outstanding lab request列表
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="beginTime"></param>
	// / <returns></returns>
	public static WSLabRequestMain[] GetOutstandingLabRequestList(String patientId) throws Exception {
		try {
			getOutstandingLabRequestList request = new getOutstandingLabRequestList();
			request.patientId = patientId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabRequestMain[] ret = null;

			getOutstandingLabRequestListResponse response = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getOutstandingLabRequestList(patientId, _cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(response, WSLabRequestMain.class);

					// if (response != null)
					// ret = (WSLabRequestMain [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// ret = _egretService.getOutstandingLabRequestList(request);
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei 取得某个病人的submitted状态lab request列表
	// / </summary>
	// / <param name="patientId"></param>
	// / <returns></returns>
	public static WSLabRequestMain[] GetSubmittedLabRequestList(String patientId) throws Exception {
		try {
			getSubmittedLabRequestList request = new getSubmittedLabRequestList();
			request.patientId = patientId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabRequestMain[] ret = null;
				
			getSubmittedLabRequestListResponse response = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getSubmittedLabRequestList(patientId, _cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(response, WSLabRequestMain.class);

					// if (response != null)
					// ret = (WSLabRequestMain [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getSubmittedLabRequestList(request);
			// });
			if (ret == null){
				return new WSLabRequestMain[0];
			}
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei 根据lab request no获取这个request包含的所有service信息
	// / </summary>
	// / <param name="labRequestNo"></param>
	// / <param name="isSubmitted"></param>
	// / <returns></returns>
	public static WSLabRequestService[] GetLabRequestServiceListByLabRequestNo(String labRequestNo, boolean isSubmitted) throws Exception {
		try {
			getLabRequestServiceListByLabRequestNo request = new getLabRequestServiceListByLabRequestNo();
			request.labRequestNo = labRequestNo;
			request.isSubmitted = isSubmitted;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabRequestService[] ret = null;

			getLabRequestServiceListByLabRequestNoResponse response = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getLabRequestServiceListByLabRequestNo(labRequestNo, isSubmitted, _cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(response, WSLabRequestService.class);

					// if (response != null)
					// ret = (WSLabRequestService [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret =
			// _egretService.getLabRequestServiceListByLabRequestNo(request);
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei 获取所有可选的service
	// / </summary>
	// / <returns></returns>
	public static WSLabRequestService[] GetAllLabRequestServiceList() throws Exception {
		try {
			getLabRequestServiceList request = new getLabRequestServiceList();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabRequestService[] ret = null;

			getLabRequestServiceListResponse response = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getLabRequestServiceList(_cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(response, WSLabRequestService.class);

					// if (response != null)
					// ret = (WSLabRequestService [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			for (int i = 0; i < ret.length; i++) {
				if (ret[i].servieDesc == null || ret[i].servieDesc.isEmpty() || ret[i].servieDesc.equalsIgnoreCase(" "))
					ret[i].servieDesc = ret[i].serviceNo;
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getLabRequestServiceList(request);
			// });
			// foreach (var item in ret)
			// {
			// if (item.servieDesc == null || item.servieDesc == "" ||
			// item.servieDesc == " ")
			// {
			// item.servieDesc = item.serviceNo;
			// }
			// }
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei 创建一条lab request并保存为Outstanding状态
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="serviceList"></param>
	// / <param name="creator"></param>
	public static void CreateLabRequest(String patientId, WSLabRequestService[] serviceList, WSUser creator) throws Exception {
		try {
			createLabRequest request = new createLabRequest();
			request.creator = creator;
			request.patientId = patientId;
			request.serviceList = new ArrayList<WSLabRequestService>(Arrays.asList(serviceList));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.createLabRequest(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.createLabRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei 创建一条lab request并提交为submitted状态
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="reportType"></param>
	// / <param name="techNote"></param>
	// / <param name="sendToAC"></param>
	// / <param name="sampleTime"></param>
	// / <param name="fastRand"></param>
	// / <param name="oldSample"></param>
	// / <param name="otherInstruction"></param>
	// / <param name="serviceList"></param>
	// / <param name="creator"></param>
	public static void CreateAndSubmitLabRequest(String patientId, String reportType, String techNote, DateTime sampleTime, String fastRand, boolean oldSample, String otherInstruction, WSLabRequestService[] serviceList, WSUser creator, WSUser checker) throws Exception {
		try {
			createAndSubmitLabRequest request = new createAndSubmitLabRequest();
			request.patientId = patientId;
			request.reportType = reportType;
			request.techNote = techNote;
			if (sampleTime != null) {
				request.sampleTime = sampleTime;
				// request.sampleTimeSpecified = true;
			} else {
				// request.sampleTimeSpecified = false;
			}
			request.fastRand = fastRand;
			request.oldSample = oldSample;
			// request.oldSampleSpecified = true;
			request.otherInstruction = otherInstruction;
			request.serviceCodeList = new ArrayList<WSLabRequestService>(Arrays.asList(serviceList));
			request.creator = creator;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.checker = checker.code;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.createAndSubmitLabRequest(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.createAndSubmitLabRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei submit一条Outstanding lab request
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="reportType"></param>
	// / <param name="techNote"></param>
	// / <param name="sendToAC"></param>
	// / <param name="sampleTime"></param>
	// / <param name="fastRand"></param>
	// / <param name="oldSample"></param>
	// / <param name="otherInstruction"></param>
	// / <param name="serviceList"></param>
	// / <param name="user"></param>
	public static void SubmitLabRequest(String labRequest, String patientId, String reportType, String techNote, DateTime sampleTime, String fastRand, boolean oldSample, String otherInstruction, WSLabRequestService[] serviceList, WSUser user, WSUser checker) throws Exception {
		try {
			submitLabRequest request = new submitLabRequest();
			request.labRequestNo = labRequest;
			request.patientId = patientId;
			request.reportType = reportType;
			request.techNote = techNote;
			if (sampleTime != null) {
				request.sampleTime = sampleTime;
				// request.sampleTimeSpecified = true;
			} else {
				// request.sampleTimeSpecified = false;
			}
			request.fastRand = fastRand;
			request.oldSample = oldSample;
			// request.oldSampleSpecified = true;
			request.otherInstruction = otherInstruction;
			request.serviceCodeList = new ArrayList<WSLabRequestService>(Arrays.asList(serviceList));
			request.user = user;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.checker = checker.code;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.submitLabRequest(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.submitLabRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei Edit一条Outstanding lab request
	// / </summary>
	// / <param name="labRequestNo"></param>
	// / <param name="serviceList"></param>
	// / <param name="user"></param>
	public static void EditOutstandingLabRequest(String labRequestNo, WSLabRequestService[] serviceList, WSUser user) throws Exception {
		try {
			editOutstandingLabRequest request = new editOutstandingLabRequest();
			request.labRequestNo = labRequestNo;
			request.serviceCodeList = new ArrayList<WSLabRequestService>(Arrays.asList(serviceList));
			request.user = user;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.editOutstandingLabRequest(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.editOutstandingLabRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei Edit一条submitted状态的lab request
	// / </summary>
	// / <param name="labRequestNo"></param>
	// / <param name="reportType"></param>
	// / <param name="techNote"></param>
	// / <param name="sendToAC"></param>
	// / <param name="blooldTakeTime"></param>
	// / <param name="fastRand"></param>
	// / <param name="fastingStartTime"></param>
	// / <param name="dayTwoOrThree"></param>
	// / <param name="noPreparation"></param>
	// / <param name="oldSample"></param>
	// / <param name="user"></param>
	public static void EditSubmittedLabRequest(String labRequestNo, String reportType, String techNote, DateTime blooldTakeTime, String fastRand, boolean oldSample, String otherInstruction, WSLabRequestService[] serviceList, WSUser user, WSUser checker) throws Exception {
		try {
			editSubmittedLabRequest request = new editSubmittedLabRequest();
			request.labRequestNo = labRequestNo;
			request.reportType = reportType;
			request.techNote = techNote;
			request.fastRand = fastRand;
			if (blooldTakeTime != null) {
				request.sampleTime = blooldTakeTime;
				// request.sampleTimeSpecified = true;
			} else {
				// request.sampleTimeSpecified = false;
			}
			request.oldSample = oldSample;
			request.otherInstruction = otherInstruction;
			request.serviceCodeList = new ArrayList<WSLabRequestService>(Arrays.asList(serviceList));
			request.user = user;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.checker = checker.code;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.editSubmittedLabRequest(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.editSubmittedLabRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei 删除一条Outstanding lab request
	// / </summary>
	// / <param name="labRequestNo"></param>
	public static void DeleteOutstandingLabRequest(String labRequestNo) throws Exception {
		try {
			deleteOutstandingLabRequest request = new deleteOutstandingLabRequest();
			request.labRequestNo = labRequestNo;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.deleteOutstandingLabRequest(labRequestNo, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.deleteOutstandingLabRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei 删除一条submitted lab request
	// / </summary>
	// / <param name="labRequestNo"></param>
	public static void DeleteSubmittedLabRequest(String labRequestNo, WSUser countersignUser, WSUser currentUser) throws Exception {
		try {
			deleteSubmittedLabRequest request = new deleteSubmittedLabRequest();
			request.labRequestNo = labRequestNo;
			request.checker = countersignUser;
			request._operator = currentUser;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.deleteSubmittedLabRequest(labRequestNo, currentUser, countersignUser, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.deleteSubmittedLabRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei 根据service no获取service信息
	// / </summary>
	// / <param name="serviceNoList"></param>
	// / <returns></returns>
	public static WSLabRequestService[] GetLabRequestServiceListByServiceNo(String[] serviceNoList) throws Exception {
		try {
			getLabRequestServiceListByServiceNo request = new getLabRequestServiceListByServiceNo();
			request.serviceNoList = new ArrayList<String>(Arrays.asList(serviceNoList));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabRequestService[] rets = null;

			getLabRequestServiceListByServiceNoResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getLabRequestServiceListByServiceNo(request);
					rets = GetObjectArrayFromResponse(response, WSLabRequestService.class);

					// if (response != null)
					// rets = (WSLabRequestService [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets =
			// _egretService.getLabRequestServiceListByServiceNo(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// /// <summary>
	// /// 2010-11-12 yunfei 获取report location列表
	// /// </summary>
	// /// <returns></returns>
	// public static String[] GetReportLocationList()
	// {
	// try
	// {
	// getReportLocationList request = new getReportLocationList();
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// String[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.getReportLocationList(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	// / <summary>
	// / 2010-11-12 yunfei 根据service no获取service信息
	// / </summary>
	// / <param name="serviceNoList"></param>
	// / <returns></returns>
	public static String[] GetSpecimenList() throws Exception {
		try {
			getSpecimenList request = new getSpecimenList();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			String[] rets = null;

			getSpecimenListResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getSpecimenList(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, String.class);
					// if (response != null)
					// rets = (String [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getSpecimenList(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2011-10-17 guotao 根据labRequestNo获得一个display flag
	// / </summary>
	// / <param name="labRequestNo"></param>
	// / <returns></returns>
	public static String GetSendToAC(String labRequestNo) throws Exception {
		try {
			getSendToAC request = new getSendToAC();
			request.labRequestNo = labRequestNo;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			String rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getSendToAC(labRequestNo, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getSendToAC(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 2010-11-12 yunfei 根据service no获取service信息
	// / </summary>
	// / <param name="serviceNoList"></param>
	// / <returns></returns>
	public static DateTime[] GetBloodTakeTimeList() throws Exception {
		try {
			getBloodTakeTimeList request = new getBloodTakeTimeList();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			DateTime[] rets = null;

			getBloodTakeTimeListResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getBloodTakeTimeList(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, DateTime.class);

					// if (response != null)
					// rets = (DateTime [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getBloodTakeTimeList(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void GiveSpecimenContainers(String labRequestNo, WSLabRequestService[] services, WSUser executer) throws Exception {
		try {
			giveSpecimenContainers request = new giveSpecimenContainers();
			request.labRequestNo = labRequestNo;
			request.serviceList = new ArrayList<WSLabRequestService>(Arrays.asList(services));
			request.executer = executer;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.giveSpecimenContainers(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.giveSpecimenContainers(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void CollectSpecimenContainers(String labRequestNo, WSLabRequestService[] services, WSUser user) throws Exception {
		try {
			collectSpecimenContainers request = new collectSpecimenContainers();
			request.labRequestNo = labRequestNo;
			request.serviceList = new ArrayList<WSLabRequestService>(Arrays.asList(services));
			request.executer = user;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.collectSpecimenContainers(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.collectSpecimenContainers(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static String GetTestBarcodeId(WSLabRequestService[] serviceList) throws Exception {
		try {
			getTestBarcodeId request = new getTestBarcodeId();
			request.serviceList = new ArrayList<WSLabRequestService>(Arrays.asList(serviceList));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			String rets = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getTestBarcodeId(request);

				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getTestBarcodeId(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSLabRequestService[] GetTestInfoListByBarcodeId(String specimenUqCode) throws Exception {
		try {
			getTestInfoListByBarcodeId request = new getTestInfoListByBarcodeId();
			request.barcodeId = specimenUqCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSLabRequestService[] rets = null;

			getTestInfoListByBarcodeIdResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getTestInfoListByBarcodeId(specimenUqCode, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSLabRequestService.class);

					// if (response != null)
					// rets = (WSLabRequestService [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getTestInfoListByBarcodeId(request);
			// });

			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static String GetSpecimenTypeListByServiceNo(String serviceNo) throws Exception {
		try {
			getSpecimenTypeListByServiceNo request = new getSpecimenTypeListByServiceNo();
			request.serviceNo = serviceNo;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			String[] rets = null;

			getSpecimenTypeListByServiceNoResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getSpecimenTypeListByServiceNo(serviceNo, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, String.class);

					// if (response != null)
					// rets = (String [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getSpecimenTypeListByServiceNo(request);
			// });

			if (rets == null || rets.length < 1) {
				return "";
			} else if (rets.length == 1) {
				return rets[0];
			} else {
				return "Others";
			}
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static boolean DoBabyAndMotherMatching(String babyID, String motherID) throws Exception // Edit
																									// By
																									// Zyq
																									// 2012-09-17
																									// #6259
	{
		try {
			doBabyAndMotherMatching request = new doBabyAndMotherMatching();
			request.babyId = babyID;
			request.motherId = motherID;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			boolean rets = false;

			for (int i = 0; i < _retryTimes && rets == false; i++) {
				try {
					rets = _egretService.doBabyAndMotherMatching(babyID, motherID, _cliCode, _updateNo);

				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.doBabyAndMotherMatching(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static boolean CheckBabyIdExists(String babyID) throws Exception // Edit
																			// By
																			// Zyq
																			// 2012-09-17
																			// #6259
	{
		try {
			checkBabyIdExists request = new checkBabyIdExists();
			request.babyId = babyID;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			boolean rets = false;

			for (int i = 0; i < _retryTimes && rets == false; i++) {
				try {
					rets = _egretService.checkBabyIdExists(babyID, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.checkBabyIdExists(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region Blood Trans

	// / <summary>
	// / Wade 2010-5-22
	// / </summary>
	// / <param name="bloodId"></param>
	// / <param name="patientId"></param>
	// / <returns></returns>
	public static WSNursingBloodTransfusionRecord GetBloodProductInfoByBloodId(String wbn, String productCode, String patientId, String tds) throws Exception {
		try {
			getBloodProductInfoByBloodId request = new getBloodProductInfoByBloodId();
			request.wbn = wbn;
			request.productCode = productCode;
			request.patientId = patientId;
			// request.tds = tds;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingBloodTransfusionRecord ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getBloodProductInfoByBloodId(patientId, wbn, productCode, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret =
			// _egretService.getBloodProductInfoByBloodId(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-22
	// / </summary>
	// / <param name="barcodeId"></param>
	// / <returns></returns>
	public static WSNursingBloodTransfusionRecord GetBloodProductInfoByBarcodeId(String barcodeId) throws Exception {
		try {
			getBloodProductInfoByBarcodeId request = new getBloodProductInfoByBarcodeId();
			request.barcodeId = barcodeId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingBloodTransfusionRecord ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getBloodProductInfoByBarcodeId(barcodeId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret =
			// _egretService.getBloodProductInfoByBarcodeId(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-22
	// / </summary>
	// / <param name="bloodId"></param>
	// / <param name="plantime"></param>
	// / <param name="duration"></param>
	// / <param name="durationDesc"></param>
	// / <param name="rate"></param>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="wardId"></param>
	// / <param name="preparer"></param>
	// / <returns></returns>
	public static WSNursingBloodTransfusionRecord PrepareBloodProduct(String wbn, String productCode, DateTime plantime, int duration, String durationDesc, BigDecimal rate, String patientId, int series, String admissionId, String wardId, WSUser preparer, BigDecimal amount, String tds) throws Exception {
		try {
			prepareBloodProduct request = new prepareBloodProduct();
			request.wbn = wbn;
			request.productCode = productCode;
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.duration = duration;
			// request.durationSpecified = true;
			request.durationDesc = durationDesc;
			request.rate = rate;
			// request.rateSpecified = true;
			request.scheduledStartTime = plantime;
			// request.scheduledStartTimeSpecified = true;
			request.wardId = wardId;
			request.preparer = preparer;
			request.amount = amount;
			// request.amountSpecified = true;
			// request.tds = tds;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingBloodTransfusionRecord ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.prepareBloodProduct(patientId, series, admissionId, wbn, productCode, preparer, wardId, plantime, duration, durationDesc, rate, amount, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.prepareBloodProduct(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-22
	// / </summary>
	// / <param name="bloodId"></param>
	// / <param name="patientId"></param>
	// / <param name="wardId"></param>
	// / <param name="checker"></param>
	// / <returns></returns>
	public static WSNursingBloodTransfusionRecord CheckBloodProduct(String wbn, String productCode, String patientId, String wardId, WSUser checker, String tds) throws Exception {
		try {
			checkBloodProduct request = new checkBloodProduct();
			request.wbn = wbn;
			request.productCode = productCode;
			request.patientId = patientId;
			request.wardId = wardId;
			request.checker = checker;
			// request.tds = tds;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingBloodTransfusionRecord ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.checkBloodProduct(patientId, wbn, productCode, checker, wardId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.checkBloodProduct(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-22
	// / </summary>
	// / <param name="bloodId"></param>
	// / <param name="plantime"></param>
	// / <param name="duration"></param>
	// / <param name="durationDesc"></param>
	// / <param name="rate"></param>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="wardId"></param>
	// / <param name="preparer"></param>
	// / <param name="checker"></param>
	// / <returns></returns>
	public static WSNursingBloodTransfusionRecord PrepareAndCheckBloodProduct(String wbn, String productCode, DateTime plantime, int duration, String durationDesc, BigDecimal rate, String patientId, int series, String admissionId, String wardId, WSUser preparer, WSUser checker, BigDecimal amount, String tds) throws Exception {
		try {
			prepareAndCheckBloodProduct request = new prepareAndCheckBloodProduct();
			request.wbn = wbn;
			request.productCode = productCode;
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.duration = duration;
			// request.durationSpecified = true;
			request.durationDesc = durationDesc;
			request.rate = rate;
			// request.rateSpecified = true;
			request.scheduledStartTime = plantime;
			// request.scheduledStartTimeSpecified = true;
			request.wardId = wardId;
			request.preparer = preparer;
			request.checker = checker;
			request.amount = amount;
			// request.amountSpecified = true;
			// request.tds = tds;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingBloodTransfusionRecord ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.prepareAndCheckBloodProduct(patientId, series, admissionId, wbn, productCode, preparer, wardId, plantime, duration, durationDesc, rate, amount, checker, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.prepareAndCheckBloodProduct(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-22
	// / </summary>
	// / <param name="record"></param>
	// / <param name="giver"></param>
	public static void StartBloodTransfusion(WSNursingBloodTransfusionRecord record, WSUser giver, String wardId) throws Exception {
		try {
			startBloodTransfusion request = new startBloodTransfusion();
			request.record = record;
			request.giver = giver;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.startBloodTransfusion(record, giver, wardId, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.startBloodTransfusion(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-22
	// / </summary>
	// / <param name="barcodeId"></param>
	// / <param name="stopper"></param>
	// / <param name="amountGiven"></param>
	// / <param name="reason"></param>
	public static void StopBloodTransfusion(String barcodeId, WSUser stopper, BigDecimal amountGiven, String reason, String wardId) throws Exception {
		try {
			stopBloodTransfusion request = new stopBloodTransfusion();
			request.barcodeId = barcodeId;
			request.stopper = stopper;
			request.amountGiven = amountGiven;
			// request.amountGivenSpecified = true;
			request.reason = reason;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.stopBloodTransfusion(barcodeId, stopper, wardId, amountGiven, reason, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.stopBloodTransfusion(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-22
	// / </summary>
	// / <param name="barcodeId"></param>
	// / <param name="operator"></param>
	// / <param name="checker"></param>
	public static void RestartBloodTransfusion(String barcodeId, WSUser _operator, WSUser checker, String wardId) throws Exception {
		try {
			restartBloodTransfusion request = new restartBloodTransfusion();
			request.barcodeId = barcodeId;
			request._operator = _operator;
			request.checker = checker;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.restartBloodTransfusion(barcodeId, _operator, checker, wardId, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.restartBloodTransfusion(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-22
	// / </summary>
	// / <param name="barcodeId"></param>
	// / <param name="finisher"></param>
	public static void FinishBloodTransfusion(String barcodeId, BigDecimal amountGiven, WSUser finisher, String reason) throws Exception {
		try {
			finishBloodTransfusion request = new finishBloodTransfusion();
			request.barcodeId = barcodeId;
			request.finisher = finisher;
			request.amountGiven = amountGiven;
			// request.amountGivenSpecified = true;
			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.finishBloodTransfusion(barcodeId, finisher, amountGiven, reason, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.finishBloodTransfusion(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-22
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="issueBeginTime"></param>
	// / <param name="issueEndTime"></param>
	// / <param name="preparedBeginTime"></param>
	// / <param name="preparedEndTime"></param>
	// / <param name="startBeginTime"></param>
	// / <param name="startEndTime"></param>
	// / <param name="finishBeginTime"></param>
	// / <param name="finishEndTime"></param>
	// / <param name="status"></param>
	// / <returns></returns>
	public static WSNursingBloodTransfusionRecord[] GetPatientBloodTransfusionRecords(String patientId, int series, String admissionId, DateTime issueBeginTime, DateTime issueEndTime, DateTime preparedBeginTime, DateTime preparedEndTime, DateTime startBeginTime, DateTime startEndTime, DateTime finishBeginTime, DateTime finishEndTime, bloodTransfusionStatus[] statusList) throws Exception {
		try {
			getPatientBloodTransfusionRecords request = new getPatientBloodTransfusionRecords();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			if (issueBeginTime != null) {
				request.issueBeginTime = issueBeginTime;
				// request.issueBeginTimeSpecified = true;
			}

			if (issueEndTime != null) {
				request.issueEndTime = issueEndTime;
				// request.issueEndTimeSpecified = true;
			}

			if (preparedBeginTime != null) {
				request.preparedBeginTime = preparedBeginTime;
				// request.preparedBeginTimeSpecified = true;
			}

			if (preparedEndTime != null) {
				request.preparedEndTime = preparedEndTime;
				// request.preparedEndTimeSpecified = true;
			}

			if (startBeginTime != null) {
				request.startBeginTime = startBeginTime;
				// request.startBeginTimeSpecified = true;
			}

			if (startEndTime != null) {
				request.startEndTime = startEndTime;
				// request.startEndTimeSpecified = true;
			}

			if (finishBeginTime != null) {
				request.finishBeginTime = finishBeginTime;
				// request.finishEndTimeSpecified = true;
			}

			if (finishEndTime != null) {
				request.finishEndTime = finishEndTime;
				// request.finishEndTimeSpecified = true;
			}

			// request.statusList = new
			// ArrayList<bloodTransfusionStatus>(Arrays.asList(statusList));
			// request.statusList = new ArrayList<bloodTransfusionStatus>();
			request.statusList = new ArrayList<String>();
			// for (bloodTransfusionStatus s : statusList)
			for (bloodTransfusionStatus s : statusList) {
				// request.statusList.add(s);
				request.statusList.add(s.name());
			}

			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingBloodTransfusionRecord[] rets = null;

			getPatientBloodTransfusionRecordsResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getPatientBloodTransfusionRecords(request);
					rets = GetObjectArrayFromResponse(response, WSNursingBloodTransfusionRecord.class);

					// if (response != null)
					// rets = (WSNursingBloodTransfusionRecord
					// [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getPatientBloodTransfusionRecords(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-5-22
	// / </summary>
	// / <param name="wardId"></param>
	// / <returns></returns>
	public static WSNursingBloodTransfusionConfiguration GetWardBloodTransfusionConfiguration(String wardId) throws Exception {
		try {
			getWardBloodTransfusionConfiguration request = new getWardBloodTransfusionConfiguration();
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingBloodTransfusionConfiguration ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getWardBloodTransfusionConfiguration(wardId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret =
			// _egretService.getWardBloodTransfusionConfiguration(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-4-28
	// / </summary>
	// / <param name="orders"></param>
	// / <param name="stopper"></param>
	public static WSDictionaryItem[] GetBloodTransfusionReactions() throws Exception {
		try {
			getBloodTransfusionReactions request = new getBloodTransfusionReactions();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getBloodTransfusionReactionsResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getBloodTransfusionReactions(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSDictionaryItem.class);

					// if (response != null)
					// rets = (WSDictionaryItem [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getBloodTransfusionReactions(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region Notes

	// / <summary>
	// / Wade 2010-5-24
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="beginTime"></param>
	// / <param name="endTime"></param>
	// / <param name="noteType"></param>
	// / <returns></returns>
	public static WSNursingNote[] GetPatientNursingNotes(String patientId, int series, String admissionId, DateTime beginTime, DateTime endTime, nursingNoteType noteType) throws Exception {
		try {
			getPatientNursingNotes request = new getPatientNursingNotes();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			if (beginTime != null) {
				request.beginTime = beginTime;
				// request.beginTimeSpecified = true;
			}

			if (endTime != null) {
				request.endTime = endTime;
				// request.endTimeSpecified = true;
			}

			if (noteType != null) {
				request.noteType = noteType;
				// request.noteTypeSpecified = true;
			}

			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingNote[] rets = null;

			getPatientNursingNotesResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getPatientNursingNotes(patientId, series, admissionId, beginTime, endTime, noteType, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNursingNote.class);

					// if (response != null)
					// rets = (WSNursingNote [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getPatientNursingNotes(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region MEI

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="formId"></param>
	// / <param name="id"></param>
	// / <param name="scheduledStartTimeLowerBound"></param>
	// / <param name="scheduledStartTimeUpperBound"></param>
	// / <param name="source"></param>
	// / <param name="status"></param>
	// / <param name="type"></param>
	// / <returns></returns>
	public static WSRequest[] GetRequestList(String patientId, int series, String admissionId, int formId, int id, DateTime confirmedStartTimeLowerBound, DateTime confirmedStartTimeUpperBound, requestSource source, requestStatus[] statusList, requestType type, DateTime createTimeLowerBound, DateTime createTimeUpperBound) throws Exception {
		try {
			getRequestList request = new getRequestList();
			request.patientId = patientId;
			// if (series != null)
			// {
			// request.series = series.Value;
			// request.seriesSpecified = true;
			// }
			// else
			// request.seriesSpecified = false;
			request.series = Integer.valueOf(series);
			request.admissionId = admissionId;
			// if (formId != null)
			// {
			// request.formId = formId.Value;
			// request.formIdSpecified = true;
			// }
			request.formId = Integer.valueOf(formId);

			// if (id != null)
			// {
			// request.id = id.Value;
			// request.idSpecified = true;
			// }
			request.id = Integer.valueOf(id);

			if (confirmedStartTimeLowerBound != null) {
				request.confirmedStartTimeLowerBound = confirmedStartTimeLowerBound;
				// request.confirmedStartTimeLowerBoundSpecified = true;
			}

			if (confirmedStartTimeUpperBound != null) {
				request.confirmedStartTimeUpperBound = confirmedStartTimeUpperBound;
				// request.confirmedStartTimeUpperBoundSpecified = true;
			}

			if (source != null) {
				request.source = source;
				// request.sourceSpecified = true;
			}

			request.statusList = new ArrayList<requestStatus>(Arrays.asList(statusList));
			if (type != null) {
				request.type = type;
				// request.typeSpecified = true;
			}

			if (createTimeLowerBound != null) {
				request.createTimeLowerBound = createTimeLowerBound;
				// request.createTimeLowerBoundSpecified = true;
			}

			if (createTimeUpperBound != null) {
				request.createTimeUpperBound = createTimeUpperBound;
				// request.createTimeUpperBoundSpecified = true;
			}

			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSRequest[] rets = null;

			getRequestListResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getRequestList(request);
					rets = GetObjectArrayFromResponse(response, WSRequest.class);

					// if (response != null)
					// rets = (WSRequest [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getRequestList(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="starttime"></param>
	// / <param name="formId"></param>
	// / <param name="preparations"></param>
	// / <param name="patientName"></param>
	// / <param name="requestName"></param>
	// / <param name="creator"></param>
	// / <param name="source"></param>
	// / <param name="type"></param>
	// / <param name="urgentFlag"></param>
	public static void CreateRequest(String patientId, int series, String admissionId, DateTime starttime, int formId, WSPreparation[] preparations, String patientName, String requestName, WSUser creator, requestSource source, requestType type, requestUrgentFlag urgentFlag) throws Exception {
		try {
			createRequest request = new createRequest();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.creator = creator;
			request.formId = formId;
			request.patientName = patientName;
			request.preparations = new ArrayList<WSPreparation>(Arrays.asList(preparations));
			request.requestName = requestName;
			request.scheduledStartTime = starttime;
			// request.scheduledStartTimeSpecified = true;
			request.source = source;
			// request.sourceSpecified = true;
			request.type = type;
			// request.typeSpecified = true;
			request.urgentFlag = urgentFlag;
			// request.urgentFlagSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.createRequest(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.createRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="requestId"></param>
	// / <param name="dueTime"></param>
	// / <param name="description"></param>
	// / <param name="creator"></param>
	public static void AddPreparation(int requestId, DateTime dueTime, String description, WSUser creator) throws Exception {
		try {
			addPreparation request = new addPreparation();
			request.requestId = requestId;
			request.dueTime = dueTime;
			// request.dueTimeSpecified = true;
			request.description = description;
			request.creator = creator;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.addPreparation(requestId, description, dueTime, creator, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.addPreparation(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="requestId"></param>
	// / <param name="checker"></param>
	public static void CounterSignRequest(int requestId, WSUser checker) throws Exception {
		try {
			counterSignRequest request = new counterSignRequest();
			request.requestId = requestId;
			request.checker = checker;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.counterSignRequest(requestId, checker, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.counterSignRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="requestId"></param>
	// / <param name="resourceId"></param>
	// / <param name="confirmer"></param>
	// / <param name="scheduledStartTime"></param>
	// / <param name="scheduledFinishedTime"></param>
	public static void ConfirmRequest(int requestId, int resourceId, WSUser confirmer, DateTime confirmedStartTime, DateTime confirmedFinishTime) throws Exception {
		try {
			confirmRequest request = new confirmRequest();
			request.requestId = requestId;
			request.resourceId = resourceId;
			request.confirmer = confirmer;
			request.confirmedStartTime = confirmedStartTime;
			// request.confirmedStartTimeSpecified = true;
			request.confirmedFinishTime = confirmedFinishTime;
			// request.confirmedFinishTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.confirmRequest(requestId, confirmer, resourceId, confirmedStartTime, confirmedFinishTime, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.confirmRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="requestId"></param>
	// / <param name="confirmer"></param>
	public static void ConfirmPatientLeftWard(String patientId, int series, String admissionId, WSUser confirmer) throws Exception {
		try {
			confirmPatientLeftWard request = new confirmPatientLeftWard();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.confirmer = confirmer;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.confirmPatientLeftWard(patientId, series, admissionId, confirmer, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.confirmPatientLeftWard(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="appointmentId"></param>
	// / <param name="confirmer"></param>
	public static void ConfirmPatientArrivedMEI(String patientId, int series, String admissionId, WSUser confirmer) throws Exception {
		try {
			confirmPatientArrivedMEI request = new confirmPatientArrivedMEI();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.confirmer = confirmer;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.confirmPatientArrivedMEI(patientId, series, admissionId, confirmer, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.confirmPatientArrivedMEI(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="appointmentId"></param>
	// / <param name="confirmer"></param>
	public static void ConfirmPatientLeftMEI(String patientId, int series, String admissionId, int appointmentId, appointmentStatus status, WSUser confirmer, String reason) throws Exception {
		try {
			confirmPatientLeftMEI request = new confirmPatientLeftMEI();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.appointmentId = appointmentId;
			request.status = status;
			// request.statusSpecified = true;
			request.confirmer = confirmer;
			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.confirmPatientLeftMEI(patientId, series, admissionId, appointmentId, confirmer, status, reason, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.confirmPatientLeftMEI(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="requestId"></param>
	// / <param name="confirmer"></param>
	public static void ConfirmPatientReturnedWard(String patientId, int series, String admissionId, WSUser confirmer) throws Exception {
		try {
			confirmPatientReturnedWard request = new confirmPatientReturnedWard();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.confirmer = confirmer;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.confirmPatientReturnedWard(patientId, series, admissionId, confirmer, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.confirmPatientReturnedWard(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="requestId"></param>
	// / <param name="canceller"></param>
	public static void CancelRequest(int requestId, String stationCode, WSUser canceller, String reason) throws Exception {
		try {
			cancelRequest request = new cancelRequest();
			request.requestId = requestId;
			request.stationCode = stationCode;
			request.canceller = canceller;
			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.cancelRequest(requestId, canceller, stationCode, reason, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.cancelRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="appointmentId"></param>
	// / <param name="canceller"></param>
	public static void CancelAppointment(int appointmentId, WSUser canceller, String reason) throws Exception {
		try {
			cancelAppointment request = new cancelAppointment();
			request.appointmentId = appointmentId;
			request.canceller = canceller;
			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.cancelAppointment(appointmentId, canceller, reason, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.cancelAppointment(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="seqNo"></param>
	// / <param name="finisher"></param>
	public static void ConfirmPreparationFinished(int seqNo, WSUser finisher) throws Exception {
		try {
			confirmPreparationFinished request = new confirmPreparationFinished();
			request.seqNo = seqNo;
			request.finisher = finisher;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.confirmPreparationFinished(seqNo, finisher, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.confirmPreparationFinished(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="seqNo"></param>
	// / <param name="canceller"></param>
	public static void CancelPreparation(int seqNo, WSUser canceller) throws Exception {
		try {
			cancelPreparation request = new cancelPreparation();
			request.seqNo = seqNo;
			request.canceller = canceller;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.cancelPreparation(seqNo, canceller, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.cancelPreparation(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="requestId"></param>
	// / <param name="resourceId"></param>
	// / <param name="appointmentId"></param>
	// / <param name="status"></param>
	// / <param name="scheduledStartTimeLowerBound"></param>
	// / <param name="scheduledStartTimeUpperBound"></param>
	// / <param name="scheduledEndTimeLowerBound"></param>
	// / <param name="scheduledEndTimeUpperBound"></param>
	// / <returns></returns>
	public static WSAppointment[] GetAppointmentList(String patientId, int series, String admissionId, int requestId, int resourceId, int appointmentId, appointmentStatus status, DateTime confirmededStartTimeLowerBound, DateTime confirmedStartTimeUpperBound, resourceType resType) throws Exception {
		try {
			// getAppointmentList request = new getAppointmentList();
			// request.patientId = patientId;
			// if (series != null)
			// {
			// request.series = series.Value;
			// request.seriesSpecified = true;
			// }
			// else
			// request.seriesSpecified = false;
			// request.admissionId = admissionId;
			// if (requestId != null)
			// {
			// request.requestId = requestId.Value;
			// request.requestIdSpecified = true;
			// }
			// else
			// request.requestIdSpecified = false;
			// if (resourceId != null)
			// {
			// request.resourceId = resourceId.Value;
			// request.resourceIdSpecified = true;
			// }
			// else
			// request.resourceIdSpecified = false;
			// if (appointmentId != null)
			// {
			// request.appointmentId = appointmentId.Value;
			// request.appointmentIdSpecified = true;
			// }
			// else
			// request.appointmentIdSpecified = false;
			// if (status != null)
			// {
			// request.status = status.Value;
			// request.statusSpecified = true;
			// }
			// else
			// request.statusSpecified = false;
			// if (confirmededStartTimeLowerBound != null)
			// {
			// request.confirmededStartTimeLowerBound =
			// confirmededStartTimeLowerBound.Value;
			// request.confirmededStartTimeLowerBoundSpecified = true;
			// }
			// else
			// request.confirmededStartTimeLowerBoundSpecified = false;
			// if (confirmedStartTimeUpperBound != null)
			// {
			// request.confirmedStartTimeUpperBound =
			// confirmedStartTimeUpperBound.Value;
			// request.confirmedStartTimeUpperBoundSpecified = true;
			// }
			// else
			// request.confirmedStartTimeUpperBoundSpecified = false;
			// if (resType != null)
			// {
			// request.resourceType = resType.Value;
			// request.resourceTypeSpecified = true;
			// }
			// else
			// request.resourceTypeSpecified = false;
			// request.cliCode = _cliCode;
			// request.updateNo = _updateNo;
			WSAppointment[] rets = null;

			getAppointmentListResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getAppointmentList(appointmentId, requestId, resourceId, patientId, series, admissionId, confirmedStartTimeUpperBound, confirmededStartTimeLowerBound, status, resType, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSAppointment.class);

					// if (response != null)
					// rets = (WSAppointment [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getAppointmentList(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <returns></returns>
	public static patientTrace GetCurrentPatientTrace(String patientId, int series, String admissionId) throws Exception {
		try {
			getCurrentPatientTrace request = new getCurrentPatientTrace();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			patientTrace ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getCurrentPatientTrace(patientId, series, admissionId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getCurrentPatientTrace(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-11
	// / </summary>
	// / <param name="requestName"></param>
	// / <param name="formId"></param>
	// / <param name="preparations"></param>
	// / <param name="starttime"></param>
	// / <param name="updater"></param>
	// / <param name="urgentFlag"></param>
	public static void UpdateRequest(String requestName, int formId, WSPreparation[] preparations, DateTime starttime, WSUser updater, requestUrgentFlag urgentFlag) throws Exception {
		try {
			updateRequest request = new updateRequest();
			request.requestName = requestName;
			request.formId = formId;
			request.preparations = new ArrayList<WSPreparation>(Arrays.asList(preparations));
			request.scheduledStartTime = starttime;
			// request.scheduledStartTimeSpecified = true;
			request.updater = updater;
			request.urgentFlag = urgentFlag;
			// request.urgentFlagSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.updateRequest(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.updateRequest(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-18
	// / </summary>
	// / <param name="seqNo"></param>
	// / <param name="finisher"></param>
	public static void StartAppointment(int appointmentId, WSUser confirmer) throws Exception {
		try {
			startAppointment request = new startAppointment();
			request.appointmentId = appointmentId;
			request.confirmer = confirmer;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.startAppointment(appointmentId, confirmer, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.startAppointment(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSResource[] GetResourceList(int id, String name, resourceStatus status, String type) throws Exception {
		try {
			// getResourceList request = new getResourceList();
			// if (id != null)
			// {
			// request.id = id.Value;
			// request.idSpecified = true;
			// }
			// else
			// request.idSpecified = false;
			// request.name = name;
			// if (status != null)
			// {
			// request.status = status.Value;
			// request.statusSpecified = true;
			// }
			// else
			// request.statusSpecified = false;
			// request.type = type;
			// request.cliCode = _cliCode;
			// request.updateNo = _updateNo;
			WSResource[] rets = null;

			getResourceListResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getResourceList(id, type, name, status, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSResource.class);

					// if (response != null)
					// rets = (WSResource [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getResourceList(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSTimePeriod[] GetResourceSchedule(String resourceName) throws Exception {
		try {
			getResourceSchedule request = new getResourceSchedule();
			request.resourceName = resourceName;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSTimePeriod[] rets = null;

			getResourceScheduleResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getResourceSchedule(resourceName, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSTimePeriod.class);

					// if (response != null)
					// rets = (WSTimePeriod [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getResourceSchedule(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-30
	// / </summary>
	// / <param name="patientId"></param>
	// / <param name="series"></param>
	// / <param name="admissionId"></param>
	// / <param name="type"></param>
	// / <param name="requestName"></param>
	// / <returns></returns>
	public static int GetUncompletedRequestCount(String patientId, int series, String admissionId, requestType type, String requestName) throws Exception {
		try {
			getUncompletedRequestCount request = new getUncompletedRequestCount();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.requestType = type;
			// request.requestTypeSpecified = true;
			request.requestName = requestName;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			int ret = 0;

			for (int i = 0; i < _retryTimes && ret == 0; i++) {
				try {
					ret = _egretService.getUncompletedRequestCount(patientId, series, admissionId, type, requestName, _cliCode, _updateNo);

				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getUncompletedRequestCount(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Wade 2010-6-30
	// / </summary>
	// / <param name="appId"></param>
	// / <param name="resourceId"></param>
	// / <param name="updater"></param>
	// / <param name="confirmedStartTime"></param>
	// / <param name="confirmedFinishTime"></param>
	public static void UpdateAppointment(int appId, int resourceId, WSUser updater, DateTime confirmedStartTime, DateTime confirmedFinishTime) throws Exception {
		try {
			updateAppointment request = new updateAppointment();
			request.confirmedStartTime = confirmedStartTime;
			// request.confirmedStartTimeSpecified = true;
			request.confirmedFinishTime = confirmedFinishTime;
			// request.confirmedFinishTimeSpecified = true;
			request.id = appId;
			request.resourceId = resourceId;
			// request.resourceIdSpecified = true;
			request.updater = updater;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.updateAppointment(appId, resourceId, confirmedStartTime, confirmedFinishTime, updater, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.updateAppointment(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static resourceType[] GetResourceTypeList() throws Exception {
		try {
			getResourceTypeList request = new getResourceTypeList();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			resourceType[] rets = null;

			getResourceTypeListResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getResourceTypeList(_cliCode, _updateNo);

					rets = GetObjectArrayFromResponse(response, resourceType.class);

					// if (response != null)
					// rets = (resourceType [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getResourceTypeList(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region Users

	public static WSSysRole[] GetWardRoles(String userCode, String wardId) throws Exception {
		try {
			getWardRoles request = new getWardRoles();
			request.userCode = userCode;
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSSysRole[] rets = null;

			getWardRolesResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getWardRoles(wardId, userCode, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSSysRole.class);

					// if (response != null)
					// rets = (WSSysRole [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getWardRoles(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSUser[] GetUserListInEgret(String wardId) throws Exception {
		try {
			getUserListInEgret request = new getUserListInEgret();
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSUser[] rets = null;

			getUserListInEgretResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getUserListInEgret(wardId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSUser.class);

					// if (response != null)
					// rets = (WSUser [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getUserListInEgret(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void AddNewUserInEgret(WSSysRole[] roles, WSUser user) throws Exception {
		try {
			addNewUserInEgret request = new addNewUserInEgret();
			request.roles = new ArrayList<WSSysRole>(Arrays.asList(roles));
			request.user = user;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.addNewUserInEgret(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.addNewUserInEgret(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void ModifyUserInEgret(WSSysRole[] roles, WSUser user) throws Exception {
		try {
			modifyUserInEgret request = new modifyUserInEgret();
			request.roles = new ArrayList<WSSysRole>(Arrays.asList(roles));
			request.user = user;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.modifyUserInEgret(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.modifyUserInEgret(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void RemoveNewUserInEgret(String userCode, WSSysRole[] roles) throws Exception {
		try {
			removeNewUserInEgret request = new removeNewUserInEgret();
			request.userCode = userCode;
			request.roles = new ArrayList<WSSysRole>(Arrays.asList(roles));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.removeNewUserInEgret(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.removeNewUserInEgret(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSUser GetUserByCodeFromHIS(String usercode) throws Exception {
		try {
			getUserByCodeFromHIS request = new getUserByCodeFromHIS();
			request.userCode = usercode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSUser user = null;

			for (int i = 0; i < _retryTimes && user == null; i++) {
				try {
					user = _egretService.getUserByCodeFromHIS(usercode, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// user = _egretService.getUserByCodeFromHIS(request).@return;
			// });
			return user;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region E-px

	// public static void CreatePrepareStatusEpOrder(epOrder[] orders)
	// {
	// try
	// {
	// createPrepareStatusEpOrder request = new createPrepareStatusEpOrder();
	// request.epOrder = orders;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// Retry(_retryTimes, request, delegate
	// {
	// _egretService.createPrepareStatusEpOrder(request);
	// });
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	// public static void UpdatePrepareStatusEpOrder(epOrder order)
	// {
	// try
	// {
	// updatePrepareStatusEpOrder request = new updatePrepareStatusEpOrder();
	// request.epOrder = order;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// Retry(_retryTimes, request, delegate
	// {
	// _egretService.updatePrepareStatusEpOrder(request);
	// });
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	// public static void DeletePrepareStatusEpOrder(epOrder order)
	// {
	// try
	// {
	// deletePrepareStatusEpOrder request = new deletePrepareStatusEpOrder();
	// request.epOrder = order;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// Retry(_retryTimes, request, delegate
	// {
	// _egretService.deletePrepareStatusEpOrder(request);
	// });
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	// public static epOrder FindLastPrepareEpOrderByAutoNo(String autoNo)
	// {
	// return null;
	// try
	// {
	// findLastPrepareEpOrderByAutoNo request = new
	// findLastPrepareEpOrderByAutoNo();
	// request.autoNo = autoNo;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// epOrder ret = null;
	// Retry(_retryTimes, request, delegate
	// {
	// ret = _egretService.findLastPrepareEpOrderByAutoNo(request).@return;
	// });
	// return ret;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	// public static codeAndDisplay[] FindDrugWarningByName(String msg)
	// {
	// return null;
	// try
	// {
	// findDrugWarningByName request = new findDrugWarningByName();
	// request.msg = msg;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// codeAndDisplay[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.findDrugWarningByName(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	// public static codeAndDisplay[] FindDrugOtherByName(String msg)
	// {
	// return null;
	// try
	// {
	// findDrugOtherByName request = new findDrugOtherByName();
	// request.msg = msg;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// codeAndDisplay[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.findDrugOtherByName(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	// public static codeAndDisplay[] FindDrugRouteByItemCode(String itemCode)
	// {
	// return null;
	// try
	// {
	// findDrugRouteByItemCode request = new findDrugRouteByItemCode();
	// request.itemCode = itemCode;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// codeAndDisplay[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.findDrugRouteByItemCode(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	//
	// public static codeAndDisplay[] FindDrugUnitPerDoseByItemCode(String
	// itemCode)
	// {
	// return null;
	// try
	// {
	// findDrugUnitPerDoseByItemCode request = new
	// findDrugUnitPerDoseByItemCode();
	// request.itemCode = itemCode;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// codeAndDisplay[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.findDrugUnitPerDoseByItemCode(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	//
	// public static codeAndDisplay[] FindPackageDrugByName(String name, String
	// doctorid)
	// {
	// return null;
	// try
	// {
	// findPackageDrugByName request = new findPackageDrugByName();
	// request.name = name;
	// request.doctorid = doctorid;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// codeAndDisplay[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.findPackageDrugByName(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	//
	// public static codeAndDisplay[] FindDrugNameByName(String name)
	// {
	// return null;
	// try
	// {
	// findDrugNameByName request = new findDrugNameByName();
	// request.name = name;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// codeAndDisplay[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.findDrugNameByName(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	//
	// public static codeAndDisplay[] FindDrugFreqByName(String name)
	// {
	// return null;
	// try
	// {
	// findDrugFreqByName request = new findDrugFreqByName();
	// request.name = name;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// codeAndDisplay[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.findDrugFreqByName(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	//
	// public static epOrder[] FindDrugListByPackageID(String packageId, String
	// doctorid)
	// {
	// return null;
	// try
	// {
	// findDrugListByPackageID request = new findDrugListByPackageID();
	// request.packageId = packageId;
	// request.doctorid = doctorid;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// epOrder[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.findDrugListByPackageID(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	//
	// public static epOrder[] FindDrugListByDrugID(String drugId)
	// {
	// return null;
	// try
	// {
	// findDrugListByDrugID request = new findDrugListByDrugID();
	// request.drugId = drugId;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// epOrder[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.findDrugListByDrugID(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	//
	// public static void ChangeEpOrderStatus(String orderNo, int beginStatus,
	// int endStatus, DateTime? stopTime)
	// {
	// try
	// {
	// changeEpOrderStatus request = new changeEpOrderStatus();
	// request.epOrderNo = orderNo;
	// request.beginStatus = beginStatus;
	// request.endStatus = endStatus;
	// if (stopTime != null)
	// {
	// request.stopTime = stopTime.Value;
	// request.stopTimeSpecified = true;
	// }
	// else
	// request.stopTimeSpecified = false;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// Retry(_retryTimes, request, delegate
	// {
	// _egretService.changeEpOrderStatus(request);
	// });
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	//
	// public static void UpdateGroupPrepareEpOrder(String[] autoNo, boolean
	// grouping, String remarks, String rate)
	// {
	// try
	// {
	// updateGroupPrepareEpOrder request = new updateGroupPrepareEpOrder();
	// request.autoNo = autoNo;
	// request.grouping = grouping;
	// request.commonreamrks = remarks;
	// request.flowrate = rate;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// Retry(_retryTimes, request, delegate
	// {
	// _egretService.updateGroupPrepareEpOrder(request);
	// });
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	//
	// public static epOrder[] FindEpOrderListByPatientIdAndStatus(String
	// patientId, DateTime? starttime, DateTime? endtime, int status)
	// {
	// return null;
	// try
	// {
	// findEpOrderListByPatientIdAndStatus request = new
	// findEpOrderListByPatientIdAndStatus();
	// request.patientId = patientId;
	// if (starttime != null)
	// {
	// request.startTime = starttime.Value;
	// request.startTimeSpecified = true;
	// }
	// else
	// request.startTimeSpecified = false;
	// if (endtime != null)
	// {
	// request.endTime = endtime.Value;
	// request.endTimeSpecified = true;
	// }
	// else
	// request.endTimeSpecified = false;
	// request.status = status;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// epOrder[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.findEpOrderListByPatientIdAndStatus(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	//
	// /// <summary>
	// /// 判断是否有重复药物
	// /// </summary>
	// /// <param name="itemCode"></param>
	// /// <returns></returns>
	// public static boolean HasDuplicateEpOrder(epOrder order)
	// {
	// return false;
	// try
	// {
	// hasDuplicateEpOrder request = new hasDuplicateEpOrder();
	// epOrder[] orders = new epOrder[1];
	// orders[0] = order;
	// request.epOrders = orders;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// boolean rets = false;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.hasDuplicateEpOrder(request).@return;
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	// #endregion

	// #region Task
	// / <summary>
	// / 新增一个task
	// / </summary>
	// / <param name="task">需要创建的Task</param>
	public static void CreateTask(WSNursingTask task, WSNursingTaskAction[] actions) throws Exception {
		try {
			createTask request = new createTask();
			request.task = task;
			request.actions = new ArrayList<WSNursingTaskAction>(Arrays.asList(actions));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.createTask(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.createTask(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 批量创建Tasks
	// / </summary>
	// / <param name="tasks">需要创建的Task数组</param>
	// public static void CreateTasks(WSNursingTask[] tasks)
	// {
	// try
	// {
	// createTasks request = new createTasks();
	// request.tasks = tasks;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// Retry(_retryTimes, request, delegate
	// {
	// _egretService.createTasks(request);
	// });
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	// / <summary>
	// / 停止一个task
	// / </summary>
	// / <param name="taskIds">TaskID</param>
	// / <param name="user">当前用户</param>
	public static void StopTasks(long[] taskIds, WSUser user) throws Exception {
		try {
			stopTasks request = new stopTasks();
			ArrayList<Long> taskIdTemp = new ArrayList<Long>();
			for (int i = 0; i < taskIds.length; i++) {
				taskIdTemp.add(new Long(taskIds[i]));
			}

			request.taskIds = taskIdTemp;
			request.user = user;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.stopTasks(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.stopTasks(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 修改一个Task
	// / </summary>
	// / <param name="task">需要修改的Task</param>
	public static void ModifyTask(WSNursingTask task, WSNursingTaskAction[] actions) throws Exception {
		try {
			modifyTask request = new modifyTask();
			request.task = task;
			request.actions = new ArrayList<WSNursingTaskAction>(Arrays.asList(actions));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.modifyTask(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.modifyTask(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 根据病人Id来获取
	// / </summary>
	// / <param name="pv">当前病人</param>
	// / <param name="type">Task类别</param>
	// / <param name="subType">Task子类别</param>
	// / <param name="status">需要获取的Task状态</param>
	// / <param name="actionId">需要获取的Task种类</param>
	// / <param name="beginTime">Task开始时间</param>
	// / <param name="endTime">Task结束时间</param>
	// / <returns></returns>
	public static WSNursingTask[] GetPatientTaskList(WSPatientVisit pv, taskType type, taskSubType subType, taskStatus1 status, Integer actionId, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			// getPatientTaskList request = new getPatientTaskList();
			// request.ipVisit = pv;
			// if (type != null)
			// {
			// request.type = type.Value;
			// request.typeSpecified = true;
			// }
			// else
			// {
			// request.typeSpecified = false;
			// }
			// if (subType != null && (int)subType != -1)
			// {
			// request.subType = subType.Value;
			// request.subTypeSpecified = true;
			// }
			// else
			// {
			// request.subTypeSpecified = false;
			// }
			// if (status != null)
			// {
			// request.taskStatus = status.Value;
			// request.taskStatusSpecified = true;
			// }
			// else
			// {
			// request.taskStatusSpecified = false;
			// }
			// if (actionId != null)
			// {
			// request.actionId = actionId.Value;
			// request.actionIdSpecified = true;
			// }
			// else
			// {
			// request.actionIdSpecified = false;
			// }
			// if (beginTime != null)
			// {
			// request.beginTime = beginTime.Value;
			// request.beginTimeSpecified = true;
			// }
			// else
			// {
			// request.beginTimeSpecified = false;
			// }
			// if (endTime != null)
			// {
			// request.endTime = endTime.Value;
			// request.endTimeSpecified = true;
			// }
			// else
			// {
			// request.endTimeSpecified = false;
			// }
			// request.cliCode = _cliCode;
			// request.updateNo = _updateNo;

			// WSNursingTask[] rets = new WSNursingTask[0];
			WSNursingTask[] rets = null;

			getPatientTaskListResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getPatientTaskList(pv, status, type, subType, actionId, beginTime, endTime, _cliCode, _updateNo);

					rets = GetObjectArrayFromResponse(response, WSNursingTask.class);

					// if (response != null)
					// rets = (WSNursingTask [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getPatientTaskList(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 根据taskId来获取
	// / </summary>
	// / <param name="taskId"></param>
	// / <returns></returns>
	public static WSNursingTask GetTask(long taskId) throws Exception {
		try {
			getTask request = new getTask();
			request.taskId = taskId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingTask rets = new WSNursingTask();

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getTask(taskId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getTask(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 获取所有On状态的Task
	// / </summary>
	// / <param name="endTime"></param>
	// / <returns></returns>
	public static WSNursingTask[] GetOnTaskList(DateTime endTime) throws Exception {
		try {
			getOnTaskList request = new getOnTaskList();
			// if (endTime != null)
			// {
			// request.endTimeSpecified = true;
			// request.endTime = endTime.Value;
			// }
			// else
			// {
			// request.endTimeSpecified = false;
			// }
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingTask[] rets = new WSNursingTask[0];

			getOnTaskListResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getOnTaskList(endTime, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNursingTask.class);

					// if (response != null)
					// rets = (WSNursingTask [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getOnTaskList(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// public static WSNursingTaskFrequency[] GetWardTaskFrequency(String
	// wardId)
	// {
	// try
	// {
	// getWardTaskFrequency request = new getWardTaskFrequency();
	// request.wardId = wardId;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// WSNursingTaskFrequency[] rets = null;
	// Retry(_retryTimes, request, delegate
	// {
	// rets = _egretService.getWardTaskFrequency(request);
	// });
	// return rets;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	public static WSNursingTaskFrequency[] GetWardTaskFrequency(String wardId, taskType type) throws Exception {
		try {
			getWardTaskFrequency request = new getWardTaskFrequency();
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			request.type = type;
			// request.typeSpecified = true;
			WSNursingTaskFrequency[] rets = null;

			getWardTaskFrequencyResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getWardTaskFrequency(wardId, type, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNursingTaskFrequency.class);

					// if (response != null)
					// rets = (WSNursingTaskFrequency [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getWardTaskFrequency(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 获取某一类Task的所有可执行项目
	// / </summary>
	// / <param name="type"></param>
	// / <param name="subType"></param>
	// / <returns></returns>
	public static WSNursingTaskAction[] GetTaskActionList(String wardId, taskType type, taskSubType subType) throws Exception {
		try {
			getTaskActionList request = new getTaskActionList();
			request.wardId = wardId;
			request.taskType = type;
			// request.taskTypeSpecified = true;
			// if (subType != null)
			// {
			// request.taskSubType = subType.Value;
			// request.taskSubTypeSpecified = true;
			// }
			// else
			// {
			// request.taskSubTypeSpecified = false;
			// }
			WSNursingTaskAction[] rets = null;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			getTaskActionListResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getTaskActionList(wardId, type, subType, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNursingTaskAction.class);

					// if (response != null)
					// rets = (WSNursingTaskAction [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getTaskActionList(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / Outstanding手动完成
	// / </summary>
	public static void FinishOutstandingTasks(long[] taskIds, WSUser executor, WSUser checker) throws Exception {
		try {
			finishOutstandingTasks request = new finishOutstandingTasks();
			request.osTaskIds = new ArrayList<Long>();

			for (int i = 0; i < taskIds.length; i++)
				request.osTaskIds.add(new Long(taskIds[i]));

			request.executer = executor;
			request.checker = checker;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.finishOutstandingTasks(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.finishOutstandingTasks(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 处理Outstanding Task Not Done Finish异常
	// / </summary>
	public static void HandleOutstandingTasksNotDoneFinish(long[] taskIds, WSUser executor, String reason) throws Exception {
		try {
			handleOutstandingTasksNotDoneFinish request = new handleOutstandingTasksNotDoneFinish();
			request.executer = executor;
			// request.osTaskIds = taskIds;
			for (int i = 0; i < taskIds.length; i++)
				request.osTaskIds.add(new Long(taskIds[i]));

			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.handleOutstandingTasksNotDoneFinish(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.handleOutstandingTasksNotDoneFinish(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 处理Outstanding Task Not Done Finish异常
	// / </summary>
	public static void HandleOutstandingTasksNotDoneUnfinish(long[] taskIds, WSUser executor, String reason) throws Exception {
		try {
			handleOutstandingTasksNotDoneUnFinish request = new handleOutstandingTasksNotDoneUnFinish();
			request.executer = executor;
			// request.osTaskIds = taskIds;

			for (int i = 0; i < taskIds.length; i++)
				request.osTaskIds.add(new Long(taskIds[i]));

			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.handleOutstandingTasksNotDoneUnFinish(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.handleOutstandingTasksNotDoneUnFinish(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / PRN task手动处理
	// / </summary>
	public static void HandlePrnTasksFinished(long[] taskIds, WSUser executor, String reason) throws Exception {
		try {
			handlePrnTasksFinished request = new handlePrnTasksFinished();
			// request.taskIds = taskIds;
			for (int i = 0; i < taskIds.length; i++)
				request.taskIds.add(new Long(taskIds[i]));

			request.executer = executor;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.handlePrnTasksFinished(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.handlePrnTasksFinished(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 处理PRN Task Not Done Finish异常
	// / </summary>
	public static void HandlePrnTasksNotDoneFinished(long[] taskIds, WSUser executor, String reason) throws Exception {
		try {
			handlePrnTasksNotDoneFinished request = new handlePrnTasksNotDoneFinished();
			request.executer = executor;
			// request.taskIds = taskIds;
			for (int i = 0; i < taskIds.length; i++)
				request.taskIds.add(new Long(taskIds[i]));

			request.remarks = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.handlePrnTasksNotDoneFinished(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.handlePrnTasksNotDoneFinished(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingTaskRule[] GetRoutineTaskRules(String wardId) throws Exception {
		try {
			getRoutineTaskRules request = new getRoutineTaskRules();
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingTaskRule[] rets = null;

			getRoutineTaskRulesResponse response = null;

			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getRoutineTaskRules(wardId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNursingTaskRule.class);

					// if (response != null)
					// rets = (WSNursingTaskRule [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getRoutineTaskRules(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// public static void CreateRoutineTask(WSPatientVisit ipv, int[]
	// routineIds, WSUser user)
	// {
	// try
	// {
	// createRoutineTask request = new createRoutineTask();
	// request.ipVisit = ipv;
	// request.routineTaskRuleIds = routineIds;
	// request.user = user;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// Retry(_retryTimes, request, delegate
	// {
	// _egretService.createRoutineTask(request);
	// });
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }

	public static WSNursingTaskRulePackage[] GetTaskRulePackages(String wardId) throws Exception {
		try {
			getTaskRulePackages request = new getTaskRulePackages();
			request.wardId = wardId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			WSNursingTaskRulePackage[] ret = null;

			getTaskRulePackagesResponse response = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getTaskRulePackages(wardId, _cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(response, WSNursingTaskRulePackage.class);

					// if (response != null)
					// ret = (WSNursingTaskRulePackage [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getTaskRulePackages(request);
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void CreateRulePackageTasks(WSPatientVisit ipv, int[] packageIds, WSUser user, DateTime startTime) throws Exception {
		try {
			createRulePackageTasks request = new createRulePackageTasks();
			request.ipVisit = ipv;
			request.packageIds = new ArrayList<Integer>();
			for (int i = 0; i < packageIds.length; i++) {
				request.packageIds.add(new Integer(packageIds[i]));
			}
			request.user = user;
			request.startTime = startTime;
			// request.startTimeSpecified = true;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.createRulePackageTasks(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.createRulePackageTasks(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region Form
	public static WSNursingRecordFormClientDef GetClientDefFormByCode(String templateCode, int[] itemCodes) throws Exception {
		try {
			getClientDefFormByCode request = new getClientDefFormByCode();
			request.updateNo = _updateNo;
			request.cliCode = _cliCode;
			request.templateCode = templateCode;
			// request.itemCodes = itemCodes;

			request.itemCodes = new ArrayList<Integer>();
			for (int i = 0; i < itemCodes.length; i++) {
				request.itemCodes.add(new Integer(itemCodes[i]));
			}

			WSNursingRecordFormClientDef ret = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getClientDefFormByCode(request);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getClientDefFormByCode(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingRecordFormItem[] SaveClientDefNursingRecordForm(WSNursingRecordFormClientDef wsForm) throws Exception {
		try {
			saveClientDefNursingRecordForm request = new saveClientDefNursingRecordForm();
			request.updateNo = _updateNo;
			request.cliCode = _cliCode;
			request.wsForm = wsForm;
			WSNursingRecordFormItem[] ret = null;

			saveClientDefNursingRecordFormResponse response = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.saveClientDefNursingRecordForm(wsForm, _cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(response, WSNursingRecordFormItem.class);

					// if (response != null)
					// ret = (WSNursingRecordFormItem [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.saveClientDefNursingRecordForm(request);
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingRecordFormItem[] GetLatestNursingRecords(String templateCode, String patientId, int series, String admissionId) throws Exception {
		try {
			getLatestNursingRecords request = new getLatestNursingRecords();
			request.updateNo = _updateNo;
			request.cliCode = _cliCode;
			request.templateCodes = templateCode;
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			WSNursingRecordFormItem[] ret = null;

			getLatestNursingRecordsResponse response = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getLatestNursingRecords(patientId, series, admissionId, templateCode, _cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(response, WSNursingRecordFormItem.class);

					// if (response != null)
					// ret = (WSNursingRecordFormItem [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getLatestNursingRecords(request);
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region Nursery
	// VAC
	public static WSVaccinationWarning CheckVaccineAdminTime(String barcodeId) throws Exception {
		try {
			checkVaccineAdminTime request = new checkVaccineAdminTime();
			request.updateNo = _updateNo;
			request.cliCode = _cliCode;
			request.barcodeId = barcodeId;
			// WSVaccinationWarning ret = new WSVaccinationWarning();
			WSVaccinationWarning ret = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.checkVaccineAdminTime(barcodeId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.checkVaccineAdminTime(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSVaccinationItem GetVaccinationItemByCode(String itemCode, WSPatientVisit pv) throws Exception {
		try {
			getVaccinationItemByCode request = new getVaccinationItemByCode();
			request.updateNo = _updateNo;
			request.cliCode = _cliCode;
			request.itemCode = itemCode;
			request.patientId = pv.patient.id;
			request.admissionId = pv.admissionId;
			request.series = pv.series;
			WSVaccinationItem ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getVaccinationItemByCode(pv.patient.id, pv.series, pv.admissionId, itemCode, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getVaccinationItemByCode(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static boolean IsVaccineContained(String[] itemCodes) throws Exception {
		try {
			isVaccineContained request = new isVaccineContained();
			request.updateNo = _updateNo;
			request.cliCode = _cliCode;
			request.itemCodes = new ArrayList<String>(Arrays.asList(itemCodes));
			boolean ret = false;

			for (int i = 0; i < _retryTimes && ret == false; i++) {
				try {
					ret = _egretService.isVaccineContained(request);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.isVaccineContained(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static String GetVaccineCategory(WSNursingOrderAdminRecord record) throws Exception {
		try {
			getVaccineCategory request = new getVaccineCategory();
			request.updateNo = _updateNo;
			request.cliCode = _cliCode;
			request.adminRecord = record;
			String ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getVaccineCategory(record, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						return null;
//						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getVaccineCategory(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingOrderAdminRecord[] GetVaccineAdministrationRecords(String patientId, int series, String admissionId, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			getVaccineAdministrationRecords request = new getVaccineAdministrationRecords();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.beginTime = beginTime;
			request.endTime = endTime;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNursingOrderAdminRecord[] rets = null;

			getVaccineAdministrationRecordsResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getVaccineAdministrationRecords(patientId, series, admissionId, beginTime, endTime, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNursingOrderAdminRecord.class);

					// if (response != null)
					// rets = (WSNursingOrderAdminRecord [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getVaccineAdministrationRecords(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// MILK
	public static WSMilkBrand[] GetAllMilkBrands() throws Exception {
		try {
			getAllMilkBrands request = new getAllMilkBrands();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSMilkBrand[] rets = null;

			getAllMilkBrandsResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getAllMilkBrands(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSMilkBrand.class);

					// if (response != null)
					// rets = (WSMilkBrand [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getAllMilkBrands(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNurseryFeedFreq[] GetFeedFreq() throws Exception {
		try {
			getFeedFreq request = new getFeedFreq();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNurseryFeedFreq[] rets = null;

			getFeedFreqResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getFeedFreq(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNurseryFeedFreq.class);

					// if (response != null)
					// rets = (WSNurseryFeedFreq [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getFeedFreq(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void CreateFeedOrder(WSFeedTask task) throws Exception {
		try {
			createFeedOrder request = new createFeedOrder();
			request.wsFeedTask = task;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.createFeedOrder(task, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.createFeedOrder(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNurseryFeedOrder[] GetBabyFeedOnOrders(String admissionId, String patientId, int series) throws Exception {
		try {
			getBabyFeedOnOrders request = new getBabyFeedOnOrders();
			request.admissionId = admissionId;
			request.patientId = patientId;
			request.series = series;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNurseryFeedOrder[] ret = null;

			getBabyFeedOnOrdersResponse response = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getBabyFeedOnOrders(patientId, series, admissionId, _cliCode, _updateNo);
					ret = GetObjectArrayFromResponse(response, WSNurseryFeedOrder.class);

					// if (response != null)
					// ret = (WSNurseryFeedOrder [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret= _egretService.getBabyFeedOnOrders(request);
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static String GetBabyFeedDay(String adminssionId, String patientId, int series) throws Exception {
		try {
			getBabyFeedDay request = new getBabyFeedDay();
			request.admissionId = adminssionId;
			request.patientId = patientId;
			request.series = series;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			String strTemp = "";

			for (int i = 0; i < _retryTimes && strTemp.isEmpty(); i++) {
				try {
					strTemp = _egretService.getBabyFeedDay(patientId, series, adminssionId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// strTemp= _egretService.getBabyFeedDay(request).@return;
			// });
			return strTemp;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// PrepareMilk
	public static WSNurseryFeedOrder[] GetFeedOrderByCode(String patientId, int series, String admissionId, String brandCode) throws Exception {
		try {
			getFeedOrderByCode request = new getFeedOrderByCode();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.brandCode = brandCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNurseryFeedOrder[] rets = null;

			getFeedOrderByCodeResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getFeedOrderByCode(patientId, series, admissionId, brandCode, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNurseryFeedOrder.class);

					// if (response != null)
					// rets = (WSNurseryFeedOrder [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getFeedOrderByCode(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNurseryFeedPreparedRecord[] GetFeedRecordsToPrepare(int feedOrderId) throws Exception {
		try {
			getFeedRecordsToPrepare request = new getFeedRecordsToPrepare();
			request.feedOrderId = feedOrderId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNurseryFeedPreparedRecord[] rets = null;

			getFeedRecordsToPrepareResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getFeedRecordsToPrepare(feedOrderId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNurseryFeedPreparedRecord.class);

					// if (response != null)
					// rets = (WSNurseryFeedPreparedRecord
					// [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getFeedRecordsToPrepare(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNurseryFeedPreparedRecord[] PrepareFeedRecords(WSNurseryFeedPreparedRecord[] wsRecords, WSUser preparer, String remark) throws Exception {
		try {
			prepareFeedRecords request = new prepareFeedRecords();
			request.wsRecords = new ArrayList<WSNurseryFeedPreparedRecord>(Arrays.asList(wsRecords));
			request.preparer = preparer;
			request.remark = remark;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNurseryFeedPreparedRecord[] rets = null;
			prepareFeedRecordsResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.prepareFeedRecords(request);
					rets = GetObjectArrayFromResponse(response, WSNurseryFeedPreparedRecord.class);

					// if (response != null)
					// rets = (WSNurseryFeedPreparedRecord
					// [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.prepareFeedRecords(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNurseryFeedPreparedRecord[] PrepareExtraFeedRecords(WSFeedSpecDetail[] wsSpecDetails, int feedOrderId, WSUser preparer, String remark) throws Exception {
		try {
			prepareExtraFeedRecords request = new prepareExtraFeedRecords();
			request.wsSpecDetails = new ArrayList<WSFeedSpecDetail>(Arrays.asList(wsSpecDetails));
			request.feedOrderId = feedOrderId;
			request.preparer = preparer;
			request.remark = remark;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNurseryFeedPreparedRecord[] rets = null;

			prepareExtraFeedRecordsResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.prepareExtraFeedRecords(request);
					rets = GetObjectArrayFromResponse(response, WSNurseryFeedPreparedRecord.class);

					// if (response != null)
					// rets = (WSNurseryFeedPreparedRecord
					// [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.prepareExtraFeedRecords(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #region //GiveMilk与OT的View&Handle
	public static WSNurseryFeedAdminRecord GetFeedAdminRecordByBarcode(String barcodeId) throws Exception {
		try {
			getFeedAdminRecordByBarcode request = new getFeedAdminRecordByBarcode();
			request.barcode = barcodeId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNurseryFeedAdminRecord ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getFeedAdminRecordByBarcode(barcodeId, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getFeedAdminRecordByBarcode(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// GiveMilk成功
	public static void DoFeedAdministration(WSNurseryFeedAdminRecord wsRecord, WSUser executer) throws Exception {
		try {
			doFeedAdministration request = new doFeedAdministration();
			request.wsRecord = wsRecord;
			request.executer = executer;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.doFeedAdministration(wsRecord, executer, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.doFeedAdministration(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// 点击done
	public static WSNurseryFeedAdminRecord HandleFeedOutstandingTaskDone(long outstandingTaskId, WSUser executer) throws Exception {
		try {
			handleFeedOutstandingTaskDone request = new handleFeedOutstandingTaskDone();
			request.outstandingTaskId = outstandingTaskId;
			// request.outstandingTaskIdSpecified = true;
			request.executer = executer;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNurseryFeedAdminRecord ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.handleFeedOutstandingTaskDone(outstandingTaskId, executer, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret =
			// _egretService.handleFeedOutstandingTaskDone(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// 点击not done -- > no follow up接口
	public static void HandleFeedOutstandingTaskNotDoneFinish(long outstandingTaskId, WSUser executer, String reason) throws Exception {
		try {
			handleFeedOutstandingTaskNotDoneFinish request = new handleFeedOutstandingTaskNotDoneFinish();
			request.outstandingTaskId = outstandingTaskId;
			// request.outstandingTaskIdSpecified = true;
			request.executer = executer;
			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.handleFeedOutstandingTaskNotDoneFinish(outstandingTaskId, reason, executer, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.handleFeedOutstandingTaskNotDoneFinish(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// 点击not done -- > to be done later接口
	public static void HandleFeedOutstandingTaskNotDoneUnFinish(long outstandingTaskId, WSUser executer, String reason) throws Exception {
		try {
			handleFeedOutstandingTaskNotDoneUnFinish request = new handleFeedOutstandingTaskNotDoneUnFinish();
			request.outstandingTaskId = outstandingTaskId;
			// request.outstandingTaskIdSpecified = true;
			request.executer = executer;
			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.handleFeedOutstandingTaskNotDoneUnFinish(outstandingTaskId, executer, reason, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.handleFeedOutstandingTaskNotDoneUnFinish(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// //根据全局取OT
	// public static WSNursingOutstandingTaskListPage
	// GetCurrentOutstandingTasksByWard(wsPageRequest pageRequest,
	// taskType taskType, taskSubType? taskSubType,String teamId,String
	// wardId,String userGroup)
	// {
	// try
	// {
	// getCurrentOutstandingTasksByWard request = new
	// getCurrentOutstandingTasksByWard();
	// request.pageRequest = pageRequest;
	// request.teamId = teamId;
	// request.wardId = wardId;
	// request.userGroup = userGroup;
	// request.taskTypeSpecified = true;
	// request.taskType = taskType;
	// //request.taskSubTypeSpecified = true;
	// //request.taskSubType = taskSubType;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// WSNursingOutstandingTaskListPage ret = null;
	// Retry(_retryTimes, request, delegate
	// {
	// ret = _egretService.getCurrentOutstandingTasksByWard(request).@return;
	// });
	// return ret;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	// //根据个人取OT
	// public static WSNursingOutstandingTaskListPage
	// GetCurrentOutstandingTasksByPatient(wsPageRequest pageRequest,
	// taskType taskType, taskSubType? taskSubType, WSPatientVisit ipVisit,
	// String userGroup)
	// {
	// try
	// {
	// getCurrentOutstandingTasksByPatient request = new
	// getCurrentOutstandingTasksByPatient();
	// request.pageRequest = pageRequest;
	// request.ipVisit = ipVisit;
	// request.userGroup = userGroup;
	// request.taskTypeSpecified = true;
	// request.taskType = taskType;
	// //request.taskSubTypeSpecified = true;
	// //request.taskSubType = taskSubType;
	// request.cliCode = _cliCode;
	// request.updateNo = _updateNo;
	// WSNursingOutstandingTaskListPage ret = null;
	// Retry(_retryTimes, request, delegate
	// {
	// ret = _egretService.getCurrentOutstandingTasksByPatient(request).@return;
	// });
	// return ret;
	// }
	// catch (Exception ex)
	// {
	// throw MakeWSException(ex);
	// }
	// }
	// #endregion
	// MilkRecord
	public static WSNurseryFeedAdminRecord[] GetBabyFeedAdminRecords(String patientId, int series, String admissionId, DateTime beginTime, DateTime endTime) throws Exception {
		try {
			// getBabyFeedAdminRecords request = new getBabyFeedAdminRecords();
			// request.patientId = patientId;
			// request.series = series;
			// request.admissionId = admissionId;
			// if (beginTime != null)
			// {
			// request.beginTime = beginTime.Value;
			// request.beginTimeSpecified = true;
			// }
			// else
			// request.beginTimeSpecified = false;
			// if (endTime != null)
			// {
			// request.endTime = endTime.Value;
			// request.endTimeSpecified = true;
			// }
			// else
			// request.endTimeSpecified = false;
			// request.cliCode = _cliCode;
			// request.updateNo = _updateNo;
			WSNurseryFeedAdminRecord[] rets = null;

			getBabyFeedAdminRecordsResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getBabyFeedAdminRecords(patientId, series, admissionId, _cliCode, beginTime, endTime, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNurseryFeedAdminRecord.class);

					// if (response != null)
					// rets = (WSNurseryFeedAdminRecord [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getBabyFeedAdminRecords(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNurseryFeedPreparedRecord[] GetBabyFeedPreparedRecords(String patientId, int series, String admissionId) throws Exception {
		try {
			getBabyFeedPreparedRecords request = new getBabyFeedPreparedRecords();
			request.patientId = patientId;
			request.series = series;
			request.admissionId = admissionId;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNurseryFeedPreparedRecord[] rets = null;

			getBabyFeedPreparedRecordsResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getBabyFeedPreparedRecords(patientId, series, admissionId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNurseryFeedPreparedRecord.class);

					// if (response != null)
					// rets = (WSNurseryFeedPreparedRecord
					// [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getBabyFeedPreparedRecords(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// Order显示(get,3种形式:all,on,off)
	public static WSNurseryFeedOrder[] GetBabyFeedAllOrders(String admissionId, String patientId, int series) throws Exception {
		try {
			getBabyFeedAllOrders request = new getBabyFeedAllOrders();
			request.admissionId = admissionId;
			request.patientId = patientId;
			request.series = series;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNurseryFeedOrder[] rets = null;

			getBabyFeedAllOrdersResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getBabyFeedAllOrders(patientId, series, admissionId, _cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSNurseryFeedOrder.class);

					// if (response != null)
					// rets = (WSNurseryFeedOrder [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getBabyFeedAllOrders(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// OrderCancel(Do)
	public static void OffFeedOrder(WSNurseryFeedOrder wsFeedOrder, WSUser stopper) throws Exception {
		try {
			offFeedOrder request = new offFeedOrder();
			request.wsFeedOrder = wsFeedOrder;
			request.stopper = stopper;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.offFeedOrder(wsFeedOrder, stopper, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.offFeedOrder(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSDictionaryItem[] GetVacOTNotGivenReason() throws Exception {
		try {
			getVacOTNotGivenReason request = new getVacOTNotGivenReason();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getVacOTNotGivenReasonResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getVacOTNotGivenReason(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSDictionaryItem.class);

					// if (response != null)
					// rets = (WSDictionaryItem [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getVacOTNotGivenReason(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void DoVacOTAbnormalGiven(WSUser executer, int flowInfoId, String reason) throws Exception {
		try {
			doVacOTAbnormalGiven request = new doVacOTAbnormalGiven();
			request.executer = executer;
			request.flowInfoId = flowInfoId;
			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.doVacOTAbnormalGiven(flowInfoId, executer, reason, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}
			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.doVacOTAbnormalGiven(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void DoVacOTNotGivenKeep(WSUser executer, int flowInfoId, String reason) throws Exception {
		try {
			doVacOTNotGivenKeep request = new doVacOTNotGivenKeep();
			request.executer = executer;
			request.flowInfoId = flowInfoId;
			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.doVacOTNotGivenKeep(flowInfoId, executer, reason, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.doVacOTNotGivenKeep(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void DoVacOTNotGivenDiscard(WSUser executer, int flowInfoId, String reason) throws Exception {
		try {
			doVacOTNotGivenDiscard request = new doVacOTNotGivenDiscard();
			request.executer = executer;
			request.flowInfoId = flowInfoId;
			request.reason = reason;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.doVacOTNotGivenDiscard(flowInfoId, executer, reason, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.doVacOTNotGivenDiscard(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSDictionaryItem[] GetVaccinationSpecialRemark() throws Exception {
		try {
			getVaccinationSpecialRemark request = new getVaccinationSpecialRemark();
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSDictionaryItem[] rets = null;

			getVaccinationSpecialRemarkResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.getVaccinationSpecialRemark(_cliCode, _updateNo);
					rets = GetObjectArrayFromResponse(response, WSDictionaryItem.class);

					// if (response != null)
					// rets = (WSDictionaryItem [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getVaccinationSpecialRemark(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void AddSpecialRemarkForVacAdmin(int adminId, String remark) throws Exception {
		try {
			addSpecialRemarkForVacAdmin request = new addSpecialRemarkForVacAdmin();
			request.adminId = adminId;
			request.remark = remark;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.addSpecialRemarkForVacAdmin(adminId, remark, _cliCode, _updateNo);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.addSpecialRemarkForVacAdmin(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 根据牛奶牌子的code获取该牛奶牌子信息
	// / </summary>
	// / <returns></returns>
	public static WSMilkBrand GetMilkBrandByCode(String itemCode) throws Exception {
		try {
			getMilkBrandByCode request = new getMilkBrandByCode();
			request.brandCode = itemCode;
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSMilkBrand rets = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					rets = _egretService.getMilkBrandByCode(itemCode, _cliCode, _updateNo);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.getMilkBrandByCode(request).@return;
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// / <summary>
	// / 根据牛奶牌子准备extra milk
	// / </summary>
	public static WSNurseryFeedPreparedRecord[] PrepareExtraFeedRecordsFromBrand(String admissionId, String patientId, int series, WSMilkBrand milkBrand, WSFeedSpecDetail[] wsSpecDetails, WSUser preparer, String remark) throws Exception {
		try {
			prepareExtraFeedRecordsFromBrand request = new prepareExtraFeedRecordsFromBrand();
			request.admissionId = admissionId;
			request.patientId = patientId;
			request.series = series;
			request.brandCode = milkBrand.brandCode;
			request.preparer = preparer;
			request.remark = remark;
			request.wsSpecDetails = new ArrayList<WSFeedSpecDetail>(Arrays.asList(wsSpecDetails));
			request.cliCode = _cliCode;
			request.updateNo = _updateNo;
			WSNurseryFeedPreparedRecord[] rets = null;

			prepareExtraFeedRecordsFromBrandResponse response = null;
			for (int i = 0; i < _retryTimes && rets == null; i++) {
				try {
					response = _egretService.prepareExtraFeedRecordsFromBrand(request);
					rets = GetObjectArrayFromResponse(response, WSNurseryFeedPreparedRecord.class);

					// if (response != null)
					// rets = (WSNurseryFeedPreparedRecord
					// [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// rets = _egretService.prepareExtraFeedRecordsFromBrand(request);
			// });
			return rets;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region OBS AutoTriger
	public static void CreateAutoTriggerRulePackageTasks(WSPatientVisit ipVisit, String others, int packageId, DateTime startTime, DateTime recordTime, WSUser user) throws Exception {
		try {
			createAutoTriggerRulePackageTasks request = new createAutoTriggerRulePackageTasks();
			request.ipVisit = ipVisit;
			request.others = others;
			request.rulePackageId = packageId;
			request.startTime = startTime;
			// request.startTimeSpecified = true;
			request.user = user;
			request.recordTime = recordTime;
			// request.recordTimeSpecified = true;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.createAutoTriggerRulePackageTasks(packageId, ipVisit, recordTime, startTime, user, others);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.createAutoTriggerRulePackageTasks(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingAutoTriggerCategory[] GetNursingAutoTriggerCategories() throws Exception {
		try {
			getNursingAutoTriggerCategories request = new getNursingAutoTriggerCategories();
			WSNursingAutoTriggerCategory[] ret = null;

			getNursingAutoTriggerCategoriesResponse response = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getNursingAutoTriggerCategories();
					ret = GetObjectArrayFromResponse(response, WSNursingAutoTriggerCategory.class);

					// if (response != null)
					// ret = (WSNursingAutoTriggerCategory
					// [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret= _egretService.getNursingAutoTriggerCategories(request);
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingAutoTriggerRulePackage[] GetNursingAutoTriggerRulePackagesByCategoryAndWardId(int categoryId, String wardId) throws Exception {
		try {
			getNursingAutoTriggerRulePackagesByCategoryAndWardId request = new getNursingAutoTriggerRulePackagesByCategoryAndWardId();
			request.categoryId = categoryId;
			request.wardId = wardId;
			WSNursingAutoTriggerRulePackage[] ret = null;

			getNursingAutoTriggerRulePackagesByCategoryAndWardIdResponse response = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getNursingAutoTriggerRulePackagesByCategoryAndWardId(categoryId, wardId);
					ret = GetObjectArrayFromResponse(response, WSNursingAutoTriggerRulePackage.class);

					// if (response != null)
					// ret = (WSNursingAutoTriggerRulePackage
					// [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret =
			// _egretService.getNursingAutoTriggerRulePackagesByCategoryAndWardId(request);
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static WSNursingAutoTriggerEvent[] GetNursingAutoTriggerEventsByPatientAndWard(String admissionId, String patientId, int series, String wardId) throws Exception {
		try {
			getNursingAutoTriggerEventsByPatientAndWard request = new getNursingAutoTriggerEventsByPatientAndWard();
			request.admissionId = admissionId;
			request.patientId = patientId;
			request.series = series;
			request.wardId = wardId;
			WSNursingAutoTriggerEvent[] ret = null;

			getNursingAutoTriggerEventsByPatientAndWardResponse response = null;
			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					response = _egretService.getNursingAutoTriggerEventsByPatientAndWard(patientId, series, admissionId, wardId);
					ret = GetObjectArrayFromResponse(response, WSNursingAutoTriggerEvent.class);

					// if (response != null)
					// ret = (WSNursingAutoTriggerEvent [])response.toArray();
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret =
			// _egretService.getNursingAutoTriggerEventsByPatientAndWard(request);
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void CancelNursingAutoTriggerEvent(long eventId, WSUser stoper) throws Exception {
		try {
			cancelNursingAutoTriggerEvent request = new cancelNursingAutoTriggerEvent();
			request.eventId = eventId;
			request.stopper = stoper;

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.cancelNursingAutoTriggerEvent(eventId, stoper);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.cancelNursingAutoTriggerEvent(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static DateTime GetCurrentAutoTriggerEvenTimePoint(String caseStr) throws Exception {
		try {
			getCurrentAutoTriggerEvenTimePoint request = new getCurrentAutoTriggerEvenTimePoint();
			request.caseStr = caseStr;
			DateTime ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getCurrentAutoTriggerEvenTimePoint(caseStr);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret=
			// _egretService.getCurrentAutoTriggerEvenTimePoint(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static DateTime CalcAutoTriggerEvenTimePoint(DateTime timeNow, String caseStr) throws Exception {
		try {
			calcAutoTriggerEvenTimePoint request = new calcAutoTriggerEvenTimePoint();
			request.caseStr = caseStr;
			request.time = timeNow;
			// request.timeSpecified = true;
			// DateTime ret = DateTime.Now;
			DateTime ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.calcAutoTriggerEvenTimePoint(timeNow, caseStr);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret =
			// _egretService.calcAutoTriggerEvenTimePoint(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// #region BBMathorMatching

	public static WSMotherBabyMatcher MatchMotherAndBaby(WSMotherBabyMatcher matchingInfo, WSUser oper) throws Exception {
		try {
			matchMotherAndBaby request = new matchMotherAndBaby();
			request.enterBy = oper;
			request.matchInfo = matchingInfo;
			WSMotherBabyMatcher ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.matchMotherAndBaby(matchingInfo, oper);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.matchMotherAndBaby(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion
	// #region Testing
	public static WSOrderFrequency GetFrequencyByCode(String freqCode, String wardID) throws Exception {
		try {
			getFrequencyByCode request = new getFrequencyByCode();
			request.freqCode = freqCode;
			request.wardId = wardID;

			// WSOrderFrequency ret = new WSOrderFrequency();
			WSOrderFrequency ret = null;

			for (int i = 0; i < _retryTimes && ret == null; i++) {
				try {
					ret = _egretService.getFrequencyByCode(freqCode, wardID);
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// ret = _egretService.getFrequencyByCode(request).@return;
			// });
			return ret;
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	public static void UploadPlanTimeLog(String wardID, String freqCode, String freqName, boolean isManualSetPlantime, String timeExpression, DateTime[] configRegularCycleTimes, DateTime[] defaultTimePoints, DateTime[] orderRegularCycleTimes) throws Exception {
		try {
			uploadPlanTimeLog request = new uploadPlanTimeLog();
			request.wardId = wardID;
			request.freqCode = freqCode;
			request.freqName = freqName;
			request.isManualSetPlanTime = isManualSetPlantime;
			request.timeExpression = timeExpression;
			request.configRegularCycleTimes = new ArrayList<DateTime>(Arrays.asList(configRegularCycleTimes));
			request.defaultTimePoints = new ArrayList<DateTime>(Arrays.asList(defaultTimePoints));
			request.orderRegularCycleTimes = new ArrayList<DateTime>(Arrays.asList(orderRegularCycleTimes));

			boolean success = false;
			for (int i = 0; i < _retryTimes && success == false; i++) {
				try {
					_egretService.uploadPlanTimeLog(request);
					success = true;
				} catch (Exception ex) {
					
						throw MakeWSException(ex);
				}
			}

			// Retry(_retryTimes, request, delegate
			// {
			// _egretService.uploadPlanTimeLog(request);
			// });
		} catch (Exception ex) {
			throw MakeWSException(ex);
		}
	}

	// #endregion

	// public static <T> ArrayList<T> GetObjectArrayFromResponse(KvmSerializable
	// response, Class<T> type)
	public static <T> T[] GetObjectArrayFromResponse(KvmSerializable response, Class<T> type) {
		// Object objectArray =
		ArrayList<T> rets2 = new ArrayList<T>();
		
		for (int j = 0; j < response.getPropertyCount(); j++) {
			rets2.add((T) response.getProperty(j));
		}
		T[] array_t = (T[]) Array.newInstance(type, rets2.size());
		array_t = (T[]) rets2.toArray(array_t);

		return array_t;

	}

}
