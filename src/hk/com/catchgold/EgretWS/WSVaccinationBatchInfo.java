package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class WSVaccinationBatchInfo extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public Integer adminId=0;
    
    public String batchNumber;
    
    public String remark;

    public WSVaccinationBatchInfo ()
    {
    }

    public WSVaccinationBatchInfo (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("adminId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("adminId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.adminId = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.adminId = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("batchNumber"))
        {	
	        java.lang.Object obj = soapObject.getProperty("batchNumber");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.batchNumber = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.batchNumber = (String)obj;
            }    
        }
        if (soapObject.hasProperty("remark"))
        {	
	        java.lang.Object obj = soapObject.getProperty("remark");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.remark = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.remark = (String)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return adminId;
        }
        if(propertyIndex==1)
        {
            return this.batchNumber!=null?this.batchNumber:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.remark!=null?this.remark:SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 3;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "adminId";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "batchNumber";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "remark";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
