package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class cancelNursingAutoTriggerEvent extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public Long eventId=0L;
    
    public WSUser stopper;

    public cancelNursingAutoTriggerEvent ()
    {
    }

    public cancelNursingAutoTriggerEvent (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("eventId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("eventId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.eventId = new Long(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Long){
                this.eventId = (Long)obj;
            }    
        }
        if (soapObject.hasProperty("stopper"))
        {	
	        java.lang.Object j = soapObject.getProperty("stopper");
	        this.stopper = (WSUser)__envelope.get(j,WSUser.class);
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return eventId;
        }
        if(propertyIndex==1)
        {
            return this.stopper!=null?this.stopper:SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 2;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.LONG_CLASS;
            info.name = "eventId";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = WSUser.class;
            info.name = "stopper";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
