package hk.com.catchgold.EgretCommon;

import hk.com.catchgold.EgretCommon.GlobalType.HANDLER_MESSAGE;
import hk.com.catchgold.EgretCommon.Enums.StationType;
import hk.com.catchgold.EgretGlobal.Global;
import hk.com.catchgold.EgretWS.*;
import hk.com.catchgold.EgretWS.Enums.taskType;
import hk.com.catchgold.EgretWS.Exception;
import hk.com.catchgold.EgretWSController.WSController;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;

public class AlertsManager implements Serializable {
	AssetFileDescriptor descriptor;
	// Steve comment: mark the delegate and take as handler
	private static final long serialVersionUID = 1943501007990757555L;
	private Timer timerAlert = null;
	private StationType _type;
	// public event EventHandler AlarmMessageClick;
	public Handler AlarmMessageClick;

	// ztm Offdrug alerts相关 2011-2-22
	// public event EventHandler OffDrugAlarmMessageClick;
	public Handler OffDrugAlarmMessageClick;
	// 根据窗口的Text查找当前活动窗口API
	// [DllImport("Coredll.dll")]
	// private extern static IntPtr FindWindow(string lpClassName, string
	// lpWindowName);

	private static boolean _hasLabNB = true;
	private static boolean _hasObs = true;
	private static boolean _hasMed = true;
	private static boolean _hasAbn = true;
	private static boolean _hasBed = true;
	private static boolean _hasOffdrug = true;

	public static boolean getHasAbn() {
		return _hasAbn;
	}

	public static void setHasAbn(boolean value) {
		_hasAbn = value;
	}

	public static boolean getHasMed() {
		return _hasMed;
	}

	public static void setHasMed(boolean value) {
		_hasMed = value;
	}

	public static boolean getHasObs() {
		return _hasObs;
	}

	public static void setHasObs(boolean value) {
		_hasObs = value;
	}

	public static boolean getHasLabNB() {
		return _hasLabNB;
	}

	public static void setHasLabNB(boolean value) {
		_hasLabNB = value;
	}

	public static boolean getHasBed() {
		return _hasBed;
	}

	public static void setHasBed(boolean value) {
		_hasBed = value;
	}

	public static boolean getHasOffdrug() {
		return _hasOffdrug;
	}

	public static void setHasOffdrug(boolean value) {
		_hasOffdrug = value;
	}

	public AlertsManager(StationType type) {
		_type = type;
	}

	public void setAlertStatusEvent(Handler handler) {
		AlarmMessageClick = handler;
	}

	public void setOffdrugAlertEvent(Handler handler) {
		OffDrugAlarmMessageClick = handler;
	}

	public void InitAlertWatcher() {

		// Steve comment: need discuss and see how to implement the delegate
		// event
		// SetAlertStatusEvent += delegate(bool status)
		// {
		// PanelTitle.Title.AlarmMessage = status;
		// };
		// PanelTitle.Title.AlarmMessageClick += new
		// EventHandler(Title_AlarmMessageClick);
		//
		// SetOffdrugAlertEvent += delegate()
		// {
		// //如果当前FormAlertsView处于打开状态，则不启动Popup Window
		// IntPtr hMainForm = FindWindow("#NETCF_AGL_BASE_", "Union--Alerts");
		// if (hMainForm.ToInt32() == 0)
		// {
		// FormOffdrugPopup.Popup.ShowDialog();
		// }
		// };
		// SetFeedingRecordEvent += delegate(string[] Alerts)//Edit By Zyq
		// 2013-03-05 #7247
		// {
		// IntPtr hMainForm = FindWindow("#NETCF_AGL_BASE_", "Union--Alerts");
		// if (hMainForm.ToInt32() == 0)
		// {
		// FormFeedingRecordPopup.Alerts = Alerts;
		// FormFeedingRecordPopup.Popup.ShowDialog();
		// }
		// };
		// FormOffdrugPopup.Popup.OffdrugAlarmMessageClick += new
		// EventHandler(Offdrug_AlarmMessageClick);

		// try {
		// timerAlert = new Timer();
		//
		// AlertsManagerTasks timerTask = new AlertsManagerTasks();
		// timerAlert.schedule(timerTask, GlobalVars.AlertInterval);
		//
		// } catch (java.lang.Exception ex)
		// {
		// Log.e("EgretCommon", "Failed to create AlertTimer: " +
		// ex.getMessage());
		// ex.printStackTrace();
		//
		// }

		// timerAlert = new System.Threading.Timer(new
		// System.Threading.TimerCallback(CheckAlertStatus), true,
		// System.Threading.Timeout.Infinite, 10000);
		// timerAlert.Change(0, GlobalVars.AlertInterval);
	}

	public void DisposeAlertWatcher() {
		if (timerAlert != null)
			timerAlert.cancel();

		// PanelTitle.Title.AlarmMessageClick -= new
		// EventHandler(Title_AlarmMessageClick);
		// PanelTitle.Title.AlarmMessage = false;
		// timerAlert.Change(System.Threading.Timeout.Infinite,
		// System.Threading.Timeout.Infinite);
		// timerAlert.Dispose();
	}

	// private void Title_AlarmMessageClick(object sender, EventArgs e)
	private void Title_AlarmMessageClick() {
		if (AlarmMessageClick != null) {
			AlarmMessageClick.sendEmptyMessage((int) HANDLER_MESSAGE.ALERT_ALARMMESSAGE_CLICK.showMessage());
			// AlarmMessageClick(sender, e);
		}
	}

	// private void Offdrug_AlarmMessageClick(object sender, EventArgs e)
	private void Offdrug_AlarmMessageClick() {
		if (AlarmMessageClick != null) {
			AlarmMessageClick.sendEmptyMessage((int) HANDLER_MESSAGE.OFFDRUG_ALARMMESSAGE_Click.showMessage());
			// OffDrugAlarmMessageClick(sender, e);
		}
	}

	// private delegate void SetAlertStatusHandler(bool status);
	// private event SetAlertStatusHandler SetAlertStatusEvent;
	//
	// private delegate void SetOffdrugAlertHandler();
	// private event SetOffdrugAlertHandler SetOffdrugAlertEvent;

	// Steve comment: not sure if this handler is still useful or not
	// private delegate void SetFeedingRecordHandler(string[] Alerts);//Edit By
	// Zyq 2013-03-05 #7247
	// private event SetFeedingRecordHandler SetFeedingRecordEvent;

	// / <summary>
	// / Wade 2010-5-7 status true需要声音提醒 从用药 outstanding界面退出刷新alert但不声音提醒
	// / </summary>
	// / <param name="sender"></param>
	// / <param name="e"></param>

	public void CheckAlertStatus(Object status, Activity activity, MediaPlayer mp) {
		try {
			if (GlobalVars.IsScreenLocked)
				return;

			WSNursingAlertTotal needAlert = new WSNursingAlertTotal();
			if (_type == StationType.NURSE) {
				needAlert = WSController.HasUnreadAlerts(Global.User.departmentcode, GlobalVars.CurrentTeam.code, GlobalVars.CurrentRole.name, null, null, InetAddress.getLocalHost().getHostName());
			} else if (_type == StationType.MEI) {
				// needAlert = WSController.HasUnreadAlerts(null, null, "IC",
				// (taskType)3, (taskSubType)1, Dns.GetHostName());
				needAlert = WSController.HasUnreadAlerts(null, null, "IC", Enums.taskType.APPOINTMENT, Enums.taskSubType.ORAL, InetAddress.getLocalHost().getHostName());
			} else if (_type == StationType.CA) {
				needAlert = WSController.HasUnreadAlerts(Global.User.departmentcode, GlobalVars.CurrentTeam.code, "CA", Enums.taskType.APPOINTMENT, Enums.taskSubType.ORAL, InetAddress.getLocalHost().getHostName());
			}

			// Steve comment: this code is to invoke a pop-up dialog to show the
			// message or battery low
			// PanelTitle.Title.Invoke(SetAlertStatusEvent, new object[] {
			// needAlert.hasAbnormalValueAlerts || needAlert.hasOverdueAlerts ||
			// needAlert.hasOntimeAlerts || needAlert.hasSpecimenIntervalAlerts
			// });
			if ((Boolean) status) {
				// Steve comment: this code is to invoke a pop-up dialog to show
				// the message or battery low
				mp.reset();
				if (needAlert.hasAbnormalValueAlerts) {
					descriptor = activity.getAssets().openFd("alert/Abnormal_alert.wav");
				} else if (needAlert.hasOverdueAlerts) {
					descriptor = activity.getAssets().openFd("alert/Overdue_alert.wav");
				} else if (needAlert.hasOntimeAlerts) {
					descriptor = activity.getAssets().openFd("alert/On_Time_alert.wav");
				} else if (needAlert.hasOffOrderRequest) {
					descriptor = activity.getAssets().openFd("alert/Abnormal_alert.wav");
				} else if (needAlert.hasLabRequestAbnormalValueAlerts) {
					descriptor = activity.getAssets().openFd("alert/Abnormal_alert.wav");
				}

				mp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
				mp.prepare();
				mp.start();
			}

			_hasObs = needAlert.hasOBSType;
			_hasMed = needAlert.hasMedicationType;
			_hasAbn = needAlert.hasAbnormalValueAlerts || needAlert.hasLabRequestAbnormalValueAlerts;
			_hasBed = needAlert.hasBedsideCare;
			_hasOffdrug = needAlert.hasOffOrderRequest;
			_hasLabNB = needAlert.hasSpecimenIntervalAlerts;

			// 启动Offdrug Popup Window
			if (_hasOffdrug && (Boolean) status == true && _type == StationType.NURSE) {
				// Steve comment: offdrug (欠藥) dialog
				// FormOffdrugPopup.Popup.Invoke(SetOffdrugAlertEvent, new
				// object[] { });
			}

			/*
			 * #region Cadi 1.5 if (needAlert.cadiAlerts != null) { if
			 * (needAlert.cadiAlerts.Length > 0) { string strAlertMessage = "";
			 * List<string> DismissCode = new List<string>(); foreach (string
			 * str in needAlert.cadiAlerts) {
			 * 
			 * string[] split = str.Split("@@".ToCharArray());
			 * 
			 * if (split.Length == 3) { DismissCode.Add(split[0]);
			 * strAlertMessage += split[2] + "; "; } }
			 * WSController.dismissCadiAlerts(DismissCode.ToArray(),
			 * Dns.GetHostName());//使用过后再Dismiss
			 * MessageBox.Show(strAlertMessage, "INFO", MessageBoxButtons.OK,
			 * MessageBoxIcon.Exclamation, 0);
			 * //FormFeedingRecordPopup.Popup.Invoke(SetFeedingRecordEvent, new
			 * object[] { needAlert.cadiAlerts }); } } #endregion
			 */
		} catch (java.lang.Exception ex) {
			ex.printStackTrace();
		}
	}

	// class AlertsManagerTasks extends TimerTask {
	//
	// @Override
	// public void run() {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// }

	public boolean hasAlert(Object status, Activity activity, MediaPlayer mp) {
		CheckAlertStatus(status, activity, mp);
		return _hasObs | _hasMed | _hasAbn | _hasBed | _hasOffdrug | _hasLabNB;
	}
}
