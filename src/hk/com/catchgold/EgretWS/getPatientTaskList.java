package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class getPatientTaskList extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public WSPatientVisit ipVisit;
    
    public Enums.taskStatus1 taskStatus;
    
    public Enums.taskType type;
    
    public Enums.taskSubType subType;
    
    public Integer actionId;
    
    public org.joda.time.DateTime beginTime;
    
    public org.joda.time.DateTime endTime;
    
    public String cliCode;
    
    public Integer updateNo=0;

    public getPatientTaskList ()
    {
    }

    public getPatientTaskList (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("ipVisit"))
        {	
	        java.lang.Object j = soapObject.getProperty("ipVisit");
	        this.ipVisit = (WSPatientVisit)__envelope.get(j,WSPatientVisit.class);
        }
        if (soapObject.hasProperty("taskStatus"))
        {	
	        java.lang.Object obj = soapObject.getProperty("taskStatus");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.taskStatus = Enums.taskStatus1.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.taskStatus1){
                this.taskStatus = (Enums.taskStatus1)obj;
            }    
        }
        if (soapObject.hasProperty("type"))
        {	
	        java.lang.Object obj = soapObject.getProperty("type");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.type = Enums.taskType.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.taskType){
                this.type = (Enums.taskType)obj;
            }    
        }
        if (soapObject.hasProperty("subType"))
        {	
	        java.lang.Object obj = soapObject.getProperty("subType");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.subType = Enums.taskSubType.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.taskSubType){
                this.subType = (Enums.taskSubType)obj;
            }    
        }
        if (soapObject.hasProperty("actionId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("actionId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.actionId = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.actionId = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("beginTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("beginTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.beginTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.beginTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("endTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("endTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.endTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.endTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("cliCode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("cliCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.cliCode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.cliCode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("updateNo"))
        {	
	        java.lang.Object obj = soapObject.getProperty("updateNo");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.updateNo = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.updateNo = (Integer)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return this.ipVisit!=null?this.ipVisit:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==1)
        {
            return this.taskStatus!=null?this.taskStatus.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.type!=null?this.type.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return this.subType!=null?this.subType.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex==4)
        {
            return this.actionId!=null?this.actionId:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return this.beginTime!=null?this.beginTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==6)
        {
            return this.endTime!=null?this.endTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==7)
        {
            return this.cliCode!=null?this.cliCode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==8)
        {
            return updateNo;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 9;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = WSPatientVisit.class;
            info.name = "ipVisit";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "taskStatus";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "type";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "subType";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "actionId";
            info.namespace= "";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "beginTime";
            info.namespace= "";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "endTime";
            info.namespace= "";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "cliCode";
            info.namespace= "";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "updateNo";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
