package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class WSPreparation extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public Integer requestId=0;
    
    public Integer seqNo=0;
    
    public String description;
    
    public org.joda.time.DateTime createTime;
    
    public String creatorCode;
    
    public String creatorName;
    
    public org.joda.time.DateTime dueTime;
    
    public org.joda.time.DateTime finishedTime;
    
    public String finisherCode;
    
    public String finisherName;
    
    public Enums.preparationStatus status;

    public WSPreparation ()
    {
    }

    public WSPreparation (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("requestId"))
        {	
	        java.lang.Object obj = soapObject.getProperty("requestId");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.requestId = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.requestId = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("seqNo"))
        {	
	        java.lang.Object obj = soapObject.getProperty("seqNo");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.seqNo = Integer.parseInt(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Integer){
                this.seqNo = (Integer)obj;
            }    
        }
        if (soapObject.hasProperty("description"))
        {	
	        java.lang.Object obj = soapObject.getProperty("description");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.description = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.description = (String)obj;
            }    
        }
        if (soapObject.hasProperty("createTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("createTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.createTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.createTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("creatorCode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("creatorCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.creatorCode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.creatorCode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("creatorName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("creatorName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.creatorName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.creatorName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("dueTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("dueTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.dueTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.dueTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("finishedTime"))
        {	
	        java.lang.Object obj = soapObject.getProperty("finishedTime");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.finishedTime = Helper.ConvertFromWebService(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof org.joda.time.DateTime){
                this.finishedTime = (org.joda.time.DateTime)obj;
            }    
        }
        if (soapObject.hasProperty("finisherCode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("finisherCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.finisherCode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.finisherCode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("finisherName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("finisherName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.finisherName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.finisherName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("status"))
        {	
	        java.lang.Object obj = soapObject.getProperty("status");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.status = Enums.preparationStatus.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.preparationStatus){
                this.status = (Enums.preparationStatus)obj;
            }    
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return requestId;
        }
        if(propertyIndex==1)
        {
            return seqNo;
        }
        if(propertyIndex==2)
        {
            return this.description!=null?this.description:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return this.createTime!=null?this.createTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==4)
        {
            return this.creatorCode!=null?this.creatorCode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return this.creatorName!=null?this.creatorName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==6)
        {
            return this.dueTime!=null?this.dueTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==7)
        {
            return this.finishedTime!=null?this.finishedTime:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==8)
        {
            return this.finisherCode!=null?this.finisherCode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==9)
        {
            return this.finisherName!=null?this.finisherName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==10)
        {
            return this.status!=null?this.status.toString():SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 11;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "requestId";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "seqNo";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "description";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "createTime";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "creatorCode";
            info.namespace= "";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "creatorName";
            info.namespace= "";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "dueTime";
            info.namespace= "";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "finishedTime";
            info.namespace= "";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "finisherCode";
            info.namespace= "";
        }
        if(propertyIndex==9)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "finisherName";
            info.namespace= "";
        }
        if(propertyIndex==10)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "status";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
