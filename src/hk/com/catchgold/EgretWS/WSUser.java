package hk.com.catchgold.EgretWS;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.1.4.0
//
// Created by Quasar Development at 22-01-2015
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;
import java.util.ArrayList;
import org.ksoap2.serialization.PropertyInfo;

public class WSUser extends AttributeContainer implements KvmSerializable,java.io.Serializable
{
    
    public Long id=0L;
    
    public String code = "";
    
    public String password = "";
    
    public String enFirstName = "";
    
    public String enLastName = "";
    
    public String cnFirstName = "";
    
    public String cnLastName = "";
    
    public String type = "";
    
    public String departmentcode = "";
    
    public String cnName = "";
    
    public String enName = "";
    
    public String phone = "";
    
    public String mobile = "";
    
    public String userName = "";
    
    public Enums.userRank rank;
    
    public ArrayList< WSSysRole>roles =new ArrayList<WSSysRole>();

    public WSUser ()
    {
    }

    public WSUser (java.lang.Object paramObj,ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        SoapObject soapObject=(SoapObject)inObj;  
        if (soapObject.hasProperty("id"))
        {	
	        java.lang.Object obj = soapObject.getProperty("id");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.id = new Long(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Long){
                this.id = (Long)obj;
            }    
        }
        if (soapObject.hasProperty("code"))
        {	
	        java.lang.Object obj = soapObject.getProperty("code");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.code = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.code = (String)obj;
            }    
        }
        if (soapObject.hasProperty("password"))
        {	
	        java.lang.Object obj = soapObject.getProperty("password");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.password = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.password = (String)obj;
            }    
        }
        if (soapObject.hasProperty("enFirstName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("enFirstName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.enFirstName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.enFirstName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("enLastName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("enLastName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.enLastName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.enLastName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("cnFirstName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("cnFirstName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.cnFirstName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.cnFirstName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("cnLastName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("cnLastName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.cnLastName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.cnLastName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("type"))
        {	
	        java.lang.Object obj = soapObject.getProperty("type");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.type = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.type = (String)obj;
            }    
        }
        if (soapObject.hasProperty("departmentcode"))
        {	
	        java.lang.Object obj = soapObject.getProperty("departmentcode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.departmentcode = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.departmentcode = (String)obj;
            }    
        }
        if (soapObject.hasProperty("cnName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("cnName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.cnName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.cnName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("enName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("enName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.enName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.enName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("phone"))
        {	
	        java.lang.Object obj = soapObject.getProperty("phone");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.phone = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.phone = (String)obj;
            }    
        }
        if (soapObject.hasProperty("mobile"))
        {	
	        java.lang.Object obj = soapObject.getProperty("mobile");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.mobile = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.mobile = (String)obj;
            }    
        }
        if (soapObject.hasProperty("userName"))
        {	
	        java.lang.Object obj = soapObject.getProperty("userName");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.userName = j.toString();
                }
	        }
	        else if (obj!= null && obj instanceof String){
                this.userName = (String)obj;
            }    
        }
        if (soapObject.hasProperty("rank"))
        {	
	        java.lang.Object obj = soapObject.getProperty("rank");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class))
            {
                SoapPrimitive j =(SoapPrimitive) obj;
                if(j.toString()!=null)
                {
                    this.rank = Enums.userRank.fromString(j.toString());
                }
	        }
	        else if (obj!= null && obj instanceof Enums.userRank){
                this.rank = (Enums.userRank)obj;
            }    
        }
        if (soapObject.hasProperty("roles"))
        {	
	        int size = soapObject.getPropertyCount();
	        this.roles = new ArrayList<WSSysRole>();
	        for (int i0=0;i0< size;i0++)
	        {
	            PropertyInfo info=new PropertyInfo();
	            soapObject.getPropertyInfo(i0, info);
                java.lang.Object obj = info.getValue();
	            if (obj!=null && info.name.equals("roles"))
	            {
                    java.lang.Object j =info.getValue();
	                WSSysRole j1= (WSSysRole)__envelope.get(j,WSSysRole.class);
	                this.roles.add(j1);
	            }
	        }
        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return id;
        }
        if(propertyIndex==1)
        {
            return this.code!=null?this.code:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.password!=null?this.password:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return this.enFirstName!=null?this.enFirstName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==4)
        {
            return this.enLastName!=null?this.enLastName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return this.cnFirstName!=null?this.cnFirstName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==6)
        {
            return this.cnLastName!=null?this.cnLastName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==7)
        {
            return this.type!=null?this.type:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==8)
        {
            return this.departmentcode!=null?this.departmentcode:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==9)
        {
            return this.cnName!=null?this.cnName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==10)
        {
            return this.enName!=null?this.enName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==11)
        {
            return this.phone!=null?this.phone:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==12)
        {
            return this.mobile!=null?this.mobile:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==13)
        {
            return this.userName!=null?this.userName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==14)
        {
            return this.rank!=null?this.rank.toString():SoapPrimitive.NullSkip;
        }
        if(propertyIndex>=+15 && propertyIndex< + 15+this.roles.size())
        {
            return this.roles.get(propertyIndex-(+15));
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 15+roles.size();
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.LONG_CLASS;
            info.name = "id";
            info.namespace= "";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "code";
            info.namespace= "";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "password";
            info.namespace= "";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "enFirstName";
            info.namespace= "";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "enLastName";
            info.namespace= "";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "cnFirstName";
            info.namespace= "";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "cnLastName";
            info.namespace= "";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "type";
            info.namespace= "";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "departmentcode";
            info.namespace= "";
        }
        if(propertyIndex==9)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "cnName";
            info.namespace= "";
        }
        if(propertyIndex==10)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "enName";
            info.namespace= "";
        }
        if(propertyIndex==11)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "phone";
            info.namespace= "";
        }
        if(propertyIndex==12)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "mobile";
            info.namespace= "";
        }
        if(propertyIndex==13)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "userName";
            info.namespace= "";
        }
        if(propertyIndex==14)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "rank";
            info.namespace= "";
        }
        if(propertyIndex>=+15 && propertyIndex <= +15+this.roles.size())
        {
            info.type = WSSysRole.class;
            info.name = "roles";
            info.namespace= "";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }
}
